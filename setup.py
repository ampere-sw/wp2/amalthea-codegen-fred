# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

# dump the file content to a string
with open('README.md') as f:
    readme = f.read()

with open('LICENSE') as f:
    license = f.read()

# creates a list where the lines are stored in consecutive positions
required = [line.strip() for line in open("requirements.txt").readlines()]

setup(
    name='app4mc',
    version='v0.1',
    description='Pythonic app4mc parser and code generator',
    long_description=readme,
    author='Alexandre Amory',
    author_email='amamory@gmail.com',
    url='https://gitlab.retis.sssup.it/ampere/amalthea-codegen-linux',
    license=license,
    # specify the dir of the python modules to be installed
    packages=find_packages('app4mc_viewer'),
    # specify the data files that must be installed
    include_package_data=True,
    package_data={
        # If any package contains *.txt files, include them:
        #"": ["*.txt"],
        # And include any *.dat files found in the "data" subdirectory
        # of the "mypkg" package, also:
        "": ["models/*.amxmi"],
    },
    # exclude READMEs from instalation
    exclude_package_data={"": ["README.md", 'readme.md']},  
    # depedencies  
    install_requires=required,
    # execution entry point. to launch the python application as an executable.
    entry_points='''
        [console_scripts]
        app4mc=app4mc.__main__:main
    ''')


