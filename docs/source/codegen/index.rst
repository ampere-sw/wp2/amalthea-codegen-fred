**************
Code Generator
**************

The code generator :mod:`app4mc.code_gen` currently generates code for the following types of processing units
and its programming environments:

.. toctree::
    :maxdepth: 2

    ./posix
    ./fred
    ./openmp-threads
    ./openmp-gpu
    ./cuda
    ./opencl


Installing
==========

Installing *pyapp4mc* using virtualenv:

.. code-block:: console

    $ sudo apt-get install python3-venv
    $ export GIT_SSL_NO_VERIFY=1
    $ git clone https://gitlab.retis.santannapisa.it/ampere/amalthea-codegen-linux.git
    $ cd amalthea-codegen-linux
    $ python3 -m venv env
    $ source env/bin/activate
    $ pip install --upgrade setuptools
    $ pip install --editable .

Creating the Amalthea.jar
-------------------------

This file is a requirement for the next step. It only needs to be done once,
or then the app4mc is upgraded. 

- Open *app4mc*, version 1.0.0;
- In the menu, select File -> Examples;
- In the *New Example* window, there is a wizard. Select *APP4MC Tools Example* -> *Java Examples*. Press Next and later Finish;
- A project named *app4mc.example.tool.java* will be created;
- Compile one of the example applications. Left-click in *src* -> *app4mc.example.tool.java* -> *IndexExample.java*. Select *Run as* -> *Java application*.
- Left-click in the project name and select *Export*. In the *Export* window, select *Java* -> *Runnable JAR file*;
- In the *Runnable JAR File Export* window, select the destination file, and click in *Extract required libraries into generated JAR*. Click *Finish*.

The generated jar file must have around 22,8MBytes.

Environment variables
---------------------

Create the PYAPP4MC_HOME environment variable and point it to the installation path. For example:

.. code-block:: console

   $ export PYAPP4MC_HOME=/home/$USER/pyapp4mc

Next, set the environment variable CLASSPATH, pointing it to the 
Amalthea java server dependencies. The CLASSPATH must have the following paths:

- $PYAPP4MC_HOME/py4j_server/py4j_server/target/classes: the java Amalthea server;
- Amalthea.jar: the jar file with the Amalthea model generated previously. Place this file in $PYAPP4MC_HOME;
- `py4j-0.10.9.1.jar <https://github.com/py4j/py4j/archive/refs/tags/0.10.9.1.tar.gz>`_: the jar file with py4j. Download and place this file in $PYAPP4MC_HOME.

It's recommended to place the jar files in the $PYAPP4MC_HOME dir:

.. code-block:: console

   $ export CLASSPATH=${PYAPP4MC_HOME}/py4j_server/py4j_server/target/classes:${PYAPP4MC_HOME}/Amalthea.jar:${PYAPP4MC_HOME}/env/share/py4j/py4j0.10.9.1.jar

It's recommended to save both definitions in the *~/.bashrc* file.

Compiling the Java Server
=========================

*pyapp4mc* uses the official Amalthea parser in Java. For this reason, it has a Java server
that provides the Amalthea definitions to *pyapp4mc*. This Java server must be compiled before 
using *pyapp4mc* for the first time. 

It uses *ant* as Java build system to assist the compilation. Follow these steps
to compile the Java server: 

.. code-block:: console

    $ sudo apt install ant
    $ cd $PYAPP4MC_HOME/py4j_server/py4j_server/
    $ ant

Ant uses the build.xml file to point to the Java server dependencies (jar files).
The exact location of these jar files depend on the system, so you might need to 
edit the build.xml file to point to the correct location.

Examples and Usage
==================

These are the supported arguments for *pyapp4mc*:

.. code-block:: console

    $ app4mc -h
    usage: app4mc [-h] [--py4j-port PY4J_PORT]
                [-tick_type {WCET,BCET,ACET,distribution}]
                [-par_lib {posix,openmp}]
                in_file out_dir

    positional arguments:
    in_file               Input AMXMI file with the Amalthea model.
    out_dir               Output directory where the generated code will be
                            saved.

    optional arguments:
    -h, --help            show this help message and exit
    --py4j-port PY4J_PORT
                            The py4j port for the Amalthea server. The default
                            value is 25333. The port number must be > 1024.
    -tick_type {WCET,BCET,ACET,distribution}
                            The options of behaviour for the Amalthea Ticks used
                            in the Runnables. The possible values are BCET, ACET,
                            WCET, and distribution. WCET is the default.
    -par_lib {posix,openmp}
                            The supported parallel libraries.


For testing purposes, it's possible to generate code for the example models.
The following example shows the expected output for the *democar* example.

.. code-block:: console

   $ cd $PYAPP4MC_HOME/
   $ app4mc models/democar.amxmi /tmp/democar/

    TASKS:
    - Task_10MS
    - Task_20MS
    - Task_5MS
    Total of 3 tasks

    RUNNABLES:
    - ABSCalculation
    - APedSensor
    - ...
    - WheelSpeedSensorTranslation
    - WheelSpeedSensorVoter
    Total of 43 runnables

    STIM:
    - Timer_10MS
    - Timer_20MS
    - Timer_5MS
    Total of 3 stimuli

    HW:
    - Democar
    Total of 1 Hw structures

    PU:
    - Core1
    Total of 1 processing units

    Constraints:
    WARNINIG: Amalthea TimingConstraint is not supported
    WARNINIG: Amalthea AffinityConstraint is not supported
    - Deadline_05 UpperLimit
    - Deadline_10 UpperLimit
    - Deadline_20 UpperLimit

    Task mapping:
    - Core1
      * Task_5MS
      * Task_10MS
      * Task_20MS

    Loading the Amalthea model for code generation...
    Generating code for tasks ...
    TASK: Task_10MS
    TASK: Task_20MS
    TASK: Task_5MS
    Generating code for global variables ...
    Generating the tasks header ...
    Generating the main code ...
    Copying configuration files ...

    Code generated successfully!

Not that it might have warnings. They are used to recall the users that some of the executed model features are currently not used by the code generator. In this case, 
Amalthea timing and affinity constraints are not used.

Generated code structure
========================

The generated code has the following directory structure.

::

    .
    ├── Makefile
    ├── CMakeLists.txt
    ├── include
    │   ├── runnables
    │   │   └── label.h
    │   ├── tasks
    │   │   ├── task_1.h
    │   │   ├── task_2.h
    │   │   ├── ...
    │   │   └── tasks.h
    │   └── utils
    │       ├── dl_syscalls.h
    │       ├── label_utils.h
    │       ├── offloading_sync.h
    │       ├── task_utils.h
    │       └── tick.h
    ├── src
    │   ├── main.c
    │   ├── runnables
    │   │   ├── runnable1.c
    │   │   ├── runnable2.c
    │   │   └── ...
    │   ├── tasks
    │   │   ├── task_1.c
    │   │   ├── task_2.c
    │   │   └── ...
    │   └── utils
    │       ├── dl_syscalls.c
    │       ├── label_utils.cpp
    │       ├── task_utils.cpp
    │       └── tick.cpp
    ├── lib
    │   └── fred
    └── synthetic

In the first level, there is the Makefile, and directories for source, includes, and libs.
Currently, there is a single library being used for FRED framework, to access Xilinx FPGAs.
Both the src and include directories have sub-dirs for runnables, tasks, and utils.
The src directory also includes the application main.c. The main has an include *tasks.h*
with prototype to all tasks. It also has an include *label.h* with the definition of all labels
as global variables.

A source file is generated for each Amalthea task and runnable. Each task also has an include,
with prototypes of its runnable functions. 

The runnables access their labels and execute a function called *Count_Ticks* to mimic an actual 
processing. More details about the generated code are presented in :ref:`posix_label`.


Compiling and Executing the Generated Code
==========================================

There are two build systems for the generated code, one base in Make and the other in CMake.
In the future, only CMake will be supported.

Compiling with Make
-------------------

Go to the directory where codegen saved the generated code and execute:


.. code-block:: console

    $ make
    $ ./synthetic

This will compile the code using GCC and G++ compilers by default.
It is also possible to compile with Clang:

.. code-block:: console

    $ make CC=clang CXX=clang++
    $ ./synthetic

Compiling with CMake
--------------------

Go to the directory where codegen saved the generated code and execute:


.. code-block:: console

    $ mkdir build
    $ cd build
    $ cmake ..
    $ make all
    $ cd ..
    $ ./synthetic

As in the previous case, it is also possible to compile with Clang:

.. code-block:: console

    $ mkdir build
    $ cd build
    $ cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ ..
    $ make all
    $ cd ..
    $ ./synthetic

When CMake generates the Makefile, there is a useful flag call
*VERBOSE* to increase the compilation verbosity.
Execute the following command to compile with increased verbosity:

.. code-block:: console
 
    $ make all VERBOSE=1

Performance Analysis
====================

`perf <http://www.brendangregg.com/perf.html>`_ can be used to check if the generated code
is behaving as expected in terms of performance.

.. code-block:: console

   $ sudo apt install linux-tools-$(uname -r) linux-tools-generic

This is an example of the usage to capture several hardware performance counters.

.. code-block:: console

   $ perf stat -e LLC-loads,LLC-load-misses,LLC-stores,LLC-prefetches,L1-dcache-loads,L1-dcache-load-misses,L1-dcache-stores,instructions,cycles,cycles:k,cycles:u,cache-references,cache-misses,bus-cycles ./synthetic

Extending the codegen
=====================

The code structure has been designed to allow to extend the code generator easily with these steps:

- For instance, to generate a code for FreeRTOS, one would copy the directory 
  *code_gen/linux_posix* to *code_gen/freertos*;

- Edit the code in *code_gen/freertos* accordingly to generate the code;

- Change the import line of app4mc.py from 

.. code-block:: python

    import app4mc.code_gen.linux_posix.main as posix

to 

.. code-block:: python

    import app4mc.code_gen.freertos.main as freertos

Limitations
===========

The :ref:`posix_label` code generator has the following limitations: 

- It generates code only for periodic tasks;
- It generates code for FPGA offloading, but not for GPU offloading;

The :ref:`openmp_thread_label` code generator is virtually the same as the Posix, with few modifications in the generated *main()*. This way, the limitations that apply
for Posix will also apply for OpenMP.

For a complete list of its limitation, please refer to the *ToDo List*.
