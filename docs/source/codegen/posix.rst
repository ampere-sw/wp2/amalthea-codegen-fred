.. _posix_label:

Linux/POSIX
===========

This section explains the Linux code generated based on an AMALTHEA model. 
The code is explained in four main parts: the main, task code, runnable code, and global labels.

The main
--------

The template generated application *main* is presented below. It consists of 
two header files that declare the tasks called in the main and it creates the data 
shared among the tasks implemented as theads. This example generated 
3 tasks called *Task_5MS*, *Task_10MS*, and *Task_20MS*.

.. code-block:: C

  //...
  #include "tasks.h" // task prototypes
  #include "label.h" // create the shared data

  //...

  int main()
  {
    std::thread thread_task_10ms(Task_10MS);
    std::thread thread_task_20ms(Task_20MS);
    std::thread thread_task_5ms(Task_5MS);

    thread_task_10ms.join();
    thread_task_20ms.join();
    thread_task_5ms.join();

    return EXIT_SUCCESS;
  }

Task structure
--------------

The following partial code shows the generated task code structure. Currently, 
**only periodic tasks are supported**. A periodic task needs 3 pieces of information:

- *task period*: Taken from task Stimuli attribute of the AMALTHEA model.
  The task Stimuli attribute must be of the type PeriodicStimulus with the Recurrence attribute,
  defined in the StimuliModel. 
  The task period is then written in the define TASK_PERIOD of the generated task code;
- *task runtime*: Taken from RuntimeUtil.getExecutionTimeForProcess in the AMALTHEA model.
  This function sums up the ticks of all the task's runnables, taken into consideration 
  a chosen PU type, in case a runnable annotates ticks for multiple PU types. 
  The task runtime is then written in the define TASK_RUNTIME of the generated task code;
- *task deadline*: Taken from ProcessRequirement, inside the ConstraintsModel, in the AMALTHEA model.
  The ProcessRequirement must have a TimeRequirementLimit,  where the deadline is set.
  The task deadline is then written in the define TASK_DEADLINE of the generated task code;

If a task in the AMALTHEA model does not have these three information, 
then the code generation  is aborted with an error.

All timing information of the code generator is **ALWAYS in microseconds**.

.. code-block:: C

    #define TASK_PERIOD   123456
    #define TASK_RUNTIME  1234
    #define TASK_DEADLINE 123456

    /* 
    ###############################
    TASK DESCRIPTION
    Stimuli type: PeriodicStimuli
    Mapped PU: Core1
    PU type: CPU
    PU definition: DefaultCore
    ###############################
    */
    void Task_1()
    {
        // timing definitions
        struct period_info pinfo;
        pinfo.period_ns = TASK_PERIOD;
        periodic_task_init(&pinfo);
      
        // the task mapping definition
        sched_setaffinity(0, sizeof(cpuset), &cpuset);

        // setting the scheduling attributes
        sched_setattr(0, &sa, 0);

        // the main task code, where the runnables are called
        while (1)
        {
            // runnable calls
            runnable1();
            runnable2();
            // ...

            // checking the task period
            wait_rest_of_period(&pinfo);
            #ifdef DEBUG
            std::cout << "Task_1: " << iter << "\n";
            iter++;
            #endif
        }
    }

The task period, runtime, and deadline are then set in the OS by using `sched_setattr function <https://man7.org/linux/man-pages/man2/sched_setattr.2.html>`_. 
*SCHED_DEADLINE* is used as the scheduling policy.

In case the task is mapped to the multi-core processor instead of a GPU or FPGA, 
then the *task affinity* information is taken from the AMALTHEA MappingModel and set 
in the OS using the `sched_setaffinity function <https://linux.die.net/man/2/sched_setaffinity>`_.

The task period is implemented by the function *wait_rest_of_period* which internally uses the 
functions `clock_gettime(CLOCK_MONOTONIC_RAW,) <https://linux.die.net/man/3/clock_gettime>`_ and
`clock_nanosleep(CLOCK_MONOTONIC_RAW, TIMER_ABSTIME,,) <https://linux.die.net/man/2/clock_nanosleep>`_.
The runnables spend the time they are supposed to spend and, if there is still some remaining time 
to complete the period, the task is suspended with clock_nanosleep.

Besides, a task code also has:
 - Comments presenting additional information such as
   the core mapping, type of PU, and type of stimuli used by the task;
 - Debug messages at the end of the code that can be turned on with DEBUG define. 


Runnable structure
------------------

All runnables have the following structure presented in this partial source code.

.. code-block:: C

    /* tick exec in microseconds */
    #define TICK_EXECUTION_TIME 440

    /* 
    ###############################
    RUNNABLE DESCRIPTION
    Calling task: Task_1
    Mapped PU: Core1
    PU definition: DefaultCore
    ###############################
    */
    void runnable1()
    {
        // list of shared label declarations
        extern TYPE1 label1;
        extern TYPE2 label2;
        extern TYPE3 label3;
        // ...
        // common declarations
        uint64_t val=0;
        Tick_DiscreteValueConstant tick(TICK_EXECUTION_TIME);

        // list of label reads
        val = (uint64_t)label1;
        val = (uint64_t)label2;

        // runnable processing
        Count_Ticks(tick());

        // list of label writes
        label1 = (TYPE1)val;
        label3 = (TYPE3)val;
    }

The define *TICK_EXECUTION_TIME* informs the number of microseconds spent in computation.
This is the time spent in the function *Count_Ticks*. The *tick* variable is used
to abstract different methods to compute the execution time. The AMALTHEA model has
several ways to specify the ticks, identified as *DiscreteValue**. Currently, the 
following DiscreteValue are implemented in the code generator:

- *DiscreteValueConstant*: This is the simplest method the delivers a constant value.
  This is currently the only supported type of ticks;
- *DiscreteValueGaussianDistribution*: It implements a normal distribution with as 
  defined in `C++ std::normal_distribution <https://www.cplusplus.com/reference/random/normal_distribution/>`_
  This distribution is defined by the parameters mean, std deviation, lower bound, and upper bound.
  This feature is not fully implemented in the code generator;
- *DiscreteValueStatistics*: Is implemented like DiscreteValueGaussianDistribution;
- *DiscreteValueWeibullEstimatorsDistribution*: This distribution is not fully implemented
  because there is a mismatch between the parameters of the AMALTHEA model and 
  `C++ std::weibull_distribution <https://www.cplusplus.com/reference/random/weibull_distribution/>`_.

There are two functions used to simulate the computation part of the runnable: Count_Ticks and Count_Time.
*Count_Ticks* is a function that loops over 256 operations like *nop ^=1*. The underlying assumption 
is that this function counts instructions and not time. It also assumes that all instructions have 
the same execution time in clock cycles.
If the user wants to specify the time spent in computation, and not the number of executed instructions, 
then the *Count_Time* function is a better option.
*Count_Time* actually measure the amount of time spent in the computation.
The following code gives an idea of its behavior. While the time spent executing is 
lower than the expected number of ticks, it keeps running for another loop iteration. 

.. code-block:: C

   while (dur < ticks) {
      begin = std::chrono::steady_clock::now();
      for(j=0;j<1000000;j++);
      end = std::chrono::steady_clock::now();
      dur += std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();
      i++;
   }

Currently, *Count_Ticks* is the default method to mimic the runnable execution time.
Nevertheless, if it is decided to migrate definitely to *Count_Time*, this 
requires changing only one line of code.
These two functions had compiler optimizations turned off.
These two functions are the only part of the generated code that turns off the compiler optimizations.

One final remark regarding ticks is that the AMALTHEA model enables  the definition of multiple ticks
for each possible kind of processing unit that might execute this runnable. For instance,
the runnable might be mapped to two different cores with different performances and a different
number of ticks, depending on the mapping. The code generator currently supports this feature. 
However, each task must be mapped to a single processing unit.
It means that the model needs to include the complete mapping information so that the code generator 
knows which ticks must be taken into account.

Before the runnable function definition, there is a comment with the following format: 

.. code-block:: C

    /* 
    ###############################
    RUNNABLE DESCRIPTION
    Calling task: Task_1
    Mapped PU: Core1
    PU definition: DefaultCore
    ###############################
    */

This comment captures into the source code some basic information about the runnable
like the task that calls the runnable, the processing unit this is mapped to
and the type of processing unit.

The internals of the function defining the runnable are divided into 5 parts:

- Extern declarations to all label read or written in this runnable;
- Some variable declarations that are common to all runnables;
- A list of all label reads, if the runnable has read labels;
- The runnable execution, parameterized by the tick object;
- A list of all label writes, if the runnable has written labels.

When the runnable accesses vectors instead of scalar types, the code generator
uses auxiliary functions to perform the read and write operations, as presented
in the following example representing part of a runnable:

.. code-block:: C

    // list of label reads
    read_label(Vehicle_status_host,1000);
    read_label(x_car_host,1000);
    read_label(y_car_host,1000);
    read_label(yaw_car_host,1000);

    // runnable processing
    Count_Ticks(tick());

    // list of label writes
    write_label(x_car_host,1000);
    write_label(y_car_host,1000);
    write_label(yaw_car_host,1000);
    write_label(vel_car,1000);
    write_label(yaw_rate,1000);

The functions *read_label* and *write_label* are used when the label is defined as an array. 
These functions can access arrays of any size, including size 1.
These four functions are defined in the *label_utils.h*

More information about the label generation is presented in the next section.

Global labels
-------------

All the labels of an AMALTHEA model are declared as global variables in the *label.h* file.
The labels support the following *stdint* primitive types:

- uint8_t:  for AMALTHEA labels with size between [1,8] bits;
- uint16_t: for AMALTHEA labels with size between [9,16] bits;
- uint32_t: for AMALTHEA labels with size between [17,32] bits;
- uint64_t: for AMALTHEA labels with size between [33,64] bits;
- C vector: for AMALTHEA labels with size greater than 64. All vector positions are of type uint8_t.

When a runnable needs to read or write in a label, it uses the C keyword *extern* so that the 
linker can link to the externally defined (in the label.h file) global variable.

An AMALTHEA label has an attribute called *DataStability* to inform the type of protection 
the label needs. The code generator supports *DataStability = automaticProtection*.
In this case, if the label has a scalar type, it uses 
`C++ std::atomic <https://en.cppreference.com/w/cpp/atomic/atomic>`_. 
If the label is a vector and DataStability = automaticProtection, then it uses a type
called *Atomic_Array*, defined in the file label_util.h. It uses 
`C++ std::shared_timed_mutex <https://en.cppreference.com/w/cpp/thread/shared_timed_mutex>`_ 
and C++ locks to protect the access to the vector.
