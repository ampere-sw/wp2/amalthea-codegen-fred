.. _openmp_thread_label:

OpenMP for multithreads
=======================

When the PyApp4MC generates OpenMP code, and the model includes a task mapped to a multicore processor 
(i.e., ProcessingUnit == CPU), then the generated code will look like this snippet code

.. code-block:: C

    int main()
    {
        #pragma omp parallel
        {
            #pragma omp single
            {
                #pragma omp task
                Task_1();
                #pragma omp task
                Task_2();
                #pragma omp task
                Task_3();
            }
        }
    }

This code generates a single thread to each task and it blocks by the end of the *omp single* block.
The code of the tasks and runnables are not changed compared to the code generated for POSIX.

