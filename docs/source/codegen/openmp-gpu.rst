
Using GPU with OpenMP
=====================

When the PyApp4MC generates OpenMP code, and the model includes a task mapped to GPU
(i.e., ProcessingUnit == GPU), then the generated code will look like this snippet code.

.. code-block:: C

    int main()
    {
        #pragma omp parallel
        {
            #pragma omp single
            {
                #pragma omp task
                Task_1();
                #pragma omp task
                Task_2();
                #pragma omp target
                #pragma omp task
                Task_3();
            }
        }
    }

The keyword *omp target* indicates that *Task_3* is offloaded to the GPU while the other tasks are executed 
in the multicore.


