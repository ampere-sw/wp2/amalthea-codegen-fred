
Using FPGA with FRED
====================

PyApp4MC code generator supports offloading to Xilinx Zynq and UltraScale FPGAs.
The generated code is compatible with  `FRED framework <https://fred-framework-docs.readthedocs.io>`_ ,which provides the tools and abstraction to interface the application with the FPGA.

The FRED framework consists of two main parts: DART and FRED runtime. `DART <https://fred-framework-docs.readthedocs.io/en/latest/docs/02_dart/index.html>`_ is the design tool that assists in the generation of optimized bitstreams to be handled by FRED during the execution. `FRED runtime <https://fred-framework-docs.readthedocs.io/en/latest/docs/03_runtime/index.html>`_ requires that the offloaded software uses `fred_lib <https://github.com/fred-framework/fred-linux-client-lib>`_ to communicate to the `fred-server <https://github.com/fred-framework/fred-linux>`_ which manages the FPGA programming requests. 

Please refer to Biondi et al. [1] or the FRED web page for further information about FRED.

AMALTHEA requirements for an FPGA runnable
------------------------------------------

This section uses the FRED enabled model in the directory *examples/minimal-codegen/fpga/minimal-out.amxmi*
to present the modeling extensions used by the code generator. Execute the following command to generate 
the code for this example.

.. code-block:: console

    $ app4mc examples/minimal-codegen/fpga/minimal-out.amxmi /tmp/min-fpga

The following figure shows the CustomProperties required by an FPGA runnable. 
The runnable called simple\_runnable, which belongs to the task called single\_task, 
has its input and output labels and a tick specification.
Besides, it has Group called FPGA with the following required CustomProperties:

- *hdl\_top\_name*: The name of the top module of the IP core;
- *ip\_name*: The directory name of the IP core under the path pointed by the environment variable *DART\_IP\_PATH*;
- *hw\_id*: This is a numeric identifier for each IP, assigned by DART.

.. image:: ../images/fred-swmodel.png
  :width: 800
  :alt: FRED hardware model

This example task also has two other runnables called in_runnable and out_runnable, responsible for
genenerating the stimuli and verify the expected response from the FPGA, respectively. In this example,
128 bytes are sent to the FPGA, 128 are sent from the FPGA, and the FPGA output is compared with the expected one.

The following figure shows the *CustomProperties* required by an FPGA *ProcessingUnitDefinition* called *FPGA\_rr\_0*.
Each FPGA *ProcessingUnitDefinition* in the AMALTHEA model represents a reconfigurable region 
in FRED which multiple IP cores can share.

- *fpga\_model*: The type of FPGA used. The supported boards are PYNQ and ZYNQ;
- *reconf\_if\_bandwidth*: The reconfiguration interface bandwidth in MB/s. 
  The bandwidth depends on the type of DPR interface supported by the board, such as ICAP;
- *reconf\_region\_id*: This is a numeric identifier of the FRED reconfigurable region. DART automatically assigns this value;
- *clb*: The number of CLBs used by this reconfigurable region. DART automatically assigns this value;
- *bram*: The number of BRAMs used by this reconfigurable region. DART automatically assigns this value;
- *dsp*: The number of DSPs used by this reconfigurable region. DART automatically assigns this value;

.. image:: ../images/fred-hwmodel.png
  :width: 800
  :alt: FRED hardware model

The next and final figure shows how the IP cores are mapped to their respective reconfigurable regions 
with the AMALTHEA model. An AMALTHEA *TaskAllocation* is extended with CustomProperty called *Runnable*
This specific example says that the task called single\_task has an FPGA runnable called simple\_runnable
which is mapped to the *ProcessingUnit* called FPGA\_rr\_0, which represents the FRED reconfigurable region 0.


.. image:: ../images/fred-mapping.png
  :width: 800
  :alt: FRED runnable mapping

The XML representation for the mapping illustrated above is presented next.

.. code-block:: XML

  <taskAllocation task="single_task?type=Task" scheduler="task_os?type=TaskScheduler" affinity="FPGA_rr_0?type=ProcessingUnit">
    <customProperties key="Runnable">
        <value xsi:type="am:StringObject" value="simple_runnable"/>
    </customProperties>
  </taskAllocation>

Let us assume that the *single_task* in this example has multiple runnables and two of them, called
simple\_runnable fpga\_runnable2, must be offloaded to the FPGA.
In this case, the XML representation of the mapping would be:

.. code-block:: XML

  <taskAllocation task="single_task?type=Task" scheduler="task_os?type=TaskScheduler" affinity="FPGA_rr_0?type=ProcessingUnit">
    <customProperties key="Runnable">
        <value xsi:type="am:StringObject" value="simple_runnable"/>
    </customProperties>
  </taskAllocation>
  <taskAllocation task="single_task?type=Task" scheduler="task_os?type=TaskScheduler" affinity="FPGA_rr_0?type=ProcessingUnit">
    <customProperties key="Runnable">
        <value xsi:type="am:StringObject" value="fpga_runnable2"/>
    </customProperties>
  </taskAllocation>

Note that there is a TaskAllocation section for each FPGA runnable. In this case, both FPGA runnables are 
mapped to the same reconfigurable region called *FPGA\_rr\_0*.

Now, let us assume that the model includes another task called task2, and it has an 
FPGA runnable called fpga_runnable3 also mapped to the reconfigurable region *FPGA\_rr\_0*.

.. code-block:: XML

    <taskAllocation task="single_task?type=Task" scheduler="task_os?type=TaskScheduler" affinity="FPGA_rr_0?type=ProcessingUnit">
    <customProperties key="Runnable">
        <value xsi:type="am:StringObject" value="simple_runnable"/>
    </customProperties>
    </taskAllocation>
    <taskAllocation task="single_task?type=Task" scheduler="task_os?type=TaskScheduler" affinity="FPGA_rr_0?type=ProcessingUnit">
    <customProperties key="Runnable">
        <value xsi:type="am:StringObject" value="fpga_runnable2"/>
    </customProperties>
    </taskAllocation>
    <taskAllocation task="task2?type=Task" scheduler="task_os?type=TaskScheduler" affinity="FPGA_rr_0?type=ProcessingUnit">
    <customProperties key="Runnable">
        <value xsi:type="am:StringObject" value="fpga_runnable3"/>
    </customProperties>
    </taskAllocation>    

Finally, let us assume that the timing characteristics of the third FPGA runnable are such that 
DART decided to create a second reconfigurable region. In other words, it means that the first and the second IPs 
are shared the first reconfigurable region, but the third IP requires a dedicated reconfigurable 
region. In this case, the mapping would be like in this example below:

.. code-block:: XML

    <taskAllocation task="single_task?type=Task" scheduler="task_os?type=TaskScheduler" affinity="FPGA_rr_0?type=ProcessingUnit">
    <customProperties key="Runnable">
        <value xsi:type="am:StringObject" value="simple_runnable"/>
    </customProperties>
    </taskAllocation>
    <taskAllocation task="single_task?type=Task" scheduler="task_os?type=TaskScheduler" affinity="FPGA_rr_0?type=ProcessingUnit">
    <customProperties key="Runnable">
        <value xsi:type="am:StringObject" value="fpga_runnable2"/>
    </customProperties>
    </taskAllocation>
    <taskAllocation task="task2?type=Task" scheduler="task_os?type=TaskScheduler" affinity="FPGA_rr_1?type=ProcessingUnit">
    <customProperties key="Runnable">
        <value xsi:type="am:StringObject" value="fpga_runnable3"/>
    </customProperties>
    </taskAllocation>    

Note that the third IP is now mapped to the reconfigurable region called *FPGA\_rr\_1* while the 
first two IPs are assigned to the reconfigurable region *FPGA\_rr\_0*.

The generated code for FPGA with FRED
-------------------------------------

FRED is a framework to support the design, development, and execution 
of predictable software on FPGA system-on-chips. 
In FRED, an application running in the host side interacts with the IPs running in the FPGA via the 
`FRED API <https://github.com/fred-framework/fred-linux-client-lib>`_.
Please refer to the reference [1] or the FRED web page for further information about FRED.

Compared to the conventional Linux/POSIX code presented before, 
FPGA offloading changes only partially the structure of the runnable code,
as presented in the following example. It means that, considering the
task structure, a FPGA runnable is the same as a runnable for a
multicore CPU.

The code example presented below details the structure of an FPGA runnable with FRED.
In general, it has a similar structure to a regular runnable with shared labels declared on 
top of the runnable function, common declarations, a sequence of label reads, tick running, 
and a sequence of label writes.

.. code-block:: C

  void simple_runnable()
  {
    // shared label declarations
    extern uint8_t vet_in [128];
    extern uint8_t vet_out [128];
    // common declarations
    uint64_t val=0;
    #ifndef EMULATE_FPGA_OFFLOADING
      struct fred_data *fred;
      struct fred_hw_task *hw_task;
      uint32_t hw_task_id = 100;
      void *buff_in = NULL;
      void *buff_out = NULL;
      // Initialize communication and bind a HW-task
      fred_init(&fred);
      fred_bind(fred, &hw_task, hw_task_id);
      // Map the FPGA data buffers
      buff_in = fred_map_buff(fred, hw_task, 0);
      buff_out = fred_map_buff(fred, hw_task, 1);
    #endif

    read_label(vet_in,128);
    Tick_DiscreteValueConstant tick0(1000000);
    long tick_val = tick0();
    #ifdef EMULATE_FPGA_OFFLOADING
      Count_Ticks(tick_val);
    #else
      fred_accel(fred, hw_task);
    #endif
    write_label(vet_out,128);
  }

On the other hand, an FPGA runnable with FRED has an additional code section for FRED-related declarations
and two alternatives to execute the runnable controlled by the *EMULATE_FPGA_OFFLOADING* define.

When the *EMULATE_FPGA_OFFLOADING* define exists, the runnable will not run the FPGA programmable logic 
(called PL part) but emulated in the host CPU of the FPGA (called PS part). This is useful
in the early stages of the design, where the hardware IPs might not be available yet. In a future deliverable of the 
AMPERE project, an AMALTHEA compatible hardware IP will be developed to mimic the behavior of the actual hardware IP, 
i.e., it will perform the same number of memory accesses and spend the same number of clock cycles in FPGA computation.

When the *EMULATE_FPGA_OFFLOADING* define does not exist, then it is executed in the actual hardware IP in the PL part of the FPGA.
In this case, the FRED API (see functions starting with *fred\_*) implements the communication with the FPGA by 
performing a client-server software architecture, where the application side plays the role of the client-side, and
the FRED server plays the role of the server-side. The actual FPGA offloading starts when the FRED function 
*fred\_accel* is called.

A current limitation of the code generator, which is a limitation inherited from DART, supports only one input and 
one output buffer per IP. It means that an FPGA runnable should have a single input label and
a single output label. The pointer to these labels should be assigned to the *buff\_in* and *buff\_out* buffer pointers.

Running the Amalthea FPGA Flow
-----------------------------

This codegen is part of the `Amalthea FPGA Flow <https://gitlab.retis.santannapisa.it/a.amory/amalthea4dart_plugin.git>`_, where it automates the all the steps from the Amalthea model, 
DART optimization, and FRED execution in the FPGA. Follow the steps described in *examples/minimal-codegen/fpga/readme.md* to run this example within the flow.

References
----------

.. [1] Biondi, A., Balsini, A., Pagani, M., Rossi, E., Marinoni, M., & Buttazzo, G. (2016, November). A framework for supporting real-time applications on dynamic 
   reconfigurable FPGAs. In 2016 IEEE Real-Time Systems Symposium (RTSS) (pp. 1-12). IEEE.

