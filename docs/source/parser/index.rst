***************
Amalthea Parser
***************

.. DANGER::
   **Deprecated !!!**

The :mod:`app4mc.parser` module is depreated. :mod:`app4mc.code_gen` is
currently using the official and complete Java-based parser via a `py4j <https://www.py4j.org>`_ gateway. If you still want to use this parser, refer to the tag 
`v1.0 <https://gitlab.retis.sssup.it/ampere/amalthea-codegen-linux/tags/v1.0>`_.

Be aware that the :mod:`app4mc.parser` parser has extremely limited capabilities. It basically parses **part** of the Amalthea SwModel (:mod:`~app4mc.parser.sw_model`) and StimuliModel (:mod:`~app4mc.parser.stimuli_model`). It does not parse the HWModel, OSModel, MappingModel, ConstaintsModel, etc. Check the **TODOs** link for a list of the existing limitations. 

Also, differently from the oficial Java parser, this parser is built *by hand*, so it is probably full of bugs. Thus, I would not recomend this parser for any full grown tool.
Despite of all these warnings and limitations, it still proved to be usefull for experimental usage and for learning purposes.

When running a model with a unsupported feature, the user will see a msg like this

::

    WARNING: task item WaitEvent
    WARNING: task item ClearEvent
    WARNING: task item WaitEvent

It means that the Amalthea tag *WaitEvent* and *ClearEvent* are not implemented in the parser, thus, they are being ignored in this model.

Efforts to expand its parsing capabilites are always wellcome !
