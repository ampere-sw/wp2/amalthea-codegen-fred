OpenMP Codegen
==============

The idea would be to generate Amalthea tasks as OpenMP `task` instead of Posix threads. It would also support OpenMP `target`` for GPU offloading. However, this is very experimental and incomplete. Fell free to step in and send a *push*. 

It includes detailed instructions on `how to setup CLANG <./offloading-setup.html>`_ for using libomptarget fo NVidia Xavier. 

References
==========

 - Antao, Samuel F., et al. "Offloading support for OpenMP in Clang and LLVM." 2016 Third Workshop on the LLVM Compiler Infrastructure in HPC (LLVM-HPC). 2016.
   `presentation <https://llvm-hpc3-workshop.github.io/slides/Bertolli.pdf>`_.
 - Prithayan Barua. `Static Analysis of OpenMP Data Mapping for Target Offloading <https://www.youtube.com/watch?v=9ZPQpo8QuyI&feature=youtu.be>`_.
 - `LLVM Developers' Meeting <https://llvm.org/devmtg/>`_.

