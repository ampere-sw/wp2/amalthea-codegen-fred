************
OpenMP Setup
************
Currently, OpenMP supports heterogeneous with GPU only. One of the 
goals of the AMPERE project is also to enable FPGA processing units 
transparently for the programmer.

This document explains how to setup OpenMP for GPU offloading in both
X86-64 computer (referred to as the host computer) and in an NVidia Xavier board.
In the future, this document will be extended to include the setup also
for FPGA offloading.

Requirements
============

These are the requirements to be performed before the OpenMP setup itself.

Hardware requirements
---------------------

In the host computer, it's necessary to check the *Computing Capability*
of the existing GPU. In the
`clang/CmakeList.txt <https://github.com/llvm/llvm-project/blob/ef32c611aa214dea855364efd7ba451ec5ec3f74/clang/CMakeLists.txt#L297>`__
one will find these comments that say that the minimum hardware
requirement for GPU offloading with OpenMP is a GPU with *Computing
Capability 3.5*, referred as *sm_35*. Please check
`wikipedia <https://en.wikipedia.org/wiki/CUDA>`__ to see the CUDA
compute capability of your GPU.

::

   # OpenMP offloading requires at least sm_35 because we use shuffle instructions
   # to generate efficient code for reductions and the atomicMax instruction on
   # 64-bit integers in the implementation of conditional lastprivate.
   set(CLANG_OPENMP_NVPTX_DEFAULT_ARCH "sm_35" CACHE STRING
     "Default architecture for OpenMP offloading to Nvidia GPUs.")
   string(REGEX MATCH "^sm_([0-9]+)$" MATCHED_ARCH "${CLANG_OPENMP_NVPTX_DEFAULT_ARCH}")
   if (NOT DEFINED MATCHED_ARCH OR "${CMAKE_MATCH_1}" LESS 35)
     message(WARNING "Resetting default architecture for OpenMP offloading to Nvidia GPUs to sm_35")
     set(CLANG_OPENMP_NVPTX_DEFAULT_ARCH "sm_35" CACHE STRING
       "Default architecture for OpenMP offloading to Nvidia GPUs." FORCE)
   endif()

The Xavier board has `Computing Capability
7.2 <https://www.techpowerup.com/gpu-specs/jetson-agx-xavier-gpu.c3232>`__.
So it is compatible with OpenMP GPU offloading.

The specific board owned by SSSA is **NVIDIA Jetson AGX Xavier**, module code P2888-0001, with 32GB of RAM.
It has JetPack 4.4 installed.

The environment variable *JETSON_L4T_REVISION* and *JETSON_MODULE* can be used to check, respectively,
 the JetPack and the module version.

Driver requirements
-------------------

The CUDA environment needs to be installed first in both the host
computer and in the Xavier board. Please refer to the instructions for the
`Ubuntu 18.04
(host) <https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html#ubuntu-installation>`__
and for `Xavier <https://developer.nvidia.com/embedded/jetpack>`__.

Hard disk requirements
----------------------

Before starting the compilation, make sure that the computer has about
10GBytes of free disk space. This is especially important for the Xavier
board because the OS and base GPU libraries use more than 20GB out of the 32GBytes disk. The configuration presented here used
800MB of disk at the end of the compilation, but apparently, it uses more
disk space in the middle of the compilation process. Due to this issue,
it is highly recommended to install a second disk. There is an excellent
tutorial about this in
`JetsonHacks <https://www.jetsonhacks.com/2018/10/18/install-nvme-ssd-on-nvidia-jetson-agx-developer-kit/>`__.

In case you installing the extra hard disk is not possible, 
one alternative is to mount a bigger remote disk at the expense of performance. 
To minimize the impact in performance, it is suggested to select a remote disk in the same local network as Xavier and make sure that a Gbyte link is used.

Let's assume that the Clang source code is stored in *~/llvm-project* in
the host computer, and we want to mount it in *~/mnt/llvm-project* in the
Xavier board. In the Xavier board, execute:

.. code-block:: console

   $ sudo apt-get install sshfs
   $ sshfs <user>@<host-ip>:~/llvm-project ~/mnt/llvm-project

Another alternative is to mount the remote drive via
`NFS <https://www.digitalocean.com/community/tutorials/how-to-set-up-an-nfs-mount-on-ubuntu-18-04>`__.

Then, proceed with the Clang configuration as presented in the next sections.


Setting up the CLANG compiler
=============================


The CLANG dependencies
----------------------

These dependencies are required for both the X86-64 and for the Xavier computers.

.. code-block:: console

   $ sudo apt install build-essential
   $ sudo apt install cmake
   $ sudo apt install -y libelf-dev libffi-dev
   $ sudo apt install -y pkg-config
   $ sudo apt-get install -y ninja-build 

*ccmake* is not mandatory, but it's recommended to inspect the CMake
configuration.

.. code-block:: console

   $ sudo apt-get install cmake-curses-gui

Clang for the Host computer
---------------------------

In the host computer, follow these instructions to download Clang:

.. code-block:: console

   $ git clone https://github.com/llvm/llvm-project.git
   $ cd llvm-project
   $ git checkout release/11.x
   $ mkdir build; cd build

There is no actual constraint regarding the Clang version for X86-64.
Version 11 is selected, but it could also be version 10, for example.
However, for Xavier, there is a constraint, as explained next. As stated
in the
`libomptarget <https://github.com/llvm/llvm-project/tree/release/10.x/openmp/libomptarget>`__,
the supported compilers are **clang version 3.7 or later** and **gcc
version 4.8.2 or later**.

Next, let's configure the CMAKE building system, with the option for OpenMP
offloading:

.. code-block:: console

   $ cmake -G Ninja -DLLVM_ENABLE_PROJECTS="clang;openmp" -DLLVM_TARGETS_TO_BUILD="X86;NVPTX" -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=/opt/clang11 -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ ../llvm
   $ ninja

It's highly recommended to **use ninja build system instead of make**.
The generated Makefile has some sort of weird bug that, when you type
*make install*, it compiles all over again.

The options *-DLLVM_TARGETS_TO_BUILD="X86;NVPTX"* and
*-DCMAKE_BUILD_TYPE=Release* are important to reduce the compilation time.
The parameter *LLVM_TARGETS_TO_BUILD* compiles CLANG only for the required
platforms. The argument *-DBUILD_SHARED_LIB=ON* is a good idea if the
computer has less than 16GBytes of RAM. In a PC, the compilation time is
about 35 min using **make -j 8**.

The options below are recommended for offloading debug:

-  -DLIBOMPTARGET_ENABLE_DEBUG=ON
-  -DLIBOMPTARGET_NVPTX_DEBUG=ON

There are other LLVM projects that might be of interest. So, enable them
individually as in this example:

.. code-block:: console

   $ CMAKE .... -DLLVM_ENABLE_PROJECTS="clang;clang-tools-extra;libcxx;libcxxabi;lld;openmp" ...

Clang for the Xavier
--------------------

Clang for Xavier (aarch64) can be version 11, without applying any
modification. On the other hand, the change is simple if, for any reason, an older LLVM version is required.

If one tries to compile LLVM version 10 or earlier for aarch64, 
the following message will appear hidden among several other messages:

.. code-block:: console

   Not building CUDA offloading plugin: only support CUDA in Linux x86_64 or ppc64le hosts

It turns out, by reading this
`issue <https://reviews.llvm.org/D76469>`__, that aarch64 was not tested
for LLVM 10. However, it works. The modification in the source code is
straightforward. Just compare the following line of code in `LLVM
version
11 <https://github.com/llvm/llvm-project/blob/280e47ea0e837b809be03f2048ac8abc14dbc387/openmp/libomptarget/plugins/cuda/CMakeLists.txt#L12>`__
and and `LLVM version
10 <https://github.com/llvm/llvm-project/blob/ef32c611aa214dea855364efd7ba451ec5ec3f74/openmp/libomptarget/plugins/cuda/CMakeLists.txt#L12>`__.
It is just a matter of copying these two lines from LLVM version 11 to
10 to enable *libomptarget* compilation for LLVM 10.

Once this version issue is settled, follow these instructions to
download Clang for LLVM version 10:

.. code-block:: console

   $ git clone https://github.com/llvm/llvm-project.git
   $ cd llvm-project
   $ git checkout release/10.x
   apply the patch as explained above
   $ mkdir build; cd build

Or these instructions to download Clang for LLVM version 11, where no
patch is required.

.. code-block:: console

   $ git clone https://github.com/llvm/llvm-project.git
   $ cd llvm-project
   $ git checkout release/11.x
   $ mkdir build; cd build

Next, it is the LLVM/Clang configuration itself. For the `NVidia
Xaxier <https://releases.llvm.org/11.0.0/docs/HowToBuildOnARM.html>`__
board, the configuration command is:

.. code-block:: console

   cmake -G Ninja -DCLANG_OPENMP_NVPTX_DEFAULT_ARCH=sm_72 -DLIBOMPTARGET_NVPTX_COMPUTE_CAPABILITIES=72 -DLIBOMPTARGET_ENABLE_DEBUG=ON -DLIBOMPTARGET_NVPTX_DEBUG=ON -DLLVM_ENABLE_PROJECTS="clang;openmp" -DLLVM_TARGETS_TO_BUILD="AArch64;NVPTX" -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=ON -DCMAKE_INSTALL_PREFIX=/opt/clang11 -DCMAKE_C_COMPILER=gcc -DCMAKE_CXX_COMPILER=g++ -DCMAKE_C_FLAGS="-march=armv8.2-a" -DCMAKE_CXX_FLAGS="-march=armv8.2-a" ../llvm


Where the main differences are:

 - *-DLLVM_TARGETS_TO_BUILD="AArch64;NVPTX"*: compatible with Xavier's ARM CPU architecture;
 - *-DCMAKE_C_FLAGS="-march=armv8.2-a" and *-DCMAKE_CXX_FLAGS="-march=armv8.2-a"*: compatible with Xavier's ARM CPU architecture;
 - *-DCLANG_OPENMP_NVPTX_DEFAULT_ARCH=sm_72*: compatible with Xavier's GPU;
 - *-DLIBOMPTARGET_NVPTX_COMPUTE_CAPABILITIES=72*: compatible with Xavier's GPU;


Before running the actual compilation in Xavier, it is recommended to
turn on the performance mode. This way, the eight cores will be available for the compilation; otherwise, only four cores are available by default. 
Next, the 1st command displays the current power mode, and the second
allows to change the power mode.

.. code-block:: console

   $ sudo /usr/sbin/nvpmodel -q
   $ sudo /usr/sbin/nvpmodel -m <x>

Where is the power mode ID (i.e. 0, 1, 2, 3, 4, 5, or 6).

Finally, run the compilation:

.. code-block:: console

   $ nohup nice -5 ninja
   $ ninja install

The command *nohup* is recommended in case the terminal to Xavier is
closed. *nice -5* is recommended to reduce the compilation priority
since Ninja uses all available cores by default.

Common procedures 
-----------------

Once the compilation is done (for both x86_64 and aarch64 computers) 
set the *PATH* and *LD_LIBRARY_PATH*. A
first check is to run the following command to see the registered
targets. We are expecting to find an NVIDIA target for GPU offloading.
The example below shows the expected output for Xavier.

.. code-block:: console

   $ llc --version
   LLVM (http://llvm.org/):
     LLVM version 11.0.1
     Optimized build.
     Default target: aarch64-unknown-linux-gnu
     Host CPU: (unknown)

     Registered Targets:
       aarch64    - AArch64 (little endian)
       aarch64_32 - AArch64 (little endian ILP32)
       aarch64_be - AArch64 (big endian)
       arm64      - ARM64 (little endian)
       arm64_32   - ARM64 (little endian ILP32)
       nvptx      - NVIDIA PTX 32-bit
       nvptx64    - NVIDIA PTX 64-bit

The second test is with an actual OpenMP application with *target*
keyword. There is a minimal offloading example at
https://freecompilercamp.org/llvm-openmp-build/. Then, the application
is profiled with *nvprof* to detect the actual use of the GPU. Assuming
a source code called *omp-test.c*, the compilation program for GPU
offloading is:

.. code-block:: console

   $ clang  -o omp-test omp-test.c -fopenmp=libomp -fopenmp-targets=nvptx64-nvidia-cuda

When compiling the application, you might get a warning like this:

.. code-block:: console

   clang-11: warning: No library 'libomptarget-nvptx-sm_72.bc' found in the default clang lib directory or in LIBRARY_PATH. Expect degraded performance due to no inlining of runtime functions on target devices. [-Wopenmp-target]

According to `this
source <https://hpc-wiki.info/hpc/Building_LLVM/Clang_with_OpenMP_Offloading_to_NVIDIA_GPUs>`__,
this will reduce the GPU performance. However, **It was not possible to make OpenMP
offloading work with this warning**. Luckily, this can be easily fixed
by recompiling OpenMP again using Clang as compiler instead of GCC. You
repeat the same CMake configuration command used before and replace
the arguments related to the compiler, like this:

.. code-block:: console

       -DCMAKE_C_COMPILER=$(CLANG_PATH)/bin/clang \
       -DCMAKE_CXX_COMPILER=$(CLANG_PATH)/bin/clang++ \

For convenience, it is suggested to edit the user's and root's *.bashrc*
with the following definitions:

.. code-block:: console

   # setting up Clang 11 with GPU offloading capability
   export PATH=/opt/clang11/bin:$PATH
   export LD_LIBRARY_PATH=/opt/clang11/lib:$LD_LIBRARY_PATH

   # setting up CUDA stuff
   export PATH=/usr/local/cuda-10.2/bin/:$PATH
   export LD_LIBRARY_PATH=/usr/local/cuda-10.2/lib64/:$LD_LIBRARY_PATH

It's also suggested to change this file */etc/skel/.bashrc*.

Checking Clang and libomptarget
-------------------------------

There are several types of checks to be applied. Here is a description
from the most basic to the most complete tests.

When running *ldd* in an OpenMP executable, 
it's possible to see that *libomptarget.so* and *libomp.so* were correctly linked.

.. code-block:: console

   $ ldd ./omp-test
       linux-vdso.so.1 (0x0000007f8f990000)
       libomp.so => /home/alexandre/tools/clang11/lib/libomp.so (0x0000007f8f88f000)
       libomptarget.so => /home/alexandre/tools/clang11/lib/libomptarget.so (0x0000007f8f86e000)
       libpthread.so.0 => /lib/aarch64-linux-gnu/libpthread.so.0 (0x0000007f8f811000)
       libc.so.6 => /lib/aarch64-linux-gnu/libc.so.6 (0x0000007f8f6b8000)
       libdl.so.2 => /lib/aarch64-linux-gnu/libdl.so.2 (0x0000007f8f6a3000)
       /lib/ld-linux-aarch64.so.1 (0x0000007f8f964000)
       libstdc++.so.6 => /usr/lib/aarch64-linux-gnu/libstdc++.so.6 (0x0000007f8f50f000)
       libgcc_s.so.1 => /lib/aarch64-linux-gnu/libgcc_s.so.1 (0x0000007f8f4eb000)
       libm.so.6 => /lib/aarch64-linux-gnu/libm.so.6 (0x0000007f8f432000)

Next, execute the profiler to check for actual GPU use.

.. code-block:: console

   $ nvprof --print-gpu-trace ./omp-test
   ==1532== NVPROF is profiling process 1532, command: ./omp-test
   ==1532== Warning: ERR_NVGPUCTRPERM - The user does not have permission to profile on the target device. See the following link for instructions to enable permissions and get more information: https://developer.nvidia.com/ERR_NVGPUCTRPERM 
   ==1532== Profiling application: ./omp-test
   ==1532== Profiling result:
   No kernels were profiled.

When it does not work, the resulting message is like this:

.. code-block:: console

   $ nvprof --print-gpu-trace ./omp-test
   ======== Warning: No CUDA application was profiled, exiting

In the first message, CUDA was detected, although the user has no
privileges to execute *nvprof*. While in the second message, no CUDA
application was detected.

This permission warning for GPU profiling can be easily solved with the following 
`commands <https://developer.nvidia.com/nvidia-development-tools-solutions-err_nvgpuctrperm-permission-issue-performance-counters#SolnAdminTag>`_.
However, for Jetson boards the instructions are a little bit different as 
`discussed here <https://forums.developer.nvidia.com/t/nvprof-needs-to-be-run-as-root-user-for-profiling-on-jetson/115293>`__.

The required command to give profiling permission for users is:

.. code-block:: console

   $ modprobe nvgpu NVreg_RestrictProfilingToAdminUsers=0

`Jetson Stats <https://github.com/rbonghi/jetson_stats>`__ is another
application to monitor the Jetson GPU that could be used to check if the
GPU offloading is actually happening.

Please note that executing the OpenMp application is not enough to check
if the offloading is working. By default, *libomp* has a fallback
mechanism that, if the GPU offloading fails, it will execute the
app as a multithread app. This way, you won't see any difference
if the GPU is being used or not. These extra tools (nvprof,
Jetson Stats, etc) are required to check if the GPU is being used. However, it is
possible also to disable this fallback mechanism by setting the
environment variable
`OMP_TARGET_OFFLOAD <https://www.openmp.org/spec-html/5.0/openmpse65.html>`__.

If *OMP_TARGET_OFFLOAD* is set and offloading fails, you should see
something like this:

.. code-block:: console

   $ export OMP_TARGET_OFFLOAD=MANDATORY
   $ ./omp-test
   Libomptarget fatal error 1: failure of target construct while offloading is mandatory

If you have sudo or the permissions to run the profile, you will see this
output after running the OpenMP app:

.. code-block:: console

   $ sudo su
   $ nvprof ./omp-test
   ==19911== NVPROF is profiling process 19911, command: ./omp-test
   ==19911== Warning: Unified Memory Profiling is not supported on the underlying platform. System requirements for unified memory can be found at: http://docs.nvidia.com/cuda/cuda-c-programming-guide/index.html#um-requirements
   ==19911== Profiling application: ./omp-test
   ==19911== Profiling result:
               Type  Time(%)      Time     Calls       Avg       Min       Max  Name
    GPU activities:   95.05%  65.471ms         1  65.471ms  65.471ms  65.471ms  __omp_offloading_b301_163dae_main_l35
                       3.47%  2.3900ms         2  1.1950ms  4.1940us  2.3858ms  [CUDA memcpy DtoH]
                       1.48%  1.0173ms         3  339.10us  1.8890us  523.58us  [CUDA memcpy HtoD]
         API calls:   53.09%  233.30ms         1  233.30ms  233.30ms  233.30ms  cuDevicePrimaryCtxRetain
                      17.06%  74.983ms         1  74.983ms  74.983ms  74.983ms  cuModuleLoadDataEx
                      15.01%  65.976ms         4  16.494ms  16.704us  65.535ms  cuStreamSynchronize
                       9.18%  40.354ms         1  40.354ms  40.354ms  40.354ms  cuDevicePrimaryCtxRelease
                       4.13%  18.134ms         1  18.134ms  18.134ms  18.134ms  cuModuleUnload
   ...

If it complains about nvprof not found or some lib is not found, it's
necessary to set the environment variables again as sudo. The following
is an alternative command that requires no *sudo su*:

.. code-block:: console

   sudo PATH=<clang-bin>:<cuda-bin> LD_LIBRARY_PATH=clang-lib>:<cuda-lib> -E bash -c 'nvprof ./omp-test'

These lines show the execution time of each CUDA interface function
implemented below OpenMP.

The next step in terms of testing is to perform the tests of the LLVM
distribution. These tests require Clang version 6 or earlier.
To run those tests, execute:

.. code-block:: console

   $ ninja check-libomptarget

   ...
   ********************
   ********************
   Failed Tests (3):
     libomptarget :: api/omp_get_num_devices_with_empty_target.c
     libomptarget :: mapping/declare_mapper_api.cpp
     libomptarget :: mapping/pr38704.c


   Testing Time: 44.09s
     Passed: 19
     Failed:  3

.. code-block:: console

   $ ninja check-libomptarget-nvptx 

   ...

   ********************
   ********************
   Failed Tests (5):
     libomptarget-nvptx :: api/get_max_threads.c
     libomptarget-nvptx :: data_sharing/alignment.c
     libomptarget-nvptx :: parallel/level.c
     libomptarget-nvptx :: parallel/nested.c
     libomptarget-nvptx :: parallel/num_threads.c


   Testing Time: 69.49s
     Passed: 8
     Failed: 5

Few tests fail. Addition investigation is required to check the cause
of these failed tests.

Other relevant tests include:

.. code-block:: console

   $ ninja check-openmp 
   $ ninja check-libomp

Debugging libomptarget
----------------------

The define *LIBOMPTARGET_ENABLE_DEBUG* enables debug messages for
libomptarget. Add this definition in CMake as in this example to
recompile libomptarget.

.. code-block:: console

   $ cmake ... -DLIBOMPTARGET_ENABLE_DEBUG=ON -DLIBOMPTARGET_NVPTX_DEBUG=ON ...

As stated in the
`readme <https://github.com/llvm/llvm-project/tree/release/11.x/openmp/libomptarget>`__,
it is possible to compile only libomptarget. It's required to compile libomp with 
`-DOMPTARGET_DEBUG <https://github.com/clang-ykt/clang/issues/14#issuecomment-301114816>`_  
so that debug output is enabled.

After all testing and debugging is done, it's suggested to recompile
Clang removing the libomptarget's debug attributes. An alternative is to
keep two Clang installed. One for debugging and another for performance
evaluations.

.. code-block:: console

   -DLIBOMPTARGET_ENABLE_DEBUG=OFF -DLIBOMPTARGET_NVPTX_DEBUG=OFF


Compiling an OpenMP application
================================

Ensure that both CUDA and Clang are with their required environment
variables (PATH and LD_LIBRARY_PATH) set as explained during the testing
phase. Then, compile the OpenMP code with *target*:

.. code-block:: console

   $ cd /tmp
   $ clang -v -o <exec> source_code.c -fopenmp=libomp -fopenmp-targets=nvptx64-nvidia-cuda

Then, type this command to check if the dynamic libraries were found.

.. code-block:: console

   $ cd /tmp
   $ ldd ./omp-test 
       linux-vdso.so.1 (0x00007ffde05cf000)
       libc.so.6 => /lib/x86_64-linux-gnu/libc.so.6 (0x00007ff6b5e48000)
       libcudart.so.10.2 => /usr/local/cuda/lib64/libcudart.so.10.2 (0x00007ff6b5bca000)
       libomp.so => /usr/local/lib/libomp.so (0x00007ff6b5901000)
       libomptarget.so => /usr/local/lib/libomptarget.so (0x00007ff6b56f2000)
       libpthread.so.0 => /lib/x86_64-linux-gnu/libpthread.so.0 (0x00007ff6b56d1000)
       /lib64/ld-linux-x86-64.so.2 (0x00007ff6b600f000)
       libdl.so.2 => /lib/x86_64-linux-gnu/libdl.so.2 (0x00007ff6b56cc000)
       librt.so.1 => /lib/x86_64-linux-gnu/librt.so.1 (0x00007ff6b56c0000)
       libstdc++.so.6 => /usr/lib/x86_64-linux-gnu/libstdc++.so.6 (0x00007ff6b553c000)
       libm.so.6 => /lib/x86_64-linux-gnu/libm.so.6 (0x00007ff6b53b9000)
       libgcc_s.so.1 => /lib/x86_64-linux-gnu/libgcc_s.so.1 (0x00007ff6b539f000)

Remote Developing
=================

This procedure shows how to configure a remote development environment for Xavier board.
This setup only works on host computers with `Ubuntu 16.04 or 18.04 <https://forums.developer.nvidia.com/t/getting-target-is-not-supported-in-nvidia-nsight-systems-for-tx2/125704>`__.

First, it's required to check the JetPack version installed in the Xavier board.
The host computer must use the same version. So, in the Xavier board, execute:

.. code-block:: console

   $ cat /etc/nv_tegra_release
   # R32 (release), REVISION: 4.4, GCID: 23942405, BOARD: t186ref, EABI: aarch64, DATE: Fri Oct 16 19:37:08 UTC 2020

This shows that version 4.4 is used. An alternative way is to check the environment variable *JETSON_L4T_REVISION*.

Next, it's time to find out the type of Jetson module available.

.. code-block:: console

   $ echo $JETSON_MODULE
   P2888-0001

The code P2888-0001 corresponds to the Jetson AGX Xavier module while P3668-0000 corresponds to the Jetson Xavier NX, which is the newer model.

Then, go to the `JetPack version 4.4 <https://developer.nvidia.com/jetpack-sdk-44-archive>`__,
download **SDK Manager** to the host computer, install it, and run:

.. code-block:: console

   $ sdkmanager&

In the *Target Hardware* session of SDK Manager, select Jetson AGX Xavier board.
In the *Target OS* session, select version 4.4. Press next, select the instalation location,
and press next again. Note that this installation requires about 45GB of disk space in the host computer.
It will take a while to download all the environment.
For further details about using SDK Manager, please check its `manual <https://docs.nvidia.com/sdk-manager/install-with-sdkm-jetson/index.html>`__.

After installation is finished, there will be several tools available to use with the Xavier board.
Some of these tools include:

 - `NSight Systems <https://docs.nvidia.com/nsight-systems/UserGuide/index.html#profiling-linux>`__ version 2020.2, which is included in JetPack v4.4, has additional support to 
   OpenMP 5. This feature will be important for the AMPERE project. This `video <https://www.youtube.com/watch?v=R_GzhZe8IcM>`__ shows how to remote profile with nsight-systems;
 - `analyze applications parallelized using OpenMP <https://docs.nvidia.com/nsight-systems/UserGuide/index.html#openmp-trace>`__;
 - `nsys CLI commands <https://docs.nvidia.com/nsight-systems/UserGuide/index.html#cli-profiling>`__.
 - `NSight Eclipse Edition <https://developer.nvidia.com/nsight-eclipse-edition>`__, also included in the JetPack, has resources for cross-compiling and remote debugging;
 - `NSight Compute <https://developer.nvidia.com/nsight-compute>`__.
