===================
Welcome to PyAPP4MC
===================

Eclipse `APP4MC <https://projects.eclipse.org/proposals/app4mc>`_ is a platform for engineering
embedded multi- and many-core software systems. The platform enables the creation and management 
of complex tool chains including simulation and validation. As an open platform, proven in the 
automotive sector by Bosch and their partners, it supports interoperability and extensibility 
and unifies data exchange in cross-organizational projects.

The following papers [1]_, [2]_, [3]_ can be used to learn more about the Amalthea model and how it is used.

PyAPP4MC has two main modules (:mod:`app4mc.parser` and :mod:`app4mc.code_gen`) to parse and generate C code 
for Amalthea models. Some example models can be found in the *models* directory.

Eclipse **APP4MC v1.0.0** is required to use this tool.

The following topics present the main structure of the document. 

.. toctree::
    :maxdepth: 2

    codegen/index
    openmp/index
    parser/index
    todo

The next topic present the automatically generated code for :mod:`app4mc`.

.. autosummary::
   :toctree: _autosummary
   :recursive:

   app4mc
   

*************
Documentation
*************

There is a Sphinx-based documentation describing both the parser and code generation.
Just execute these steps to open it.

.. code-block:: console

  $ cd docs
  $ make html
  $ firefox ./build/html/index.html

or these command to generate an PDF output of Latex format.

.. code-block:: console

  $ cd docs
  $ make latex
  $ cd ./build/latex/
  $ make all-pdf
  $ evince pyapp4mc.pdf &


*******
Funding
*******

This tool has been developed in the context of the `AMPERE project <https://ampere-euproject.eu/>`_.
This project has received funding from the European Union’s Horizon 2020 
research and innovation programme under grant agreement No 871669.

.. include:: todo

**********
References
**********

.. [1] Höttger, Robert, et al. "APP4MC: Application platform project for multi-and many-core systems." 
   it-Information Technology 59.5 (2017): 243-251.
.. [2] Krawczyk, Lukas, et al. "Model-based Timing Analysis and Deployment Optimization for Heterogeneous 
   Multi-Core Systems using Eclipse APP4MC." ACM/IEEE International Conference on Model Driven 
   Engineering Languages and Systems Companion (MODELS-C). 2019.
.. [3] Wurst, Falk, et al. "System Performance Modelling of Heterogeneous HW Platforms: An Automated Driving 
   Case Study." Euromicro Conference on Digital System Design (DSD). IEEE, 2019.

