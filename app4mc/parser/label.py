import ast
from app4mc.common.amalthea_item import Amalthea_item

class Label(Amalthea_item):
    """ A class to represent Label attributes in the Amalthea model.

    Args:

        tree (lxml.etree): xml tree with all information of the item.
        verbose (int): verbosity level. 0 is none and 5 is the max.
            This class prints message when verbose > = 1.

    """

    def __init__(self, tree, verbose):
        self.displayName  = str("")
        self.namespace = str("")
        self.constant = False
        self.bVolatile = False

        self.dataStability = "_undefined_"
        """ dataStability(str): supports one of these values:

            * _undefined_
            * inherited
            * noProtection
            * automaticProtection
            * customProtection
            * handledByModelElements
        """

        self.stabilityLevel = "_undefined_"
        """
        `stabilityLevel` (str): supports one of these values:

            * _undefined_
            * period
            * process
            * scheduleSection
            * runnable
        """

        self.section  = str("")
        self.size = ()
        self.verbose = verbose

        self.parse_xml(tree)

    def get(self):
        """ A getter for this class.

        Returns:
            pointer: 'pointer' to the item.
        """   
        return self

    def __atrib2str(self):
        """ Convert all attributes of this class into a str.

        Returns:
            str: string with all attributes of the class.
        """
        aux = "{'name': '%s', 'displayName': '%s', 'namespace': '%s', 'constant': '%s', 'dataStability': '%s', 'stabilityLevel': '%s', 'section': '%s', 'bVolatile': '%s', 'size': %s}" %  (self.name, self.displayName, self.namespace, str(self.constant), self.dataStability, self.stabilityLevel, self.section, str(self.bVolatile), str(self.size))
        # literal_eval used only to check whether the string really represents a dictionary
        return str(ast.literal_eval(aux))

    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        return self.__atrib2str()

    def __repr__(self):
        return self.__atrib2str()

    def dump(self):
        """ Generates a string with the atributes of the class. 

        The amount of information in the string is controled by the 'verbose' attribute.

        Returns:
            str: string with the attributes of the class.
        """        
        if self.verbose == 1:
            return self.name
        elif self.verbose >= 2:
            return self.__atrib2str()
        return ""        

    def parse_xml(self, tree):
        """ Parses the attributes of this class from the app4mc XML file. 
        
        Pay attention to the parsing limitations marked with TODO.

        Args:
            tree (lxml.etree): xml tree with all information of the item.

        Returns:
            bool: return True if parsing is ok.

        .. code-block:: XML

            <xsd:complexType name="Label">
                <xsd:annotation>
                <xsd:documentation>Data representation, which can be accessed by run entities.</xsd:documentation>
                </xsd:annotation>
                <xsd:complexContent>
                <xsd:extension base="am:AbstractMemoryElement">
                    <xsd:choice maxOccurs="unbounded" minOccurs="0">
                        <xsd:element name="namespace" type="am:Namespace"/>
                        <xsd:element name="dataType" type="am:DataType"/>
                        <xsd:element name="section" type="am:Section"/>
                    </xsd:choice>
                    <xsd:attribute name="displayName" type="xsd:string"/>
                    <xsd:attribute name="namespace" type="xsd:string"/>
                    <xsd:attribute name="constant" type="xsd:boolean">
                    <xsd:annotation>
                        <xsd:documentation>Defines the label as a constant, not modifiable entity</xsd:documentation>
                    </xsd:annotation>
                    </xsd:attribute>
                    <xsd:attribute name="bVolatile" type="xsd:boolean">
                    <xsd:annotation>
                        <xsd:documentation>Defines if the label value is volatile or persistent to survive shutdown and start of system</xsd:documentation>
                    </xsd:annotation>
                    </xsd:attribute>
                    <xsd:attribute name="dataStability" type="am:LabelDataStability">
                    <xsd:annotation>
                        <xsd:documentation>Defines the data stability needs of the label</xsd:documentation>
                    </xsd:annotation>
                    </xsd:attribute>
                    <xsd:attribute name="stabilityLevel" type="am:DataStabilityLevel"/>
                    <xsd:attribute name="section" type="xsd:string"/>
                </xsd:extension>
                </xsd:complexContent>
            </xsd:complexType>        

        """

        if not tree.tag == "labels":
            print ("ERROR: not a label")
            return False

        # parsing the name. name must be always parsed in Amalthea_item
        super().parse_xml(tree)

        # Issue warning if there is some unsupported attribute
        supported_attribs = ['name', 'displayName', 'namespace', 'constant', 'bVolatile','section', 'dataStability', 'stabilityLevel']
        for k in tree.attrib.keys():
            if k not in supported_attribs:
                # get the last 2 chars, in case of the 'id' attribute
                if k[-2:] != "id":
                    print ("WARNING: attribute",k, "is not supported")
                    #return False

        # they are all optional
        if tree.get('displayName') != None:
            self.displayName  = tree.get('displayName')
        if tree.get('namespace') != None:
            self.namespace  = tree.get('namespace')
        if tree.get('constant') != None:
            if tree.get('constant').lower() == "true":
                self.constant  = True
        if tree.get('bVolatile') != None:
            if tree.get('bVolatile').lower() == "true":
                self.bVolatile  = True
        if tree.get('dataStability') != None:
            # OBS: It is always assumed that the input xml file is already checked in app4mc.
            # this way, this type of consistency check is not required here
            expected = ["_undefined_", "inherited", "noProtection", "automaticProtection", "customProtection", "handledByModelElements"]
            val = tree.get('dataStability')
            if val not in expected:
                print("ERROR: value", val, "for dataStability field is not expected")
            else:
                self.dataStability  = val
        if tree.get('stabilityLevel') != None:
            # OBS: It is always assumed that the input xml file is already checked in app4mc.
            # this way, this type of consistency check is not required here
            expected = ["_undefined_", "period", "process", "scheduleSection", "runnable"]
            val = tree.get('stabilityLevel')
            if val not in expected:
                print("ERROR: value", val, "for stabilityLevel field is not expected")
            else:
                self.stabilityLevel  = val
        if tree.get('section') != None:
            self.section  = tree.get('section')

        # Issue warning if there is some unsupported tag
        supported_tags = ['size']
        for c in tree.getchildren():
            if c.tag not in supported_tags:
                print ("WARNING: tag",c.tag, "is not supported")
                #return False
            else:
                self.size = (int(c.get('value')), c.get('unit'))


        return True
