""" This module describes the supported types of stimuli.

Currently supporting the following types of stimuli:
    * PeriodicStimulus;
    * InterProcessStimulus.
    * RelativePeriodicStimulus: for sporadic tasks.

TODO:
    * Implement the other types of stimuli.
"""

import ast
from app4mc.common.amalthea_obj import Amalthea_obj
from app4mc.common.amalthea_item import Amalthea_item
from app4mc.parser.time import builder as time_builder

def builder(tree, verbose):
    """ Builder for all supported types of *Stimulus*.

    Args:
        tree (lxml.etree): Xml tree with all information of the item.
        verbose (int): verbosity level. 0 is none and 5 is the max.

    Returns:
        :class:`~app4mc.parser.stimulus.Stimulus`.: return an object derived from *Stimulus*.

    Note:
    """
    for k, v in tree.attrib.iteritems():
        # TODO this code would be simpler if i could use wildcard to search for atributes
        # get the last 4 chars, in case of the 'type' attribute
        if k[-4:] == "type":
            # get only the part of the value after the ':'.
            # example. 'am:LabelAccess' => 'LabelAccess'
            stim_type = v.split(":",1)[1]
            if stim_type == 'PeriodicStimulus':
                return PeriodicStimulus_stimulus(tree, verbose)
            elif stim_type == 'InterProcessStimulus':
                return InterProcessStimulus_stimulus(tree, verbose)
            elif stim_type == 'RelativePeriodicStimulus':
                return RelativePeriodicStimulus_stimulus(tree, verbose)
            else:
                print ("WARNING: type of stimuli ", stim_type, "is not supported")

    return None

class Stimulus(Amalthea_obj,Amalthea_item):
    """" Abstract class for all supported types of *Stimulus* "

    Args:
        Amalthea_obj (:class:`~app4mc.common.Amalthea_obj`): get the *type* field.
        Amalthea_item (:class:`~app4mc.common.Amalthea_Amalthea_item`): get the *name* field.

    """
    def __init__(self, tree, verbose):
        self.verbose = verbose

    def dump(self): pass

    def parse_xml(self, tree):
        """ Parses the *name* and *type* attributes.

        Args:
            tree (lxml.etree): xml tree with all information of the item.

        Returns:
            bool: return True if parsing is ok.

        This is the example of the democar model:

        .. code-block:: XML
        
            <stimuli xsi:type="am:PeriodicStimulus" xmi:id="Timer_10MS?type=PeriodicStimulus" name="Timer_10MS">
        """
        # assigns value for the attributes specific to this type of tick        
        if tree.tag != 'stimuli':
            print ("ERROR: expecting stimuli tag")
            return False

        # assign the type attribute
        Amalthea_obj.parse_xml(self,tree)
        # assign the name attribute
        Amalthea_item.parse_xml(self,tree)
        
        return True


class PeriodicStimulus_stimulus(Stimulus):
    """ Represents a Stimulus of type PeriodicStimulus.

    Args:
        tree (lxml.etree): xml tree with all information of the item.
        verbose (int): verbosity level. 0 is none and 5 is the max.
            This class prints message when verbose > = 3.

    TODO:
        * how to use the field *minDistance* ?
        * support jitter for PeriodicStimulus.

    """

    def __init__(self, tree, verbose):
        self.verbose = verbose
        self.recurrence = ()
        self.offset = ()
        self.minDistance = ()
        self.jitter = None
        
        self.parse_xml(tree)

    def get(self):
        """ A getter for this class.

        Returns:
            pointer: 'pointer' to the item.
        """   
        return self

    def __atrib2str(self):
        """ Convert all attributes of this class into a str.

        Returns:
            str: string with all attributes of the class.
        """
        aux = "{'name': '%s', 'type': '%s', 'recurrence': %s, 'offset': %s, 'minDistance': %s}" % (self.name, self.obj_type, str(self.recurrence), str(self.offset), str(self.minDistance))
        # TODO: jitter is not printed
        return str(ast.literal_eval(aux))

    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        return self.__atrib2str()

    def __repr__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """        
        return self.__atrib2str()

    def dump(self):
        """ Generates a string with the atributes of the class. 

        The amount of information in the string is controled by the 'verbose' attribute.

        Returns:
            str: string with the attributes of the class.
        """        
        if self.verbose == 1:
            return self.name
        elif self.verbose >= 2:
            return self.__atrib2str() 
        return "" 

    def parse_xml(self, tree):
        """ Parse stimuli of type PeriodicStimulus.

        Args:
            tree (lxml.etree): xml tree with all information of the item.

        Returns:
            bool: return True if parsing is ok.

        This is the example of the democar model:

        .. code-block:: XML

            <stimuli xsi:type="am:PeriodicStimulus" xmi:id="Timer_10MS?type=PeriodicStimulus" name="Timer_10MS">
                <recurrence value="10" unit="ms"/>
                <offset value="0" unit="ms"/>

        """
        # Issue warning if there is some unsupported tag
        supported_tags = [ 'recurrence', 'offset', 'minDistance']
        for c in tree.getchildren():
            if c.tag not in supported_tags:
                print ("WARNING: tag",c.tag, "is not supported in PeriodicStimulus")
                return False

        # assign both the type and name attributes
        Stimulus.parse_xml(self,tree)

        # TODO is recurrence mandatory ? I am  assuming it is
        tag = tree.find('recurrence')
        if (tag != None):
            self.recurrence = (int(tag.get('value')), tag.get('unit'))
        else:
            print ("WARNING: recurrence field is mandatory for PeriodicStimulus")
                            
        tag = tree.find('minDistance')
        if (tag != None):
            self.minDistance = (int(tag.get('value')), tag.get('unit'))

        tag = tree.find('offset')
        if (tag != None):
            self.offset = (int(tag.get('value')), tag.get('unit'))

        return True

#############################################

class InterProcessStimulus_stimulus(Stimulus):
    """ Represents a Stimulus of type InterProcessStimulus.

    Args:
        tree (lxml.etree): xml tree with all information of the item.
        verbose (int): verbosity level. 0 is none and 5 is the max.
            This class prints message when verbose > = 3.

    TODO:
        * how to use InterProcessStimulus if it has no additional parameter ?

    """

    def __init__(self, tree, verbose):
        self.verbose = verbose

        self.parse_xml(tree)

    def get(self):
        """ A getter for this class.

        Returns:
            pointer: 'pointer' to the item.
        """   
        return self

    def __atrib2str(self):
        """ Convert all attributes of this class into a str.

        Returns:
            str: string with all attributes of the class.
        """
        aux = "{'name': '%s', 'type': '%s'}" % (self.name, self.obj_type)
        # TODO: jitter is not printed
        return str(ast.literal_eval(aux))

    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        return self.__atrib2str()

    def __repr__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """        
        return self.__atrib2str()

    def dump(self):
        """ Generates a string with the atributes of the class. 

        The amount of information in the string is controled by the 'verbose' attribute.

        Returns:
            str: string with the attributes of the class.
        """        
        if self.verbose == 1:
            return self.name
        elif self.verbose >= 2:
            return self.__atrib2str() 
        return "" 

    def parse_xml(self, tree):
        """ Parse stimuli of type PeriodicStimulus.

        Args:
            tree (lxml.etree): xml tree with all information of the item.

        Returns:
            bool: return True if parsing is ok.

        This is the example of the democar model:

        .. code-block:: XML

            <stimuli xsi:type="am:PeriodicStimulus" xmi:id="Timer_10MS?type=PeriodicStimulus" name="Timer_10MS">
                <recurrence value="10" unit="ms"/>
                <offset value="0" unit="ms"/>

        """
        # this stimulus has no additional attributes. weird !
        Stimulus.parse_xml(self,tree)


        return True


#############################################

class RelativePeriodicStimulus_stimulus(Stimulus):
    """ Represents a Stimulus of type RelativePeriodicStimulus.

    Args:
        tree (lxml.etree): xml tree with all information of the item.
        verbose (int): verbosity level. 0 is none and 5 is the max.
            This class prints message when verbose > = 3.
    """

    def __init__(self, tree, verbose):
        self.verbose = verbose
        self.nextOccurrence = ()
        self.offset = ()
        
        self.parse_xml(tree)

    def get(self):
        """ A getter for this class.

        Returns:
            pointer: 'pointer' to the item.
        """   
        return self

    def __atrib2str(self):
        """ Convert all attributes of this class into a str.

        Returns:
            str: string with all attributes of the class.
        """
        aux = "{'name': '%s', 'type': '%s', 'offset': %s, 'nextOccurrence': %s}" % (self.name, self.obj_type, str(self.offset), str(self.nextOccurrence))
        return str(ast.literal_eval(aux))

    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        return self.__atrib2str()

    def __repr__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """        
        return self.__atrib2str()

    def dump(self):
        """ Generates a string with the attributes of the class. 

        The amount of information in the string is controlled by the 'verbose' attribute.

        Returns:
            str: string with the attributes of the class.
        """        
        if self.verbose == 1:
            return self.name
        elif self.verbose >= 2:
            return self.__atrib2str() 
        return "" 

    def parse_xml(self, tree):
        """ Parse stimuli of type PeriodicStimulus.

        Args:
            tree (lxml.etree): xml tree with all information of the item.

        Returns:
            bool: return True if parsing is ok.

        This is the example of the waters17 model:

        .. code-block:: XML

            <stimuli xsi:type="am:RelativePeriodicStimulus" xmi:id="sporadic_6000us_6100us_ISR_9?type=RelativePeriodicStimulus" name="sporadic_6000us_6100us_ISR_9">
            <nextOccurrence xsi:type="am:TimeUniformDistribution">
                <lowerBound value="6000" unit="us"/>
                <upperBound value="6100" unit="us"/>
            </nextOccurrence>
            </stimuli>

        """
        # Issue warning if there is some unsupported tag
        supported_tags = [ 'nextOccurrence', 'offset']
        for c in tree.getchildren():
            if c.tag not in supported_tags:
                print ("WARNING: tag",c.tag, "is not supported in RelativePeriodicStimulus")
                return False

        # assign both the type and name attributes
        Stimulus.parse_xml(self,tree)

        tag = tree.find('offset')
        if (tag != None):
            self.offset = (int(tag.get('value')), tag.get('unit'))

        tag = tree.find('nextOccurrence')
        if (tag != None):
            for k, v in tag.attrib.iteritems():                    
                if k[-4:] == 'type':
                    value = v.split(":",1)[1]
                    time_obj = time_builder(value, tag, self.verbose)
                    if (time_obj != None):
                        self.nextOccurrence = time_obj
                    else:
                        return False

        return True
