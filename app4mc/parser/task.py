import ast  # for log messages
from app4mc.parser import task_item
from app4mc.common.amalthea_item import Amalthea_item

class Task(Amalthea_item):
    """ Represents a Task in the Amalthea model.

    Args:
        tree (lxml.etree): xml tree with all information of a item.
        sw_model_ref (sw_model): a reference to the sw_model class.
        verbose (int): verbosity level. 0 is none and 5 is the max.
            This class prints message when verbose > = 1.

    TODO:
        - Set the period from the stimuli field in code_gen.
        - Assuming all item groups are of the type ordered *CallSequence*.

    Example of ordered *CallSequence*:

    .. code-block:: XML        

        <items xsi:type="am:Group" name="CallSequence" ordered="true">


    """

    def __init__(self, tree, sw_model_ref, stimuli_model_ref, verbose):
        # reference to stimuliModel
        self.stimuli = None
        self.preemption = str("")
        self.multipleTaskActivationLimit = int(0)
        self.verbose = verbose

        # container to hold all types of items of a task
        self.item_list = []
        self.sw_model_ref = sw_model_ref
        self.stimuli_model_ref = stimuli_model_ref

        self.parse_xml(tree)

    def get(self):
        """ A getter for this class.

        Returns:
            pointer: 'pointer' to the item.
        """   
        return self

    def __atrib2str(self):
        """ Convert all attributes of this class into a str.

        Returns:
            str: string with all attributes of the class.
        """
        # TODO: replace self.stimuli by self.stimuli.name once self.stimuli has a ref to StimuliModel
        aux = "{'name': '%s', 'stimuli': '%s', 'preemption': '%s', 'multipleTaskActivationLimit': '%s'}" % (self.name, self.stimuli.name, self.preemption, str(self.multipleTaskActivationLimit))
        # literal_eval used only to check whether the string really represents a dictionary
        return str(ast.literal_eval(aux))

    def __item2str(self):
        """ Convert all ticks into a str.

        Returns:
            str: string with ticks.
        """
        data_str = ""
        for i in self.item_list:
            data_str += i.dump() 
        return data_str

    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        data_str = "{\n"
        data_str += self.__atrib2str()
        data_str += "[\n"
        data_str += self.__item2str()
        data_str += "}\n"
        return data_str

    def __repr__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        return self.__atrib2str()

    def dump(self):
        """ Generates a string with the attributes of the class. 

        The amount of information in the string is controlled by the 'verbose' attribute.

        Returns:
            str: string with the attributes of the class.
        """        
        if self.verbose == 1:
            return self.name
        elif self.verbose == 2:
            return self.__atrib2str()
        elif self.verbose >= 3:
            aux = self.__atrib2str()
            aux += "\nTask items:\n"
            aux += self.__item2str()
            return aux
        return ""

    def parse_xml(self, tree):
        """ Parses the attributes of this class from the app4mc XML file. 
        
        Pay attention to the parsing limitations marked with TODO.

        Args:
            tree (:py:class:`lxml.etree`): xml tree with all information of a item.

        Schema for tasks:

        .. code-block:: XML
        
            <xsd:complexType name="Task">
                <xsd:complexContent>
                <xsd:extension base="am:Process">
                    <xsd:attribute name="preemption" type="am:Preemption"/>
                    <xsd:attribute name="multipleTaskActivationLimit" type="xsd:int"/>
                </xsd:extension>
                </xsd:complexContent>
            </xsd:complexType>
            <xsd:element name="Task" type="am:Task">    
            <xsd:complexType abstract="true" name="Process">
                <xsd:complexContent>
                <xsd:extension base="am:AbstractProcess">
                    <xsd:choice maxOccurs="unbounded" minOccurs="0">
                    <xsd:element name="activityGraph" type="am:ActivityGraph"/>
                    <xsd:element name="stimuli" type="am:Stimulus"/>
                    </xsd:choice>
                    <xsd:attribute name="stimuli" type="xsd:string"/>
                </xsd:extension>
                </xsd:complexContent>
            </xsd:complexType>  
            Example from WATERS 2017:
            <tasks xmi:id="Task_5ms?type=Task" name="Task_5ms" stimuli="periodic_5ms?type=PeriodicStimulus" preemption="preemptive" multipleTaskActivationLimit="1">
                <activityGraph>
                    ...

        """
        if not tree.tag == "tasks":
            print ("ERROR: not a task")
            return False

        # parsing the name. name must be always parsed in Amalthea_item
        super().parse_xml(tree)

        # Issue warning if there is some unsupported attribute
        supported_attribs = ['name', 'stimuli', 'preemption', 'multipleTaskActivationLimit']
        for k in tree.attrib.keys():
            if k not in supported_attribs:
                # get the last 2 chars, in case of the 'id' attribute
                if k[-2:] != "id":
                    print ("WARNING: attribute",k, "is not supported")
                    #return False

        # get the stimuli name and type
        for k, v in tree.attrib.iteritems():                    
            if k == 'stimuli':
                # example. 'Timer_10MS?type=PeriodicStimulus' => 'Timer_10MS'
                value = v.split("?",1)[0]
                # example. 'Timer_10MS?type=PeriodicStimulus' => 'PeriodicStimulus'
                #stim_type = v.split("=",1)[1]
                # search in the list of stimuli to find the stimuli for this task
                stim = self.stimuli_model_ref.search(value)
                if stim != None:
                    self.stimuli = stim
                else:
                    print ("WARNING: stimulus",value, "was not found in the stimuliModel")

        # optional attributes
        if tree.get('preemption') != None:
            self.preemption  = tree.get('preemption')
        if tree.get('multipleTaskActivationLimit') != None:
            self.multipleTaskActivationLimit  = int (tree.get('multipleTaskActivationLimit'))

        # the only expected child for runnables is a activityGraph
        supported_tags = ['activityGraph']
        for c in tree.getchildren():
            if c.tag not in supported_tags:
                print ("WARNING: tag",c.tag, "is not supported")
                #return False

        # Logic to ignore items for groups
        #Assuming all item groups are of the type ordered CallSequence, like this exmple:
        #    <items xsi:type="am:Group" name="CallSequence" ordered="true">
        find_items = tree.find('activityGraph').find('items').find('items')
        # test if there are two levels of items, like in this example:
        #    <activityGraph>
        #        <items xsi:type="am:Group" name="CS_Task_5ms" ordered="true">
        #            <items xsi:type="am:RunnableCall" runnable="Runnable_5ms_0?type=Runnable"/>
        if find_items == None:
            # test if there is only one level of items, i.e., there is no group of items
            #    <activityGraph>
            #        <items xsi:type="am:RunnableCall" runnable="Runnable_5ms_0?type=Runnable"/>
            #        <items xsi:type="am:RunnableCall" runnable="Runnable_5ms_1?type=Runnable"/>
            find_items = find_items = tree.find('activityGraph').find('items')
            if find_items == None:
                print ("ERROR: expecting items in an activityGraph")
                return False

        # the only expected tag in an activityGraph is a set of items
        supported_tags = ['items']
        for a in find_items.itersiblings():
            if a.tag not in supported_tags:
                print ("WARNING: tag",a.tag, "is not supported")
                #return False

        # check if it is one of the supported types of items for each item
        #supported_item_types = ['RunnableCall','InterProcessTrigger', 'WaitEvent', 'ClearEvent', ]
        item_parent = find_items.getparent()
        for a in item_parent.getchildren():
            if a.tag in supported_tags:
                for k, v in a.attrib.iteritems():                    
                    if k[-4:] == 'type':
                        # example. 'am:RunnableCall' => 'RunnableCall'
                        value = v.split(":",1)[1]
                        # TODO is it possible to create a builder pattern in here ?
                        # the issue it that each type of item has different parameters
                        new_item = None
                        if value == 'RunnableCall':
                            new_item = task_item.Task_RunnableCall_item(a, self.sw_model_ref.runnables, self.verbose)
                        elif value == 'InterProcessTrigger':
                            new_item = task_item.Task_InterProcessTrigger_item(a, self.stimuli_model_ref.stimuli, self.verbose)
                        elif value == 'WaitEvent':
                            print ("WARNING: task item", value)
                        elif value == 'ClearEvent':
                            print ("WARNING: task item", value)
                        else:
                            print ("WARNING: task items type ",v, "is not supported")
                            #return False
                        if new_item != None:
                            self.item_list.append(new_item)
        return True
