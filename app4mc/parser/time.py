import ast
from app4mc.common.amalthea_obj import Amalthea_obj

def builder(time_type, tree, verbose):
    """ Builder for Time derived classes.

    Args:
        time_type (str): string with the name of the Time object to be built.
        tree (lxml.etree): Xml tree with all information of the item.
        verbose (int): verbosity level. 0 is none and 5 is the max.

    Returns:
        Return an object derived from *Time*.

    Note:
    """
    # time types used in stimuli
    if time_type == 'TimeUniformDistribution':
        return TimeUniformDistribution_time(tree, verbose)
    else:
        print ("WARNING: type of time ", time_type, "is not supported")

    return None

#################################

class TimeUniformDistribution_time(Amalthea_obj):
    """[summary]

    Args:
        tree ([type]): [description]
        verbose ([type]): [description]
    """

    def __init__(self, tree, verbose):
        self.verbose = verbose
        self.lowerBound = ()
        self.upperBound = ()
        
        self.parse_xml(tree)

    def __atrib2str(self):
        """ Convert all attributes of this class into a str.

        Returns:
            str: string with all attributes of the class.
        """
        aux = "{'type': '%s', 'lowerBound': %s, 'upperBound': %s}" % (self.obj_type, str(self.lowerBound), str(self.upperBound))
        return str(ast.literal_eval(aux))

    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        return self.__atrib2str()

    def dump(self):
        """ Generates a string with the attributes of the class. 

        The amount of information in the string is controlled by the 'verbose' attribute.

        Returns:
            str: string with the attributes of the class.
        """        
        if self.verbose == 1:
            return self.obj_type
        elif self.verbose >= 2:
            return self.__atrib2str() 
        return "" 

    def parser_xml(self,tree,verbose):
        """ Parses the attributes of this class from the app4mc XML file. 
        
        Pay attention to the parsing limitations marked with TODO.

        Args:
            tree ([type]): [description]
            verbose ([type]): [description]

        Example of XML used in Stimuli_Model.

        .. code-block:: XML

            <nextOccurrence xsi:type="am:TimeUniformDistribution">
                <lowerBound value="6000" unit="us"/>
                <upperBound value="6100" unit="us"/>
            </nextOccurrence>            
        """
        # Issue warning if there is some unsupported tag
        supported_tags = [ 'lowerBound', 'upperBound']
        for c in tree.getchildren():
            if c.tag not in supported_tags:
                print ("WARNING: tag",c.tag, "is not supported in TimeUniformDistribution")
                return False

        # assign type attribute
        Amalthea_obj.parse_xml(self,tree)

        tag = tree.find('lowerBound')
        if (tag != None):
            self.offset = (int(tag.get('value')), tag.get('unit'))
        else:
            print ("WARNING: tag lowerBound is expected in TimeUniformDistribution")
            return False

        tag = tree.find('upperBound')
        if (tag != None):
            self.offset = (int(tag.get('value')), tag.get('unit'))
        else:
            print ("WARNING: tag upperBound is expected in TimeUniformDistribution")
            return False

        pass
