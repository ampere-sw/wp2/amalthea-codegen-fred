# pretty print https://github.com/wolever/pprintpp
from app4mc.parser import label, runnable, task

class swModel:
    """ Amalthea model for software-related information.
    
    This Amalthea model holds all information related to the system software, 
    i.e. :class:`~app4mc.parser.task.Task`, :class:`~app4mc.parser.runnable.Runnable`, :class:`~app4mc.parser.label.Label`, etc. 

    Args:
        tree (lxml.etree): xml tree with all information of the item.
        stimuli_model_ref (:class:`~app4mc.parser.stimuli_model.Stimuli_Model`): a reference to the 
            stimuli model in order to link the tasks to theirs stimuli.
        verbose (int): verbosity level. 0 is none and 5 is the max.
            
            This class prints message when verbose > = 1.

            * *Level 0*: quiet mode (default);
            * *Level 1*: show the name of the 1st level tags (i.e. swModel, hwModel, etc);
              For swModel, it lists the names of Labels, Runnables, and Tasks;
            * *Level 2*: show the attributes of the 2nd level tags.
              For swModel, it lists all the attributes of Labels, Runnables, and Tasks;
            * *Level 3*: show the attributes of the 3rd level tags.
              For Runnables, it lists all the attributes of ticks and Label_access items;

    TODO:
        * Use some nice pretty printer to better present the logs

    """

    def __init__(self, tree, stimuli_model_ref, verbose):
        self.verbose = verbose
        self.stimuli_model_ref = stimuli_model_ref
        self.labels = []
        self.runnables = []
        self.tasks = []

        self.parse_xml(tree)    

    def get(self):
        """ A getter for this class.

        Returns:
            pointer: 'pointer' to the item.
        """   
        return self

    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        str_data = "[\n"
        for l in self.labels:
            str_data += str(l) + ',\n'
        str_data += "]\n"

        str_data += "[\n"
        for r in self.runnables:
            str_data += str(r) + ',\n'
        str_data += "]\n"

        return str_data


    def parse_xml(self, tree):
        """ Parses the attributes of this class from the app4mc XML file. 
        
        Pay attention to the parsing limitations marked with TODO.

        Args:
            tree (lxml.etree): xml tree with all information of the item.

        """
        #############################################
        # create all the labels from the XML file
        label_tags = tree.find('labels')
        if label_tags == None:
            print ("ERROR: swModel has no labels")
            return False
            
        print ("Creating labels ...")
        for l in label_tags:
            self.labels.append(label.Label(l, self.verbose))

        if self.verbose>=1:
            for l in self.labels:
                print (' -',l.dump())
            print('Total of', len(self.labels), 'label(s).\n')

        #############################################
        # create all the runnables from the XML file
        runnable_tags = tree.find('runnables')
        if runnable_tags == None:
            print ("ERROR: swModel has no runnables")
            return False
            
        print ("Creating runnables ...")
        for l in runnable_tags:
            # pass ref to sw_model because Label_acces must reference to the Labels
            self.runnables.append(runnable.Runnable(l, self, self.verbose))

        if self.verbose>=1:
            for l in self.runnables:
                print (' -',l.dump())
            print('Total of', len(self.runnables), 'runnable(s).\n')


        #############################################
        # create all the tasks from the XML file
        task_tags = tree.find('tasks')
        if task_tags == None:
            print ("ERROR: swModel has no tasks")
            return False
            
        print ("Creating tasks ...")
        for l in task_tags:
            self.tasks.append(task.Task(l, self, self.stimuli_model_ref, self.verbose))

        if self.verbose>=1:
            for l in self.tasks:
                print (' -',l.dump())
            print('Total of', len(self.tasks), 'task(s).\n')

