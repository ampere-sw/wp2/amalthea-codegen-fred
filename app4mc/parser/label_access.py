import ast
from app4mc.common.amalthea_obj import Amalthea_obj

# https://python-3-patterns-idioms-test.readthedocs.io/en/latest/Factory.html#simple-factory-method

# TODO: should not Label_access be based on Amalthea_obj, just like Tick ?
class Label_access(Amalthea_obj):
    """ Class with the information related to the access modes of a Label.

    Todo:
        * TransmissionPolicy and DataDependency were not implemented.

    """

    def __init__(self,tree, label_list, verbose):
        # data is the reference to the label
        self.data="_undefined_"
        self.access="_undefined_" 
        self.dataStability="_undefined_" 
        """ dataStability(str): supports one of these values:

            * _undefined_
            * inherited
            * noProtection
            * automaticProtection
            * customProtection
            * handledByModelElements
        """

        self.implementation="_undefined_"
        #optional values for label statistics
        # TODO how to use this for code generation ?
        self.value_stat = {}
        self.cacheMisses_stat = {}

        self.label_list_ref = label_list
        self.verbose = verbose

        self.parse_xml(tree)


    def get(self):
        """ A getter for this class.

        Returns:
            pointer: 'pointer' to the item.
        """   
        return self

    def __atrib2str(self):
        """ Convert all attributes of this class into a str.

        Returns:
            str: string with all attributes of the class.
        """
        aux = "{'type': '%s', 'data': '%s', 'access': '%s', 'dataStability': '%s', 'implementation': '%s'}" % (self.obj_type, self.data.name, self.access, self.dataStability, self.implementation)
        # literal_eval used only to check whether the string really represents a dictionary
        return str(ast.literal_eval(aux))

    def __stat2str(self):
        # TODO to be done
        self.value_stat = {}
        self.cacheMisses_stat = {}

    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        return self.__atrib2str()

    def __repr__(self):
        return self.__atrib2str()

    def dump(self):
        """ Generates a string with the attributes of the class. 

        The amount of information in the string is controlled by the 'verbose' attribute.

        Returns:
            str: string with the attributes of the class.
        """        
        if self.verbose == 3:
            return self.data.name + '\n'
        elif self.verbose >= 4:
            return self.__atrib2str() + '\n'
        return "" 

    def parse_xml(self, tree):
        """ Parses the attributes of this class from the app4mc XML file. 
        
        Pay attention to the parsing limitations marked with TODO.

        Args:
            tree (lxml.etree): xml tree with all information of the item.

        Returns:
            bool: return True if parsing is ok.

        LabelAccess Schema:

        .. code-block:: XML

            <xsd:complexType name="LabelAccess">
                <xsd:annotation>
                <xsd:documentation>Representation of a label access of a run entity.</xsd:documentation>
                </xsd:annotation>
                <xsd:complexContent>
                <xsd:extension base="am:ComputationItem">
                    <xsd:choice maxOccurs="unbounded" minOccurs="0">
                    <xsd:element name="tags" type="am:Tag"/>
                    <xsd:element name="dependsOn" type="am:DataDependency"/>
                    <xsd:element name="data" type="am:Label"/>
                    <xsd:element name="statistic" type="am:LabelAccessStatistic">
                        <xsd:annotation>
                        <xsd:documentation>Optional parameter for statistic values</xsd:documentation>
                        </xsd:annotation>
                    </xsd:element>
                    <xsd:element name="transmissionPolicy" type="am:TransmissionPolicy">
                        <xsd:annotation>
                        <xsd:documentation>Optional parameter for transmission policy of larger data</xsd:documentation>
                        </xsd:annotation>
                    </xsd:element>
                    </xsd:choice>
                    <xsd:attribute name="tags" type="xsd:string"/>
                    <xsd:attribute name="access" type="am:LabelAccessEnum"/>
                    <xsd:attribute name="dataStability" type="am:LabelAccessDataStability">
                    <xsd:annotation>
                        <xsd:documentation>Defines the data stability needs of the label access</xsd:documentation>
                    </xsd:annotation>
                    </xsd:attribute>
                    <xsd:attribute name="implementation" type="am:LabelAccessImplementation"/>
                    <xsd:attribute name="data" type="xsd:string"/>
                </xsd:extension>
                </xsd:complexContent>
            </xsd:complexType>


        This is an example including all the supported features:
        
        .. code-block:: XML
        
            <items xsi:type="am:LabelAccess" data="LabelA?type=Label" access="write" dataStability="inherited" implementation="timed">
                <statistic>
                    <value xsi:type="am:SingleValueStatistic" value="0.0"/>
                    <cacheMisses xsi:type="am:MinAvgMaxStatistic" min="0" avg="0.0" max="0"/>
                </statistic>
            </items>

        This is an example from WATERS 2019, similar to democar and tutorial.
        
        .. code-block:: XML

            <items xsi:type="am:LabelAccess" data="x_car_device?type=Label" access="read"/>
        
        This is an example from WATERS 2017. Note that custom properties are not supported directly. 
        One has to extend these classes to implement custom attributes.
        
        .. code-block:: XML

            <items xsi:type="am:LabelAccess" data="Label_4881?type=Label" access="read" implementation="implicit">
                <customProperties key="COMMUNINCATION_TYPE">
                    <value xsi:type="am:StringObject" value="IMPLICIT"/>
                </customProperties>
                <statistic>
                    <value xsi:type="am:SingleValueStatistic" value="4.0"/>
                </statistic>
            </items>

        """
        if not tree.tag == "items":
            print ("ERROR: not a LabelAccess item")
            return False

        # parsing the type. type must be always parsed in Amalthea_obj
        super().parse_xml(tree)

        supported_atribs = ['type', 'data', 'access', 'dataStability', 'implementation']
        # Issue warning if there is some unsupported attribute
        for k, v in tree.attrib.iteritems():
            if k not in supported_atribs:
                # get the last 4 chars, in case of the 'type' attribute
                if k[-4:] == "type":
                    # get only the part of the value after the ':'.
                    # example. 'am:LabelAccess' => 'LabelAccess'
                    value = v.split(":",1)[1]
                    if value != 'LabelAccess':
                        print ("WARNING: attribute 'type' value",v, "is not expected for LabelAccess")
                        #return False
                else:
                    print ("WARNING: attribute",k, "is not supported")
                    #return False

        # data is mandatory
        v = tree.get('data')
        if v != None:
            # save not the name of the label, but its reference (i.e. pointer)
            #tag_type = tag.attrib['type']
            label_name = v.split("?",1)[0]
            label_ref = self.__search_label(label_name)
            if label_ref != None:
                self.data = label_ref
            else:
                print ("ERROR: LabelAccess is referencing to a Label", v, " that does not exist!")
                return False
        else:
            print ("ERROR: attribute 'data' not found for LabelAccess")
            return False

        # access is mandatory
        v = tree.get('access')
        if v != None:
            self.access = v
        else:
            print ("ERROR: attribute 'access' is not defined in Label_access!")
            return False        

        # all of these are optionals
        v = tree.get('dataStability')
        if v != None:
            self.dataStability = v

        v = tree.get('implementation')
        if v != None:
            self.implementation = v

        # it is not mandatory to have 'statistic' tag in LabelAccess
        #   block used to fill self.value_stat = {}
        #   block used to fill self.cacheMisses_stat = {}
        # transmissionPolicy is not supported yet
        supported_tags = ['statistic']
        for c in tree.getchildren():
            if c.tag not in supported_tags:
                print ("WARNING: tag",c.tag, "is not supported")
                #return False
            else:
                if c.tag == 'statistic':
                    # both 'value' and 'cacheMisses' can be either 
                    #   SingleValueStatistic or MinAvgMaxStatistic
                    expected_stat_tags = ['value','cacheMisses']
                    for s in expected_stat_tags:
                        tag = c.find(s)
                        if  tag != None:
                            # get the type
                            for k, v in tag.attrib.iteritems():
                                # TODO this code would be simpler if i could use wildcard to search for attributes
                                # get the last 4 chars, in case of the 'type' attribute
                                if k[-4:] == "type":
                                    # get only the part of the value after the ':'.
                                    # example. 'am:LabelAccess' => 'LabelAccess'
                                    type_name = v.split(":",1)[1]
                                    if type_name == 'SingleValueStatistic':
                                        self.value_stat = Label_access_SingleValueStatistic(tag, self.verbose)
                                    elif type_name == 'MinAvgMaxStatistic':
                                        self.cacheMisses_stat = Label_access_MinAvgMaxStatistic(tag, self.verbose)
                                    else:
                                        print ('ERROR: type of statistic', v, 'is not supported')
                                        return False
                        #TODO perhaps it's necessary to check if there is at least one of the 
                        # the tags: 'value' or 'cacheMisses'

        return True

    def __search_label(self,label_name):
        """ This search links the Label_access to its Label.

        The Label_access 'data' attribute refers to the actual label. 
        This way, no additioanl search is necessary.

        Args:
            label_name (str): name of the label to be searched in the label list.

        Returns:
            Label ref: A reference to the label with the same name.

        """
        for i in self.label_list_ref:
            if i.name == label_name:
                return i
        return None

#############################################

class Label_access_SingleValueStatistic:
    """ A class to represent LabelAccess of SingleValueStatistic type.

    Attributes:
        tree (lxml.etree): xml tree with all information of the item.

    """

    def __init__(self, tree, verbose):
        self.value  = float(0.0)
        self.verbose = verbose

        self.parse_xml(tree)

    def get(self):
        """ A getter for this class.

        Returns:
            pointer: 'pointer' to the item.
        """   
        return self

    def __atrib2str(self):
        """ Convert all attributes of this class into a str.

        Returns:
            str: string with all attributes of the class.
        """
        aux = "{'value': '%s'}" % (self.value)
        # literal_eval used only to check whether the string really represents a dictionary
        return str(ast.literal_eval(aux))

    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        return self.__atrib2str()

    def __repr__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """        
        return self.__atrib2str()

    def dump(self):
        """ Generates a string with the attributes of the class. 

        The amount of information in the string is controlled by the 'verbose' attribute.

        Returns:
            str: string with the attributes of the class.
        """        
        if self.verbose >= 4:
            return self.__atrib2str()
        return "" 

    def parse_xml(self, tree):
        """ Parses the attributes of this class from the app4mc XML file. 
        
        Pay attention to the parsing limitations marked with TODO.
       
        Args:
            tree (lxml.etree): xml tree with all information of the item.

        Returns:
            bool: return True if parsing is ok.

        .. code-block:: XML
        
            <cacheMisses xsi:type="am:SingleValueStatistic" min="0" avg="0.0" max="0"/>
            <value xsi:type="am:SingleValueStatistic" min="0" avg="0.0" max="0"/>

        """
        # assigns value for the attributes specific to this type of tick        
        if tree.tag == 'cacheMisses' or tree.tag == 'value':
            # assuming the 3 attributes are mandatory
            # this is the attribute value, not the tag value
            v = tree.get('value')
            if v != None:
                self.value = float(v)
            else:
                print ("ERROR: attribute 'value' not found for", self.obj_type, " statistic")
                return False
        else:
            print ("ERROR: tag", tree.tag, "is not supported in statistic")
            return False
        
        return True
        
#############################################

class Label_access_MinAvgMaxStatistic:
    """ A class to represent LabelAccess of MinAvgMaxStatistic type.

    Attributes:
        tree (lxml.etree): xml tree with all information of the item.

    """

    def __init__(self, tree, verbose):
        self.avg  = float(0.0)
        self.min  = 0
        self.max  = 0
        self.verbose = verbose

        self.parse_xml(tree)

    def get(self):
        """ A getter for this class.

        Returns:
            pointer: 'pointer' to the item.
        """   
        return self

    def __atrib2str(self):
        """ Convert all attributes of this class into a str.

        Returns:
            str: string with all attributes of the class.
        """
        aux = "{'avg': '%s', 'min': '%s', 'max': '%s'}" % (self.avg, self.min, self.max)
        # literal_eval used only to check whether the string really represents a dictionary
        return str(ast.literal_eval(aux))

    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        return self.__atrib2str()

    def __repr__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """        
        return self.__atrib2str()

    def dump(self):
        """ Generates a string with the attributes of the class. 

        The amount of information in the string is controlled by the 'verbose' attribute.

        Returns:
            str: string with the attributes of the class.
        """        
        if self.verbose >= 4:
            return self.__atrib2str()
        return "" 

    def parse_xml(self, tree):
        """ Parses the attributes of this class from the app4mc XML file. 
        
        Pay attention to the parsing limitations marked with TODO.

        Args:
            tree (lxml.etree): xml tree with all information of the item.

        Returns:
            bool: return True if parsing is ok.

        These are two possible examples:

        .. code-block:: XML

            <cacheMisses xsi:type="am:MinAvgMaxStatistic" min="0" avg="0.0" max="0"/>
            <value xsi:type="am:MinAvgMaxStatistic" min="0" avg="0.0" max="0"/>
        """
        # assigns value for the attributes specific to this type of tick        
        if tree.tag == 'cacheMisses' or tree.tag == 'value':
            # assuming the 3 attributes are mandatory
            # this is the attribute value, not the tag value
            v = tree.get('avg')
            if v != None:
                self.avg = float(v)
            else:
                print ("ERROR: attribute 'avg' not found for", self.obj_type, " statistic")
                return False
            
            v = tree.get('min')
            if v != None:
                self.min = int(v)
            else:
                print ("ERROR: attribute 'min' not found for", self.obj_type, " statistic")
                return False
            
            v = tree.get('max')
            if v != None:
                self.max = int(v)
            else:
                print ("ERROR: attribute 'max' not found for", self.obj_type, " statistic")
                return False
        else:
            print ("ERROR: tag", tree.tag, "is not supported in statistic")
            return False
        
        return True
        
