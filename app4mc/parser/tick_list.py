import ast
from app4mc.parser import tick_item

class Tick_list():
    """ A placeholder for all types of Ticks.

    A runnable has one Tick_list that has a list of at least one Tick object.

    Args:
        tree (str): tree of the item.
        verbose (int): verbosity level. 0 is none and 5 is the max.
          This class does not print messages. It just passes the verbose val in the builder method.

    Attributes:
        tick_list (list): List of all ticks, independent of its type.

        Notes:
            One alternative design is to extend the list class in python
            https://treyhunner.com/2019/04/why-you-shouldnt-inherit-from-list-and-dict-in-python/

    """

    def __init__(self, tree, verbose):
       
        self.tick_list = []
        self.verbose = verbose

        self.parse_xml(tree)

    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        data_str = "[\n"
        for tick in self.tick_list:
            data_str += str(tick) + ",\n"
        data_str += "]\n"
        return data_str

    def parse_xml(self,tree):
        """ Parses the attributes of this class from the app4mc XML file. 
        
        Pay attention to the parsing limitations marked with TODO.

        Args:
            tree (str): tree of the item.

        """

        if not tree.tag == "items":
            print ("ERROR: not a tick item")
            return False

        # Issue warning if there is some unsupported attribute
        for k, v in tree.attrib.iteritems():
            # get the last 4 chars, in case of the 'type' attribute
            if k[-4:] != "type":
                print ("WARNING: attribute",k, "is not supported")
                #return False
            else:
                # get only the part of the value after the ':'.
                # example. 'am:Ticks' => 'Ticks'
                value = v.split(":",1)[1]
                if value != 'Ticks':
                    print ("WARNING: attribute 'type' value",v, "is not expected for Ticks")

        # is it a simple/default tick or or multiple/extended ticks ?
        supported_tags = ['default', 'extended']
        for c in tree.getchildren():
            if c.tag not in supported_tags:
                print ("WARNING: tag",c.tag, "is not supported")
                #return False
            else:
                if c.tag == 'default':
                    # when there only one type of processing unit, it is not necessary to inform its type
                    self.__tick_type_parse('',c)
                elif c.tag == 'extended':
                    value = c.attrib['key']
                    # if you have '<extended key="Denver?type=ProcessingUnitDefinition">'
                    # this will extract = 'Denver'
                    proc_unit_type = value.split("?",1)[0]
                    #print ("Proc_unit:", proc_unit_type)
                    #find the 'value' tag inside an 'extended' tag
                    value_tag = c.find('value')
                    self.__tick_type_parse(proc_unit_type,value_tag)


    def __tick_type_parse(self, proc_unit, tree):
        """ Parse ticks where there are multiple types of processing units.

        Args:
            tree (lxml.etree): xml tree with all information of the item.

        Returns:
            bool: return True if parsing is ok.


        Todo:
            * support all types of ticks
        """

        # is it a simple/default tick or or multiple/extended ticks ?
        if tree.tag == 'default' or tree.tag == 'value':
            # Issue warning if there is some unsupported attribute
            for k, v in tree.attrib.iteritems():
                # get the last 4 chars, in case of the 'type' attribute
                if k[-4:] == "type":
                    # get only the part of the value after the ':'.
                    # example. 'am:Ticks' => 'Ticks'
                    value = v.split(":",1)[1]
                    # create the tick, no matter its type, then add it to the tick_list
                    tick_obj = tick_item.builder(value,proc_unit,tree,self.verbose)
                    if tick_obj != None:
                        self.tick_list.append(tick_obj)
        else:
            print ("ERROR: tag",tree.tag, "is not expected for Ticks")
        return True

