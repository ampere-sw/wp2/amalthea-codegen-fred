""" This module describes all the types of items supported by Task.

Currently supporting:
    * RunnableCall;
    * InterProcessTrigger.

Attributes:
    None:.

Todo:
    Task items WaitEvent, ClearEvent, SetEvent are not implemented
"""

import ast
from app4mc.common.amalthea_obj import Amalthea_obj

# https://python-3-patterns-idioms-test.readthedocs.io/en/latest/Factory.html#simple-factory-method

"""
def builder(task_type, tree, verbose, ref):
     Builder for all supported types of *Stimulus*.

    Args:
        tree (lxml.etree): Xml tree with all information of the item.
        verbose (int): verbosity level. 0 is none and 5 is the max.
        ref (pointer):  a reference to a list of Amalthea objects

    Returns:
        Task_*_item.: return an object derived from *Stimulus*.

    Note:
    
    if task_type == 'RunnableCall':
        return Task_RunnableCall_item(tree, ref, verbose)
    elif task_type == 'InterProcessTrigger':
        return Task_InterProcessTrigger_item(tree, ref, verbose)
        #elif task_type == 'WaitEvent':
        #    return RelativePeriodicStimulus_stimulus(tree, verbose)
        #elif task_type == 'ClearEvent':
        #    return RelativePeriodicStimulus_stimulus(tree, verbose)
    else:
        print ("WARNING: task item ", task_type, "is not supported")

    return None
"""

####################################################

class Task_RunnableCall_item(Amalthea_obj):
    """ RunnableCall within a task.

    Args:
        tree (lxml.etree): xml tree with all information of the item.
        runnable_list (list[Runnable]): List of all existing runnables. 
            used to link this runnable call to a existing runnables.
        verbose (int): verbosity level. 0 is none and 5 is the max.
            This class prints message when verbose > = 3.

    """

    def __init__(self,tree, runnable_list, verbose):
        # a reference to a runnable
        self.runnable= None
        self.verbose = verbose

        # used to search for the existing runnables
        self.runnable_list = runnable_list

        self.parse_xml(tree)

    def get(self):
        """ A getter for this class.

        Returns:
            pointer: 'pointer' to the item.
        """   
        return self

    def __atrib2str(self):
        """ Convert all attributes of this class into a str.

        Returns:
            str: string with all attributes of the class.
        """
        aux = "{'type': '%s', 'runnable': '%s'}" % (self.obj_type, self.runnable.name)
        # literal_eval used only to check whether the string really represents a dictionary
        return str(ast.literal_eval(aux))

    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        return self.__atrib2str()

    def __repr__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """        
        return self.__atrib2str()

    def dump(self):
        """ Generates a string with the attributes of the class. 

        The amount of information in the string is controlled by the 'verbose' attribute.

        Returns:
            str: string with the attributes of the class.
        """        
        if self.verbose == 3:
            return self.runnable.name + '\n'
        elif self.verbose >= 4:
            return self.__atrib2str() + '\n'
        return "" 

    def parse_xml(self, tree):
        """ Parses the attributes of this class from the app4mc XML file. 
        
        Pay attention to the parsing limitations marked with TODO.

        Args:
            tree (lxml.etree): xml tree with all information of the item.

        Returns:
            bool: return True if parsing is ok.

        An example:

        .. code-block:: XML

            <items xsi:type="am:RunnableCall" runnable="Runnable_50ms_6?type=Runnable"/>
        """
        if not tree.tag == "items":
            print ("ERROR: not a task RunnableCall item")
            return False

        # assign the type attribute
        super().parse_xml(tree)

        supported_atribs = ['type', 'runnable']
        # Issue warning if there is some unsupported attribute
        for k, v in tree.attrib.iteritems():
            if k not in supported_atribs:
                # get the last 4 chars, in case of the 'type' attribute
                if k[-4:] == "type":
                    # get only the part of the value after the ':'.
                    # example. 'am:LabelAccess' => 'LabelAccess'
                    value = v.split(":",1)[1]
                    if value != 'RunnableCall':
                        print ("WARNING: attribute 'type' value",v, "is not expected for RunnableCall")
                        #return False
                else:
                    print ("WARNING: attribute",k, "is not supported in a task item")
                    #return False

        # runnable is mandatory
        v = tree.get('runnable')
        if v != None:
            # save not the name of the runnable, but its reference (i.e. pointer)
            name = v.split("?",1)[0]
            runnable_ref = self.__search_label(name)
            if runnable_ref != None:
                self.runnable = runnable_ref
            else:
                print ("ERROR: RunnableCall is referencing to a runnable", v, " that does not exist!")
                return False
        else:
            print ("ERROR: attribute 'runnable' not found in task item")
            return False
        return True

    def __search_label(self,runnable_name):
        """ This search links the RunnableCall to its runnable.

        The RunnableCall 'runnable' attribute refers to the actual runnable. 
        This way, no additioanl search is necessary.

        Args:
            runnable_name (str): name of the runnable to be searched in the runnable list.

        Returns:
            Runnable ref: A reference to the runnable with the same name.

        """
        for i in self.runnable_list:
            if i.name == runnable_name:
                return i
        return None

####################################################

class Task_InterProcessTrigger_item(Amalthea_obj):
    """ InterProcessTrigger within a task.

    Args:
        tree (lxml.etree): xml tree with all information of the item.
        runnable_list (list[Runnable]): List of all existing runnables. 
            used to link this runnable call to a existing runnables.
        verbose (int): verbosity level. 0 is none and 5 is the max.
            This class prints message when verbose > = 3.

    """

    def __init__(self,tree, stimuli_list, verbose):
        # a reference to a stimuli
        self.stimuli= None
        self.verbose = verbose

        # used to search for the existing stimuli
        self.stimuli_list = stimuli_list

        self.parse_xml(tree)

    def get(self):
        """ A getter for this class.

        Returns:
            pointer: 'pointer' to the item.
        """   
        return self

    def __atrib2str(self):
        """ Convert all attributes of this class into a str.

        Returns:
            str: string with all attributes of the class.
        """
        aux = "{'type': '%s', 'stimuli': '%s'}" % (self.obj_type, self.stimuli.name)
        return str(ast.literal_eval(aux))

    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        return self.__atrib2str()

    def __repr__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """        
        return self.__atrib2str()

    def dump(self):
        """ Generates a string with the attributes of the class. 

        The amount of information in the string is controlled by the 'verbose' attribute.

        Returns:
            str: string with the attributes of the class.
        """        
        if self.verbose == 3:
            return self.stimuli.name + '\n'
        elif self.verbose >= 4:
            return self.__atrib2str() + '\n'
        return "" 

    def parse_xml(self, tree):
        """ Parses the attributes of this class from the app4mc XML file. 
        
        Pay attention to the parsing limitations marked with TODO.

        Args:
            tree (lxml.etree): xml tree with all information of the item.

        Returns:
            bool: return True if parsing is ok.

        An example:

        .. code-block:: XML

            <items xsi:type="am:InterProcessTrigger" stimulus="SFM_stim?type=InterProcessStimulus"/>
        """
        if not tree.tag == "items":
            print ("ERROR: not a task InterProcessTrigger item")
            return False

        # assign the type attribute
        super().parse_xml(tree)

        supported_atribs = ['type', 'stimulus']
        # Issue warning if there is some unsupported attribute
        for k, v in tree.attrib.iteritems():
            if k not in supported_atribs:
                # get the last 4 chars, in case of the 'type' attribute
                if k[-4:] == "type":
                    # get only the part of the value after the ':'.
                    # example. 'am:LabelAccess' => 'LabelAccess'
                    value = v.split(":",1)[1]
                    if value != 'InterProcessTrigger':
                        print ("WARNING: attribute 'type' value",v, "is not expected for InterProcessTrigger")
                        #return False
                else:
                    print ("WARNING: attribute",k, "is not supported in a task item")
                    #return False

        # stimulus is mandatory
        v = tree.get('stimulus')
        if v != None:
            # save not the name of the stimulus, but its reference (i.e. pointer)
            name = v.split("?",1)[0]
            stim_ref = self.__search_stimuli(name)
            if stim_ref != None:
                self.stimuli = stim_ref
            else:
                print ("ERROR: InterProcessTrigger is referencing to a stimulus", v, " that does not exist!")
                return False
        else:
            print ("ERROR: attribute 'stimulus' not found in task item")
            return False
        return True

    def __search_stimuli(self,stimulus_name):
        """ This search links the InterProcessTrigger to its stimulus.

        The InterProcessTrigger 'stimulus' attribute refers to the actual stimulus. 
        This way, no additional search is necessary.

        Args:
            stimulus_name (str): name of the stimulus to be searched in the stimuli list.

        Returns:
            stimulus ref: A reference to the stimulus with the same name.

        """
        for i in self.stimuli_list:
            if i.name == stimulus_name:
                return i
        return None