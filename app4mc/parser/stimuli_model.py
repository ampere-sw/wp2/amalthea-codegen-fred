# pretty print https://github.com/wolever/pprintpp
from app4mc.parser.stimulus import builder as stim_builder

class Stimuli_Model:
    """ Place holder for a list of *StimuliModel* app4mc models.

    It parses the relavant part of the XML file to fill this class with 
    :class:`~app4mc.parser.stimulus.Stimulus`.

    Args:
        tree (lxml.etree): xml tree with all information of the item.
        verbose (int): verbosity level. 0 is none and 5 is the max.
            
            This class prints message when verbose > = 1.

            * *Level 0*: quiet mode (default);
            * *Level 1*: show the name of the 1st level tags (i.e. swModel, hwModel, etc);
              For Stimuli_Model, it lists the names of Stimuli;
            * *Level 2*: show the attributes of the 2nd level tags.
              For Stimuli_Model, it lists all the attributes of the Stimuli;

    TODO:
        * Clocks are not currently supported.
        * Use some nice pretty printer to better present the logs
    """

    def __init__(self, tree, verbose):
        self.verbose = verbose

        self.stimuli = []
        """ List of stimuli of type :class:`~app4mc.parser.stimulus.Stimulus`.

            The list of stimuli is related to the app4mc model *StimuliModel*.
        """

        self.parse_xml(tree)    

    def get(self):
        """ A getter for this class.

        Returns:
            pointer: 'pointer' to the item.
        """   
        return self

    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        str_data = "[\n"
        for l in self.stimuli:
            str_data += str(l) + ',\n'
        str_data += "]\n"

        return str_data

    def search(self, stimulus_name):
        """ Search for a stimulus in the stimuli list.

        Returns:
            Returns 'None' if stimulus is not found or  :class:`~app4mc.parser.stimulus.Stimulus` Reference to the stimulus if it is found.
        """
        for s in self.stimuli:
            if s.name == stimulus_name:
                return s.get()
        return None

    def parse_xml(self, tree):
        """ It parses the *StimuliModel* node.

        Args:
            tree (lxml.etree): xml tree with the *StimuliModel* node.

        """
        # Issue warning if there is some unsupported tags
        supported_tags = ['stimuli']
        for t in tree.getchildren():
            if t.tag not in supported_tags:
                print ("WARNING: tag",t.tag, "is not supported in StimuliModel")
                #return False
                #         

        # check for the mandatory tag
        tags = tree.find('stimuli')
        if tags == None:
            print ("ERROR: StimuliModel has no stimuli")
            return False
            
        print ("Creating stimuli ...")
        for t in tags:
            if t.tag == "stimuli":
                stim = stim_builder(t, self.verbose)
                if stim != None:
                    self.stimuli.append(stim)

        if self.verbose>=1:
            for l in self.stimuli:
                print (' -',l.dump())
            print('Total of', len(self.stimuli), 'stimuli.\n')


