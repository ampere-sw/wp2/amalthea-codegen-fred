
""" This module describes all the types of items supported by Ticks.

Currently supporting the following sub_types of Ticks:
    * DiscreteValueConstant;
    * DiscreteValueStatistics;
    * DiscreteValueGaussDistribution;
    * DiscreteValueWeibullEstimatorsDistribution.

Example:
    This example shows where the `proc_unit` example comes from. It is counter intuitive, but it come from parent tags.
    In this case, this Tick item has one item for Denver and another to A57.

    .. code-block:: XML

        <items xsi:type="am:Ticks">
            <extended key="Denver?type=ProcessingUnitDefinition">
                <value xsi:type="am:DiscreteValueStatistics" lowerBound="4755924" upperBound="6354526" average="5853824.0"/>
            </extended>
            <extended key="A57?type=ProcessingUnitDefinition">
                <value xsi:type="am:DiscreteValueStatistics" lowerBound="6378560" upperBound="7379120" average="6921260.0"/>
            </extended>
        </items>

TODO:
    * Label_access items ... are not implemented
    * support TimeUniformDistribution, used in waters17 stimuliModel for sporadic tasks.

"""

import ast
from app4mc.common.amalthea_obj import Amalthea_obj


def builder(tick_type, proc_unit, tree, verbose):
    """ Parse ticks where there is only one type of processing unit.

    Parse ticks where there is only one type of processing unit.

    Args:
        tick_type (str): The name of the tick type.
        proc_unit (str): The processor unit related to this tick.
        tree (lxml.etree): Xml tree with all information of the item.
        verbose (int): verbosity level. 0 is none and 5 is the max.

    Returns:
        tick: return a tick object.

    Note:
    """

    if tick_type == 'DiscreteValueConstant':
        return DiscreteValueConstant_tick(proc_unit,tree, verbose)
    elif tick_type == 'DiscreteValueStatistics':
        return DiscreteValueStatistics_tick(proc_unit,tree, verbose)
    elif tick_type == 'DiscreteValueGaussDistribution':
        return DiscreteValueGaussDistribution_tick(proc_unit,tree, verbose)
    elif tick_type == 'DiscreteValueWeibullEstimatorsDistribution':
        return DiscreteValueWeibullEstimatorsDistribution_tick(proc_unit,tree, verbose)
    else:
        print ("WARNING: type of tick ", tick_type, "is not supported")

    return None

class Tick(Amalthea_obj):
    """ This class only adds a new attribute called sub_type for the ticks.

    A tick is used to describe computation time spent in a runnable. 
    There are several types of Ticks, but only few types are currently 
    supported by this parser. Check the (:func:`~app4mc.parser.tick_item.builder`) 
    function in this file to see the supported types of ticks.

    Args:
        Amalthea_obj ([type]): [description]
    """

    def parse_xml(self,tree):
        """ Parses the attributes of this class from the app4mc XML file. 
        
        Pay attention to the parsing limitations marked with TODO.

        Args:
            tree (lxml.etree): xml tree with all information of the item.

        Returns:
            bool: return True if parsing is ok.

        """

        self.sub_type = ""
        """ The subtype is filled by the derived classes.
            
            Currently supporting the following ticks sub_types:
                * DiscreteValueConstant;
                * DiscreteValueStatistics;
                * DiscreteValueGaussDistribution;
                * DiscreteValueWeibullEstimatorsDistribution.
        """

        self.obj_type = "Ticks"
        """ In this case this field is used to differentiate from Access_Label.

        """

        # value is expected when it is an extended tick
        if not tree.tag == "default" and not tree.tag == "value":
            print ("ERROR: not a LabelAccess item")
            return False

        # the parser cannot be called, otherwise it would assign the sub_type to the type.
        #Amalthea_obj.parse_xml(self,tree)


class DiscreteValueConstant_tick(Tick):
    """ A Amalthea tick of type DiscreteValueConstant.

    Used to describe computation time spent in a runnable.
    
    Args:
        tree (lxml.etree): xml tree with all information of the item.
        proc_unit (str): Points to the Processing Unit definition.
            Recall that the same runnable can be profiled to different 
            types of processing  unit in case of a heterogeneous system 
            (e.g. multiples types of core, gpus, accelerators). 
            Later in the optimization step one proc unit is selected.
        verbose (int): verbosity level. 0 is none and 5 is the max.
            This class prints message when verbose > = 3.

    """

    def __init__(self, proc_unit, tree, verbose):
        self.value  = 0
        self.verbose = verbose

        # type of proc definition related to this item.
        # it comes from parent nodes !!! messy definition !!!!
        self.proc_unit = proc_unit

        self.parse_xml(tree)

    def get(self):
        """ A getter for this class.

        Returns:
            pointer: 'pointer' to the item.
        """   
        return self

    def __atrib2str(self):
        """ Convert all attributes of this class into a str.

        Returns:
            str: string with all attributes of the class.
        """
        aux = "{'type': '%s', 'sub_type': '%s', 'proc_unit': '%s', 'value': '%s'}" % (self.obj_type, self.sub_type, self.proc_unit, self.value)
        # literal_eval used only to check whether the string really represents a dictionary
        return str(ast.literal_eval(aux))

    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        return self.__atrib2str()

    def __repr__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """        
        return self.__atrib2str()

    def dump(self):
        """ Generates a string with the attributes of the class. 

        The amount of information in the string is controlled by the 'verbose' attribute.

        Returns:
            str: string with the attributes of the class.
        """        
        if self.verbose >= 3:
            return self.__atrib2str() + '\n'
        return "" 

    def parse_xml(self, tree):
        """ Parses the attributes of this class from the app4mc XML file. 
        
        Pay attention to the parsing limitations marked with TODO.

        Args:
            tree (lxml.etree): xml tree with all information of the item.

        Returns:
            bool: return True if parsing is ok.

        This is the example of the tutorial model:

        .. code-block:: XML
        
            <default xsi:type="am:DiscreteValueConstant" value="20"/>
        """
        # assigns value for the attributes specific to this type of tick        
        if tree.tag == 'default' or tree.tag == 'value':
            # assign the type attribute
            Tick.parse_xml(self,tree)
            # assign the subtype attribute
            self.sub_type = "DiscreteValueConstant"
            # this is the attribute value, not the tag value
            v = tree.get('value')
            if v != None:
                self.value = int(v)
            else:
                print ("ERROR: attribute 'value' not found for", self.obj_type, " tick")
                return False
        else:
            print ("ERROR: tag", tree.tag, "is not supported in tick")
            return False
        
        return True

#############################################

class DiscreteValueStatistics_tick(Tick):
    """ Amalthea Tick of type DiscreteValueStatistics.

    All ticks are treated as 'extended', which means all of them have *Processing Unit* definition.

    Args:
        tree (lxml.etree): xml tree with all information of the item.
        proc_unit (str): Points to the Processing Unit definition.
            Recall that the same runnable can be profiled to different 
            types of processing  unit in case of a heterogeneous system 
            (e.g. multiples types of core, gpus, accelerators). 
            Later in the optimization step one proc unit is selected.
        verbose (int): verbosity level. 0 is none and 5 is the max.
            This class prints message when verbose > = 3.

    """

    def __init__(self, proc_unit, tree, verbose):
        self.average  = float(0.0)
        self.lowerBound  = 0
        self.upperBound  = 0
        self.verbose = verbose

        # type of proc definition related to this item.
        # it comes from parent nodes !!! messy definition !!!!
        self.proc_unit = proc_unit

        self.parse_xml(tree)

    def get(self):
        """ A getter for this class.

        Returns:
            pointer: 'pointer' to the item.
        """   
        return self

    def __atrib2str(self):
        """ Convert all attributes of this class into a str.

        Returns:
            str: string with all attributes of the class.
        """
        aux = "{'type': '%s', 'sub_type': '%s', 'proc_unit': '%s', 'average': '%s', 'lowerBound': '%s', 'upperBound': '%s'}" % (self.obj_type, self.sub_type, self.proc_unit, self.average, self.lowerBound, self.upperBound)
        # literal_eval used only to check whether the string really represents a dictionary
        return str(ast.literal_eval(aux))

    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        return self.__atrib2str()

    def __repr__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """        
        return self.__atrib2str()

    def dump(self):
        """ Generates a string with the attributes of the class. 

        The amount of information in the string is controlled by the 'verbose' attribute.

        Returns:
            str: string with the attributes of the class.
        """        
        if self.verbose >= 3:
            return self.__atrib2str() + '\n'
        return "" 

    def parse_xml(self, tree):
        """ Parses the attributes of this class from the app4mc XML file. 
        
        Pay attention to the parsing limitations marked with TODO.

        Args:
            tree (lxml.etree): xml tree with all information of the item.

        Returns:
            bool: return True if parsing is ok.

        This is the example of the WATERS 2019 model:

        .. code-block:: XML

            <value xsi:type="am:DiscreteValueStatistics" lowerBound="162000000" upperBound="174000000" average="1.65E8"/>

        """
        # assigns value for the attributes specific to this type of tick        
        if tree.tag == 'default' or tree.tag == 'value':
            # assign the type attribute
            Tick.parse_xml(self,tree)
            # assign the subtype attribute
            self.sub_type = "DiscreteValueStatistics"
            # assuming the 3 attributes are mandatory
            # this is the attribute value, not the tag value
            v = tree.get('average')
            if v != None:
                self.average = float(v)
            else:
                print ("ERROR: attribute 'average' not found for", self.obj_type, " tick")
                return False
            
            v = tree.get('lowerBound')
            if v != None:
                self.lowerBound = int(v)
            else:
                print ("ERROR: attribute 'lowerBound' not found for", self.obj_type, " tick")
                return False
            
            v = tree.get('upperBound')
            if v != None:
                self.upperBound = int(v)
            else:
                print ("ERROR: attribute 'upperBound' not found for", self.obj_type, " tick")
                return False
        else:
            print ("ERROR: tag", tree.tag, "is not supported in tick")
            return False
        
        return True
        
#############################################

class DiscreteValueGaussDistribution_tick(Tick):
    """ Amalthea Tick of type DiscreteValueGaussDistribution.

    All ticks are treated as 'extended', which means all of them have *Processing Unit* definition.

    Args:
        tree (lxml.etree): xml tree with all information of the item.
        proc_unit (str): Points to the Processing Unit definition.
            Recall that the same runnable can be profiled to different 
            types of processing  unit in case of a heterogeneous system 
            (e.g. multiples types of core, gpus, accelerators). 
            Later in the optimization step one proc unit is selected.
        verbose (int): verbosity level. 0 is none and 5 is the max.
            This class prints message when verbose > = 3.

    """

    def __init__(self, proc_unit, tree, verbose):
        self.mean  = float(0.0)
        self.sd  = float(0.0)
        self.lowerBound  = 0
        self.upperBound  = 0
        self.verbose = verbose

        # type of proc definition related to this item.
        # it comes from parent nodes !!! messy definition !!!!
        self.proc_unit = proc_unit

        self.parse_xml(tree)

    def get(self):
        """ A getter for this class.

        Returns:
            pointer: 'pointer' to the item.
        """   
        return self

    def __atrib2str(self):
        """ Convert all attributes of this class into a str.

        Returns:
            str: string with all attributes of the class.
        """
        aux = "{'type': '%s', 'sub_type': '%s', {'proc_unit': '%s', 'mean': '%s', 'sd': '%s', 'lowerBound': '%s', 'upperBound': '%s'}" % (self.obj_type, self.sub_type, self.proc_unit, self.mean, self.sd, self.lowerBound, self.upperBound)
        # literal_eval used only to check whether the string really represents a dictionary
        return str(ast.literal_eval(aux))

    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        return self.__atrib2str()

    def __repr__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """        
        return self.__atrib2str()

    def dump(self):
        """ Generates a string with the attributes of the class. 

        The amount of information in the string is controlled by the 'verbose' attribute.

        Returns:
            str: string with the attributes of the class.
        """        
        if self.verbose >= 3:
            return self.__atrib2str() + '\n'
        return "" 

    def parse_xml(self, tree):
        """ Parses the attributes of this class from the app4mc XML file. 
        
        Pay attention to the parsing limitations marked with TODO.

        Args:
            tree (lxml.etree): xml tree with all information of the item.

        Returns:
            bool: return True if parsing is ok.

        This is the example of the FRED model:

        .. code-block:: XML

            <default xsi:type="am:DiscreteValueGaussDistribution" lowerBound="0" upperBound="5" mean="2.0" sd="1.0"/>

        """
        # assigns value for the attributes specific to this type of tick        
        if tree.tag == 'default' or tree.tag == 'value':
            # assign the type attribute
            Tick.parse_xml(self,tree)
            # assign the subtype attribute
            self.sub_type = "DiscreteValueGaussDistribution"
            # assuming the 4 attributes are mandatory
            v = tree.get('mean')
            if v != None:
                self.mean = float(v)
            else:
                print ("ERROR: attribute 'mean' not found for", self.obj_type, " tick")
                return False
            
            v = tree.get('sd')
            if v != None:
                self.sd = float(v)
            else:
                print ("ERROR: attribute 'sd' not found for", self.obj_type, " tick")
                return False
            
            v = tree.get('lowerBound')
            if v != None:
                self.lowerBound = int(v)
            else:
                print ("ERROR: attribute 'lowerBound' not found for", self.obj_type, " tick")
                return False
            
            v = tree.get('upperBound')
            if v != None:
                self.upperBound = int(v)
            else:
                print ("ERROR: attribute 'upperBound' not found for", self.obj_type, " tick")
                return False
        else:
            print ("ERROR: tag", tree.tag, "is not supported in tick")
            return False
        
        return True

#############################################

class DiscreteValueWeibullEstimatorsDistribution_tick(DiscreteValueStatistics_tick):
    """ Amalthea Tick of type DiscreteValueWeibullEstimatorsDistribution.

    All ticks are treated as 'extended', which means all of them have *Processing Unit* definition.

    Args:
        tree (lxml.etree): xml tree with all information of the item.
        proc_unit (str): Points to the Processing Unit definition.
            Recall that the same runnable can be profiled to different 
            types of processing  unit in case of a heterogeneous system 
            (e.g. multiples types of core, gpus, accelerators). 
            Later in the optimization step one proc unit is selected.
        verbose (int): verbosity level. 0 is none and 5 is the max.
            This class prints message when verbose > = 3.

    Todo:
        * i dont know how to use this tick. In the app4mc distribution 
          app4mc/org.eclipse.app4mc/plugins/org.eclipse.app4mc.amalthea.model/src/org/eclipse/app4mc/amalthea/model/util
          there is a reference to WeibullUtil.java where it refers to this paper
          https://papers.ssrn.com/sol3/papers.cfm?abstract_id=2690255

    """

    def __init__(self, proc_unit, tree, verbose):
        super().__init__(proc_unit, tree, verbose)

        self.PRemain  = float(0.0)
        self.verbose = verbose

        self.parse_xml(tree)

    def get(self):
        """ A getter for this class.

        Returns:
            pointer: 'pointer' to the item.
        """   
        return self

    def __atrib2str(self):
        """ Convert all attributes of this class into a str.

        Returns:
            str: string with all attributes of the class.
        """
        aux = "{'type': '%s', 'sub_type': '%s', 'proc_unit': '%s', 'average': '%s', 'lowerBound': '%s', 'upperBound': '%s', 'PRemain': '%s'}" % (self.obj_type, self.sub_type, self.proc_unit, self.average, self.lowerBound, self.upperBound, self.PRemain)
        # literal_eval used only to check whether the string really represents a dictionary
        return str(ast.literal_eval(aux))

    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        return self.__atrib2str()

    def __repr__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """        
        return self.__atrib2str()

    def dump(self):
        """ Generates a string with the attributes of the class. 

        The amount of information in the string is controlled by the 'verbose' attribute.

        Returns:
            str: string with the attributes of the class.
        """        
        if self.verbose >= 3:
            return self.__atrib2str() + '\n'
        return "" 

    def parse_xml(self, tree):
        """ Parses the attributes of this class from the app4mc XML file. 
        
        Pay attention to the parsing limitations marked with TODO.

        Args:
            tree (lxml.etree): xml tree with all information of the item.

        Returns:
            bool: return True if parsing is ok.

        This is the example of the democar model:

        .. code-block:: XML

            <default xsi:type="am:DiscreteValueWeibullEstimatorsDistribution" lowerBound="72000" upperBound="88000" average="80000.0" pRemainPromille="0.5"/>
        """
        # assigns value for the attribute pRemainPromille
        if tree.tag == 'default' or tree.tag == 'value':
            # this will assign value to the attributes type, lowerBound, upperBound, and average
            DiscreteValueStatistics_tick.parse_xml(self,tree)
            # assign the subtype attribute
            self.sub_type = "DiscreteValueWeibullEstimatorsDistribution"       
            # assuming the 3 attributes are mandatory
            # this is the attribute value, not the tag value
            v = tree.get('pRemainPromille')
            if v != None:
                self.PRemain = float(v)
            else:
                print ("ERROR: attribute 'pRemainPromille' not found for", self.obj_type, " tick")
                return False
        else:
            print ("ERROR: tag", tree.tag, "is not supported in tick")
            return False
        
        return True
                
