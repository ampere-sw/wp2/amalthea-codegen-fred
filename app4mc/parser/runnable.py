import ast
from app4mc.common.amalthea_item import Amalthea_item
import app4mc.parser.label_access as label_access
import app4mc.parser.tick_list as tick_list

class Runnable(Amalthea_item):
    """ A class to represent the Runnables in the Amalthea model.

    Args:
        tree (lxml.etree): xml tree with all information of the item.

    Attributes:
        None:.

    Todo:
        * asilLevel not implemented.

    """

    def __init__(self, tree, sw_model_ref, verbose):
        self.namespace = str("")
        self.callback = False
        self.service = False
        self.activations = str("")
        self.section = str("")
        #self.asilLevel = #" type="am:ASILType">

        self.item_list = []
        """ Ticks and label access of a runnable must be in the same list because the order is relevant.

            Amalthea model am:ActivityGraphItem.
        """

        # ref to the sw_model
        self.sw_model_ref = sw_model_ref
        self.verbose = verbose

        # parsing the rest of its data
        self.parse_xml(tree)

    def get(self):
        """ A getter for this class.

        Returns:
            pointer: 'pointer' to the item.
        """   
        return self

    def __atrib2str(self):
        """ Convert all attributes of this class into a str.

        Returns:
            str: string with all attributes of the class.
        """
        aux = "{'name': '%s', 'namespace': '%s', 'callback': '%s', 'service': '%s', 'activations': '%s', 'section': '%s'}" % (self.name, self.namespace, str(self.callback), str(self.service), self.activations, self.section)
        # literal_eval used only to check whether the string really represents a dictionary
        return str(ast.literal_eval(aux))

    def __item2str(self):
        """ Convert all tick items into a str.

        Returns:
            str: string with tick items.
        """
        #data_str = "[\n"
        data_str = ""
        for i in self.item_list:
            data_str += i.dump() 
        #data_str += "]\n"
        return data_str


    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        data_str = "{\n"
        data_str += self.__atrib2str()
        data_str += "[\n"
        data_str += self.__item2str()
        data_str += "}\n"
        data_str += "[\n"
        #data_str += self.__label2str()
        data_str += "}\n"
        return data_str

    def __repr__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        return self.__atrib2str()

    def dump(self):
        """ Generates a string with the attributes of the class. 

        The amount of information in the string is controlled by the 'verbose' attribute.

        Returns:
            str: string with the attributes of the class.
        """        
        if self.verbose == 1:
            return self.name
        elif self.verbose == 2:
            return self.__atrib2str()
        elif self.verbose >= 3:
            aux = self.__atrib2str()
            aux += '\nRunnable items:\n'
            aux += self.__item2str()
            return aux
        return ""
        

    def parse_xml(self, tree):
        """ Parses the attributes of this class from the app4mc XML file. 
        
        Pay attention to the parsing limitations marked with TODO.

        Args:
            tree (lxml.etree): xml tree with all information of the item.

        Returns:
            bool: return True if parsing is ok.

        .. code-block:: XML

            <xsd:complexType name="Runnable">
                <xsd:annotation>
                <xsd:documentation>Smallest allocatable unit, which provides additional (optional) attributes for allocation algorithms.</xsd:documentation>
                </xsd:annotation>
                <xsd:complexContent>
                <xsd:extension base="am:AbstractMemoryElement">
                    <xsd:choice maxOccurs="unbounded" minOccurs="0">
                    <xsd:element name="namespace" type="am:Namespace"/>
                    <xsd:element name="executionCondition" type="am:ModeConditionDisjunction"/>
                    <xsd:element name="parameters" type="am:RunnableParameter"/>
                    <xsd:element name="activityGraph" type="am:ActivityGraph"/>
                    <xsd:element name="activations" type="am:Activation"/>
                    <xsd:element name="section" type="am:Section"/>
                    </xsd:choice>
                    <xsd:attribute name="namespace" type="xsd:string"/>
                    <xsd:attribute name="callback" type="xsd:boolean">
                    <xsd:annotation>
                        <xsd:documentation>Marker if runnable is used as callback.</xsd:documentation>
                    </xsd:annotation>
                    </xsd:attribute>
                    <xsd:attribute name="service" type="xsd:boolean">
                    <xsd:annotation>
                        <xsd:documentation>Marker if runnable is used as a service.</xsd:documentation>
                    </xsd:annotation>
                    </xsd:attribute>
                    <xsd:attribute name="asilLevel" type="am:ASILType">
                    <xsd:annotation>
                        <xsd:documentation>ASIL level for the runnable entity</xsd:documentation>
                    </xsd:annotation>
                    </xsd:attribute>
                    <xsd:attribute name="activations" type="xsd:string"/>
                    <xsd:attribute name="section" type="xsd:string"/>
                </xsd:extension>
                </xsd:complexContent>
            </xsd:complexType>     

        """

        if not tree.tag == "runnables":
            print ("ERROR: not a runnable")
            return False

        # parsing the name. name must be always parsed in Amalthea_item
        super().parse_xml(tree)

        # Issue warning if there is some unsupported attribute
        supported_attribs = ['name', 'callback', 'namespace', 'service', 'activations','section']
        #print ("ATRIB:", tree.attrib.keys())
        for k in tree.attrib.keys():
            if k not in supported_attribs:
                # get the last 2 chars, in case of the 'id' attribute
                if k[-2:] != "id":
                    print ("WARNING: attribute",k, "is not supported")
                    #return False

        # they are all optional
        if tree.get('callback') != None:
            self.callback  = tree.get('callback')
        if tree.get('namespace') != None:
            self.namespace  = tree.get('namespace')
        if tree.get('activations') != None:
            self.activations  = tree.get('activations')
        if tree.get('service') != None:
            self.service  = tree.get('service')
        if tree.get('section') != None:
            self.section  = tree.get('section')

        # the only expected child for runnables is a activityGraph
        supported_tags = ['activityGraph']
        activity = None
        for c in tree.getchildren():
            if c.tag not in supported_tags:
                print ("WARNING: tag",c.tag, "is not supported")
                #return False

        # the only expected child for activityGraph is a set of items
        supported_tags = ['items']
        # the only types of items of a runnable are ['Ticks', 'LabelAccess']
        activity = tree.find('activityGraph')
        for a in activity.getchildren():
            if a.tag not in supported_tags:
                print ("WARNING: tag",a.tag, "is not supported")
                #return False
            else:
                # check if it is one of the supported types of items
                for k, v in a.attrib.iteritems():                    
                    if k[-4:] == 'type':
                        # example. 'am:Ticks' => 'Ticks'
                        value = v.split(":",1)[1]
                        if value == 'Ticks':
                            # this will create the entire list of ticks
                            tick = tick_list.Tick_list(a, self.verbose)
                            self.item_list.extend(tick.tick_list)
                            # WATER 2019 has runnables without ticks
                            #if len(self.ticks) < 1:
                            #    print ("ERROR: at least on tick is required per runnable")
                        elif value == 'LabelAccess':
                            # pass the reference to the list of labels to make a ref between Label_access <-> Label
                            label = label_access.Label_access(a, self.sw_model_ref.labels, self.verbose)
                            self.item_list.append(label)
                        else:
                            print ("WARNING: items type ",v, "is not supported")
                            #return False
        return True
