""" Amalthea code generator.

This is where the project is created and the actual code generator parts are called. 

Authors: 
    * Alexandre Amory (January 2021), ReTiS Lab, Scuola Sant'Anna, Pisa, Italy.

"""
import os, sys
from shutil import copyfile
from distutils.dir_util import copy_tree

from py4j.java_gateway import is_instance_of

# the code body is here
from app4mc.code_gen.linux_posix import task, global_vars

# these are only to generate the main()
import app4mc.code_gen.linux_posix.main as posix
import app4mc.code_gen.openmp.main as openmp

def run(model, gateway, tick_type, par_lib, base_path):
    """ Generate the C (POSIX Linux) code base for the model.

    Args:
        model (org.eclipse.app4mc.amalthea.model.Amalthea): the entire Amalthea model.
        gateway (py4j.java_gateway): py4j gateway to access the Java server.
        tick_type (str): The behavior of the Tick. The possible values are BCET, ACET, WCET, or distribution.
        par_lib (str): The parallel lib used: posix or openmp.
        base_path (str): base directory where the code will be saved.
    """
    try:
        #print ("################## makedirs ####################")
        path = str.lower(base_path)
        os.makedirs(path,exist_ok=True)
        path = str.lower(base_path) + '/src/'
        os.makedirs(path,exist_ok=True)
        path = str.lower(base_path) + '/src/tasks'
        os.makedirs(path,exist_ok=True)
        path = str.lower(base_path) + '/src/runnables'
        os.makedirs(path,exist_ok=True)
        path = str.lower(base_path) + '/src/utils'
        os.makedirs(path,exist_ok=True)
        path = str.lower(base_path) + '/include/'
        os.makedirs(path,exist_ok=True)
        path = str.lower(base_path) + '/include/tasks'
        os.makedirs(path,exist_ok=True)
        path = str.lower(base_path) + '/include/runnables'
        os.makedirs(path,exist_ok=True)
        path = str.lower(base_path) + '/include/utils'
        os.makedirs(path,exist_ok=True)        
        #print ("################## makedirs DONE ####################")
    except OSError as e:
        raise print("ERROR: could not create directories in",base_path)

    print ("Loading the Amalthea model for code generation...")
    sw_model = model.getSwModel()
    tasks = sw_model.getTasks()
    stim_model = model.getStimuliModel().getStimuli()
    hw_model = model.getHwModel()

    print ("Copying configuration files ...")
    pyapp4mc = os.environ['PYAPP4MC_HOME']
    # copy auxiliar libs
    src = pyapp4mc+"/app4mc/code_gen/linux_posix/utils/tick.cpp"
    dest = str.lower(base_path) + '/src/utils/tick.cpp'
    copyfile(src, dest)
    src = pyapp4mc+"/app4mc/code_gen/linux_posix/utils/dl_syscalls.cpp"
    dest = str.lower(base_path) + '/src/utils/dl_syscalls.cpp'
    copyfile(src, dest)
    src = pyapp4mc+"/app4mc/code_gen/linux_posix/utils/tick.h"
    dest = str.lower(base_path) + '/include/utils/tick.h'
    copyfile(src, dest)
    src = pyapp4mc+"/app4mc/code_gen/linux_posix/utils/task_utils.cpp"
    dest = str.lower(base_path) + '/src/utils/task_utils.cpp'
    copyfile(src, dest)
    src = pyapp4mc+"/app4mc/code_gen/linux_posix/utils/task_utils.h"
    dest = str.lower(base_path) + '/include/utils/task_utils.h'
    copyfile(src, dest)
    src = pyapp4mc+"/app4mc/code_gen/linux_posix/utils/label_utils.h"
    dest = str.lower(base_path) + '/include/utils/label_utils.h'
    copyfile(src, dest)
    src = pyapp4mc+"/app4mc/code_gen/linux_posix/utils/label_utils.cpp"
    dest = str.lower(base_path) + '/src/utils/label_utils.cpp'
    copyfile(src, dest)
    src = pyapp4mc+"/app4mc/code_gen/linux_posix/utils/offloading_sync.h"
    dest = str.lower(base_path) + '/include/utils/offloading_sync.h'
    copyfile(src, dest)
    src = pyapp4mc+"/app4mc/code_gen/linux_posix/utils/dl_syscalls.h"
    dest = str.lower(base_path) + '/include/utils/dl_syscalls.h'
    copyfile(src, dest)
    # copy the entire FRED lib
    src = pyapp4mc+"/app4mc/code_gen/linux_posix/tasks/fred/lib"
    dest = str.lower(base_path) + '/lib/fred/'
    copy_tree(src, dest)
    #distutils.dir_util.copy_tree


    # makefile
    src = pyapp4mc+"/app4mc/code_gen/linux_posix/utils/Makefile_template"
    dest = str.lower(base_path) + '/Makefile'
    copyfile(src, dest)
    src = pyapp4mc+"/app4mc/code_gen/linux_posix/utils/CMakeLists.txt"
    dest = str.lower(base_path) + '/CMakeLists.txt'
    copyfile(src, dest)


    print ("Generating code for tasks ...")
    for t in tasks:
        print ("TASK:", t.getName())
        task.save(t, model, gateway, tick_type, base_path)

    # getting the thread syncronization items: OSEvents and InterProcessStimulus
    sync_items = []
    for item in stim_model:
        if is_instance_of(gateway,item,gateway.jvm.org.eclipse.app4mc.amalthea.model.InterProcessStimulus):
            sync_items.append(item)
    for item in sw_model.getEvents():
        sync_items.append(item)
    
    print ("Generating code for global variables ...")
    global_vars.save(sw_model.getLabels(), sync_items, base_path)

    tasks = sw_model.getTasks()
    print ("Generating the tasks header ...")
    task.header(tasks,base_path)

    # this is the only part that is different between posix and openmp
    print ("Generating the main code ...")
    if par_lib == 'posix':
        posix.main(tasks,base_path)
    elif par_lib == 'openmp':
        openmp.main(tasks,base_path)
    else:
        print ("ERROR: unsupported library", par_lib)
        sys.exit(1)

    print ("\nCode generated successfully!")
