
#from abc import ABC, abstractmethod
import cgen as c
import math

#class base_task(ABC):
class base_task:
    """ Abstract base class for task code generators.

    Args:
        task (:class:`org.eclipse.app4mc.amalthea.model.Task`): An Amalthea task.
        model (:class:`org.eclipse.app4mc.amalthea.model.Amalthea`): The entire Amalthea model.
        gateway (:class:`py4j.java_gateway.JavaGateway`): py4j gateway. 
        time_type (:class:`org.eclipse.app4mc.amalthea.model.util.RuntimeUtil.TimeType`): The behavior of the Tick. The possible values are BCET, ACET, WCET.
    """

    def __init__(self, task, model, gateway, time_type):
        # model related attributes
        super().__init__()
        self.task = task
        self.model = model
        self.gateway = gateway
        self.time_type = time_type
        self.runnables = self.gateway.jvm.org.eclipse.app4mc.amalthea.model.util.SoftwareUtil.getRunnableList(self.task,None)
        task_mapping_tuple = self.__get_task_mapping()
        if task_mapping_tuple == ():
            print ("ERROR: task %s has no mapping" % (self.task.getName()))
            #return
        # tuple unpacking
        self.pu, self.pu_name, self.pu_def_name, self.pu_type_name = task_mapping_tuple
        # use only microseconds as time unit
        self.us = self.gateway.jvm.org.eclipse.app4mc.amalthea.model.TimeUnit.US
        # this is the sum of the runnables runtimes. Only works if the task has a unique mapping
        # by default, getExecutionTimeForProcess returns Time in ps.
        self.task_runtime = self.gateway.jvm.org.eclipse.app4mc.amalthea.model.util.RuntimeUtil.getExecutionTimeForProcess(self.task, self.pu, None, self.time_type).getValue().doubleValue()
        # this function will ceil the resulting value, so it wont work
        #self.task_runtime = self.gateway.jvm.org.eclipse.app4mc.amalthea.model.util.TimeUtil.convertToTimeUnit(truntime_ps, self.us)
        # make time in us
        self.task_runtime /= 1000000.0
        # this will avoid task_runtime == 0
        self.task_runtime = math.ceil(self.task_runtime)


    #@abstractmethod
    def generate_code(self):
        pass

    #@abstractmethod
    def __generate_includes(self, code_block):
        pass

    #@abstractmethod
    def __generate_initialization(self, code_block):
        pass

    #@abstractmethod
    def __generate_task_comments(self, code_block):
        pass

    #@abstractmethod
    def __generate_runnable_call(self, code_block):
        pass

    #@abstractmethod
    def __generate_task_body(self, code_block):
        pass

    def __get_task_mapping(self) -> tuple():
        """ Extract the task mapping information out of the model.

        Returns:
            tuple: 
                * Returns an empty if there is no mapping for this task.
                * Return tuple type is (pu, pu_name, pu_def_name, pu_type_name).
        """
        # These data must return in the tuple
        pu_name = ""
        pu_def_name = ""
        pu_type_name = ""
        # get the processing unit mapped for this task. PUs is a java Set, not an Array
        pu = None
        PUs = self.gateway.jvm.org.eclipse.app4mc.amalthea.model.util.DeploymentUtil.getAssignedCoreForProcess(self.task,self.model)
        if (len(PUs) != 1):
            print ("WARNING: expecting a task mapped to a single PU")
        # this loop is getting the 1st element of the java Set
        for p in PUs:
            # save the 1st element of the Set
            pu = p
            break
        pu_name = pu.getName()
        pu_def_name = pu.getDefinition().getName()
        pu_type_name = pu.getDefinition().getPuType().getName()
        
        return (pu, pu_name, pu_def_name, pu_type_name)

    #@abstractmethod
    def __test_requirements(self):
        """ Place here all validity checks for each type of task.
        """
