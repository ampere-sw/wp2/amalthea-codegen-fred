import cgen as c # for code_gen
from py4j.java_gateway import is_instance_of

from app4mc.code_gen.linux_posix.tasks.base_task import base_task

class OpenMP(base_task):
    """ Code generator for OpenMP-based tasks for GPUs.

    Args:
        task (:class:`org.eclipse.app4mc.amalthea.model.Task`): An Amalthea task.
        model (:class:`org.eclipse.app4mc.amalthea.model.Amalthea`): The entire Amalthea model.
        gateway (:class:`py4j.java_gateway.JavaGateway`): py4j gateway. 
    
    TODO:
        * Currently, only the shell code for GPU/OpenMP is generated.
          Use this example as a starting point https://github.com/matthiasdiener/omptest/blob/master/testomp.cpp
          https://github.com/TApplencourt/OvO/blob/master/test_src/cpp/hierarchical_parallelism/reduction-float/target_parallel.cpp
          https://stackoverflow.com/questions/43788776/how-to-configure-gcc-for-openmp-4-5-offloading-to-nvidia-ptx-gpgpus
          https://developer.ibm.com/technologies/systems/articles/gpu-programming-with-openmp/
          

    """

    def __init__(self, task, model, gateway, time_type):
        base_task.__init__(self,task, model, gateway, time_type)
        self.event_name = ""
        # test if the GPU task is correctly defined
        if not self.__test_requirements():
            print ("ERROR: Aborting GPU task generation due to missing requirements")
        # InterProcessTrigger required to fire the GPU execution
        self.ipc_stim_name = self.task.getStimuli()[0].getName()
        # TODO how to findout the event name ?

    def generate_code(self):
        """ The only method called externally to start the code generation.
        """
        # the task function declaration
        task_code = c.Module([])
        function_body = c.Block()

        task_code = self.__generate_includes(task_code)
        task_code = self.__generate_task_comments(task_code)
        function_body = self.__generate_initialization(function_body)
        function_body = self.__generate_task_body(function_body)
        task_code.append(c.FunctionBody(c.FunctionDeclaration((c.Value("void", self.task.getName())), []),
                function_body))         

        return task_code

    def __generate_includes(self, code_block):
        """ Adds the includes and defines for GPU.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.
        """
        filename = str.lower(self.task.getName()) + '.h'
        code_block.append(c.Include("cstdint"))
        code_block.append(c.Include("iostream"))
        code_block.append(c.Include("mutex"))
        code_block.append(c.Include("unistd.h"))
        code_block.append(c.Include("task_utils.h", False))
        code_block.append(c.Include("offloading_sync.h", False))
        code_block.append(c.Include(filename, False))
        code_block.append(c.Line(""))        

        return code_block


    def __generate_initialization(self, code_block):
        """ Include the additional statements to initialize the OpenMP runtime.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.
        """

        code_block.append(c.Line("//init"))
        code_block.append(c.Statement("uint32_t iter = 0"))
        code_block.append(c.Statement("extern struct offloading_sync sync_"+self.ipc_stim_name))
        code_block.append(c.Statement("extern struct offloading_sync sync_"+self.event_name))
        code_block.append(c.Line(""))

        return code_block

    def __generate_task_comments(self, code_block):
        """ Comments added before the task function declararion.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.

        """
        task_description = """
    ###############################
    TASK DESCRIPTION
    Stimuli type: %s
    Mapped PU: %s
    PU type: %s
    PU definition: %s
    ###############################
    """ % ("InterProcessStimulus",self.pu_name,self.pu_type_name,self.pu_def_name)
        code_block.append(c.Comment(task_description))
        return code_block

    def __generate_runnable_call(self, code_block):
        """ Include the additional statements to call a GPU task.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.
        """
        code_block.append(c.Line("//gpu runnable calls"))

        task_items = self.task.getActivityGraph().getItems()
        for task_item in task_items:
            if is_instance_of(self.gateway, task_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.Group):
                # first, just check the supported items
                for group_item in task_item.getItems():
                    if is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.RunnableCall):
                        pass
                    elif is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.SetEvent):
                        pass
                    else:
                        print ("WARNING: task item '%s' not supported for OpenMP tasks" % group_item.getName())
                # genererate the code for the runnables first
                # this additional block is requred to limit the scope of the lock
                sync_block = c.Block()
                sync_block.append(c.Line("// wait until offloading is unblocked by the HOST ..."))
                sync_block.append(c.Statement("std::unique_lock<std::mutex> lck(sync_%s.mtx)" % self.ipc_stim_name))
                sync_block.append(c.While("!sync_%s.ready" % self.ipc_stim_name, c.Block([c.Statement("sync_%s.cvar.wait(lck)" % self.ipc_stim_name)])))
                sync_block.append(c.Line("// executes the runnables and perform the GPU offloading with OpenMP ..."))
                #sync_block.append(c.Statement("cv_%s.wait(lck)" % self.event_name))
                for group_item in task_item.getItems():
                    if is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.RunnableCall):
                       sync_block.append(c.Statement(group_item.getRunnable().getName()+"()"))
                #sync_block.append(c.Statement("lck.unlock()"))
                sync_block.append(c.Statement("sync_%s.ready = false" % self.ipc_stim_name))
                code_block.append(sync_block)
                # generete the unblocking code
                for group_item in task_item.getItems():
                    if is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.SetEvent):
                        self.use_mutex = True
                        code_block.append(c.Line("// unlocking the HOST ..."))
                        code_block.append(c.Statement("std::unique_lock<std::mutex> lck2(sync_%s.mtx)" % self.event_name))
                        code_block.append(c.Statement("sync_%s.ready = true" % self.event_name))
                        code_block.append(c.Statement("sync_%s.cvar.notify_one()" % self.event_name))


        return code_block

    def __generate_task_body(self, code_block):
        """ The task main loop.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.

        """
        inner_task_loop_block = c.Block()

        # TODO missing the C++ conditional_variables to control the task execution
        inner_task_loop_block = self.__generate_runnable_call(inner_task_loop_block)
        inner_task_loop_block.append(c.Line(""))
        inner_task_loop_block.append(c.Line("#ifdef DEBUG\n"))
        inner_task_loop_block.append(c.Statement("std::cout << \"%s: \" << iter << \"\\n\"" % (self.task.getName())))
        inner_task_loop_block.append(c.Statement("iter++"))
        inner_task_loop_block.append(c.Line("#endif\n"))

        # task main loop    
        code_block.append(c.Line("// the main task code, where the runnables are called"))
        code_block.append(c.While("1", inner_task_loop_block))
        
        return code_block

    def __test_requirements(self):
        """ Test here all the requirements for a GPU task.

        Returns:
            Boolean: Returns True if the requirements are met. Returns False otherwise.
        """
        if len(self.task.getStimuli()) != 1:
            print ("ERROR: a GPU task must have a single Stimulus of type InterProcess")
            return False
        if not is_instance_of(self.gateway, self.task.getStimuli()[0], self.gateway.jvm.org.eclipse.app4mc.amalthea.model.InterProcessStimulus):
            print ("ERROR: a GPU task must have a InterProcessTrigger")
            return False
        
        # check if the task has the required activityItems
        task_items = self.task.getActivityGraph().getItems()
        gpu_runnable = False
        for task_item in task_items:
            if is_instance_of(self.gateway, task_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.Group):
                for group_item in task_item.getItems():
                    if is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.RunnableCall):
                        # must have only one runnable call and this runnable must have certain CustomAttributes
                        gpu_runnable = True
                    elif is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.SetEvent):
                        events = group_item.getEventMask().getEvents()
                        if len(events) != 1:
                            print ("WARNINIG: Expecting a single Event for task %s" % self.task.getName())
                        # an OSEvent
                        self.event_name = events[0].getName()
                    else:
                        print ("ERROR: a GPU task must have one RunnableCall and one SetEvent")
                        return False
            else:
                print ("ERROR: a GPU task must have only a Group in the ActivityGraph")
                return False

        if not gpu_runnable:
            print ("ERROR: The OpenMP task %s must have at least one Runnable with GPU ticks")
            return False
        # TODO check mapping attributes

        return True

