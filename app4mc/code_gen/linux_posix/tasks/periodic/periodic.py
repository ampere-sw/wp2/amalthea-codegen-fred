import cgen as c # for code_gen
from py4j.java_gateway import is_instance_of

from app4mc.code_gen.linux_posix.tasks.base_task import base_task

class Periodic(base_task):
    """ Code generator for ordinary periodic tasks for multicores.

    Args:
        task (:class:`org.eclipse.app4mc.amalthea.model.Task`): An Amalthea task.
        model (:class:`org.eclipse.app4mc.amalthea.model.Amalthea`): The entire Amalthea model.
        gateway (:class:`py4j.java_gateway.JavaGateway`): py4j gateway. 
        time_type (:class:`org.eclipse.app4mc.amalthea.model.util.RuntimeUtil.TimeType`): The behavior of the Tick. The possible values are BCET, ACET, WCET.
    """

    def __init__(self, task, model, gateway, time_type):
        base_task.__init__(self,task, model, gateway, time_type)
        # the core where this task will be mapped
        self.core_id = self.__get_core_id()

        # attributes of a period task
        # if this task is indeed a periodic task, then it must return these values
        self.task_period = 0
        # offset is optional
        self.offset_time = 0
        # deadline is optional
        self.deadline_time = 0
        # TODO minDistance of PeriodicStimulus is not implemented yet
        self.min_distance_time = 0 
        # TODO Jitter of PeriodicStimulus is not implemented yet
        self.jitter = 0
        # this call will fill all the previous attributes
        self.__get_task_data()
        # a flag to say whether it is necessary to declare mutexs and conditional_variables
        self.use_mutex = False
        self.event_name = ""
        self.ipc_stim_name = ""
        # testing if the periodic task description is complete. it also assings the 3 previous attributes
        if not self.__test_requirements():
            print ("ERROR: Aborting periodic task generation due to missing requirements")


    def generate_code(self):
        """ The only method called externally to start the code generation.
        """
        # the task function declaration
        task_code = c.Module([])
        function_body = c.Block()

        task_code = self.__generate_includes(task_code)
        task_code = self.__generate_task_comments(task_code)
        function_body = self.__generate_initialization(function_body)
        function_body = self.__generate_task_body(function_body)
        task_code.append(c.FunctionBody(c.FunctionDeclaration((c.Value("void", self.task.getName())), []),
                function_body))         

        return task_code

    def __generate_includes(self, code_block):
        """ Adds the defines and include required by a periodic task.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.
        """
        filename = str.lower(self.task.getName()) + '.h'
        code_block.append(c.Include("cstdint"))
        code_block.append(c.Include("iostream"))
        code_block.append(c.Include("unistd.h"))
        code_block.append(c.Include("time.h"))
        code_block.append(c.Include("sched.h"))
        code_block.append(c.Include("dl_syscalls.h", False))
        code_block.append(c.Include("task_utils.h", False))
        if (self.use_mutex):
            code_block.append(c.Include("mutex"))
            code_block.append(c.Include("offloading_sync.h", False))
        code_block.append(c.Include(filename, False))
        code_block.append(c.Line(""))        

        code_block.append(c.Line("//Assuming SCHED_DEADLINE Linux Policy. period, deadline, and runtime are in microseconds"))
        code_block.append(c.Line("#define TASK_PERIOD   %d" % self.task_period))
        code_block.append(c.Line("#define TASK_RUNTIME  %d" % self.task_runtime))
        code_block.append(c.Line("#define TASK_DEADLINE %d" % self.deadline_time))

        return code_block

    def __generate_initialization(self, code_block):
        """ Include the additional statements to initialize the the periodic task.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.
        """        
        #code_block = c.Block()
        code_block.append(c.Line("// local variables and initialization procedures"))
        code_block.append(c.Statement("uint32_t iter=0"))
        code_block.append(c.Statement("struct timespec ts"))
        if self.use_mutex:
            code_block.append(c.Line("// thread sync variables"))
            code_block.append(c.Statement("extern struct offloading_sync sync_"+self.ipc_stim_name))
            code_block.append(c.Statement("extern struct offloading_sync sync_"+self.event_name))
        code_block.append(c.Line(""))
        # TODO: CLOCK_MONOTONIC_RAW is only is only available in Linux 2.6.28+.
        code_block.append(c.Line("// timing definitions"))
        code_block.append(c.Statement("struct period_info pinfo"))
        code_block.append(c.Statement("pinfo.period_ns = TASK_PERIOD"))
        code_block.append(c.Statement("periodic_task_init(&pinfo)"))

        if self.offset_time != 0:
            code_block.append(c.Line("// the task offset"))
            code_block.append(c.Statement("timespec_add_us(&ts, %d)" % (self.offset_time)))
            code_block.append(c.Statement("clock_nanosleep(CLOCK_MONOTONIC_RAW, TIMER_ABSTIME, &ts, NULL)"))

        code_block.append(c.Line(""))
        code_block.append(c.Line("// the task mapping definition"))
        code_block.append(c.Statement("cpu_set_t cpuset"))
        code_block.append(c.Statement("int cpu = %d" % (self.core_id)))
        code_block.append(c.Statement("CPU_ZERO(&cpuset)"))
        code_block.append(c.Statement("CPU_SET( cpu , &cpuset)"))
        then = c.Block([c.Statement("perror(\"Error sched_setaffinity()\")")])
        code_block.append(c.If("sched_setaffinity(0, sizeof(cpuset), &cpuset) < 0",then))
        code_block.append(c.Line(""))

        # set scheduling attributes
        code_block.append(c.Line("// setting the scheduling attributes"))
        code_block.append(c.Statement("struct sched_attr sa = {0}"))
        code_block.append(c.Statement("sa.size           = sizeof(sa)"))
        code_block.append(c.Statement("sa.sched_policy   = SCHED_DEADLINE"))
        code_block.append(c.Line("// https://www.kernel.org/doc/Documentation/scheduler/sched-deadline.txt"))
        code_block.append(c.Line("// time in microseconds for SCHED_DEADLINE"))
        code_block.append(c.Statement("sa.sched_runtime  = TASK_RUNTIME"))
        code_block.append(c.Statement("sa.sched_deadline = TASK_DEADLINE"))
        code_block.append(c.Statement("sa.sched_period   = TASK_PERIOD"))
        then = c.Block([c.Statement("perror(\"Error sched_setattr()\")")])
        code_block.append(c.If("sched_setattr(0, &sa, 0) < 0",then))
        code_block.append(c.Line(""))  
        return code_block


    def __generate_task_comments(self,code_block):
        """ Comments added before the task function declararion.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.

        """
        task_description = """
    ###############################
    TASK DESCRIPTION
    Stimuli type: %s
    Mapped PU: %s
    PU type: %s
    PU definition: %s
    ###############################
    """ % ("PeriodicStimuli",self.pu_name,self.pu_type_name,self.pu_def_name)        
        code_block.append(c.Comment(task_description))
        return code_block


    def __generate_runnable_call(self,code_block):
        """ Include the additional statements to call a periodic task.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.
        """

        code_block.append(c.Line("// runnable calls"))
        task_items = self.task.getActivityGraph().getItems()
        for task_item in task_items:
            if is_instance_of(self.gateway, task_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.Group):
                for group_item in task_item.getItems():
                    if is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.RunnableCall):
                        code_block.append(c.Statement(group_item.getRunnable().getName()+"()"))
                    elif is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.InterProcessTrigger):
                        # this additional block is requred to limit the scope of the lock
                        sync_block = c.Block([])
                        sync_block.append(c.Line("// unlocking device offloading..."))
                        sync_block.append(c.Statement("std::unique_lock<std::mutex> lck(sync_%s.mtx)" % self.ipc_stim_name))
                        sync_block.append(c.Statement("sync_%s.ready = true" % self.ipc_stim_name))
                        sync_block.append(c.Statement("sync_%s.cvar.notify_one()" % self.ipc_stim_name))
                        code_block.append(sync_block)
                    elif is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.WaitEvent):
                        code_block.append(c.Line("// blocking until device offloading is finished ..."))
                        code_block.append(c.Statement("std::unique_lock<std::mutex> lck2(sync_%s.mtx)" % self.event_name))
                        code_block.append(c.While("sync_%s.ready" % self.event_name, c.Block([c.Statement("sync_%s.cvar.wait(lck2)" % self.event_name)])))
                    elif is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.ClearEvent):
                        code_block.append(c.Statement("lck2.unlock()"))
                        code_block.append(c.Statement("sync_%s.ready = false" % self.event_name))
                    else:
                        print ("WARNING: task item '%s' not supported" % group_item.getName())
        
        return code_block

    def __generate_task_body(self,code_block):
        """ The task main loop.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.

        """
        #code_block = c.Block()
        inner_task_loop_block = c.Block()
        inner_task_loop_block = self.__generate_runnable_call(inner_task_loop_block)
        inner_task_loop_block.append(c.Line(""))
        inner_task_loop_block.append(c.Line("// checking the task period\n"))
        inner_task_loop_block.append(c.Statement("wait_rest_of_period(&pinfo)"))
        inner_task_loop_block.append(c.Line("#ifdef DEBUG\n"))
        inner_task_loop_block.append(c.Statement("std::cout << \"%s: \" << iter << \"\\n\"" % (self.task.getName())))
        inner_task_loop_block.append(c.Statement("iter++"))
        inner_task_loop_block.append(c.Line("#endif\n"))

        # task main loop    
        code_block.append(c.Line("// the main task code, where the runnables are called"))
        code_block.append(c.While("1", inner_task_loop_block))

        return code_block

    def __get_core_id(self):
        """ It uses the PU name to extract the core ID required to mapped the task.

        Returns:
            int: Code id extracted from the PU name.
        
        TODO:
            * make a more robust and less restric method to extract the code ID from the PU name.
        """
        # write the part of the code related to the task mapping into a multicore 
        core_id = 0
        if self.pu_type_name == "CPU":
            pu_str_lwr = self.pu_name.lower()
            if "core" in pu_str_lwr:
                # extract the core number out of the PU name
                core_id_str = pu_str_lwr.split('core')[1]
                if core_id_str.isnumeric():
                    core_id = int(core_id_str)
                else:
                    print ("WARNING: expecting PU name with the pattern 'Core<int>'")
            else:
                print ("WARNING: expecting PU name with the pattern 'Core<int>'")
        return core_id


    def __get_task_data(self):
        """ Query the model to get information required by periodic tasks.

        This method writes in the periodic task attributes:
            * period, 
            * deadline, 
            * offset, 
            * min_distance, 
            * jitter

        TODO: 
            * The attributes jitter and minDistance are not implemented for code generation.
        """
        stimuli = self.task.getStimuli()
        deadline = self.gateway.jvm.org.eclipse.app4mc.amalthea.model.util.ConstraintsUtil.getDeadline(self.task)
        if len(stimuli) != 1:
            print ("ERROR: expecinting a Stimuli definition for task",self.task.getName())
        else:
            stimulus = stimuli[0]
            ###############################
            ### test for periodic task ####
            ###############################
            if is_instance_of(self.gateway,stimulus,self.gateway.jvm.org.eclipse.app4mc.amalthea.model.PeriodicStimulus):
                
                # the recurrence attribute is  considered mandatory for periodic tasks
                if stimulus.getRecurrence() == None:
                    print ("ERROR: the Stimuli recurrence for task",self.task.getName(), "is not defined")
                # convert the Amalthea time to 'str value'
                self.task_period = self.gateway.jvm.org.eclipse.app4mc.amalthea.model.util.TimeUtil.convertToTimeUnit(stimulus.getRecurrence(), self.us).getValue().longValue()

                # get the deadline constraints related to this task
                if deadline != None:
                    self.deadline_time = self.gateway.jvm.org.eclipse.app4mc.amalthea.model.util.TimeUtil.convertToTimeUnit(deadline, self.us).getValue().longValue()

                # the offset attribute is optional
                if stimulus.getOffset() != None:
                    # convert the Amalthea time to 'str value'
                    self.offset_time = self.gateway.jvm.org.eclipse.app4mc.amalthea.model.util.TimeUtil.convertToTimeUnit(stimulus.getOffset(), self.us).getValue().longValue()
                # TODO test the definition of jitter, and minDistance, which are not implemented, just to avoid to fail silently
                if stimulus.getMinDistance() != None:
                    print ("WARNING: the model defines MinDistance for task",self.task.getName()," but this feature is not implemented!")
                if stimulus.getJitter() != None:
                    print ("WARNING: the model defines Jitter for task",self.task.getName()," but this feature is not implemented!")
            else:
                # unsupported type of task stimuli for task
                print ("ERROR: A periodic task requires a PeriodicStimulus")


    def __test_requirements(self):
        """ Test here all the requirements for a periodic task.

        This method writes into attributes:
            * self.use_mutex
            * self.event_name
            * self.ipc_stim_name

        Returns:
            Boolean: Returns True if the requirements are met. Returns False otherwise.

        TODO: 
            * test all the required CustomProperties
        """
        if len(self.task.getStimuli()) != 1:
            print ("ERROR: a Periodic task must have a single Stimulus of type PeriodicStimulus")
            return False
        if not is_instance_of(self.gateway, self.task.getStimuli()[0], self.gateway.jvm.org.eclipse.app4mc.amalthea.model.PeriodicStimulus):
            print ("ERROR: a Periodic task must have a Stimulus of type PeriodicStimulus")
            return False
        
        if self.task.getActivityGraph() == None:
            print ("ERROR: ActivityGraph not found for task", self.task.getName())
            return False
        if len(self.task.getActivityGraph().getItems()) == 0:
            print ("ERROR: ActivityGraph for task", self.task.getName(), 'is empty')
            return False

        # check if the task has the required activityItems
        task_items = self.task.getActivityGraph().getItems()
        for task_item in task_items:
            if is_instance_of(self.gateway, task_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.Group):
                for group_item in task_item.getItems():
                    if is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.RunnableCall):
                        # must have only one runnable call and this runnable must have certain CustomAttributes
                        fred_runnable = group_item.getRunnable()
                    elif is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.InterProcessTrigger):
                        stim = group_item.getStimulus()
                        self.ipc_stim_name = stim.getName()
                        self.use_mutex = True
                    elif is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.WaitEvent):
                        self.use_mutex = True
                        events = group_item.getEventMask().getEvents()
                        if len(events) != 1:
                            print ("WARNINIG: Expecting a single Event for task %s" % self.task.getName())
                        # an OSEvent
                        self.event_name = events[0].getName()
                    elif is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.SetEvent):
                        self.use_mutex = True
                    elif is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.ClearEvent):
                        self.use_mutex = True
                    else:
                        print ("ERROR: task item '%s' not supported" % group_item.getName())
                        return False
            else:
                print ("ERROR: a Periodic task must have only a Group in the ActivityGraph")
                return False

        # TODO check mapping attributes
        # TODO check stimuli attributes

        # TODO check if the Runnable is compatible with FRED

        return True