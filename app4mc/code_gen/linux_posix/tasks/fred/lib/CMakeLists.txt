message("Found FRED.")
#project(fred VERSION 1.0 DESCRIPTION "FRED runtime for Zynq FPGA offloading")

set(CMAKE_C_FLAGS "-std=gnu99 -O2 -Wall -Werror -fpic ")

include_directories(
        ${CMAKE_CURRENT_SOURCE_DIR}/
        ${CMAKE_CURRENT_SOURCE_DIR}/fred_support
)

add_library(fred SHARED
    fred_lib.c
    fred_support/user_buff.c
)
