import cgen as c # for code_gen
from py4j.java_gateway import is_instance_of

from app4mc.code_gen.linux_posix.tasks.base_task import base_task

class FRED(base_task):
    """ Code generator for FRED-based tasks for FPGAs.

    Args:
        task (:class:`org.eclipse.app4mc.amalthea.model.Task`): An Amalthea task.
        model (:class:`org.eclipse.app4mc.amalthea.model.Amalthea`): The entire Amalthea model.
        gateway (:class:`py4j.java_gateway.JavaGateway`): py4j gateway. 
    
    TODO:
        The FRED buffer configuration is not fixed. I need to get this info somehow from the model.
    """

    def __init__(self, task, model, gateway, time_type):
        base_task.__init__(self,task, model, gateway, time_type)

        self.event_name = ""
        self. hw_id = -1
        # test if the FRED task is correctly defined
        if not self.__test_requirements():
            print ("ERROR: Aborting FRED task generation due to missing requirements")
        # InterProcessTrigger required to fire the FPGA execution
        self.ipc_stim_name = self.task.getStimuli()[0].getName()
        # TODO how to findout the event name ?

    def generate_code(self):
        """ The only method called externally to start the code generation.
        """
        # the task function declaration
        task_code = c.Module([])
        function_body = c.Block()

        task_code = self.__generate_includes(task_code)
        task_code = self.__generate_task_comments(task_code)
        function_body = self.__generate_initialization(function_body)
        function_body = self.__generate_task_body(function_body)
        task_code.append(c.FunctionBody(c.FunctionDeclaration((c.Value("void", self.task.getName())), []),
                function_body))         

        return task_code

    def __generate_includes(self, code_block):
        """ Adds the includes and defines for FRED.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.
        """
        filename = str.lower(self.task.getName()) + '.h'
        code_block.append(c.Include("cstdint"))
        code_block.append(c.Include("iostream"))
        code_block.append(c.Include("mutex"))
        code_block.append(c.Include("unistd.h"))
        code_block.append(c.Include("sched.h"))
        code_block.append(c.Include("task_utils.h", False))
        code_block.append(c.Include("offloading_sync.h", False))
        code_block.append(c.Include(filename, False))
        code_block.append(c.Line(""))

        return code_block


    def __generate_initialization(self, code_block):
        """ Include the additional statements to initialize the FRED runtime.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.

        TODO:
            * Assuming only one FRED runnable per task. If an application requires more FRED runnables, then split it into several tasks.
        """

        code_block.append(c.Statement("uint32_t iter = 0"))
        code_block.append(c.Line("// thread sync variables"))
        code_block.append(c.Statement("extern struct offloading_sync sync_"+self.ipc_stim_name))
        code_block.append(c.Statement("extern struct offloading_sync sync_"+self.event_name))
        code_block.append(c.Line(""))

        return code_block

    def __generate_task_comments(self, code_block):
        """ Comments added before the task function declararion.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.

        """
        task_description = """
    ###############################
    TASK DESCRIPTION
    Stimuli type: %s
    Mapped PU: %s
    PU type: %s
    PU definition: %s
    FRED Hw id: %d
    ###############################
    """ % ("InterProcessStimulus",self.pu_name,self.pu_type_name,self.pu_def_name, self.hw_id)
        code_block.append(c.Comment(task_description))

        return code_block

    def __generate_runnable_call(self, code_block):
        """ Include the additional statements to call a FRED hw runnable.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.
        """
        task_items = self.task.getActivityGraph().getItems()
        for task_item in task_items:
            if is_instance_of(self.gateway, task_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.Group):
                # first, just check the supported items
                for group_item in task_item.getItems():
                    if is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.RunnableCall):
                        pass
                    elif is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.SetEvent):
                        pass
                    else:
                        print ("WARNING: task item '%s' not supported for FRED tasks" % group_item.getName())
                # genererate the code for the runnables first
                # this additional block is requred to limit the scope of the lock
                sync_block = c.Block()
                sync_block.append(c.Line("// wait until offloading is unblocked by the HOST ..."))
                sync_block.append(c.Statement("std::unique_lock<std::mutex> lck(sync_%s.mtx)" % self.ipc_stim_name))
                sync_block.append(c.While("!sync_%s.ready" % self.ipc_stim_name, c.Block([c.Statement("sync_%s.cvar.wait(lck)" % self.ipc_stim_name)])))
                sync_block.append(c.Line("// executes the runnables and perform the FPGA offloading with FRED ..."))
                #sync_block.append(c.Statement("cv_%s.wait(lck)" % self.event_name))
                for group_item in task_item.getItems():
                    if is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.RunnableCall):
                        sync_block.append(c.Statement(group_item.getRunnable().getName()+"()"))
                #sync_block.append(c.Statement("lck.unlock()"))
                sync_block.append(c.Statement("sync_%s.ready = false" % self.ipc_stim_name))
                code_block.append(sync_block)
                # generete the unblocking code
                for group_item in task_item.getItems():
                    if is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.SetEvent):
                        self.use_mutex = True
                        code_block.append(c.Line("// unlocking the HOST ..."))
                        code_block.append(c.Statement("std::unique_lock<std::mutex> lck2(sync_%s.mtx)" % self.event_name))
                        code_block.append(c.Statement("sync_%s.ready = true" % self.event_name))
                        code_block.append(c.Statement("sync_%s.cvar.notify_one()" % self.event_name))


        return code_block

    def __generate_task_body(self, code_block):
        """ The task main loop.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.

        """
        inner_task_loop_block = c.Block()

        # TODO missing the C++ conditional_variables to control the task execution
        inner_task_loop_block = self.__generate_runnable_call(inner_task_loop_block)
        inner_task_loop_block.append(c.Line(""))
        inner_task_loop_block.append(c.Line("#ifdef DEBUG\n"))
        inner_task_loop_block.append(c.Statement("std::cout << \"%s: \" << iter << \"\\n\"" % (self.task.getName())))
        inner_task_loop_block.append(c.Statement("iter++"))
        inner_task_loop_block.append(c.Line("#endif\n"))


        # task main loop    
        code_block.append(c.Line("// the main task code, where the runnables are called"))
        code_block.append(c.While("1", inner_task_loop_block))
        
        return code_block



    def __get_hw_id_from_runnable(self, runnable) -> int:
        """ Return the customProperties called hw_id of Hw runnable using FRED.

        See the example of attributes expected by a Hw runnable with FRED.

        Args:
            runnable (:class:`org.eclipse.app4mc.amalthea.model.Task`): An Amalthea runnable.

        Returns:
            int: FRED hw runnable id.

        .. code-block:: XML

            <items xsi:type="am:Group" name="FPGA" ordered="true">
            <customProperties key="fpga_clb">
                <value xsi:type="am:IntegerObject" value="20"/>
            </customProperties>
            <customProperties key="fpga_dsp">
                <value xsi:type="am:IntegerObject" value="5"/>
            </customProperties>
            <customProperties key="fpga_bram">
                <value xsi:type="am:IntegerObject" value="2"/>
            </customProperties>
            <customProperties key="hw_id">
                <value xsi:type="am:IntegerObject" value="1"/>
            </customProperties>
        """
        hw_id = -1
        # search if the runnable has customProperty 'hw_id'.
        # there should be only one runnable with this property in the list of runnables
        runnable_items = runnable.getActivityGraph().getItems()
        for r_item in runnable_items:
            # The properties are expected to be found under a Group called FPGA
            if is_instance_of(self.gateway, r_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.Group):
                if r_item.getName() == "FPGA":
                    # get the CustomProperty of the FPGA group
                    for cp in r_item.getCustomProperties():
                        # cp is of type EMap
                        # EMap<String, Value> getCustomProperties();
                        if cp.getKey() == "hw_id":
                            hw_id = cp.getValue().getValue()

        # if this is true, then the task does not have the requires attributes
        #if hw_id == -1:
        #    print ("ERROR: A FRED task must have the custom attribute 'hw_id'")
        #    hw_id = 0

        return hw_id

    def __test_requirements(self):
        """ Test here all the requirements for a FRED task.

        This procedure sets the FRED hw_task id.

        Returns:
            Boolean: Returns True if the requirements are met. Returns False otherwise.

        TODO: 
            * test all the required CustomProperties
        """
        if len(self.task.getStimuli()) != 1:
            print ("ERROR: a FRED task must have a single Stimulus of type InterProcess")
            return False
        if not is_instance_of(self.gateway, self.task.getStimuli()[0], self.gateway.jvm.org.eclipse.app4mc.amalthea.model.InterProcessStimulus):
            print ("ERROR: a FRED task must have a InterProcessTrigger")
            return False
        
        # check if the task has the required activityItems
        task_items = self.task.getActivityGraph().getItems()
        fred_runnable = False
        for task_item in task_items:
            if is_instance_of(self.gateway, task_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.Group):
                for group_item in task_item.getItems():
                    if is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.RunnableCall):
                        # must have only one runnable call and this runnable must have certain CustomAttributes
                        # TODO: there are two contitions to be a FRED runnable, to have hw_id CustomAttribute and to have ticks of 
                        # a ProcessingUnit configures as Accelerator. This code is only testing for the 1st condition
                        hw_id = self.__get_hw_id_from_runnable(group_item.getRunnable())
                        if hw_id > 0:
                            self.hw_id = hw_id
                            #this is a FRED runnable
                            fred_runnable = True
                    elif is_instance_of(self.gateway, group_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.SetEvent):
                        events = group_item.getEventMask().getEvents()
                        if len(events) != 1:
                            print ("WARNINIG: Expecting a single Event for task %s" % self.task.getName())
                        # an OSEvent
                        self.event_name = events[0].getName()
                    else:
                        print ("ERROR: a FRED task must have one RunnableCall and one SetEvent")
                        return False
            else:
                print ("ERROR: a FRED task must have only a Group in the ActivityGraph")
                return False

        if not fred_runnable:
            print ("ERROR: The FPGA task %s must have at least one Runnable with FRED CustomProperties")
            return False
        # TODO check mapping attributes

        # TODO check if the Runnable is compatible with FRED

        return True
