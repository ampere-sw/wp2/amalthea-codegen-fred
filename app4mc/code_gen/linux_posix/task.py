""" Generates a C header/code files of a task.

Authors: 
    * Alexandre Amory (November 2020), ReTiS Lab, Scuola Sant'Anna, Pisa, Italy.

"""
import cgen as c # for code_gen
from py4j.java_gateway import is_instance_of

from app4mc.code_gen.linux_posix import runnable
# these are the types of tasks currently supported
from app4mc.code_gen.linux_posix.tasks.periodic.periodic import Periodic
from app4mc.code_gen.linux_posix.tasks.fred.fred import FRED
from app4mc.code_gen.linux_posix.tasks.openmp.openmp import OpenMP


def save(task, model, gateway, tick_type, base_path):
    """ Generates a C header/code files of a task.

    Refer to https://www.kernel.org/doc/Documentation/scheduler/sched-deadline.txt for more information about the scheduling algoritm.

    Args:
        task (:class:`org.eclipse.app4mc.amalthea.model.Task`): An Amalthea task.
        model (:class:`org.eclipse.app4mc.amalthea.model.Amalthea`): the entire Amalthea model.
        gateway (:class:`py4j.java_gateway.JavaGateway`): py4j gateway. 
        tick_type (str): The behavior of the Tick. The possible values are BCET, ACET, WCET, or distribution.
        base_path (str): base directory where the code will be saved.
    
    Todo:
        * The supported types of stimuli for task is PeriodicStimulus
        * Attributes jitter, and minDistance of PeriodicStimulus are not supported
        * The supported PU type is only CPU and Accelerator/FPGA. GPU is ot supported.
        * When PU type == CPU, then the PU names must be 'CoreX'
        * LIMITATION: this is a limitation of the model. taskAllocation with multiple affinity mappings is not supported. For example:

        .. code-block:: XML

          <taskAllocation task="PRE_SFM_gpu_POST?type=Task" scheduler="Scheduler_Denver?type=TaskScheduler" affinity="Core0?type=ProcessingUnit Core1?type=ProcessingUnit">

        * How to model sporadic tasks, represented by RelativePeriodicStimulus in waters17 ?
        * According to https://github.com/atdt/monotonic/issues/1
          CLOCK_MONOTONIC_RAW is only is only available in Linux 2.6.28+.
          So, it would be safer to check the Linux kernel version before, like this
          #if LINUX_VERSION_CODE <= KERNEL_VERSION(2,6,16) 
          as explained in 
          https://stackoverflow.com/questions/16721346/is-there-a-macro-definition-to-check-the-linux-kernel-version
    """
    pu = None
    PUs = gateway.jvm.org.eclipse.app4mc.amalthea.model.util.DeploymentUtil.getAssignedCoreForProcess(task,model)
    if (len(PUs) == 0):
        print ("ERROR: no task mapping found for task", task.getName())
        return
    elif (len(PUs) > 1):
        print ("WARNING: expecting a task mapped to a single PU. Task", task.getName(), "has", len(PUs), "mappings.")
    # TODO this is a limitation of the model. taskAllocation with multiple affinity mappings is not supported.
    # <taskAllocation task="PRE_SFM_gpu_POST?type=Task" scheduler="Scheduler_Denver?type=TaskScheduler" affinity="Core0?type=ProcessingUnit Core1?type=ProcessingUnit">
    # this loop is getting the 1st element of the java Set
    for p in PUs:
        # save the 1st element of the Set
        pu = p
        break
    pu_type_name = pu.getDefinition().getPuType().getName()
    
    time_type = gateway.jvm.org.eclipse.app4mc.amalthea.model.util.RuntimeUtil.TimeType.valueOf(tick_type)

    tcgen = None
    if pu_type_name == "CPU":
        # TODO implement task_type_name
        task_type_name = get_task_type(task,gateway)
        if task_type_name == "periodic":
            tcgen = Periodic(task,model,gateway,time_type)
        else:
            print ("ERROR: task type %s for CPU is not supported" % task_type_name)
            return
    elif pu_type_name == "GPU":
        tcgen = OpenMP(task,model,gateway,time_type)
    elif pu_type_name == "Accelerator":
        tcgen = FRED(task,model,gateway,time_type)
    else:
        print ("ERROR: processing unit type %s is not supported" % pu_type_name)
        return

    #cgen = tcgen.generate_code()
    # create the dir structure where the task C code is saved
    # saving the task code
    path = str.lower(base_path) + '/src/tasks/'
    filename = path + str.lower(task.getName()) + '.cpp'
    with open(filename, 'w') as gen:
        gen.writelines(str(tcgen.generate_code()))
        gen.close()

    ##################################
    # generating the task's runnables
    ##################################
    runnables = gateway.jvm.org.eclipse.app4mc.amalthea.model.util.SoftwareUtil.getRunnableList(task,None)
    # generate the code for each runnable
    for r in runnables:
        runnable.save(r, gateway, model, pu_type_name, base_path)

    #######################
    # generating the task.h
    #######################
    task_body = [c.Line("// prototype for all runnables of task '%s'" % task.getName())]
    for r in runnables:
        # TODO decide to use a runnable header or just a function prototype
        #task.extend([c.Include(i.runnable.name + ".h", False)])
        task_body.extend([c.Statement("void " + r.getName() + "(void)")])

    # saving the task header
    path = str.lower(base_path) + '/include/tasks/'
    filename = path + str.lower(task.getName()) + '.h'
    with open(filename, 'w') as gen:
        header = (str(w)+'\n' for w in task_body)
        gen.writelines(header)
        gen.close()


def get_task_type(task, gateway) -> str:
    """ Query the model to get the type of the task.

    The supported types are: periodic.
    
    Args:
        task (:class:`org.eclipse.app4mc.amalthea.model.Task`): An Amalthea task.
        gateway (:class:`py4j.java_gateway.JavaGateway`): py4j gateway. 

    Returns:
        str: Returns an empty str for an unsupported model, otherwise, returns the name of the task type.

    Todo:
        * Task code gen does not support task offloading for GPU. 
        * Task code gen does not support sporadic tasks.

    """
    task_type = ''
    stimuli = task.getStimuli()
    if len(stimuli) != 1:
        print ("ERROR: expecting a Stimuli definition for task",task.getName())
    else:
        stimulus = stimuli[0]
        if is_instance_of(gateway,stimulus,gateway.jvm.org.eclipse.app4mc.amalthea.model.PeriodicStimulus):
            # the recurrence attribute is considered mandatory for periodic tasks
            if stimulus.getRecurrence() == None:
                print ("ERROR: the Stimuli recurrence for task",task.getName(), "is not defined")
            else:
                task_type = 'periodic'
        else:
            # TODO fix here to implement other types of task codegen for multicore, like, sporadic tasks
            print ("ERROR: I still have to implement the type of Stimuli used in WATERS19")
    
    return task_type


def header(tasks, base_path):
    """ Generate C header with the prototype of all tasks.

    Args:
        tasks ([ org.eclipse.app4mc.amalthea.model.Task ]): the list of tasks.
        base_path (str): base directory where the code will be saved.
    """
    header = "\n"
    for i in tasks:
        header += "void " + i.getName() + "();\n"
    header += "\n"
    
    # create the dir structure where the task C code is saved
    path = str.lower(base_path) + '/include/tasks/'
    filename = path + 'tasks.h'
    with open(filename, 'w') as gen:
        gen.writelines(header)
        gen.close()  
