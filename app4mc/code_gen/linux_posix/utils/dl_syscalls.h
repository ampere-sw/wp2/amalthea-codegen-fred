/*
 * https://www.kernel.org/doc/Documentation/scheduler/sched-deadline.txt
 * See Appendix B. Minimal main()
 */

#ifndef __DL_SYSCALLS__
#define __DL_SYSCALLS__

#include <linux/kernel.h>
#include <linux/unistd.h>
#include <time.h>
#include <linux/types.h>

#define SCHED_DEADLINE	6

// /* XXX use the proper syscall numbers */
// #ifdef __x86_64__
// #define __NR_sched_setattr		314
// #define __NR_sched_getattr		315
// #endif

// #ifdef __i386__
// #define __NR_sched_setattr		351
// #define __NR_sched_getattr		352
// #endif

// #ifdef __arm__
// #define __NR_sched_setattr		380
// #define __NR_sched_getattr		381
// #endif

#define SCHED_FLAG_RECLAIM		0x02
#define SCHED_FLAG_SOFT_RSV		0x04

struct sched_attr {
	__u32 size;
	
	__u32 sched_policy;
	__u64 sched_flags;
	
	/* SCHED_NORMAL, SCHED_BATCH */
	__s32 sched_nice;
	
	/* SCHED_FIFO, SCHED_RR */
	__u32 sched_priority;
	
	/* SCHED_DEADLINE - times in microseconds*/
	__u64 sched_runtime;
	__u64 sched_deadline;
	__u64 sched_period;
};

int sched_setattr(pid_t pid,
		      const struct sched_attr *attr,
		      unsigned int flags);

int sched_getattr(pid_t pid,
		      struct sched_attr *attr,
		      unsigned int size,
		      unsigned int flags);

#endif /* __DL_SYSCALLS__ */

