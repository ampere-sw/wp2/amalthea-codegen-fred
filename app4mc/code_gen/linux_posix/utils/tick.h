/*
 * Description: In the app4mc model, a tick is a runnable property that specifiy the amount of computation of the runnable.
 *  
 * Althouth there are more types of ticks defined in the app4mc schema,
 * The supported app4mc tick types are:
 *  - DiscreteValueConstant: Constant value. Example:
*      <default xsi:type="am:DiscreteValueConstant" value="20"/>
 *  - DiscreteValueStatistics: Defines the upper bound, lower bound and mean of a value interval without 
 *     defining the distribution. Example:
 *        <value xsi:type="am:DiscreteValueStatistics" lowerBound="4755924" upperBound="6354526" average="5853824.0"/>
 *  - DiscreteValueGaussDistribution: Defines the upper bound, lower bound and mean of a value interval without 
 *     defining the distribution. Example:
 *        <default xsi:type="am:DiscreteValueGaussDistribution" lowerBound="0" upperBound="5" mean="2.0" sd="1.0"/>
 *  - DiscreteValueWeibullEstimatorsDistribution: Weibull Distribution The parameter of a Weibull distribution 
 *     (kappa, lambda...) are calculated from the estimators minimum, maximum and average. Example:
 *        <default xsi:type="am:DiscreteValueWeibullEstimatorsDistribution" lowerBound="284" upperBound="740" average="578.0" pRemainPromille="5.0E-4"/>
 
 * Author: 
 *  Alexandre Amory (September 2020)
 * 
 * Compilation: 
 *  $> g++ -c tick.cpp -o tick
 *  
 * TODO:
 *   - for DiscreteValueStatistics, how can DiscreteValueStatistics be genrated ? is it a normal distribution ?!?!? for know, it is implemented as a normal distribution.
 *      but than it becomes equal to DiscreteValueGaussDistribution.
 *   - for DiscreteValueWeibullEstimatorsDistribution, how to translate pRemainPromille to alpha and beta parameters used in weibull distribution ?
 * 
 * Usage Example:
 * 
 * // runnable description
 * void Runnable1 () {
 *    // setting up the random number generator
 *    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
 *    Tick_DiscreteValueGaussDistribution tick(seed,0,100,50,10);
 *   
 *    LabelAccess_1(read);
 *    LabelAccess_2(read);
 *    // spends some random tick cycles to mimic some computation time
 *    Count_Ticks(tick());
 *    LabelAccess_2(write);
 * }
 * 
 */

#ifndef __TICK__
#define __TICK__

#include <random>

// uncomment this for high verbosity level
//#define DEBUG_EXTRA

/*
Portable function to waste cycles, Optimized to map the # ticks to the # of cycles. 
It is also done such that it generates minimal memory accesses. 
See the resulting asm code.

Count_Ticks(unsigned long):
        push    r13
        push    r12
        push    rbp
        push    rbx
        mov     QWORD PTR [rsp-8], rdi
        mov     r13d, 0
        mov     rax, QWORD PTR [rsp-8]
        shr     rax, 8
        mov     r12, rax
        mov     ebx, 0
        mov     ebp, 0
        mov     ebp, 0
.L3:
        cmp     rbp, r12
        jnb     .L2
        xor     rbx, 1
        xor     rbx, 1
        ... total of 256 xor in the loop
        xor     rbx, 1
        xor     rbx, 1
        xor     rbx, 1
        xor     rbp, 1
        jmp     .L3
.L2:
        mov     ebp, 0
.L5:
        cmp     rbp, r13
        jnb     .L4
        xor     rbx, 1
        xor     rbx, 1
        xor     rbx, 1
        xor     rbx, 1
        xor     rbp, 1
        jmp     .L5
.L4:
        mov     rax, rbx
        pop     rbx
        pop     rbp
        pop     r12
        pop     r13
        ret

An equivalent code is generated for ARM64, where the 
main loop xors and no memory access.

        eor     w19, w19, 1        
*/
uint64_t Count_Ticks(uint64_t ticks);


/*
Alternative method to waste time by counting time
*/
uint64_t Count_Time(uint64_t ticks);


class Tick_DiscreteValueConstant
{
private:
   int value_;
public:
    Tick_DiscreteValueConstant(int value);
    int operator()();
};


class Tick_DiscreteValueStatistics
{
private:
   int min_;
   int max_;
   std::default_random_engine random_engine_;
   std::normal_distribution<double> distribution_;
public:
    Tick_DiscreteValueStatistics(unsigned seed, int low, int high, float avg, float std_dev);
    int operator()();
};

class Tick_DiscreteValueGaussDistribution
{
private:
   int min_;
   int max_;
   std::default_random_engine random_engine_;
   std::normal_distribution<double> distribution_;
public:
    Tick_DiscreteValueGaussDistribution(unsigned seed, int low, int high, float avg, float std_dev);
    int operator()();
};


/*
 TODO:  be aware that this distribution is not fully implemented
 because there is a mismatch between the parameters of the Amalthea model and 
 `C++ std::weibull_distribution <https://www.cplusplus.com/reference/random/weibull_distribution/>`_.
 
 So, this implementation possible has a different behavior compare to the 
 behavior proposed to the Amalthea DiscreteValueWeibullEstimatorsDistribution.
*/
class Tick_DiscreteValueWeibullEstimatorsDistribution
{
private:
   int min_;
   int max_;
   std::default_random_engine random_engine_;
   std::weibull_distribution<double> distribution_;
public:
    Tick_DiscreteValueWeibullEstimatorsDistribution(unsigned seed, int low, int high, float a, float b);
    int operator()();
};

#endif // TICK__
