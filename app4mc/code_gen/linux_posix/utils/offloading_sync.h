#ifndef __OFFLOADING_SYNC
#define __OFFLOADING_SYNC

#include <mutex>
#include <condition_variable>

struct offloading_sync
{
  std::mutex mtx;
  std::condition_variable cvar;
  bool ready;

  offloading_sync(){
    this->ready = false;
  }
};

#endif //__OFFLOADING_SYNC