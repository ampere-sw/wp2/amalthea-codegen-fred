/*
 * Description: This application tests atomic scalar data shared between threads.
 *  
 * It launches multiple threads writing in the same shared data. There are two versions: 
 * threads writing in a unprotected data and other threads writing in protected data.
 * At the end, the test prints the final value of the share data. 
 * Only the protected data shows the correct value.
 * 
 * The app4mc schema has the types Label and LabelAccess. Both types have a field called 'dataStability'. 
 * When this is in automaticProtection, then there must have a code to protect the data access.
 *     Example: 
 *        <items xsi:type="am:LabelAccess" data="bitstream_partition0?type=Label" access="read" dataStability="noProtection"/>
 *        
 *        <labels xmi:id="Cloud_map_host?type=Label" name="Cloud_map_host" constant="false" bVolatile="false" dataStability="noProtection">
 *           <size value="1500" unit="kB"/>
 *        </labels>
 *
 * Author: 
 *  Alexandre Amory (September 2020), ReTiS Lab, Scuola Sant'Anna, Pisa, Italy.
 * 
 * Compilation: 
 *  $> g++ atomic_test.cpp  -o atomic_test -lpthread
 *  
 * Usage Example: 
 *  $> ./atomic_test
 */

#include <iostream>
#include <thread>
#include <vector>
#include <atomic>

class Atomic_Scalar { 
private: 
    std::atomic<uint16_t> value;
public:
    Atomic_Scalar(): value(0){
        
    }

    uint16_t get() const { 
        return value; 
    } 

    void set(uint16_t newval) { 
        this->value =  newval; 
    } 

    void inc(){
        this->value += 1;
    }

    friend std::ostream& operator<<( std::ostream& out, const Atomic_Scalar& v){
        out << v.get();
        return out;
    }

}; 

void writer(int tid);
void writer_unsafe(int tid);
//void reader(int tid);

typedef uint16_t MY_TYPE;
#define REPEATS 10000

Atomic_Scalar my_safe_data_in;
MY_TYPE my_unsafe_data_in;

int main()
{
    std::vector<std::thread> th;
    unsigned int cores = std::thread::hardware_concurrency();
    unsigned int half_cores = cores/2;    
    std::cout << " number of cores: " << cores << std::endl;;

    //Launch a group of threads
    for (int i = 0; i < cores; ++i) {
            if (i < half_cores)
                th.push_back(std::thread(writer,i));
            else
                th.push_back(std::thread(writer_unsafe,i));
    }
    //Join the threads with the main thread
    for(auto &t : th){
        t.join();
    }

    std::cout << "FINISHING ....\n" ;
    std::cout << "EXPECTED value: " << half_cores*REPEATS << std::endl;
    std::cout << "SAFE data: " << my_safe_data_in << std::endl;
    std::cout << "UNSAFE data: " << my_unsafe_data_in << std::endl;


  //pause();
  return EXIT_SUCCESS;
}

void writer(int tid){
  MY_TYPE data;
 for (int i=0; i< REPEATS; i++){
    //data = my_safe_data_in.get();
    //my_safe_data_in.set(data+1);
    my_safe_data_in.inc();
    //std::cout << std::this_thread::get_id() << " -- " << tid << " -- WR SAFE: " << my_safe_data_in.get() << std::endl;
  }
}

void writer_unsafe(int tid){
 for (int i=0; i< REPEATS; i++){
    my_unsafe_data_in += 1;
    //std::cout << std::this_thread::get_id() << " -- " << tid << " -- WR UNSAFE: " << my_unsafe_data_in << std::endl;
  }
}

void reader(int tid){
 std::chrono::milliseconds sleepDuration(1);
  for (int i=0; i< REPEATS; i++){
    std::cout << std::this_thread::get_id() << " -- " << tid << " -- RD SAFE: " << my_safe_data_in << std::endl;
    std::cout << std::this_thread::get_id() << " -- " << tid << " -- RD UNSAFE: " << my_unsafe_data_in << std::endl;
    std::this_thread::sleep_for(sleepDuration);
  }
}