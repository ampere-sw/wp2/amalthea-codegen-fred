/*
Testing Linux message queue
https://linux.die.net/man/2/msgsnd

adapted from the example in 
https://gist.github.com/Mark-htmlgogogo/e024c36541646373581472348657304d

compile:
$ g++ msg_send.c -o msg_send

running:
$ ./msg_send
...
sender: IPC key = 4294967295
...

$ ./msg_recv 4294967295
...
sender: sending = 'Did you get this?'
sender: message "Did you get this?" Sent
receiver: ^Did you get this?
*/

#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <mqueue.h> // int mq_unlink(const char *name);

#include <thread>         // std::this_thread::sleep_for
#include <chrono>         // std::chrono::seconds

using namespace std;

#define MSGSZ 128	

// Declare the message structure

template <unsigned S> 
struct message_buf{
	long mtype;
	char mtext[S];
};

/*
typedef struct {
	long mtype;
	char mtext[MSGSZ];
} message_buf;
*/

int main(int argc, char* argv[]) {
	int msqid;
	int msgflg = IPC_CREAT | 0666;
	key_t key;
	message_buf<MSGSZ> sbuf;
	size_t buf_length;

	//key = 2234;
    key = ftok("shmfile",65);
    printf( "sender: IPC key = %u\n", key );

	printf("sender: Calling msgget(%#1x,\%#o)\n", key, msgflg);
	
	if ((msqid = msgget(key, msgflg)) < 0) {
		perror("msgget");
		exit(1);
	}
	else
		(void)fprintf(stderr, "sender: msgget succeeded: msgqid = %d\n", msqid);

	// We'll send message type 1 
	sbuf.mtype = 1;
	//strcpy(sbuf.mtext, "Did you get this?");
    strcpy(sbuf.mtext, argv[1]);
	buf_length = strlen(sbuf.mtext) + 1;

    std::this_thread::sleep_for (std::chrono::seconds(10));

    printf("sender: sending = '%s'\n", sbuf.mtext);
	// Send a message.
	if((msgsnd(msqid, &sbuf, buf_length, IPC_NOWAIT)) < 0){
		printf("%d, %ld, %s, %ld\n", msqid, sbuf.mtype, sbuf.mtext, buf_length);
		perror("msgsnd");
		exit(1);
	}
	else
		printf("sender: message \"%s\" Sent\n", sbuf.mtext);

    printf("sender: removing the message queue = %#1x\n", key);
    mq_unlink("shmfile");
	return 0;
}