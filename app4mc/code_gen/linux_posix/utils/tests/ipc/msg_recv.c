/*
Testing Linux message queue
https://linux.die.net/man/2/msgsnd

adapted from the example in 
https://gist.github.com/Mark-htmlgogogo/e024c36541646373581472348657304d

compile:
$ g++ msg_recv.c -o msg_recv

running:
$ ./msg_recv <IPC key>
...
receiver: ^Did you get this?
*/
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <stdio.h>
#include <stdlib.h>

#define MSGSZ 128

// Declare the message structure.
template <unsigned S> 
struct message_buf{
	long mtype;
	char mtext[S];
};

/*
typedef struct {
	long mtype;
	char mtext[MSGSZ];
} message_buf;
*/
int main(int argc, char* argv[]){
	int msqid;
	key_t key;
	message_buf<MSGSZ> rbuf;

	//key = 2234;
    key = atoi(argv[1]);

    // assuming the sender already created the msg queue
	if ((msqid = msgget(key, 0666)) < 0) {
		perror("msgget");
		exit(1);
	}

	// Receive an answer of message type 1.
    printf("receiver: waiting data ...\n");
	if (msgrcv(msqid, &rbuf, MSGSZ, 1, 0) < 0) {
		perror("msgrcv");
		exit(1);
	}

	printf("receiver: ^%s\n", rbuf.mtext);
	return 0;
}