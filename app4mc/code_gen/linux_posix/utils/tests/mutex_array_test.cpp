/*
 * Description: This application tests mutual exclusion for (vector) data shared between threads.
 *  
 * It launches multiple threads writing in the same shared data. There are two versions: 
 * threads writing in a unprotected data and other threads writing in protected data.
 * At the end, the test prints the final value of the share data. 
 * Only the protected data shows the correct value.
 *  
 * The app4mc schema has the types Label and LabelAccess. Both types have a field called 'dataStability'. 
 * When this is in automaticProtection, then there must have a code to protect the data access.
 *     Example: 
 *        <items xsi:type="am:LabelAccess" data="bitstream_partition0?type=Label" access="read" dataStability="noProtection"/>
 *        
 *        <labels xmi:id="Cloud_map_host?type=Label" name="Cloud_map_host" constant="false" bVolatile="false" dataStability="noProtection">
 *           <size value="1500" unit="kB"/>
 *        </labels>
 *
 * Author: 
 *  Alexandre Amory (September 2020), ReTiS Lab, Scuola Sant'Anna, Pisa, Italy.
 * 
 * Compilation: 
 *  $> g++ mutex_array_test.cpp  -o mutex_array_test -lpthread
 *  
 * Usage Example: 
 *  $> ./mutex_array_test
 */

#include <iostream>
#include <cstring>
#include <shared_mutex>
#include <thread>
#include <vector>
#include <unistd.h>
#include <random>
#include <sstream>
#include <string>


using namespace std; 


template<unsigned array_size>
class Atomic_Array { 
private: 
    /** https://en.cppreference.com/w/cpp/thread/shared_timed_mutex
     * Shared mutexes are usually used in situations when multiple 
     * readers can access the same resource at the same time without 
     * causing data races, but only one writer can do so.
     */
    mutable std::shared_timed_mutex m; 
    unsigned char value[array_size]; 
public:
    Atomic_Array(){
        // requires exclusive ownership to write to *this
        //std::unique_lock<std::shared_timed_mutex> l(m); 
        //std::exchange(value, 0); 
        //memset(value,array_size*sizeof(unsigned char),0);
        this->set(0);
    }

    unsigned char get(const unsigned idx) const { 
        // requires shared ownership to read from other
        std::shared_lock<std::shared_timed_mutex> l(m); 
        if (idx < array_size)
            return value[idx]; 
        else
            return 0;
    } 

    void copy(unsigned char* newvet, unsigned s) { 
        // requires exclusive ownership to write to *this
        std::unique_lock<std::shared_timed_mutex> l(m); 
        //return std::exchange(value, newval); 
        memcpy(this->value, newvet, min(s,array_size)); 
    } 

    void set(unsigned char v) { 
        // requires exclusive ownership to write to *this
        std::unique_lock<std::shared_timed_mutex> l(m); 
        memset(value,array_size*sizeof(unsigned char),v);
    }  

    void set(unsigned char idx, unsigned char v) { 
        // requires exclusive ownership to write to *this
        std::unique_lock<std::shared_timed_mutex> l(m); 
        value[idx] = v;
    }  
    
    void inc() { 
        // requires exclusive ownership to write to *this
        std::unique_lock<std::shared_timed_mutex> l(m); 
        for(int i=0;i<array_size;i++)
        {
            value[i] +=1;
        }
    }     
/*
    friend std::ostream& operator<<( std::ostream& out, const Atomic_Array<T>& v){
        out << v.get();
        return out;
    }
*/
/*
    std::ostream &operator <<(std::ostream &os, const unsigned char &v) {
        using namespace std;
        copy(v, v[array_size-1], ostream_iterator<T>(os, "\n"));
        return os;
    }
    */
    std::string str(){
        std::ostringstream os;
        os << "["; 
        for (int i = 0; i < array_size; ++i) { 
            os << (unsigned)value[i]; 
            if (i != array_size - 1) 
                os << ", "; 
        } 
        os << "]\n"; 
        return os.str(); 
    }


}; 

void writer(int tid);
void writer_unsafe(int tid);
//void reader(int tid);

// C++ template to print vector container elements 
template <typename T> 
ostream& operator<<(ostream& os, const vector<T>& v) 
{ 
    os << "["; 
    for (int i = 0; i < v.size(); ++i) { 
        os << (unsigned)v[i]; 
        if (i != v.size() - 1) 
            os << ", "; 
    } 
    os << "]\n"; 
    return os; 
} 

#define MY_SIZE 20
#define REPEATS 1000

Atomic_Array<MY_SIZE> my_safe_data_in;
std::vector<unsigned char> my_unsafe_data_in(MY_SIZE,0);

int main()
{

    std::vector<std::thread> th;
    unsigned int cores = std::thread::hardware_concurrency();
    unsigned int half_cores = cores/2;
    std::cout << " number of cores: " << cores << std::endl;;


    //Launch a group of threads
    for (int i = 0; i < cores; ++i) {
        if (i < half_cores)
        {
            th.push_back(std::thread(writer,i));
        }
        else
        {
            th.push_back(std::thread(writer_unsafe,i));
        }
    }
    //Join the threads with the main thread
    for(auto &t : th){
        t.join();
    }

    std::cout << "FINISHING ....\n" ;
    //std::cout << "EXPECTED value: " << half_cores*REPEATS << std::endl;
    std::cout << "SAFE data: " << my_safe_data_in.str() << std::endl;
    std::cout << "UNSAFE data: " << my_unsafe_data_in << std::endl;
    return EXIT_SUCCESS;
}

void writer(int tid){
  for (int i=0; i< REPEATS; i++){
    my_safe_data_in.inc();
    //std::cout << std::this_thread::get_id() << " -- " << tid << " -- WR SAFE: "   << my_safe_data_in.str()   << std::endl;
  }
}

void writer_unsafe(int tid){
  for (int j=0; j< REPEATS; j++){
    for(int i=0;i<my_unsafe_data_in.size();i++)
    {
        my_unsafe_data_in[i] += 1;
    }    
    //std::cout << std::this_thread::get_id() << " -- " << tid << " -- WR UNSAFE: " << my_unsafe_data_in << std::endl;
  }
}

void reader(int tid){
 std::chrono::milliseconds sleepDuration(100);
  for (int i=0; i< REPEATS; i++){
      
    std::cout << std::this_thread::get_id() << " -- " << tid << " -- RD SAFE: "   << my_safe_data_in.str()   << std::endl;
    std::cout << std::this_thread::get_id() << " -- " << tid << " -- RD UNSAFE: " << my_unsafe_data_in << std::endl;
    std::this_thread::sleep_for(sleepDuration);
  }
}
