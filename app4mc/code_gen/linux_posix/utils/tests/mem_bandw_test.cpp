/* 
###############################
TASK DESCRIPTION

Fake task used to test the memory bandwith
during label reading/writing.

PERFORMANCE MONITORS:
One can use perf or valgrind to check the 
used memory bandwith against the expected one

VALGRIND
$ valgrind --tool=cachegrind ./synthetic
$ cg_annotate --auto=yes  cachegrind.out.<pid> > cache.rpt

--------------------------------------------------------------------------------
       Ir  I1mr  ILmr        Dr   D1mr   DLmr      Dw  D1mw  DLmw 
--------------------------------------------------------------------------------
4,128,188 2,176 2,018 1,115,204 77,780 70,801 176,149 2,602 1,706  PROGRAM TOTALS

--------------------------------------------------------------------------------
       Ir I1mr ILmr      Dr   D1mr   DLmr     Dw  D1mw DLmw  file:function
--------------------------------------------------------------------------------
...
187,502    0    0       0      0      0  0    0    0    while(p_start<p_end_multiple_of_64){
750,000    1    1 250,000      1      1  0    0    0      val += p_start[0] + p_start[1] + p_start[2] + p_start[3] + 
687,500    0    0 250,000 62,500 62,500  0    0    0             p_start[4] + p_start[5] + p_start[6] + p_start[7];
 62,500    0    0       0      0      0  0    0    0      p_start += 8;
      .    .    .       .      .      .  .    .    .    }
...

One can see in the Dr column that there are 500,000 memory reads of 8 bytes each (uint64_t)
resulting in 4MBytes read per label access. Column DLmr shows the Last level misses, i.e.
the number of DRAMM access. In this case, 62,500 * 8 bytes.
Recall that valgrind simulates only the L1 and the LL. It does not have L2.
The total Dr in the entire program is 1,115,204.


PERF

Examples of use of Perf:

$ sudo perf stat -B ./synthetic
$ sudo perf stat -e L1-dcache-loads,L1-dcache-load-misses,L1-dcache-stores ./synthetic 
$ sudo perf stat -e LLC-loads,LLC-load-misses,LLC-stores,LLC-prefetches,L1-dcache-loads,L1-dcache-load-misses,L1-dcache-stores,instructions,cycles,cycles:k,cycles:u,cache-references,cache-misses,bus-cycles ./synthetic

Example:
$ sudo perf stat -e L1-dcache-loads,L1-dcache-load-misses,L1-dcache-stores ./synthetic 
Task period: 1000 µs
Expected bandwidth: 4000000000 Bytes/s
DURATION: 1276 µs
DURATION (avg): 1276 µs

 Performance counter stats for './synthetic':

         1,732,316      L1-dcache-loads                                               (86.53%)
            57,828      L1-dcache-load-misses     #    3.34% of all L1-dcache hits  
           553,235      L1-dcache-stores                                              (13.47%)

       0.003762728 seconds time elapsed

       0.003807000 seconds user
       0.000000000 seconds sys

One can see that the number of Dl1 reads in Valgrind (1,115,204) and Perf (1,732,316)
are in the same order of magnitude. More investigation is required to understand
this difference.

###############################
*/
#include <iostream>
#include <chrono>
#include <thread>
#include <time.h>
#include "task_utils.h"

#define REPETITIONS 1
#define TASK_PERIOD   1000
#define VET_SIZE 1000000

uint32_t vet[VET_SIZE];


uint64_t read_long_array2(uint8_t *p, int size){
  register uint64_t *p_start= (uint64_t*)p;
  // 64 is the number of bytes moved inside the 1st while loop
  uint64_t left_over = size & (64-1);
  uint64_t multiple_of_64 = size - left_over;
  register uint64_t *p_end_multiple_of_64= (uint64_t*)&(p[multiple_of_64-1]);
  // pointers to the 2nd loop
  register uint8_t *p_start2= &(p[multiple_of_64]);
  register uint8_t *p_end= &(p[size-1]);
  register uint64_t val=0;
  // executes 8 moves of 8 bytes each
  while(p_start<p_end_multiple_of_64){
    val += p_start[0] + p_start[1] + p_start[2] + p_start[3] + 
           p_start[4] + p_start[5] + p_start[6] + p_start[7];
    p_start += 8;
  }
  // executes the remaining moves, byte by byte
  while(p_start2<p_end){
    val += *p_start2;
    p_start2 ++;
  }
  return val;
}


void Task_ESSP0()
{
  // this sleep is not working... using the C++ instead
  //struct timespec ts;
  //clock_gettime(CLOCK_MONOTONIC_RAW, &ts);

  using namespace std::this_thread;     // sleep_for, sleep_until
  using namespace std::chrono_literals; // ns, us, ms, s, h, etc.
  using std::chrono::system_clock;
  auto task_period = std::chrono::microseconds(TASK_PERIOD);

  std::chrono::steady_clock::time_point begin_total = std::chrono::steady_clock::now();
  std::chrono::steady_clock::time_point end_total = std::chrono::steady_clock::now();
  std::chrono::steady_clock::time_point start;

  std::cout << "Task period: " << TASK_PERIOD << " µs\n";
  std::cout << "Expected bandwidth: " << TASK_PERIOD*sizeof(vet) << " Bytes/s\n";
  begin_total = std::chrono::steady_clock::now();
  for(int i=0;i<REPETITIONS;i++)
  {
    start = std::chrono::steady_clock::now();

    read_long_array2((uint8_t *)vet,sizeof(vet));
    
    //clock_nanosleep(CLOCK_MONOTONIC_RAW, TIMER_ABSTIME, &ts, NULL);
    //timespec_add_us(&ts, TASK_PERIOD);
    std::this_thread::sleep_until(start + task_period);
  }
  end_total = std::chrono::steady_clock::now();

  std::cout << "DURATION: " << std::chrono::duration_cast<std::chrono::microseconds>(end_total - begin_total).count()  << " µs\n";
  std::cout << "DURATION (avg): " << std::chrono::duration_cast<std::chrono::microseconds>(end_total - begin_total).count()/REPETITIONS  << " µs\n";
}