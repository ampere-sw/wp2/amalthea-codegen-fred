/*
 * Description: This application tests the normal number generator with the parameters provided 
 * by app4mc ticks.
 *  
 * Note that there are several types of ticks such as the following description in the app4mc schema:
 *  - DiscreteValueStatistics: Defines the upper bound, lower bound and mean of a value interval without 
 *     defining the distribution.
 *     Example: 
 *        <value xsi:type="am:DiscreteValueStatistics" lowerBound="4755924" upperBound="6354526" average="5853824.0"/>
 *
 * Author: 
 *  Alexandre Amory (September 2020), ReTiS Lab, Scuola Sant'Anna, Pisa, Italy.
 * 
 * Compilation: 
 *  $> g++ -I .. ../tick.cpp normal_dist_tester.cpp  -o normal_dist_tester
 *  
 * Application Usage:
 *  ./normal_dist_tester <bin_count> <min_val> <max_val> <data_count> <hist_compactation> <avg> <std_dev>
 * 
 * Usage Example: 
 *  $> ./normal_dist_tester 50 0 100 1000 1 50 10
     histogram:
     0
     2
     4
     6
     8
     10
     12
     14
     16
     18
     20 *
     22
     24 ***
     26 *
     28 ******
     30 ************
     32 ************
     34 ************************
     36 *********************************
     38 *****************************
     40 **********************************************
     42 *************************************************************
     44 ****************************************************************
     46 *******************************************************************************
     48 ***********************************************************************************
     50 ********************************************************************
     52 ***********************************************************************************
     54 ***************************************************************************************
     56 ********************************************************************
     58 ******************************************************
     60 **********************************************
     62 ********************************
     64 *****************************
     66 ********************************
     68 *********************
     70 ***********
     72 *****
     74 ****
     76 ***
     78 **
     80 *
     82
     84
     86
     88
     90
     92
     94
     96
     98
     100

	The following example is based on the example of xml data presented above
	where lowerBound="4755924" upperBound="6354526" average="5853824.0".
	Note that DiscreteValueStatistics does not define a distribution, so normal 
	distribution was assumed. This was, std_dev must be defined.
	The std_dev was set as the difference between 5853824−6354526 = 500702.
	1000 random numbers were fit into 20 bins.
    $> ./normal_dist_tester 20 4755924 6354526 1000 1 5853824 500702
     histogram:
     4755924.00
     4835854.00 ******
     4915784.00 **********
     4995714.00 *************************
     5075644.00 *********************
     5155574.00 **********************
     5235504.00 *********************
     5315434.00 **********************************************
     5395364.00
     5475294.00 *************************************************
     5555224.00 ********************************************
     5635154.00 *************************************************************
     5715084.00 *********************************************************************
     5795014.00 ********************************************************************
     5874944.00 ************************************************************************
     5954874.00 **************************************************************************
     6034804.00 ********************************************************************************
     6114734.00 ***************************************************************************
     6194664.00 ******************************************************************************
     6274594.00 *******************************************************************
     6354524.00 ***********************************************************

 */


#include <iostream>
#include <iomanip>
#include <chrono>
#include <map>
#include "tick.h"

#define DEBUG
// uncomment this for even more debug
//#define DEBUG_EXTRA

void Usage(char prog_name[]);

void Get_args(
      char*  argv[]        /* in  */,
      int*   bin_count_p   /* out */,
      int* min_p           /* out */,
      int* max_p           /* out */,
      int*   data_count_p  /* out */,
      int*   hist_compact_p/* out */,
      float* avg_p         /* out */,
      float* std_dev_p     /* out */);

int Get_bin(
      int min           /* in  */, 
      int max           /* in  */, 
      int val           /* in  */, 
      int   bin_count   /* in  */,
      float bin_width   /* in  */);

int main(int argc, char* argv[])
{
   int bin_count, i, bin;
   int min, max, val;
   int data_count;
   int hist_compact;
   float avg, std_dev, bin_width;

   /* Check and get command line args */
   if (argc != 8) Usage(argv[0]); 
   Get_args(argv, &bin_count, &min, &max, &data_count, &hist_compact, &avg, &std_dev);	
   bin_width = (max - min)/ (float)bin_count;

	/* setting up the random number generator*/
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    Tick_DiscreteValueStatistics tick(seed, min, max, avg, std_dev);
 
    std::map<int, int> hist{};
    for(int n=0; n<data_count; ++n) {
      val = tick();
		// get the bin where the value will be inserted
		bin = Get_bin(min, max, val, bin_count, bin_width);
		// scale the map to the values between the [min ,max]
        ++hist[min+(bin*bin_width)];
    }
    
#  ifdef DEBUG_EXTRA
    // print the histogram
    printf("\nitems per bins:\n");
    for(auto p : hist) {
        std::cout << std::setw(2) << p.first << ' ' << p.second << '\n';
    }
#endif

    printf("\nhistogram:\n");
    std::map<int, int>::iterator p = hist.begin();
    std::cout << std::fixed; // i dont want to print in scientific notation
    for(float f=min;f<=max; f+=bin_width) {
       if (f >= p->first && p != hist.end()){
         std::cout << std::setprecision(2) << f << ' ' << 
            std::string((int)std::ceil((float)p->second/(float)hist_compact), '*') << '\n';
         p++;
       }else{
          std::cout << std::setprecision(2) << f << '\n';
       }
    }
    std::cout << std::scientific;
    
}

/*---------------------------------------------------------------------
 * Function:  Usage 
 * Purpose:   Print a message showing how to run program and quit
 * In arg:    prog_name:  the name of the program from the command line
 */
void Usage(char prog_name[] /* in */) {
   fprintf(stderr, "usage: %s ", prog_name); 
   fprintf(stderr, "<bin_count> <min_val> <max_val> <data_count> <hist_compactation> <avg> <std_dev>\n");
   exit(0);
}  /* Usage */


/*---------------------------------------------------------------------
 * Function:  Get_args
 * Purpose:   Get the command line arguments
 * In arg:    argv:  strings from command line
 * Out args:  bin_count_p:   number of bins
 *            min_p:    minimum value
 *            max_p:    maximum value
 *            data_count_p:  number of measurements
 */
void Get_args(
      char*  argv[]        /* in  */,
      int*   bin_count_p   /* out */,
      int*   min_p         /* out */,
      int*   max_p         /* out */,
      int*   data_count_p  /* out */,
      int*   hist_compact_p/* out */,
      float* avg_p,
      float* std_dev_p) {

   *bin_count_p = strtol(argv[1], NULL, 10);
   *min_p = strtol(argv[2], NULL, 10);
   *max_p = strtol(argv[3], NULL, 10);
   *data_count_p = strtol(argv[4], NULL, 10);
   *hist_compact_p = strtol(argv[5], NULL, 10);
   *avg_p = strtof(argv[6], NULL);
   *std_dev_p = strtof(argv[7], NULL);

   /*consistency check*/
   if (*bin_count_p <= 0){
      printf("ERROR: expecting natural bin_count\n");
      exit(1);
   }
   if (*data_count_p <= 0){
      printf("ERROR: expecting natural data_count\n");
      exit(1);
   }
   if (*hist_compact_p <= 0){
      printf("ERROR: expecting natural hist_compact\n");
      exit(1);
   }
   if (*max_p <= *min_p){
      printf("ERROR: expecting min < max\n");
      exit(1);
   }
   if (((float)*max_p < *avg_p) || ((float)*min_p > *avg_p )){
      printf("ERROR: expecting avg between [min,max]\n");
      exit(1);
   }
   if (((float)*max_p < (*avg_p+*std_dev_p)) || ((float)*min_p > (*avg_p-*std_dev_p) )){
      printf("ERROR: expecting (avg+std_dev) < max and (avg-std_dev) > min  \n");
      exit(1);
   }
   if (*bin_count_p > (*max_p-*min_p)){
      printf("ERROR: expecting bin_count <= (max-min)\n");
      exit(1);
   }


#  ifdef DEBUG
   printf("bin_count = %d\n", *bin_count_p);
   printf("min = %d, max = %d\n", *min_p, *max_p);
   printf("data_count = %d\n", *data_count_p);
   printf("hist_compactation = %d\n", *hist_compact_p);
   printf("avg = %.2f\n", *avg_p);
   printf("std_dev = %.2f\n", *std_dev_p);
#  endif
}  /* Get_args */

int Get_bin(
      int min         /* in  */, 
      int max         /* in  */, 
      int val         /* in  */, 
      int bin_count   /* in  */,
      float bin_width /* in  */) {
   int   bin;

   bin = (int)std::ceil((float)(val - min) / bin_width);

#  ifdef DEBUG_EXTRA
   printf("bin (%d) = %d\n", bin, val);
#  endif
	return bin;
}  /* Gen_bin */
