/*
 * Description: This application tests the weibull number generator with the parameters provided 
 * by app4mc ticks. 
 * 
 * Note that there are several types of ticks such as the following description in the app4mc schema:
 *  - DiscreteValueWeibullEstimatorsDistribution: Weibull Distribution The parameter of a Weibull distribution 
 *    (kappa, lambda...) are calculated from the estimators minimum, maximum and average.
 *      Example;
 *        <default xsi:type="am:DiscreteValueWeibullEstimatorsDistribution" lowerBound="284" upperBound="740" average="578.0" pRemainPromille="5.0E-4"/>
 
 *  More information:
 *    C++ examples:
 *    http://www.cplusplus.com/reference/random/weibull_distribution
 *    https://en.cppreference.com/w/cpp/numeric/random/weibull_distribution
 *    https://www.johndcook.com/blog/cpp_tr1_random/
 * 
 *    https://scipy.github.io/devdocs/generated/scipy.stats.weibull_min.html
 *    https://www.astroml.org/book_figures/chapter3/fig_weibull_distribution.html
 *    https://stackoverflow.com/questions/17481672/fitting-a-weibull-distribution-using-scipy
 *    XCORE:
 *    https://gitlab.idial.institute/AMALTHEA4public/org.eclipse.app4mc/-/blob/develop/plugins/org.eclipse.app4mc.amalthea.model/model/amalthea.xcore
 *
 * Author: 
 *  Alexandre Amory (September 2020), ReTiS Lab, Scuola Sant'Anna, Pisa, Italy.
 * 
 * Compilation: 
 *  $> g++ -I .. ../tick.cpp weibull_dist_tester.cpp -o weibull_dist_tester
 *  
 * Application Usage:
 *  ./weibull_dist_tester <bin_count> <min_val> <max_val> <data_count> <hist_compactation> <alpha> <beta>
 * 
 * TODO: 
 * - how to use the parameters of DiscreteValueWeibullEstimatorsDistribution to create the distribution ?
 * http://www.amalthea-project.org/index.php/forum/fsupport/12-weibull-distribution-parameters
 * - TODO what would be a consistency check for alpha and beta parameters ?!?!
 *
 * Usage Example: 
 *  $> ./weibull_dist_tester 10 0 10 1000 10 2 4
 *   bin_count = 10
 *   min = 0, max = 10
 *   data_count = 1000
 *   hist_compactation = 10
 *   alpha = 2.00
 *   beta = 4.00
 * 
     histogram:
     0 ******
     1 ***************
     2 ********************
     3 ************************
     4 *****************
     5 **********
     6 *******
     7 ***
     8 *
     9 *
     10 *
 *
 *  $ ./weibull_dist_tester 10 10 20 1000 10 1 2
 *   bin_count = 10
 *   min = 10, max = 20
 *   data_count = 1000
 *   hist_compactation = 10
 *   alpha = 1.00
 *   beta = 2.00
 * 
     histogram:
      10 *****************************************
      11 ************************
      12 **************
      13 *********
      14 *****
      15 ****
      16 ***
      17 **
      18 *
      19 *
      20 *
  **/

#include <iostream>
#include <iomanip>
#include <chrono>
#include <map>
#include "tick.h"

#define DEBUG
// uncomment this for even more debug
//#define DEBUG_EXTRA

void Usage(char prog_name[]);

void Get_args(
      char*  argv[]        /* in  */,
      int*   bin_count_p   /* out */,
      int*   min_p         /* out */,
      int*   max_p         /* out */,
      int*   data_count_p  /* out */,
      int*   hist_compact_p/* out */,
      float* alpha_p       /* out */,
      float* beta_p        /* out */);

int Get_bin(
      int min         /* in  */, 
      int max         /* in  */, 
      float val       /* in  */, 
      int bin_count   /* in  */,
      float bin_width /* in  */);

int main(int argc, char* argv[])
{
   int bin_count, i, bin;
   int min, max ;
   int data_count;
   int hist_compact;
   float alpha, beta, bin_width, val;

   /* Check and get command line args */
   if (argc != 8) Usage(argv[0]); 
   Get_args(argv, &bin_count, &min, &max, &data_count, &hist_compact, &alpha, &beta);	
   bin_width = (max - min)/ (float)bin_count;

	/* setting up the random number generator*/
	/* setting up the random number generator*/
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    Tick_DiscreteValueWeibullEstimatorsDistribution tick(seed, min, max, alpha, beta);
 
    std::map<int, int> hist{};
    for(int n=0; n<data_count; ++n) {
        val = tick();
		// get the bin where the value will be inserted
		bin = Get_bin(min, max, val, bin_count, bin_width);
		// scale the map to the values between the [min ,max]
        ++hist[bin];
    }
    

#  ifdef DEBUG_EXTRA
    // print the histogram
    int first = 0;
    int run_only_once = 0;
    printf("\nitems per bins:\n");
    for(auto p : hist) {
        first = min+(p.first*bin_width);
        if (first > max && run_only_once==0){
            std::cout << "this point on will be ignored in the histogram\n" ;
            run_only_once = 1;
        }
        std::cout << std::setw(2) << first << ' ' << p.second << '\n';
    }
#endif

    // print the bins of the histogram
    printf("\nhistogram:\n");
    std::map<int, int>::iterator p = hist.begin();
    for(i=min;i<=max; i+=bin_width) {
        /*
         TODO this scaling factor below assumes that there is always a zero value, which correspond to min
          however, this might not be the case. a safer approch might be the use of these functions to get 
          the actual min and max of the weibull distrib
        - http://www.cplusplus.com/reference/random/weibull_distribution/max/
        - http://www.cplusplus.com/reference/random/weibull_distribution/min/
        */
       if (i >= p->first && p != hist.end()){
         std::cout << i << ' ' << 
            std::string((int)std::ceil((float)p->second/(float)hist_compact), '*') << '\n';
         p++;
       }else{
         // print also the empty bins
         std::cout << i << '\n';
       }       
    }
   
}

/*---------------------------------------------------------------------
 * Function:  Usage 
 * Purpose:   Print a message showing how to run program and quit
 * In arg:    prog_name:  the name of the program from the command line
 */
void Usage(char prog_name[] /* in */) {
   fprintf(stderr, "usage: %s ", prog_name); 
   fprintf(stderr, "<bin_count> <min_val> <max_val> <data_count> <hist_compactation> <alpha> <beta>\n");
   exit(0);
}  /* Usage */


/*---------------------------------------------------------------------
 * Function:  Get_args
 * Purpose:   Get the command line arguments
 * In arg:    argv:  strings from command line
 * Out args:  bin_count_p:   number of bins
 *            min_p:    minimum value
 *            max_p:    maximum value
 *            data_count_p:  number of measurements
 */
void Get_args(
      char* argv[]        /* in  */,
      int*  bin_count_p   /* out */,
      int*  min_p         /* out */,
      int*  max_p         /* out */,
      int*  data_count_p  /* out */,
      int*  hist_compact_p/* out */,
      float* alpha_p,
      float* beta_p) {

   *bin_count_p = strtol(argv[1], NULL, 10);
   *min_p = strtol(argv[2], NULL, 10);
   *max_p = strtol(argv[3], NULL, 10);
   *data_count_p = strtol(argv[4], NULL, 10);
   *hist_compact_p = strtol(argv[5], NULL, 10);
   *alpha_p = strtof(argv[6], NULL);
   *beta_p = strtof(argv[7], NULL);


   /*consistency check*/
   if (*bin_count_p <= 0){
      printf("ERROR: expecting natural bin_count\n");
      exit(1);
   }
   if (*data_count_p <= 0){
      printf("ERROR: expecting natural data_count\n");
      exit(1);
   }
   if (*hist_compact_p <= 0){
      printf("ERROR: expecting natural hist_compact\n");
      exit(1);
   }
   if (*max_p <= *min_p){
      printf("ERROR: expecting min < max\n");
      exit(1);
   }
   // TODO what would be a consistency check for alpha and beta parameters ?!?!


#  ifdef DEBUG
   printf("bin_count = %d\n", *bin_count_p);
   printf("min = %d, max = %d\n", *min_p, *max_p);
   printf("data_count = %d\n", *data_count_p);
   printf("hist_compactation = %d\n", *hist_compact_p);
   printf("alpha = %.2f\n", *alpha_p);
   printf("beta = %.2f\n", *beta_p);
#  endif
}  /* Get_args */

int Get_bin(
      int min       /* in  */, 
      int max       /* in  */, 
      float val     /* in  */, 
      int bin_count /* in  */,
      float bin_width /* in  */) {
   int   bin;

   bin = (int)std::ceil(val / bin_width);
   // bin start with 1 instead with zero
   bin --;

#  ifdef DEBUG_EXTRA
   printf("bin (%d) = %f\n", bin, val );
#  endif
	return bin;
}  /* Gen_bin */
