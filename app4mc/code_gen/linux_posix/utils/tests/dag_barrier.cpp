/*
 * Description: This application tests how to build a taskset described as a DAG, using barriers at the receiving nodes.
 * 
 * Example DAG:
 *     n1
 *    /   \
 * n0      n3
 *    \   /
 *     n2 
 * 
 * n3 can only be unlocked when all its antecessors n1 and n2 finished.
 * 
 *  
 * The barrier code is from this link https://stackoverflow.com/questions/24465533/implementing-boostbarrier-in-c11
 *
 * Assumptions:
 *  - Each DAG edge uses a Linux message queue for IPC https://linux.die.net/man/2/msgsnd. Although this would be 
 *  an overkill for mulththreaded app, the idea is to propose something that will also work in a DAG consisting 
 *  of dif process communicating with some IPC mechanism;
 *  - Each edge has one sender/receiver. A multicast-like comm has to be break down into multiple queues;
 *  - The DAG period is lower than the DAG WCET. This way, each edge of the DAG has a single queue w no more than 1 
 *  element in the queue since it's garanteed that the next element will be received only after the current one is consumed
 *  - The receiving task makes a local copy of the incomming data. This is not actually required since, as mentioned before, 
 *  each edge has one sender / receiver and the DAG period is long enough. This is enforced to model the memory accesses 
 *  described in the Amalthea model, not necessarly to be the most efficient implementation.
 * 
 * Author: 
 *  Alexandre Amory (June 2022), ReTiS Lab, Scuola Sant'Anna, Pisa, Italy.
 * 
 * Compilation: 
 *  $> g++ dag_barrier.cpp  -o dag_barrier  -lpthread
 *  
 * Usage Example: 
 *  $> ./dag_barrier
 */

// https://opensource.com/article/19/4/interprocess-communication-linux-channels
// https://stackoverflow.com/questions/49570961/message-queue-msgget-msgsnd-msgrcv-linux-eidrm
// https://gist.github.com/Mark-htmlgogogo/e024c36541646373581472348657304d

#include <iostream>           // std::cout
#include <thread>             // std::thread
#include <mutex>              // std::mutex, std::unique_lock
#include <condition_variable> // std::condition_variable
#include <vector>
#include <random>
#include <chrono>
#include <cassert>

// #if defined (__GCC__) // GCC
//     #define thread_local __thread
// #endif

using namespace std;


// global defs
#define N_TASKS 4
#define N_EDGES 4
#define REPETITIONS 1000
#define DAG_PERIOD 1000
unsigned tasks_wcet[N_TASKS] = {100,180,150,50};
unsigned edge_bytes[N_EDGES] = {20,50,4,8};

// TODO: check the end-to-end DAG deadline


typedef struct msgbuf {
	long mtype;
	char mtext[20];
} message_buf_n0_n1;

class Barrier
{
private:
    std::mutex _mutex;
    std::condition_variable _cv;
    std::size_t _count;
public:
    explicit Barrier(std::size_t count) : _count(count) { }
    void Wait()
    {
        std::unique_lock<std::mutex> lock(_mutex);
        if (--_count == 0) {
            _cv.notify_all();
        } else {
            _cv.wait(lock, [this] { return _count == 0; });
        }
    }
};


// source: https://wiki.linuxfoundation.org/realtime/documentation/howto/applications/cyclic
struct period_info {
        struct timespec next_period;
        long period_ns;
};


void inc_period(struct period_info *pinfo) 
{
	// add microseconds to timespecs nanosecond counter
	pinfo->next_period.tv_nsec += pinfo->period_ns*1000;

	while (pinfo->next_period.tv_nsec >= 1000000000) {
		/* timespec nsec overflow */
		pinfo->next_period.tv_sec++;
		pinfo->next_period.tv_nsec -= 1000000000;
	}
}
 
void periodic_task_init(struct period_info *pinfo)
{
	clock_gettime(CLOCK_MONOTONIC, &(pinfo->next_period));
}
 
void wait_rest_of_period(struct period_info *pinfo)
{
	inc_period(pinfo);

	/* for simplicity, ignoring possibilities of signal wakes */
	clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &pinfo->next_period, NULL);
}


///////////////////////////////////////
// describing each task of the dag
///////////////////////////////////////
// 'period_ns' argument is only used when the task is periodic, which is tipically only the first tasks of the DAG
// all tasks have the same signature to simplify automatic codegen in the future

// n0 is the peridic starting task
void n0(unsigned seed, unsigned wcet, unsigned period_ns=0){
  unsigned iter=0;
  unsigned execution_time;
  float rnd_val;
  assert((period_ns != 0 && period_ns>wcet) || period_ns == 0);
  
  // each task has its own rnd engine to limit blocking for shared resources.
  // the thread seed is built by summin up the main seed + a hash of the task name, which is unique
  seed+= std::hash<std::string>{}("n0");
  std::mt19937_64 engine(static_cast<uint64_t> (seed));
  // each task might use different distributions
  std::uniform_real_distribution<double> zeroToOne(0.0, 1.0);

  // period definitions
  struct period_info pinfo;
  pinfo.period_ns = period_ns;
  periodic_task_init(&pinfo);

  while(iter < REPETITIONS){
    std::cout << "n0 is producing some critical data ... iteration " << iter << std::endl;
    // randomize the actual execution time of each iteration
    rnd_val = zeroToOne(engine);
    execution_time = 4.0f/5.0f*(float)wcet + 1.0f/5.0f*rnd_val;
    // we need busy waiting to mimic some actual processing.
    // using sleep or wait wont achieve the same result
    for (int i=execution_time; i > 0; --i);
    // send data to the next tasks: n1 and n2
    // TODO
    // wait for the period
    wait_rest_of_period(&pinfo);
    ++iter;
  }
}

// n1 receives from one task and sends data to another task
void n1(unsigned seed, unsigned wcet, unsigned period_ns=0){
  unsigned iter=0;
  unsigned execution_time;
  float rnd_val;
  assert((period_ns != 0 && period_ns>wcet) || period_ns == 0);
  
  // each task has its own rnd engine to limit blocking for shared resources.
  // the thread seed is built by summin up the main seed + a hash of the task name, which is unique
  seed+= std::hash<std::string>{}("n1");
  std::mt19937_64 engine(static_cast<uint64_t> (seed));
  // each task might use different distributions
  std::uniform_real_distribution<double> zeroToOne(0.0, 1.0);

  // period definitions
  struct period_info pinfo;
  pinfo.period_ns = period_ns;
  periodic_task_init(&pinfo);

  while(true){
    std::cout << "n1 is waiting data ... iteration " << iter << std::endl;
    // randomize the actual execution time of each iteration
    rnd_val = zeroToOne(engine);
    execution_time = 4.0f/5.0f*(float)wcet + 1.0f/5.0f*rnd_val;
    // we need busy waiting to mimic some actual processing.
    // using sleep or wait wont achieve the same result
    for (int i=execution_time; i > 0; --i);
    // send data to the next tasks: n1 and n2
    // TODO

    std::cout << "n1 is done ... iteration " << iter << std::endl;
    ++iter;
  }
}

// n2 receives from one task and sends data to another task
void n2(unsigned seed, unsigned wcet, unsigned period_ns=0){
}

// n3 is the final task and receives from two tasks
void n3(unsigned seed, unsigned wcet, unsigned period_ns=0){
}



int main ()
{
  // uncomment this to get a randon seed
  //unsigned seed = time(0);
  // or set manually a constant seed to repeat the same sequence
  unsigned seed = 123456;
  cout << "SEED: " << seed << endl;  
    
  std::vector<std::thread> threads;
  threads.push_back(std::thread(n0,seed,tasks_wcet[0],DAG_PERIOD));
  threads.push_back(std::thread(n1,seed,tasks_wcet[1],0));
  threads.push_back(std::thread(n2,seed,tasks_wcet[2],0));
  threads.push_back(std::thread(n3,seed,tasks_wcet[3],0));

  for (auto& th : threads) th.join();
  std::cout << "the threads were joined...\n";

  return 0;
}
