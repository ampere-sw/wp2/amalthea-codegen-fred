/*
 * Description: This application tests whether the seeds of a multithread program seem 'random'.
 * Code based in this post https://stackoverflow.com/questions/21237905/how-do-i-generate-thread-safe-uniform-random-numbers 
 *
 * If you uncomment the line 
 *   unsigned seed = time(0);
 * the result is that every execution will result in a different sequence of pseudo-random numbers
 * Or, 'seed' can be set as a constant in case it's necessary to repeat the same execution again.
 * This is very usefull for debugging purposes.
 *
 * Author: 
 *  Alexandre Amory (April 2021), ReTiS Lab, Scuola Sant'Anna, Pisa, Italy.
 * 
 * Compilation: 
 *  $> g++ -g -I.. ../tick.cpp seed_thread.cpp  -o seed_thread_tester -lpthread
 *  
 * Application Usage:
 *  ./seed_thread_tester
 * 
 * Usage Example: 
 *  $> ./seed_thread_tester
 */


#include <iostream>
#include <thread>
#include <cstdlib>
#include <random>     // for mt19937
#include <thread>     // for this_thread
#include <functional> // for hash()
#include "tick.h"

#if defined (__GCC__) // GCC
    #define thread_local __thread
#endif

using namespace std;

// source: https://wiki.linuxfoundation.org/realtime/documentation/howto/applications/cyclic
struct period_info {
        struct timespec next_period;
        long period_ns;
};


void inc_period(struct period_info *pinfo) 
{
	// add microseconds to timespecs nanosecond counter
	pinfo->next_period.tv_nsec += pinfo->period_ns*1000;

	while (pinfo->next_period.tv_nsec >= 1000000000) {
		/* timespec nsec overflow */
		pinfo->next_period.tv_sec++;
		pinfo->next_period.tv_nsec -= 1000000000;
	}
}
 
void periodic_task_init(struct period_info *pinfo)
{
	clock_gettime(CLOCK_MONOTONIC, &(pinfo->next_period));
}
 
void wait_rest_of_period(struct period_info *pinfo)
{
	inc_period(pinfo);

	/* for simplicity, ignoring possibilities of signal wakes */
	clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &pinfo->next_period, NULL);
}


void thread1(unsigned seed){

  // timing definitions
  struct period_info pinfo;
  pinfo.period_ns = 1000000;
  periodic_task_init(&pinfo);

  // the thread seed is built by summin up the main seed + a hash of the task name, which is unique
  seed+= std::hash<std::string>{}("thread1");
  Tick_DiscreteValueStatistics tick1(seed, 10, 20, 15, 2);
  Tick_DiscreteValueStatistics tick2(seed, 0, 1000, 200, 100);
  int r1, r2;

  while (1)
  {
    // runnable 1
    r1 = tick1();
    // runnable 2
    r2 = tick2();
    cout << "thread id: " << std::this_thread::get_id() << " val: " << r1 << " - " << r2 << endl;

    // checking the task period
    wait_rest_of_period(&pinfo);
  }
}

void thread2(unsigned seed){

  // timing definitions
  struct period_info pinfo;
  pinfo.period_ns = 1000000;
  periodic_task_init(&pinfo);

  // the thread seed is built by summin up the main seed + a hash of the task name, which is unique
  seed+= std::hash<std::string>{}("thread2");
  Tick_DiscreteValueStatistics tick1(seed, 10, 20, 15, 2);
  Tick_DiscreteValueStatistics tick2(seed, 0, 1000, 200, 100);
  int r1, r2;

  while (1)
  {
    // runnable 1
    r1 = tick1();
    // runnable 2
    r2 = tick2();
    cout << "thread id: " << std::this_thread::get_id() << " val: " << r1 << " - " << r2 << endl;

    // checking the task period
    wait_rest_of_period(&pinfo);
  }
}

void thread3(unsigned seed){

  // timing definitions
  struct period_info pinfo;
  pinfo.period_ns = 1234500;
  periodic_task_init(&pinfo);

  // the thread seed is built by summin up the main seed + a hash of the task name, which is unique
  seed+= std::hash<std::string>{}("thread3");
  Tick_DiscreteValueStatistics tick1(seed, 10, 200, 150, 20);
  Tick_DiscreteValueStatistics tick2(seed, 100, 1000, 400, 100);
  int r1, r2;

  while (1)
  {
    // runnable 1
    r1 = tick1();
    // runnable 2
    r2 = tick2();
    cout << "thread id: " << std::this_thread::get_id() << " val: " << r1 << " - " << r2 << endl;

    // checking the task period
    wait_rest_of_period(&pinfo);
  }
}

void thread4(unsigned seed){

  // timing definitions
  struct period_info pinfo;
  pinfo.period_ns = 5432100;
  periodic_task_init(&pinfo);

  // the thread seed is built by summin up the main seed + a hash of the task name, which is unique
  seed+= std::hash<std::string>{}("thread4");
  Tick_DiscreteValueStatistics tick1(seed, 10, 200, 150, 20);
  Tick_DiscreteValueStatistics tick2(seed, 100, 1000, 400, 100);
  int r1, r2;

  while (1)
  {
    // runnable 1
    r1 = tick1();
    // runnable 2
    r2 = tick2();
    cout << "thread id: " << std::this_thread::get_id() << " val: " << r1 << " - " << r2 << endl;

    // checking the task period
    wait_rest_of_period(&pinfo);
  }
}


int main()
{
    // uncomment this to get a randon seed
    //unsigned seed = time(0);
    // or set manually a constant seed to repeat the same sequence
    unsigned seed = 123456;
    cout << "SEED: " << seed << endl;

    std::thread thread_1(thread1,seed);
    std::thread thread_2(thread2,seed);
    std::thread thread_3(thread3,seed);
    std::thread thread_4(thread4,seed);

    thread_1.join();
    thread_2.join();
    thread_3.join();
    thread_4.join();

    return EXIT_SUCCESS;
}