/* 
    ###############################
    TASK DESCRIPTION
    fake task used to test the accuracy of 
    Count_Time and Count_Tick
    ###############################
*/
#include <iostream>
#include <chrono>
#include "tick.h"

#define TICK_EXECUTION_TIME 10000
#define REPETITIONS 10

void Task_ESSP0()
{
  std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
  std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
  std::chrono::steady_clock::time_point begin_total = std::chrono::steady_clock::now();
  std::chrono::steady_clock::time_point end_total = std::chrono::steady_clock::now();
  Tick_DiscreteValueConstant tick(TICK_EXECUTION_TIME);
  
  Count_Time(tick());
  Count_Time(tick());
  Count_Time(tick());
  Count_Time(tick());
  Count_Time(tick());
  Count_Time(tick());
  Count_Time(tick());
  Count_Time(tick());
  Count_Time(tick());
  Count_Time(tick());

  std::cout << "Requested time: " << TICK_EXECUTION_TIME << " µs\n";
  begin_total = std::chrono::steady_clock::now();
  for(int i=0;i<REPETITIONS;i++)
  {
    begin = std::chrono::steady_clock::now();
    Count_Time(tick());
    end = std::chrono::steady_clock::now();
  }
  end_total = std::chrono::steady_clock::now();

  std::cout << "DURATION: " << std::chrono::duration_cast<std::chrono::microseconds>(end_total - begin_total).count()  << " µs\n";
  std::cout << "DURATION (avg): " << std::chrono::duration_cast<std::chrono::microseconds>(end_total - begin_total).count()/REPETITIONS  << " µs\n";
}