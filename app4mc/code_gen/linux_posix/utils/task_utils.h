#ifndef __TASK_UTILS
#define __TASK_UTILS

#include <time.h>

// source: https://wiki.linuxfoundation.org/realtime/documentation/howto/applications/cyclic
struct period_info {
        struct timespec next_period;
        long period_ns;
};

void inc_period(struct period_info *pinfo);

void periodic_task_init(struct period_info *pinfo);

void wait_rest_of_period(struct period_info *pinfo);

#endif //__TASK_UTILS