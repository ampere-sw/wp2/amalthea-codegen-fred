""" The types of items supported by Ticks.

The code generator is currently supporting:
 * org.eclipse.app4mc.amalthea.model.DiscreteValueConstant;
 * org.eclipse.app4mc.amalthea.model.DiscreteValueStatistics;
 * org.eclipse.app4mc.amalthea.model.DiscreteValueGaussDistribution;
 * org.eclipse.app4mc.amalthea.model.DiscreteValueWeibullEstimatorsDistribution.
 * check the following link for more information https://www.eclipse.org/app4mc/help/app4mc-1.0.0/index.html#section3.2.6

Todo: 
    * std_dev is fixed to 10 for DiscreteValueStatistics.
    * PRemain is not used. how to use it for DiscreteValueWeibullEstimatorsDistribution?
    * alpha and beta are fixed for DiscreteValueWeibullEstimatorsDistribution  
"""
from py4j.java_gateway import is_instance_of
import cgen as c # for code_gen

def builder(tick, tick_num, runnable_block, gateway):
    """ Generic tick builder.

    Args:
        tick (org.eclipse.app4mc.amalthea.model.Ticks): An Amalthea tick.
        tick_num (int): The order of the tick, in case there are multiple ticks in the same runnable.
        runnable_block (:class:`cgen.Block`): Cgen Block where the tick declararion must be inserted.
        gateway (py4j.java_gateway.JavaGateway): py4j gateway. 
    
    """
    # if this is the tick declaration part of the code
    if is_instance_of(gateway,tick,gateway.jvm.org.eclipse.app4mc.amalthea.model.DiscreteValueConstant):
        tick_code = "Tick_DiscreteValueConstant tick%d(%d)" % (tick_num, tick.getValue())
        runnable_block.append(c.Statement(tick_code))
    elif is_instance_of(gateway,tick,gateway.jvm.org.eclipse.app4mc.amalthea.model.DiscreteValueStatistics):
        tick_code = "unsigned seed = std::chrono::system_clock::now().time_since_epoch().count()" 
        runnable_block.append(c.Statement(tick_code))
        tick_code = "Tick_DiscreteValueStatistics tick%d(seed, %d, %d, %.2f, %.2f)" % (tick_num, tick.getLowerBound(), tick.getUpperBound(), tick.getAverage(), 10.0)
        runnable_block.append(c.Statement(tick_code))
    elif is_instance_of(gateway,tick,gateway.jvm.org.eclipse.app4mc.amalthea.model.DiscreteValueGaussDistribution):
        tick_code = "unsigned seed = std::chrono::system_clock::now().time_since_epoch().count()" 
        runnable_block.append(c.Statement(tick_code))
        tick_code = "Tick_DiscreteValueGaussDistribution tick%d(seed, %d, %d, %.2f, %.2f)" % (tick_num, tick.getLowerBound(), tick.getUpperBound(), tick.getMean(), tick.getSd())
        runnable_block.append(c.Statement(tick_code))
    elif is_instance_of(gateway,tick,gateway.jvm.org.eclipse.app4mc.amalthea.model.DiscreteValueWeibullEstimatorsDistribution):
        tick_code = "unsigned seed = std::chrono::system_clock::now().time_since_epoch().count()" 
        runnable_block.append(c.Statement(tick_code))
        tick_code = "Tick_DiscreteValueWeibullEstimatorsDistribution tick%d(seed, %d, %d, %.2f, %.2f)" % (tick_num, tick.getLowerBound(), tick.getUpperBound(), 1.0, 2.0)
        runnable_block.append(c.Statement(tick_code))
    else:
        print ("ERROR: invalid sub_type of tick", tick.sub_type)
        return None
    return runnable_block
