""" Generates a C code file of a runnable.

Authors: 
    * Alexandre Amory (November 2020), ReTiS Lab, Scuola Sant'Anna, Pisa, Italy.

TODO:
    * This code for runnable has some repetition for getting the task and the PUs.
      Transform this as a class, similar to the one in task.py
"""
import app4mc.code_gen.linux_posix.tick_item as tick_builder_cg
from app4mc.code_gen.linux_posix.label import Code_Gen_Label

import cgen as c # for code_gen
from py4j.java_gateway import is_instance_of

# these are the types of runnables currently supported
from app4mc.code_gen.linux_posix.runnables.periodic.periodic import Periodic
from app4mc.code_gen.linux_posix.runnables.fred.fred import FRED
from app4mc.code_gen.linux_posix.runnables.openmp.openmp import OpenMP


def save(runnable, gateway, model, pu_type_name, base_path):
    """ Generates a C code file of a runnable.

    Args:
        runnable (:class:`~app4mc.parser.runnable.Runnable`): the reference to a runnable.
        gateway (:class:`py4j.java_gateway.JavaGateway`): py4j gateway. 
        model (:class:`org.eclipse.app4mc.amalthea.model.Amalthea`): the entire Amalthea model.
        pu_type_name (str): 'CPU', 'GPU', and 'Accelerator'.
        base_path (str): The base directory where the code will be saved.
    
    """
    #################################
    # check if it is a FRED or OpenMP runnable-offloading
    # search if the runnable has group of certain names.
    runnable_items = runnable.getActivityGraph().getItems()
    for r_item in runnable_items:
        # The properties are expected to be found under a Group called FPGA
        if is_instance_of(gateway, r_item, gateway.jvm.org.eclipse.app4mc.amalthea.model.Group):
            if r_item.getName() == "FPGA":
                pu_type_name = "Accelerator"
                #print ("WARNING: Found runnable-based offloading for FRED")
                break
            elif r_item.getName() == "OpenMP":
                pu_type_name = "GPU"
                #print ("WARNING: Found runnable-based offloading for OpenMP")
                break
            else:
                # 
                pass

    tcgen = None
    if pu_type_name == "CPU":
        tcgen = Periodic(runnable,model,gateway,'regular')
    elif pu_type_name == "GPU":
        tcgen = OpenMP(runnable,model,gateway,'openmp')
    elif pu_type_name == "Accelerator":
        tcgen = FRED(runnable,model,gateway,'fred')
    else:
        print ("ERROR: processing unit type %s is not supported" % pu_type_name)
        return
    
    # saving the runnable code
    path = str.lower(base_path) + '/src/runnables/'
    filename = path + str.lower(runnable.getName()) + '.cpp'
    with open(filename, 'w') as gen:
        gen.writelines(str(tcgen.generate_code()))
        gen.close()    
    
    return
