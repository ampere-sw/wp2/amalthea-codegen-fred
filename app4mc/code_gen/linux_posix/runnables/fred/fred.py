import sys
import cgen as c # for code_gen
from py4j.java_gateway import is_instance_of

from app4mc.code_gen.linux_posix.runnables.base_runnable import base_runnable
from app4mc.code_gen.linux_posix.label import Code_Gen_Label
import app4mc.code_gen.linux_posix.tick_item as tick_builder_cg

class FRED(base_runnable):
    """ Code generator for runnable offloading with FRED.

    Args:
        runnable (:class:`~app4mc.parser.runnable.Runnable`): the reference to a runnable.
        model (:class:`org.eclipse.app4mc.amalthea.model.Amalthea`): The entire Amalthea model.
        gateway (:class:`py4j.java_gateway.JavaGateway`): py4j gateway. 
        runnable_type (str): 'regular', 'fred', or 'openmp'.
    """

    def __init__(self, runnable, model, gateway, runnable_type):
        base_runnable.__init__(self, runnable, model, gateway, runnable_type)
        # the mapping provided by the base class (see function self.__get_task_mapping())
        # is not usable for runnable-based offloading
        self.pu = self.__get_runnable_mapping()
        self.pu_name = self.pu.getName()
        self.pu_def = self.pu.getDefinition()
        self.pu_def_name = self.pu_def.getName()

        # FRED hw id. there must be a single FRED call per runnable
        self.hw_id = self.__get_hw_id_from_runnable(runnable)

        if not self.__test_requirements():
            print ("ERROR: Aborting runnable generation due to missing requirements")
            sys.exit(1)

    # POSSIBLE BUG WITH RunnableAllocation. It does not return the runnable
    # def __get_runnable_mapping(self):
    #     """ Return the ProcessingUnit, representing an FPGA reconfigurable region, where the self.runnable is mapped.

    #     The following example associates the Runnable 'rand_runnable' with the ProcessingUnit 'FPGA_rr_0'.

    #     .. code-block:: XML

    #         <runnableAllocation>
    #         <customProperties key="ProcessingUnit">
    #             <value xsi:type="am:ReferenceObject" value="FPGA_rr_0?type=ProcessingUnit"/>
    #         </customProperties>
    #         <entity href="amlt:/#rand_runnable?type=Runnable"/>
    #         </runnableAllocation>

    #     Returns:
    #         :class:`org.eclipse.app4mc.amalthea.model.ProcessingUnit`: ProcessingUnit representing a FPGA reconfigurable region.
    #     """
    #     pu = None
    #     for run_alloc in self.model.getMappingModel().getRunnableAllocation():
    #         if run_alloc.getEntity().getName() == self.runnable.getName():
    #             for cp in run_alloc.getCustomProperties():
    #                 # EMap<String, Value> getCustomProperties();
    #                 if cp.getKey() == "ProcessingUnit":
    #                     # get the reference to a ProcessingUnit representing a FPGA reconfigurable region
    #                     pu = cp.getValue()
    #                     pu_def = cp.getValue().getDefinition()
    #                     if pu_def.getPUType() != 'Accelerator':
    #                         print('ERROR: expecting a ProcessingUnit of type Accelerator in ', pu.getName())
    #     return pu


    # TEMPORARY HACK: This hack is used to circumvent the possible bug mentioned above
    def __get_runnable_mapping(self):
        """ Return the ProcessingUnit, representing an FPGA reconfigurable region, where the self.runnable is mapped.

        The following example associates the Runnable 'rand_runnable' with the ProcessingUnit 'FPGA_%02'.

        .. code-block:: XML

            <taskAllocation task="single_task?type=Task" scheduler="task_os?type=TaskScheduler" affinity="FPGA_00?type=ProcessingUnit">
            <customProperties key="Runnable">
                <value xsi:type="am:StringObject" value="simple_runnable"/>
            </customProperties>
            </taskAllocation>

        Returns:
            :class:`org.eclipse.app4mc.amalthea.model.ProcessingUnit`: ProcessingUnit representing a FPGA reconfigurable region.
        """
        pu = None
        for ta in self.model.getMappingModel().getTaskAllocation():
            runn = self.gateway.jvm.org.eclipse.app4mc.amalthea.model.util.CustomPropertyUtil.customGetReference(ta,"Runnable")
            if runn != None and runn.getName() == self.runnable.getName():
                if len(ta.getAffinity()) != 1:
                    print('ERROR: expecting an Affinity length equal to 1 but got', len(ta.getAffinity()))
                    sys.exit(1)
                pu_list = ta.getAffinity()
                for i in pu_list:
                    pu = i
                #print ("ACHEI PU1: ", pu.getName())
                pu_def = pu.getDefinition()
                if pu_def.getPuType().getName() != 'Accelerator':
                    print('ERROR: expecting a ProcessingUnit of type Accelerator in ', pu.getName())
                    sys.exit(1)

        if pu == None:
            print ("ERROR: expecting to find a ProcessingUnit for runnable", self.runnable.getName())
            sys.exit(1)

        #print ("ACHEI PU2: ", pu.getName())
        return pu

    def generate_code(self):
        """ The only method called externally to start the code generation.
        """
        # the task function declaration
        task_code = c.Module([])
        function_body = c.Block()

        task_code = self.__generate_includes(task_code)
        task_code = self.__generate_runnable_comments(task_code)
        function_body = self.__generate_initialization(function_body)
        function_body = self.__generate_runnable_body(function_body)
        task_code.append(c.FunctionBody(c.FunctionDeclaration((c.Value("void", self.runnable.getName())), []),
                function_body))         

        return task_code

    def __generate_includes(self, code_block):
        """ Adds the defines and include required by a runnable.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.
        """
        # return super().__generate_includes(code_block)
        # return base_runnable.__generate_includes(code_block)
        code_block.append(c.Include("chrono"))
        code_block.append(c.Include("atomic"))
        code_block.append(c.Include("cstdint"))
        code_block.append(c.Include("label_utils.h", False))
        code_block.append(c.Include("tick.h", False))
        code_block.append(c.Include("fred_lib.h", False))
        code_block.append(c.Line(""))

        return code_block


    def __generate_initialization(self, code_block):
        """ Includes label declarations and other runnable declarations.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.
        """        
        code_block.append(c.Line("// shared label declarations"))
        # sometimes the same label is accesses multiple times in the same runnable, 
        # thus, it's necessary to check it to avoid multiple declaration of the same variable
        declared_labels = []
        label_accesses = self.gateway.jvm.org.eclipse.app4mc.amalthea.model.util.SoftwareUtil.getLabelAccessList(self.runnable,None)
        for label_access in label_accesses:
            label = label_access.getData()
            if label.getName() not in declared_labels:
                declared_labels.append(label.getName())
                # is it a rw or a wr ?
                rd_wr = label_access.getAccess().toString()
                # get the label
                cg = Code_Gen_Label(label, rd_wr)
                code_block.append(c.Statement("extern " + cg.setup()))

        # TODO: this approach of using val variable will increase the cache hit ratio
        # its better to think in another approach to read/write from memory
        code_block.append(c.Line("// common declarations"))
        code_block.append(c.Statement("uint64_t val=0"))

        code_block.append(c.Line("#ifndef EMULATE_FPGA_OFFLOADING"))
        code_block.append(c.Statement("struct fred_data *fred"))
        code_block.append(c.Statement("struct fred_hw_task *hw_task"))
        code_block.append(c.Statement("uint32_t hw_task_id = %d" % self.hw_id))
        code_block.append(c.Statement("void *buff_in = NULL"))
        code_block.append(c.Statement("void *buff_out = NULL"))
        code_block.append(c.Line("// Initialize communication and bind a HW-task"))
        code_block.append(c.Statement("fred_init(&fred)"))
        code_block.append(c.Statement("fred_bind(fred, &hw_task, hw_task_id)"))
        code_block.append(c.Line("// Map the FPGA data buffers"))
        # TODO: this buffer configuration is not fixed. I need to get this info somehow from the model
        code_block.append(c.Statement("buff_in = fred_map_buff(fred, hw_task, 0)"))
        code_block.append(c.Statement("buff_out = fred_map_buff(fred, hw_task, 1)"))
        code_block.append(c.Line("#endif"))
        code_block.append(c.Line(""))

        return code_block


    def __generate_runnable_comments(self,code_block):
        """ Comments added before the runnable function declararion.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.

        """
        clb, dsp, bram,rr_id = self.__get_resource_from_pu_def(self.pu_def)
        task_description = """
    ###############################
    TASK DESCRIPTION
    Stimuli type: %s
    Mapped PU: %s
    PU type: %s
    PU definition: %s
       FRED Hw id: %d
       Reconf region id: %d
       CLBs: %d
       BRAMs: %d
       DSPs: %d
    ###############################
    """ % ("PeriodicStimuli",self.pu_name,self.pu_type_name,self.pu_def_name,self.hw_id,rr_id,clb,bram,dsp)        
        code_block.append(c.Comment(task_description))
        return code_block

    def __generate_runnable_body(self, code_block):
        """ Generates all label read/write accesses and ticks.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.
        """
        activityGraphItems = self.runnable.getActivityGraph().getItems()
        tick_num = 0
        for item in activityGraphItems:
            if is_instance_of(self.gateway,item,self.gateway.jvm.org.eclipse.app4mc.amalthea.model.LabelAccess):
                # get the label from the labelAccess
                label = item.getData()
                # is it a rw or a wr ?
                # get the label code
                cg = Code_Gen_Label(label, item.getAccess().toString())
                # this call already returns c.Statement
                code_block.append(cg.run())
            elif is_instance_of(self.gateway,item,self.gateway.jvm.org.eclipse.app4mc.amalthea.model.Ticks):
                # a default tick has a single processing unit
                # the ticks will have random behavior according to the distribution specified in the model for this runnable
                if item.getDefault() != None:
                    # get the code declaration
                    tick_builder_cg.builder(item.getDefault(),tick_num, code_block, self.gateway)
                # Extended ticks have data from multiple processing units
                # getExtended() returns a 'EMap<ProcessingUnitDefinition, IDiscreteValueDeviation>'
                # In case the runnable has extended ticks, the following steps are necessary to get the correct tick:
                # 1st) One has to check in the mapping model to which PU definition the task that calls this runnable was mapped.
                # 2nd) Once the PU is known, then it is possible to select the correct tick from the extended ticks
                ext_tick = item.getExtended()
                if len(ext_tick) > 0:
                    for tick_map in ext_tick:
                        current_pu_def_name = tick_map.getTypedKey().getName()
                        tick = tick_map.getTypedValue()
                        # search for the processing unit assigned in the mappingModel
                        if current_pu_def_name == self.pu_def_name:
                            tick_builder_cg.builder(tick,tick_num, code_block, self.gateway)
                            break
        
                code_block.append(c.Statement("long tick_val = tick%d()" % (tick_num)))
                code_block.append(c.Line("#ifdef EMULATE_FPGA_OFFLOADING"))
                code_block.append(c.Line("   Count_Ticks(tick_val);"))
                code_block.append(c.Line("#else"))
                code_block.append(c.Statement("   fred_accel(fred, hw_task)"))
                code_block.append(c.Line("#endif"))

                tick_num +=1
            elif is_instance_of(self.gateway,item,self.gateway.jvm.org.eclipse.app4mc.amalthea.model.Group):
                # ignore Groups. these are used in FPGA offloading
                pass
            else:
                # TODO: Runnable support only LabelAccess and Ticks in the list of activityGraphItems
                print ("ERROR: the runnable %s has a unsupported activityItem" % (self.runnable.getName()))
                break

        return code_block

    def __get_resource_from_pu_def(self, pu_def):
        """ Return the number of FPGA resources used by a reconfigurable region.

        See the example of attributes expected by a Hw runnable with FRED. 
        A FRED runnable also includes 'fpga_model' and 'reconf_if_bandwidth' 
        attributes but those are not used for code generation.

        Args:
            pu_def (:class:`org.eclipse.app4mc.amalthea.model.ProcessingUnitDefinition`): .

        Returns:
            int, int, int, int: clbs, brams, dsps, reconf_region_id.

        .. code-block:: XML

            <items xsi:type="am:Group" name="FPGA" ordered="true">
            <customProperties key="clb">
                <value xsi:type="am:IntegerObject" value="20"/>
            </customProperties>
            <customProperties key="dsp">
                <value xsi:type="am:IntegerObject" value="5"/>
            </customProperties>
            <customProperties key="bram">
                <value xsi:type="am:IntegerObject" value="2"/>
            </customProperties>
            <customProperties key="reconf_region_id">
                <value xsi:type="am:IntegerObject" value="0"/>
            </customProperties>
        """
        clbs = None
        brams = None
        dsps = None
        reconf_region_id = None
        # get the CustomProperty of the FPGA group
        for cp in pu_def.getCustomProperties():
            # cp is of type EMap
            # EMap<String, Value> getCustomProperties();
            if cp.getKey() == "reconf_region_id":
                reconf_region_id = cp.getValue().getValue()
            if cp.getKey() == "clb":
                clbs = cp.getValue().getValue()
            if cp.getKey() == "dsp":
                dsps = cp.getValue().getValue()
            if cp.getKey() == "bram":
                brams = cp.getValue().getValue()

        if reconf_region_id == None:
            print ("ERROR: expecting CustomProperty name 'reconf_region_id' for ProcessingUnitDefinition", pu_def.getName())
            sys.exit(1)
        if brams == None:
            print ("ERROR: expecting CustomProperty name 'bram' for ProcessingUnitDefinition", pu_def.getName())
            sys.exit(1)
        if dsps == None:
            print ("ERROR: expecting CustomProperty name 'dsp' for ProcessingUnitDefinition", pu_def.getName())
            sys.exit(1)
        if clbs == None:
            print ("ERROR: expecting CustomProperty name 'clb' for ProcessingUnitDefinition", pu_def.getName())
            sys.exit(1)
        return clbs, brams, dsps,reconf_region_id

    def __get_hw_id_from_runnable(self, runnable) -> int:
        """ Return the customProperties called hw_id of Hw runnable using FRED.

        See the example of attributes expected by a Hw runnable with FRED. 
        A FRED runnable also includes 'hdl_top_name' and 'ip_name' attributes but those
        are not used for code generation.

        Args:
            runnable (:class:`org.eclipse.app4mc.amalthea.model.Task`): An Amalthea runnable.

        Returns:
            int: FRED hw runnable id.

        .. code-block:: XML

            <items xsi:type="am:Group" name="FPGA" ordered="true">
            <customProperties key="hdl_top_name">
                <value xsi:type="am:StringObject" value="hw_task_0"/>
            </customProperties>
            <customProperties key="ip_name">
                <value xsi:type="am:StringObject" value="memcpy"/>
            </customProperties>
            <customProperties key="hw_id">
                <value xsi:type="am:IntegerObject" value="100"/>
            </customProperties>
            </items>
        """
        hw_id = None
        # search if the runnable has customProperty 'hw_id'.
        # there should be only one runnable with this property in the list of runnables
        runnable_items = runnable.getActivityGraph().getItems()
        for r_item in runnable_items:
            # The properties are expected to be found under a Group called FPGA
            if is_instance_of(self.gateway, r_item, self.gateway.jvm.org.eclipse.app4mc.amalthea.model.Group):
                if r_item.getName() == "FPGA":
                    # get the CustomProperty of the FPGA group
                    for cp in r_item.getCustomProperties():
                        # cp is of type EMap
                        # EMap<String, Value> getCustomProperties();
                        if cp.getKey() == "hw_id":
                            hw_id = cp.getValue().getValue()

        # if this is true, then the task does not have the requires attributes
        if hw_id == None:
           print ("ERROR: A FRED runnable %s must have the CustomProperty 'hw_id'" % self.runnable.getName())
           sys.exit(1)

        return hw_id

    def __test_requirements(self):
        """ Test here all the requirements for a periodic task.

        This method writes into attributes:
            * self.use_mutex
            * self.event_name
            * self.ipc_stim_name

        Returns:
            Boolean: Returns True if the requirements are met. Returns False otherwise.

        TODO: 
            * test all the required CustomProperties
        """
        # if len(self.task.getStimuli()) != 1:
        #     print ("ERROR: a Periodic task must have a single Stimulus of type PeriodicStimulus")
        #     return False
        # if not is_instance_of(self.gateway, self.task.getStimuli()[0], self.gateway.jvm.org.eclipse.app4mc.amalthea.model.PeriodicStimulus):
        #     print ("ERROR: a Periodic task must have a Stimulus of type PeriodicStimulus")
        #     return False
        
        if self.task.getActivityGraph() == None:
            print ("ERROR: ActivityGraph not found for task", self.task.getName())
            sys.exit(1)
        if len(self.task.getActivityGraph().getItems()) == 0:
            print ("ERROR: ActivityGraph for task", self.task.getName(), 'is empty')
            sys.exit(1)

        # check if the runnable has the required activityItems
        activityGraphItems = self.runnable.getActivityGraph().getItems()
        for item in activityGraphItems:
            if is_instance_of(self.gateway,item,self.gateway.jvm.org.eclipse.app4mc.amalthea.model.LabelAccess):
                # get the label from the labelAccess
                pass
            elif is_instance_of(self.gateway,item,self.gateway.jvm.org.eclipse.app4mc.amalthea.model.Ticks):
                # ignore ticks since they are generated elsewere
                pass
            elif is_instance_of(self.gateway,item,self.gateway.jvm.org.eclipse.app4mc.amalthea.model.Group):
                # ignore Groups. these are used in FPGA offloading
                pass
            else:
                # TODO: Runnable support only LabelAccess and Ticks in the list of activityGraphItems
                print ("ERROR: the runnable %s has a unsupported activityItem" % (self.runnable.getName()))
                sys.exit(1)

        return True
