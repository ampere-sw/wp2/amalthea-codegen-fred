
#from abc import ABC, abstractmethod
import cgen as c

#class base_runnable(ABC):
class base_runnable:
    """ Abstract base class for runnable code generators.

    Args:
        runnable (:class:`~app4mc.parser.runnable.Runnable`): the reference to a runnable.
        model (:class:`org.eclipse.app4mc.amalthea.model.Amalthea`): The entire Amalthea model.
        gateway (:class:`py4j.java_gateway.JavaGateway`): py4j gateway. 
        runnable_type (str): 'regular', 'fred', or 'openmp'.
    """

    def __init__(self, runnable, model, gateway, runnable_type):
        # model related attributes
        #super().__init__()
        self.runnable = runnable
        self.model = model
        self.gateway = gateway

        # the types of runnables: 'regular', 'fred', or 'openmp'
        self.runnable_type = runnable_type

        # the task that calls this runnable
        tasks = gateway.jvm.org.eclipse.app4mc.amalthea.model.util.SoftwareUtil.getCallingProcesses(runnable,None)
        self.task = None
        if (len(tasks) == 1):
            self.task = tasks[0]
        else:
            print ("WARNING: expecting a single task calling a runnable")

        task_mapping_tuple = self.__get_task_mapping()
        if task_mapping_tuple == ():
            print ("ERROR: task %s has no mapping" % (self.task.getName()))
            #return
        # tuple unpacking
        self.pu, self.pu_name, self.pu_def_name, self.pu_type_name = task_mapping_tuple


    #@abstractmethod
    def generate_code(self):
        pass

    #@abstractmethod
    def __generate_includes(self, code_block):
        """ Adds the defines and include required by a runnable.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.
        """
        # code_block.append(c.Include("chrono"))
        # code_block.append(c.Include("atomic"))
        # code_block.append(c.Include("cstdint"))
        # code_block.append(c.Include("label_utils.h", False))
        # code_block.append(c.Include("tick.h", False))
        # code_block.append(c.Line(""))

        # return code_block
        pass

    #@abstractmethod
    def __generate_initialization(self, code_block):
        pass

    #@abstractmethod
    def __generate_runnable_comments(self, code_block):
        pass

    #@abstractmethod
    def __generate_runnable_body(self, code_block):
        pass

    def __get_task_mapping(self) -> tuple():
        """ Extract the task mapping information out of the model.

        This function can only be called in conventional task-based mappings, 
        which is not the case for gpu and fpga runnable-based offloading.

        Returns:
            tuple: 
                * Returns an empty if there is no mapping for this task.
                * Return tuple type is (pu, pu_name, pu_def_name, pu_type_name).
        """
        # These data must return in the tuple
        pu_name = ""
        pu_def_name = ""
        pu_type_name = ""
        # get the processing unit mapped for this task. PUs is a java Set, not an Array
        pu = None
        PUs = self.gateway.jvm.org.eclipse.app4mc.amalthea.model.util.DeploymentUtil.getAssignedCoreForProcess(self.task,self.model)
        if (len(PUs) != 1):
            print ("WARNING: expecting a task mapped to a single PU")
        # this loop is getting the 1st element of the java Set
        for p in PUs:
            # save the 1st element of the Set
            pu = p
            break
        pu_name = pu.getName()
        pu_def_name = pu.getDefinition().getName()
        pu_type_name = pu.getDefinition().getPuType().getName()
        
        return (pu, pu_name, pu_def_name, pu_type_name)

    #@abstractmethod
    def __test_requirements(self):
        """ Place here all validity checks for each type of task.
        """
