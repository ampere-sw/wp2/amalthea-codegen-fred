import cgen as c # for code_gen
from py4j.java_gateway import is_instance_of

from app4mc.code_gen.linux_posix.runnables.base_runnable import base_runnable
from app4mc.code_gen.linux_posix.label import Code_Gen_Label
import app4mc.code_gen.linux_posix.tick_item as tick_builder_cg

class OpenMP(base_runnable):
    """ Code generator for ordinary runnable for GPU offloading with OpenMP.

    Args:
        runnable (:class:`~app4mc.parser.runnable.Runnable`): the reference to a runnable.
        model (:class:`org.eclipse.app4mc.amalthea.model.Amalthea`): The entire Amalthea model.
        gateway (:class:`py4j.java_gateway.JavaGateway`): py4j gateway. 
        runnable_type (str): 'regular', 'fred', or 'openmp'.

    TODO:
        * A lot of repetitive code compared to the periodic class. 
        * Implement non-blocking offloading. See `example<https://github.com/pc2/OMP-Offloading/blob/master/03_taskwait/src/taskwait.c>__`.
    """

    def __init__(self, runnable, model, gateway, runnable_type):
        base_runnable.__init__(self, runnable, model, gateway, runnable_type)

        if not self.__test_requirements():
            print ("ERROR: Aborting runnable generation due to missing requirements")


    def generate_code(self):
        """ The only method called externally to start the code generation.
        """
        # the task function declaration
        task_code = c.Module([])
        function_body = c.Block()

        task_code = self.__generate_includes(task_code)
        task_code = self.__generate_runnable_comments(task_code)
        function_body = self.__generate_initialization(function_body)
        function_body = self.__generate_runnable_body(function_body)
        task_code.append(c.FunctionBody(c.FunctionDeclaration((c.Value("void", self.runnable.getName())), []),
                function_body))         

        return task_code

    def __generate_includes(self, code_block):
        """ Adds the defines and include required by a runnable.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.
        """
        # return super().__generate_includes(code_block)
        # return base_runnable.__generate_includes(code_block)
        code_block.append(c.Include("chrono"))
        code_block.append(c.Include("atomic"))
        code_block.append(c.Include("cstdint"))
        code_block.append(c.Include("omp.h"))
        code_block.append(c.Include("label_utils.h", False))
        code_block.append(c.Include("tick.h", False))
        code_block.append(c.Line(""))

        return code_block


    def __generate_initialization(self, code_block):
        """ Includes label declarations and other runnable declarations.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.
        """        
        code_block.append(c.Line("// shared label declarations"))
        # sometimes the same label is accesses multiple times in the same runnable, 
        # thus, it's necessary to check it to avoid multiple declaration of the same variable
        declared_labels = []
        label_accesses = self.gateway.jvm.org.eclipse.app4mc.amalthea.model.util.SoftwareUtil.getLabelAccessList(self.runnable,None)
        for label_access in label_accesses:
            label = label_access.getData()
            if label.getName() not in declared_labels:
                declared_labels.append(label.getName())
                # is it a rw or a wr ?
                rd_wr = label_access.getAccess().toString()
                # get the label
                cg = Code_Gen_Label(label, rd_wr)
                code_block.append(c.Statement("extern " + cg.setup()))

        # TODO: this approach of using val variable will increase the cache hit ratio
        # its better to think in another approach to read/write from memory
        code_block.append(c.Line("// common declarations"))
        code_block.append(c.Statement("uint64_t val=0"))

        return code_block


    def __generate_runnable_comments(self,code_block):
        """ Comments added before the runnable function declararion.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.

        """
        task_description = """
    ###############################
    TASK DESCRIPTION
    Stimuli type: %s
    Mapped PU: %s
    PU type: %s
    PU definition: %s
    ###############################
    """ % ("PeriodicStimuli",self.pu_name,self.pu_type_name,self.pu_def_name)        
        code_block.append(c.Comment(task_description))
        return code_block

    def __generate_runnable_body(self, code_block):
        """ Generates all label read/write accesses and ticks.

        Args:
            code_block (:class:`cgen.Block`): Cgen Block where the new statments will be added.

        Returns:
            :class:`cgen.Block`: The same input Cgen Block, but with additional statments.
        """
        activityGraphItems = self.runnable.getActivityGraph().getItems()
        tick_num = 1
        for item in activityGraphItems:
            if is_instance_of(self.gateway,item,self.gateway.jvm.org.eclipse.app4mc.amalthea.model.LabelAccess):
                # get the label from the labelAccess
                label = item.getData()
                # is it a rw or a wr ?
                # get the label code
                cg = Code_Gen_Label(label, item.getAccess().toString())
                # this call already returns c.Statement
                code_block.append(cg.run())
            elif is_instance_of(self.gateway,item,self.gateway.jvm.org.eclipse.app4mc.amalthea.model.Ticks):
                # a default tick has a single processing unit
                # the ticks will have random behavior according to the distribution specified in the model for this runnable
                if item.getDefault() != None:
                    # get the code declaration
                    tick_builder_cg.builder(item.getDefault(),tick_num, code_block, self.gateway)
                # Extended ticks have data from multiple processing units
                # getExtended() returns a 'EMap<ProcessingUnitDefinition, IDiscreteValueDeviation>'
                # In case the runnable has extended ticks, the following steps are necessary to get the correct tick:
                # 1st) One has to check in the mapping model to which PU definition the task that calls this runnable was mapped.
                # 2nd) Once the PU is known, then it is possible to select the correct tick from the extended ticks
                ext_tick = item.getExtended()
                if len(ext_tick) > 0:
                    for tick_map in ext_tick:
                        current_pu_def_name = tick_map.getTypedKey().getName()
                        tick = tick_map.getTypedValue()
                        # search for the processing unit assigned in the mappingModel
                        if current_pu_def_name == self.pu_def_name:
                            tick_builder_cg.builder(tick,tick_num, code_block, self.gateway)
                            break
        
                code_block.append(c.Statement("long tick_val = tick%d()" % (tick_num)))
                code_block.append(c.Line("#ifdef EMULATE_GPU_OFFLOADING"))
                code_block.append(c.Line("   Count_Ticks(tick_val);"))
                code_block.append(c.Line("#else"))
                code_block.append(c.Line("   #pragma omp target map(from: tick_val)"))
                code_block.append(c.Line("   {"))
                code_block.append(c.Line("      for (int i=0; i < tick_val; i++);"))
                code_block.append(c.Line("   }"))
                code_block.append(c.Line("#endif"))
                tick_num +=1
            elif is_instance_of(self.gateway,item,self.gateway.jvm.org.eclipse.app4mc.amalthea.model.Group):
                # ignore Groups
                pass
            else:
                # TODO: Runnable support only LabelAccess and Ticks in the list of activityGraphItems
                print ("ERROR: the runnable %s has a unsupported activityItem" % (self.runnable.getName()))
                break
        return code_block


    def __test_requirements(self):
        """ Test here all the requirements for a periodic task.

        This method writes into attributes:
            * self.use_mutex
            * self.event_name
            * self.ipc_stim_name

        Returns:
            Boolean: Returns True if the requirements are met. Returns False otherwise.

        TODO: 
            * test all the required CustomProperties
        """
        # if len(self.task.getStimuli()) != 1:
        #     print ("ERROR: a Periodic task must have a single Stimulus of type PeriodicStimulus")
        #     return False
        # if not is_instance_of(self.gateway, self.task.getStimuli()[0], self.gateway.jvm.org.eclipse.app4mc.amalthea.model.PeriodicStimulus):
        #     print ("ERROR: a Periodic task must have a Stimulus of type PeriodicStimulus")
        #     return False
        
        if self.task.getActivityGraph() == None:
            print ("ERROR: ActivityGraph not found for task", self.task.getName())
            return False
        if len(self.task.getActivityGraph().getItems()) == 0:
            print ("ERROR: ActivityGraph for task", self.task.getName(), 'is empty')
            return False

        # check if the runnable has the required activityItems
        activityGraphItems = self.runnable.getActivityGraph().getItems()
        for item in activityGraphItems:
            if is_instance_of(self.gateway,item,self.gateway.jvm.org.eclipse.app4mc.amalthea.model.LabelAccess):
                # get the label from the labelAccess
                pass
            elif is_instance_of(self.gateway,item,self.gateway.jvm.org.eclipse.app4mc.amalthea.model.Ticks):
                # ignore ticks since they are generated elsewere
                pass
            elif is_instance_of(self.gateway,item,self.gateway.jvm.org.eclipse.app4mc.amalthea.model.Group):
                # ignore Groups
                pass
            else:
                # TODO: Runnable support only LabelAccess and Ticks in the list of activityGraphItems
                print ("ERROR: the runnable %s has a unsupported activityItem" % (self.runnable.getName()))
                return False

        return True
