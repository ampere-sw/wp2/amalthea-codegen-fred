""" Generates the global labels.

Authors: 
    * Alexandre Amory (November 2020), ReTiS Lab, Scuola Sant'Anna, Pisa, Italy.

"""
import cgen as c # for code_gen
from app4mc.common.conversions import convert_2_bytes

from app4mc.code_gen.linux_posix import label

def save(labels, inter_process_stims, base_path):
    """ Generate a C header file with all the global variables
    
    The global variables include shared labels, mutexes, conditional_variables.

    Args:
        label ([:class:`org.eclipse.app4mc.amalthea.model.Label`]): List of Amalthea labels to be saved in C.
        inter_process_stims ([:class:`org.eclipse.app4mc.amalthea.model.InterProcessStimulus`]): List of mutexes and conditional_variables used to sync threads.
        base_path (str): base directory where the code will be saved.
    """
    call_sequence = "#ifndef __LABELS\n"
    call_sequence += "#define __LABELS\n"
    call_sequence += "\n"
    call_sequence += "#include <atomic>\n"
    call_sequence += "#include <cstdint>\n"
    if len(inter_process_stims) > 0 :
        call_sequence += "#include \"offloading_sync.h\"\n"
    call_sequence += "#include \"label_utils.h\"\n"
    call_sequence += "\n"
    # mutex and conditional variable used to sync between the HOST and GPU/FPGA tasks
    for i in inter_process_stims:
        call_sequence += "struct offloading_sync sync_%s;\n" % i.getName()
    call_sequence += "\n"
    # shared labels
    for i in labels:
        # in this usage, label gen should only declarate the each label. 
        cg = label.Code_Gen_Label(i, "read")
        call_sequence += cg.setup() + ";\n"
    call_sequence += "\n"
    call_sequence += "#endif // __LABELS\n"

    path = str.lower(base_path) + '/include/runnables/'
    with open(path+"label.h", 'w') as gen:
        gen.writelines(call_sequence)
        gen.close()
