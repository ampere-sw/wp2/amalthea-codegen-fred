""" Code generator for label declaration.

Authors: 
    * Alexandre Amory (November 2020), ReTiS Lab, Scuola Sant'Anna, Pisa, Italy.

"""
import cgen as c # for code_gen
from app4mc.common.conversions import convert_2_bytes

class Code_Gen_Label:
    """ Code generator for label declaration.

    Receive the object with all the information to generate this piece of code.

    Args:
        label (:class:`org.eclipse.app4mc.amalthea.model.Label`): An Amalthea label.
        access (str): type of access to the label: read or write.

    Todo:
        * attributes namespace, stabilityLevel, section are not been used;
        * attribute constant is being ignored because it leads to link error in Linux POSIX. why ?!?!
    """
    def __init__(self, label, access):
        self.label = label
        self.access = access

    def setup(self):
        """ Generate the code related to label declaration.

        Returns:
            str: string with C code statements.
        """   
        decl = ""
        # TODO: ignoring for now because it leads to linking errors
        #if self.label.constant:
        #    decl += "const "
        if self.label.isBVolatile():
            decl += "volatile "

        dtype = self.__define_datatype(self.label.getSize())

        if self.label.getDataStability().toString() == 'automaticProtection':
            # add atomic for scalars and Atomic_Vector for arrays
            if dtype[0] == "vector":
                # vectors are always of 1 byte size
                decl += "Atomic_Array<" + str(int(dtype[1])) + "> " + self.label.getName()
            else:
                decl +="std::atomic<" + dtype[0]  + "> " + self.label.getName()
        else: 
            # for any other dataStability, it is assuming that there is no protection
            if dtype[0] == "vector":
                # vectors are always of 1 byte size
                decl += "uint8_t " + self.label.getName() + " [" + str(int(dtype[1])) + "]"
            else:
                decl += dtype[0]  + " " + self.label.getName()

        return decl

    def run(self):
        """ Generate the code where the label is accessed.

        Returns:
            :class:`cgen.Statement`: C code statements to access the labels.
        """
        dtype = self.__define_datatype(self.label.getSize())
        if dtype[0] == "vector":
            if self.label.getDataStability().toString() == 'automaticProtection':
                # requires use of the Atomic_Array class to lock the write access to a single thread
                if self.access == "read":
                    #access = [
                    #    c.Line("// read the entire vector to accumulate the value in 'val'"),
                    #    c.Statement(self.label.name + ".acc(&val)")
                    #    ]
                    #access = c.Line("// read the entire vector to accumulate the value in 'val'")
                    access = c.Statement(self.label.getName() + ".acc(&val)")
                else:
                    #access = [
                    #    c.Line("// write the entire vector with the same 'val'"),
                    #    c.Statement(self.label.name + ".set((uint8_t)val)")
                    #    ]
                    #access = c.Line("// write the entire vector with the same 'val'")
                    access = c.Statement(self.label.getName() + ".set((uint8_t)val)")
            else:
                # it calls different read/write functions depending on the size of the array
                # there 4 types of array rw/wr functions:
                #   - read_label, write_label ==> for arrays of any length, including lenght 1
                # no protection to the arrays, i.e. just a normal vector read/write
                access = c.Statement("%s_label(%s,%ld)" % (self.access, self.label.getName(),dtype[1]))
        else:
            # if it is a scalar type, then it does not matter if it is atomic or not
            if self.access == "read":
                access = c.Statement("val = (uint64_t)" + self.label.getName())
            else:
                access = c.Statement(self.label.getName() + " = (%s)val" % (dtype[0]))

        return access

    def __define_datatype(self, dtype):
        """ returns the adequate C type based on the size of the data.

        Args:
            dtype (:class:`org.eclipse.app4mc.amalthea.model.DataSize`): Amalthea type with the value and unit to be converted.

        Returns:
            tuple: tuple ('C type',size) corresponding to dtype.
        """
        # The attribute DataSize is mandatory for Labels
        if dtype != None:
            bytes = dtype.getNumberBytes()

            if bytes <= 1:
                return ("uint8_t",0)
            elif bytes <= 2:
                return ("uint16_t",0)
            elif bytes <= 4:
                return ("uint32_t",0)
            elif bytes <= 8:
                return ("uint64_t",0)
            else:
                # then it is a vector. by default, it will be a vector of char
                return ("vector",bytes)
        else:
            print ("WARNINIG: The attribute DataSize is mandatory for Labels")
            return ("uint32_t",0)
