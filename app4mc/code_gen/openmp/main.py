""" Generates the OpenMP-based main().

Authors: 
    * Alexandre Amory (January 2021), ReTiS Lab, Scuola Sant'Anna, Pisa, Italy.

"""
import cgen as c # for code_gen

def main(tasks, base_path):
    """ Generate the main function (OpenMP) for the model.

    Args:
        tasks ([ org.eclipse.app4mc.amalthea.model.Task ]): the list of tasks.
        base_path (str): base directory where the code will be saved.
    """
    code = ""
    code += "#include <unistd.h>\n"
    code += "#include <iostream>\n"
    code += "#include <omp.h>\n"
    code += "#include <cstdlib>\n"
    code += "#include <signal.h>\n"
    code += "// task prototypes\n"
    code += "#include \"tasks.h\"\n"
    code += "// create the shared data\n"
    code += "#include \"label.h\"\n"
    code += "\n"
    code += "// Define the function to be called when ctrl-c (SIGINT) is sent to process\n"
    code += "void signal_callback_handler(int signum) {\n"
    code += "   std::cout << \"Caught signal \" << signum << std::endl;\n"
    code += "   // Terminate program\n"
    code += "   exit(signum);\n"
    code += "}\n"
    code += "\n"

    # creating the threads
    main_statements = c.Block()
    main_statements.append(c.Statement("signal(SIGINT, signal_callback_handler)"))
    main_statements.append(c.Line(""))
    main_statements.append(c.Line("#pragma omp parallel"))
    openmp_single = c.Block([c.Line("#pragma omp single")])
    openmp_tasks = c.Block()
    for i in tasks:
        openmp_tasks.append(c.Line("#pragma omp task"))
        openmp_tasks.append(c.Statement("%s()" % i.getName()))
    # put the omp task block inside the omp single block
    openmp_single.append(openmp_tasks)
    # put the omp single block inside the omp parallel block
    main_statements.append(openmp_single)
    main_statements.append(c.Line(""))
    main_statements.append(c.Line("// it will never reach here"))
    main_statements.append(c.Statement("pause()"))
    main_statements.append(c.Statement("return EXIT_SUCCESS"))

    # the task function declaration
    main_body = c.Module([
        c.FunctionBody(
            c.FunctionDeclaration((c.Value("int", "main")), []),
            main_statements
        )
    ]) 
    
    # create the dir structure where the task C code is saved
    path = str.lower(base_path) + '/src/'
    filename = path + 'main.cpp'
    with open(filename, 'w') as gen:
        gen.writelines(code)
        gen.writelines(str(main_body))
        gen.close()

      