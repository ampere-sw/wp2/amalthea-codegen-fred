""" Amalthea code generator for heterogeneous systems.

This is the *main* file of the Amalthea code genrator.
It calls the official Amalthea parser to load the model's data into memory 
and then it calls the code generator. 

Authors: 
    * Alexandre Amory (November 2020), ReTiS Lab, Scuola Sant'Anna, Pisa, Italy.

TODO: 
    * Currently, if one wants to generate code for a different target, e.g. a microcontroller,
        it is necessary to mannualy change the import of this file from 

        .. code-block:: python

            import app4mc.code_gen.linux_posix.sw_model as code_gen_sw

        to 

        .. code-block:: python

            import app4mc.code_gen.freertos.sw_model as code_gen_sw

        However, it is possible to use `Dynamic Imports <https://realpython.com/python-import/#using-importlib>`_ 
        in Python. This way, the user could pass one of the supported code generators as a parameter
        then the appropriate module is loaded.
    * It would be easier for the user if it launched the Java server in this point.
        See examples in https://stackoverflow.com/questions/3652554/calling-java-from-python
"""

# THESE ARE SOME RECOMMENDED READINGS IF YOU ARE PLANNING TO CHANGE THIS CODE
# app structure
# https://realpython.com/python-application-layouts/
# for references about how to write docstrings, please refer to 
# https://sphinxcontrib-napoleon.readthedocs.io/en/latest/index.html
# recomendations for python import and modules
# https://realpython.com/python-import/

# Standard library imports
import sys
import os
import argparse
import time
import subprocess
import atexit

# internal imports
# official/automated parser
from py4j.java_gateway import JavaGateway, GatewayParameters, get_java_class
import app4mc.code_gen.main as code_gen


################################
# Global Vars
################################
proc_java = None
""" **Global var|** Process handler used with py4j_server"""
py4j_gateway = None
""" **Global var|** The client connection to the py4j_server"""

################################
# Aux Functions
################################

def dir_path(path):
    """ It checks the destination path.

    Checks if the parameter is a valid directory with write permission.
    If the dir does not exist, then it is created if there is permission to do so.

    Args:
        path (str): the destination path.

    Returns:
        str: the string to the destination path.
    """
    # test if output directory exists. If not, create it
    if os.path.exists(path):
        if not os.path.isdir(path):
            raise argparse.ArgumentTypeError(f"ERROR: {path} is not a directory!")
        else:
            # it is a valid dir
            return path
    else:
        # create the output dir
        try:
            os.makedirs(path)
            # it is a valid dir
            return path
        except OSError as e:
            # this case can happen if the user does not have privilegies to create the folder
            raise argparse.ArgumentTypeError(f"ERROR: could not create directory {path}!")


def call_py4j_server(model_file_name, py4j_port):
    """ Call py4j_server as another OS process.

    Since the Java server never ends, it launches the process and continue 
    the Python program execution.

    Args:
        model_file_name (str): Amalthea filename model.
        py4j_port (int): The port to connect to the py4j server, Java side.
    """
    global proc_java
    print ("\nMODEL:", model_file_name)
    cmdline = ["/usr/bin/java", "org.eclipse.app4mc.tools.py4j_server.App", model_file_name, str(py4j_port)]

    cur_dir = os.getcwd()
    cmd_args = " ".join(map(str,cmdline))
    print ("$PWD ==",cur_dir)
    print ("$>", cmd_args)

    try:
        proc_java = subprocess.Popen(cmdline,stdout=subprocess.PIPE,stderr=subprocess.PIPE )
    except subprocess.CalledProcessError as e:
        print(e.output)
        print ("ERROR: could not call py4j_server. Perhaps the CLASSPATH variable was not set ?!?!")
        print ("Error running '/usr/bin/java org.eclipse.app4mc.tools.py4j_server.App %s'" % model_file_name)
        sys.exit(1)

    # waste some time until the server if fully up
    print ("Waiting the py4j_server to go up ... ", end ="")
    time.sleep(5)
    print ("Done!")

    # test is the process is still up. If not, then it must be an error
    if proc_java.poll() != None:
        print("ERROR: py4j_server ended too soon. ret code:", proc_java.returncode)
        print ("Error running '/usr/bin/java org.eclipse.app4mc.tools.py4j_server.App %s'" % model_file_name)
        sys.exit(1)
    # if process is still up, then we are good to continue...

def exit_handler():
    """ Close the Java process before exiting Python.
    
    This function is automatically called before exiting Python.
    """
    global proc_java
    global py4j_gateway
    print ("Killing the processes and closing socket connections...")
    # closing py4j connection
    if py4j_gateway != None:
        py4j_gateway.shutdown(raise_exception=False)    
    # if the process is running, then kill it
    if proc_java != None:
        if proc_java.poll() == None:
            proc_java.kill()
    time.sleep(1)


def main():
    """ An example of application that parses the app4mc model and generates C code. 

    """

    # parsing arguments
    parser = argparse.ArgumentParser()
    parser.add_argument('in_file', type=str,
                        help='Input AMXMI file with the Amalthea model.'
                        )
    parser.add_argument('out_dir', type=dir_path, 
                        help='Output directory where the generated code will be saved.'
                        )
    parser.add_argument('--py4j-port', dest='py4j_port', type=int,
                        help='The py4j port for the Amalthea server. The default value is 25333. The port number must be > 1024.')
    parser.set_defaults(py4j_port=25333)
    parser.add_argument('-tick_type', type=str, choices=['WCET','BCET','ACET','distribution'], 
                        default='WCET',
                        help='''The options of behaviour for the Amalthea Ticks used in the Runnables. 
                            The possible values are BCET, ACET, WCET, and distribution. WCET is the default.'''
                        )
    parser.add_argument('-par_lib', type=str, choices=['posix','openmp'], 
                        default='posix',
                        help='''The supported parallel libraries.'''
                        )
    #parser.add_argument('-v','--verbose', dest='verbose', default=0, type=int,
    #                    help='The verbosity level. 0 means no verbose and 5 is max level.')
    #parser.add_argument('-s','--simtime', dest='sim_time', default=0, type=int,
    #            help='The number of OS ticks to be simulated.')
    #parser.add_argument('--sched', default='rms', nargs='?', choices=['rms', 'edf'],
    #                    help='list of supported task scheduling algoritms (default: %(default)s)')

    args = parser.parse_args()
    
    # TODO The vebosity parameter was removed when migrating from the custom to the official parser.
    # However, this is a nice feature that wish to address some day. 
    # For this reason, these comments were not removed.
    #if not (args.verbose >= 0 and args.verbose <= 5):
    #    print("ERROR: Verbosity level must be between 0 and 5.")
    #    return

    # PYAPP4MC_HOME is mandatory to lauch the Java py4j_server
    if 'PYAPP4MC_HOME' not in os.environ:
        print ("ERROR: environment variable PYAPP4MC_HOME not set.\n")
        exit(1)
    pyapp4mc = os.environ['PYAPP4MC_HOME']
    if not ((os.path.isdir(pyapp4mc) and (os.path.exists(pyapp4mc)))):
        print ("ERROR: environment variable PYAPP4MC_HOME is not set to a directory.\n")
        exit(1)

    # CLASSPATH is mandatory to lauch the Java py4j_server
    if 'CLASSPATH' not in os.environ:
        print ("ERROR: environment variable CLASSPATH not set.\n")
        exit(1)
    if not ("py4j" in os.environ['CLASSPATH']):
        print ("ERROR: please check whether the CLASSPATH variable is set to run py4j_server.\n")
        exit(1)

    if not os.path.isfile(args.in_file):
        print ("ERROR: File %s not found" % (args.in_file))
        exit(1)

    # the exit_handler function is called to kill the Java and DART processes
    atexit.register(exit_handler)

    # call the Java py4j_server process, the Amalthea model server
    call_py4j_server(args.in_file, args.py4j_port)

    # TODO It would be easier for the user if it launched the Java server in this point.
    # see examples in https://stackoverflow.com/questions/3652554/calling-java-from-python

    # connect to the py4j Java server
    global py4j_gateway

    # connect to the py4j Java server
    py4j_gateway = JavaGateway(gateway_parameters=GatewayParameters(port=args.py4j_port))
    dataMap = py4j_gateway.entry_point

    if dataMap==None:
        print("ERROR: Amalthea model is not available in the Gateway Server")
        return

    amaltheaModel=dataMap.get("amaltheaModel")
    print("Extracting data from Amalthea model available in the GateWay server: ", amaltheaModel.eResource().getURI())

    sw_model = amaltheaModel.getSwModel()

    tasks = sw_model.getTasks()
    print ("\nTASKS:")
    for task in tasks:
        print (" -",task.getName())
    print ("Total of %d tasks" % len(tasks))

    runnables=sw_model.getRunnables()
    print ("\nRUNNABLES:")
    for runnable in runnables:
        print(" -",runnable.getName())
    print ("Total of %d runnables" % len(runnables))

    stim_model = amaltheaModel.getStimuliModel().getStimuli()
    print ("\nSTIM:")
    for stim in stim_model:
        print (" -",stim.getName())
    print ("Total of %d stimuli" % len(stim_model))

    #hws = amaltheaModel.getHwModel().getStructures()
    hw_model = amaltheaModel.getHwModel()
    print ("\nHW:")
    for hw in hw_model.getStructures():
        print (" -",hw.getName())
    print ("Total of %d Hw structures" % len(hw_model.getStructures()))
    
    puClass = dataMap.get("ProcessingUnit.class")
    PUs = py4j_gateway.jvm.org.eclipse.app4mc.amalthea.model.util.HardwareUtil.getModulesFromHwModel(puClass,amaltheaModel)
    print("\nPU:")
    for pu in PUs:
        print(" -",pu.getName())
    print ("Total of %d processing units" % len(PUs))

    # get constraints
    print("\nConstraints:")
    constraint_model = amaltheaModel.getConstraintsModel()
    if constraint_model== None:
        print("ERROR: Amalthea constraint model not available")
        return
    if len(constraint_model.getTimingConstraints()) > 0:
        print ("WARNINIG: Amalthea TimingConstraint is not supported")
    if len(constraint_model.getAffinityConstraints()) > 0:
        print ("WARNINIG: Amalthea AffinityConstraint is not supported")
    requirement_constraints = constraint_model.getRequirements()
    for req in requirement_constraints:
        print(" -",req.getName(), req.getLimit().getLimitType().getName())

    print("\nTask mapping:")
    # show the tasks assigned to each processing unit
    mapping_model = amaltheaModel.getMappingModel()
    for task_alloc in mapping_model.getTaskAllocation():    
        pu_list = task_alloc.getAffinity()
        task = task_alloc.getTask()
        pu_names = ''
        for pu  in pu_list:
            pu_names += pu.getName() + " "
        print(" -",task.getName(),':',pu_names)
    # print("\nRunnable mapping:")
    # for runn_alloc in mapping_model.getRunnableAllocation():
    #     # POSSIBLE BUG. the RunnableAllocation class says that the return type for getEntity() is org.eclipse.app4mc.amalthea.model.Runnable but it seems that it should be Runnable
    #     runnable = runn_alloc.getEntity()
    #     # POSSIBLE BUG. The XML says the type is TaskScheduler by the RunnableAllocation class says it is Scheduler
    #     scheduler = runn_alloc.getScheduler()
    #     runnable_name = runnable.getName()
    #     sched_name = scheduler.getName()
    #     print (" -",runnable_name, sched_name)
    #     #print (len(runnable.getActivityGraph().getItems()))
    print("")

    # # generate code for Linux POSIX or OpenMP
    code_gen.run(amaltheaModel, py4j_gateway, args.tick_type, args.par_lib, args.out_dir)

if __name__ == '__main__':
    main()