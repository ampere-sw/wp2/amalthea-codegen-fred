
def convert_2_us(dtype) -> float:
    """ Converts data size units to bytes.

    DEPRECATED: use the official conversion function. For example
        us = gateway.jvm.org.eclipse.app4mc.amalthea.model.TimeUnit.US
        ticks_to_time_us = gateway.jvm.org.eclipse.app4mc.amalthea.model.util.TimeUtil.convertToTimeUnit(ticks_to_time, us).getValue().toString()

    Args:
        dtype (`tuple(int,str)`): tuple with the value and the time unit to be converted.

    Returns:
        float: number of microseconds.
    """
    val = float(dtype[0])
    units = {
        "s"   : val*(10**6),
        "ms"  : val*(10**3),
        "us"  : val,
        "ns"  : (val/(10**3)),
        "ps"  : (val/(10**6))
    }

    return float(units[dtype[1]])


def convert_2_bytes(dtype) -> float:
    """ Converts data size units to bytes.

    Args:
        dtype (org.eclipse.app4mc.amalthea.model.DataSize): tuple with the value and the data size unit to be converted.

    Returns:
        float: number of bytes
    """

    #val = float(dtype[0])
    val = dtype.getValue().floatValue()
    units = {
        "bit"   : val/8.0,
        "kbit"  : (val*(10**3))/8.0,
        "Mbit"  : (val*(10**6))/8.0,
        "Gbit"  : (val*(10**9))/8.0,
        "Tbit"  : (val*(10**12))/8.0,
        "Kibit" : (val*(2**10))/8.0,
        "Mibit" : (val*(2**20))/8.0,
        "Gibit" : (val*(2**30))/8.0,
        "Tibit" : (val*(2**40))/8.0,
        "B"     : val,
        "kB"    : (val*(10**3)),
        "MB"    : (val*(10**6)),
        "GB"    : (val*(10**9)),
        "TB"    : (val*(10**12)),
        "KiB"   : (val*(2**10)),
        "MiB"   : (val*(2**20)),
        "GiB"   : (val*(2**30)),
        "TiB"   : (val*(2**40))
    }

    return float(units[dtype.getUnit().getName()])