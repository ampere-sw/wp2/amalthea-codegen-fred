
class Amalthea_obj:
    """ A base class for the classes with the *type* attribute.

    TODO:
        * this code would be simpler if i could use wildcard to search for atributes.
    """

    def get(self):
        """ A getter for this class.

        Returns:
            pointer: 'pointer' to the object.
        """
        return self

    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        return vars(self)

    def parse_xml(self, tree):
        """ Parses the attributes of this class from the app4mc XML file. 
        
        Pay attention to the parsing limitations marked with TODO.

        Args:
            tree (lxml.etree): xml tree with all information of the object.

        Returns:
            bool: True if parsed succesfully.

        """
        self.obj_type = ""
        for k, v in tree.attrib.iteritems():
            # TODO this code would be simpler if i could use wildcard to search for atributes
            # get the last 4 chars, in case of the 'type' attribute
            if k[-4:] == "type":
                # get only the part of the value after the ':'.
                # example. 'am:LabelAccess' => 'LabelAccess'
                self.obj_type = v.split(":",1)[1]
                return True

        # if it ever gets here, then this tag has nno attribute called 'type'
        print ("ERROR: tag", tree.tag, "must have a type.")
        return False
        