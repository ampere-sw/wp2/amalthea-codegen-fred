
class Amalthea_item:
    """ A base class for the classes with the *name* attribute.

    The difference between an Amalthea_obj and an Amalthea_item is that
    the former has a mandatory 'type' and the later has a mandatory 'name'.

    Args:
        name (str): name of the item.

    """

    def get(self):
        """ A getter for this class.

        Returns:
            pointer: 'pointer' to the item.
        """
        return self

    def __str__(self):
        """ Returns a string with all attributes of the class.

        Returns:
            str: string with all attributes of the class.
        """
        return vars(self)

    def parse_xml(self, tree):
        """ Parses the attributes of this class from the app4mc XML file. 
        
        Pay attention to the parsing limitations marked with TODO.

        Args:
            tree (lxml.etree): xml tree with all information of the item.

        Returns:
            Bool: true if the name of the item was found.

        """
        n = tree.get('name')
        if n != None:
            self.name = n
        else:
            print ("ERROR: tag", tree.tag, "must have a name.")
            return False
        return True
