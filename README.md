# Amalthea C++ Codegen

A Python-based Amalthea parser and Linux/Posix C++ code generation.

## Installing depedencies and Doc generation

Execute the following commands to install the initial depedencies:

    $ sudo apt-get install python3-venv
    $ export GIT_SSL_NO_VERIFY=1
    $ git clone https://gitlab.retis.santannapisa.it/ampere/amalthea-codegen-linux.git
    $ cd amalthea-codegen-linux
    $ python3 -m venv env
    $ source env/bin/activate
    $ pip install --editable .

The complete documentation is located in the docs directory. To generate the docs, run:

    $ cd docs
    $ make html
    $ firefox ./build/html/index.html

