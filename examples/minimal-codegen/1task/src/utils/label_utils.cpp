#include "label_utils.h"

uint64_t read_label(uint8_t *p, int size){
  register uint64_t *p_start= (uint64_t*)p;
  // 64 is the number of bytes moved inside the 1st while loop
  uint64_t left_over = size & (64-1);
  uint64_t multiple_of_64 = size - left_over;
  register uint64_t *p_end_multiple_of_64= (uint64_t*)&(p[multiple_of_64-1]);
  // pointers to the 2nd loop
  register uint8_t *p_start2= &(p[multiple_of_64]);
  register uint8_t *p_end= &(p[size-1]);
  register uint64_t val=0;
  // executes 8 moves of 8 bytes each
  while(p_start<p_end_multiple_of_64){
    val += p_start[0] + p_start[1] + p_start[2] + p_start[3] + 
           p_start[4] + p_start[5] + p_start[6] + p_start[7];
    p_start += 8;
  }
  // executes the remaining moves, byte by byte
  while(p_start2<=p_end){
    val += *p_start2;
    p_start2 ++;
  }
  return val;
}


uint64_t write_label(uint8_t *p, int size){
  register uint64_t *p_start= (uint64_t*)p;
  // 64 is the number of bytes moved inside the 1st while loop
  uint64_t left_over = size & (64-1);
  uint64_t multiple_of_64 = size - left_over;
  register uint64_t *p_end_multiple_of_64= (uint64_t*)&(p[multiple_of_64-1]);
  // pointers to the 2nd loop
  register uint8_t *p_start2= &(p[multiple_of_64]);
  register uint8_t *p_end= &(p[size-1]);
  register uint64_t val=0;
  // executes 8 moves of 8 bytes each
  while(p_start<p_end_multiple_of_64){
    p_start[0] = 0xAFFFFFFE;
    p_start[1] = 0xAFFFFFFE;
    p_start[2] = 0xAFFFFFFE;
    p_start[3] = 0xAFFFFFFE;
    p_start[4] = 0xAFFFFFFE;
    p_start[5] = 0xAFFFFFFE;
    p_start[6] = 0xAFFFFFFE;
    p_start[7] = 0xAFFFFFFE;
    p_start +=8;    
  }
  // executes the remaining moves, byte by byte
  while(p_start2<=p_end){
    *(p_start2)   = 0xAF;
    p_start2 ++;
  }
  return val;
}


void Dump( const void * mem, unsigned int n ) {
  const char * p = reinterpret_cast< const char *>( mem );
  for ( unsigned int i = 0; i < n; i++ ) {
     std::cout << std::hex << int(p[i]) << " ";
  }
  std::cout << std::endl;
}