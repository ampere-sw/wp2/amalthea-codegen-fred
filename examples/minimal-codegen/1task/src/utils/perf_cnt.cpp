#ifdef USE_PAPI
#include "perf_cnt.h"
#include <stdlib.h>
#include <string.h>
#include <papi.h>


/*
see slide 15 of https://indico-jsc.fz-juelich.de/event/84/session/1/contribution/2/material/1/0.pdf
#define PAPI_CALL( call, success ) {\
  int err = call;\
  if ( success != err)\
    fprintf(stderr, "PAPI error for %s in L%d of %s: %s\n", #call, __LINE__,__FILE__, PAPI_strerror(err)); \
}
*/

Performance_Counters::Performance_Counters(const char *filename, const char *events[]){
  int retval;
  int i=0;
  this->event_set = PAPI_NULL;

  if ((retval = PAPI_library_init(PAPI_VER_CURRENT)) != PAPI_VER_CURRENT) {
    printf("ERROR: PAPI_library_init. %s.\n", PAPI_strerror(retval));
    exit(1);
  }   

  if ((retval = PAPI_create_eventset(&(this->event_set))) != PAPI_OK) {
    printf("ERROR: PAPI_create_eventset. %s.\n", PAPI_strerror(retval));
    exit(1);
  }    

  // add PAPI events until and empty string is found
  while (strcmp(events[i],"") != 0){
    if (( retval = PAPI_add_named_event(this->event_set, events[i])) != PAPI_OK) {
      printf("ERROR: Couldn't add %s. %s.\n", events[i], PAPI_strerror(retval));
      exit(1);
    }
    //this->events.push_back(events[i]);
    i++;
  }
  this->num_events = PAPI_num_events(this->event_set);
  //printf("%d events were set\n", this->num_events);
  if (this->num_events < 1){
      printf("ERROR: no event was set\n");
      exit(1);
  }

  this->reset();

  // writing the CSV header
  if (strcmp(filename,"stdout")==0)
    this->f = stdout;
  else
    this->f = fopen(filename,"w");
  if (this->f == NULL) {printf ("ERROR: cannot create %s\n",filename); exit(1);}

  for (int i = 0; i < num_events; ++i) {
    fprintf(this->f, "%s", events[i]);
    if (i < (this->num_events-1)){
      fprintf(this->f,",");
    }    
  }
  fprintf(this->f,"\n");

}

// not used so far
// Performance_Counters::~Performance_Counters(){
// }

void Performance_Counters::start(){
  int retval;
  if ((retval = PAPI_start(this->event_set)) != PAPI_OK) {
    printf("ERROR: PAPI_start. %s.\n", PAPI_strerror(retval));
    exit(1);
  }
}

void Performance_Counters::reset(){
  int retval;
  if ((retval = PAPI_reset(this->event_set)) != PAPI_OK) {
    printf("ERROR: PAPI_reset. %s.\n", PAPI_strerror(retval));
    exit(1);
  }
}

void Performance_Counters::reset_start(){
  this->reset();
  this->start();
}

void Performance_Counters::stop(){
  long long results[this->num_events]= {0};
  int retval;

  if ((retval = PAPI_stop(this->event_set, results)) != PAPI_OK) {
    printf("ERROR: PAPI_stop. %s.\n", PAPI_strerror(retval));
    exit(1);
  }  

  // save PAPI data into the CSV file
  for (int i = 0; i < this->num_events; ++i) {
    fprintf(this->f,"%lld", results[i]);
    if (i < (this->num_events-1)){
      fprintf(this->f,",");
    }
  }
  fprintf(this->f,"\n");  
}

void Performance_Counters::close(){
  long long values[10];
  int retval;

  // it's mandatory to stop PAPI before cleaning it up
  this->stop();

  if ((retval = PAPI_cleanup_eventset(this->event_set)) != PAPI_OK) {
    printf("ERROR: PAPI_cleanup_eventset. %s.\n", PAPI_strerror(retval));
    exit(1);
  }  

  if ((retval = PAPI_destroy_eventset(&(this->event_set))) != PAPI_OK) {
    printf("ERROR: PAPI_destroy_eventset. %s.\n", PAPI_strerror(retval));
    exit(1);
  }

  // free the memory
  PAPI_shutdown();

  // close the CSV file
  if (this->f != stdout)
    fclose(this->f);
}

void Performance_Counters::operator()(){
  long long results[this->num_events]= {0};
  int retval;

  this->reset();
  
  if ((retval = PAPI_read(this->event_set, results)) != PAPI_OK) {
    printf("ERROR: PAPI_read. %s.\n", PAPI_strerror(retval));
    exit(1);
  }  

  // save PAPI data into the CSV file
  for (int i = 0; i < this->num_events; ++i) {
    fprintf(this->f,"%lld,", results[i]);
  }
  fprintf(this->f,"\n");
}

#endif // USE_PAPI