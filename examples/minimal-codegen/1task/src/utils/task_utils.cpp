#include "task_utils.h"
#include <unistd.h> // syscall
#include <sys/syscall.h> //__NR_sched_setattr

void inc_period(struct period_info *pinfo) 
{
	// add microseconds to timespecs nanosecond counter
	pinfo->next_period.tv_nsec += pinfo->period_ns*1000;

	while (pinfo->next_period.tv_nsec >= 1000000000) {
		/* timespec nsec overflow */
		pinfo->next_period.tv_sec++;
		pinfo->next_period.tv_nsec -= 1000000000;
	}
}
 
void periodic_task_init(struct period_info *pinfo)
{
	clock_gettime(CLOCK_MONOTONIC, &(pinfo->next_period));
}
 
void wait_rest_of_period(struct period_info *pinfo)
{
	inc_period(pinfo);

	/* for simplicity, ignoring possibilities of signal wakes */
	clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &pinfo->next_period, NULL);
}

int sched_setattr(pid_t pid, const struct sched_attr *attr, unsigned int flags) {
    return syscall(__NR_sched_setattr, pid, attr, flags);
}