#include <cstdint>
#include "label_utils.h"
#include "tick.h"
#if defined(LABEL_RD_PERF) || defined(LABEL_WR_PERF) || defined(TICK_PERF)
#include "perf_cnt.h"
extern Performance_Counters *pc;
#endif

/* 
  ###############################
  TASK DESCRIPTION
  Stimuli type: PeriodicStimuli
  Mapped PU: Core1
  PU type: CPU
  PU definition: DefaultCore
  ###############################
*/
void runnable1()
{
  // shared label declarations
  extern uint8_t vector[LABEL_SIZE];

  // the reading part w PAPI enabled
  #ifdef LABEL_RD_PERF  
    pc->start();
  #endif
  read_label(vector,LABEL_SIZE);
  #ifdef LABEL_RD_PERF  
    pc->stop();
  #endif

  // computation part w PAPI enabled
  #ifdef TICK_PERF  
    pc->start();
  #endif
  Tick_DiscreteValueConstant tick0(TICK_SIZE);
  Count_Ticks(tick0());
  #ifdef TICK_PERF  
    pc->stop();
  #endif

  // the writing part
  #ifdef LABEL_WR_PERF  
    pc->start();
  #endif
  write_label(vector,LABEL_SIZE);
  #ifdef LABEL_WR_PERF  
    pc->stop();
  #endif
}