#include <unistd.h>
#include <iostream>
#include <thread>
#include <cstdlib>
#include <signal.h>
// task prototypes
#include "tasks.h"
// create the shared data
#include "label.h"

// Define the function to be called when ctrl-c (SIGINT) is sent to process
void signal_callback_handler(int signum) {
  std::cout << "Caught signal " << signum << std::endl;
  // Terminate program
  exit(signum);
}

int main()
{
  signal(SIGINT, signal_callback_handler);

  std::thread thread_task_5ms(Task_5MS);

  thread_task_5ms.join();

  return EXIT_SUCCESS;
}