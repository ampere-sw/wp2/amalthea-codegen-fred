#include <cstdint>
#include <iostream>
#include <pthread.h>
#include <unistd.h> // syscall
#include <linux/sched.h> // SCHED_DEADLINE,
#include <sys/syscall.h> //SYS_gettid
#include "task_utils.h"
#include "task_5ms.h"

#ifdef USE_EXTRAE
  #include "extrae.h"
#endif

#ifdef USE_PAPI
  #include "perf_cnt.h"
  // this part is to select the appropriate PAPI events based on where the evaluation is executed: labels, ticks or the entire runnable
  #ifndef PAPI_EVENT
    #define PAPI_EVENT 1
  #endif
  #include "papi_events.h"
  extern const char *events[];
  // create the main PAPI global obj
  #ifndef PAPI_FILENAME
    #define PAPI_FILENAME "papi.csv"
  #endif
  //Performance_Counters pc(PAPI_FILENAME,events);
  Performance_Counters *pc;
#endif

//Assuming SCHED_DEADLINE Linux Policy. period, deadline, and runtime are in microseconds
// it will cause 20% of CPU load. CPU running in 10 ms out of 50 ms
#ifndef TASK_PERIOD
#define TASK_PERIOD   50 * 1000 * 1000
#endif
#ifndef TASK_RUNTIME
#define TASK_RUNTIME  10 * 1000 * 1000
#endif
#ifndef TASK_DEADLINE
#define TASK_DEADLINE 11 * 1000 * 1000
#endif

/* 
  ###############################
  TASK DESCRIPTION
  Stimuli type: PeriodicStimuli
  Mapped PU: Core1
  PU type: CPU
  PU definition: DefaultCore
  ###############################
*/
void Task_5MS()
{
  // local variables and initialization procedures
  uint32_t iter=0;
  struct timespec ts;
  pid_t tid = 0;
  #ifdef USE_PAPI
    pc = new Performance_Counters(PAPI_FILENAME,events);
    #ifdef LABEL_RD_PERF
      printf("Running LABEL_RD_PERF\n");
      printf("LABEL_SIZE=%d\n", LABEL_SIZE);
    #elif LABEL_WR_PERF
      printf("Running LABEL_WR_PERF\n");
      printf("LABEL_SIZE=%d\n", LABEL_SIZE);
    #elif RUNNABLE_PERF
      printf("Running RUNNABLE_PERF\n");
      printf("TICK_SIZE=%d\n", TICK_SIZE);
      printf("LABEL_SIZE=%d\n", LABEL_SIZE);
    #elif TICK_PERF
      printf("Running TICK_PERF\n");
      printf("TICK_SIZE=%d\n", TICK_SIZE);
    #else
      #error "One PAPI run must be selected"
    #endif
    printf("PAPI_EVENT=%d\n", PAPI_EVENT);
  #endif
  // timing definitions
  struct period_info pinfo;
  pinfo.period_ns = TASK_PERIOD;
  periodic_task_init(&pinfo);

  // the task mapping definition
  // sched_deadline has not affinity

  struct sched_attr attr = {
      .size = sizeof(attr),
      .sched_policy   = SCHED_DEADLINE,
      .sched_runtime  = TASK_RUNTIME,
      .sched_deadline = TASK_DEADLINE,
      .sched_period   = TASK_PERIOD
  };

  #ifdef DEBUG
  tid = syscall(SYS_gettid);
  printf ("PID: %d\n", tid);
  #endif

  if (sched_setattr(tid, &attr, 0) < 0)
  {
    perror("Error sched_setattr()");
  }

  // the main task code, where the runnables are called
  for (int i=0;i<1;i++)
  //while (1)
  {
    // runnable calls
    #ifdef RUNNABLE_PERF  
    pc->start();
    #endif

    #ifdef USE_EXTRAE
    Extrae_init();
    #endif


    runnable1();


    #ifdef USE_EXTRAE
    Extrae_fini();
    #endif

    #ifdef RUNNABLE_PERF  
    pc->stop();
    #endif

    // checking the task period
    //wait_rest_of_period(&pinfo);
    // #ifdef DEBUG
    // std::cout << "Task_5MS: " << iter << "\n";
    // iter++;
    // #endif
    fflush(0);
    sched_yield();    
  }
}
