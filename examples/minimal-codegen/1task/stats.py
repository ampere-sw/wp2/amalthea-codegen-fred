#!/usr/bin/python
import pandas as pd
import sys

if len(sys.argv) != 2:
    print ('USAGE: stats <CSV file>')
    sys.exit(1)

df = pd.read_csv(sys.argv[1])

# delete the 1st row because this one is always bogus, w high deviation.
df = df.iloc[1:]


# When running the following code in C

# extern Performance_Counters *pc;
# ...
# pc->start();
# pc->stop();
# ...

# and then print the PAPI numbers for it, 
# it generates the following results:

#         PAPI_LD_INS	PAPI_SR_INS	PAPI_TOT_INS
# count	  161,00	    161,00	    161,00
# mean	  70,40	        48,00	    327,02
# std	  4,15	        0,00	    0,24
# min	  43,00	        48,00	    327,00
# 25,00%  70,00	        48,00	    327,00
# 50,00%  70,00	        48,00	    327,00
# 75,00%  70,00	        48,00	    327,00
# max	  111,00	    48,00	    330,00

# These values represent the overhead of PAPI itself, 
# and they must be discounted from the results.
# Only the mean values reported above are considered for this discount.



# print (df.head())
if 'PAPI_LD_INS' in df:
    df['PAPI_LD_INS'] = df['PAPI_LD_INS']-70
if 'PAPI_SR_INS' in df:
    df['PAPI_SR_INS'] = df['PAPI_SR_INS']-48
if 'PAPI_TOT_INS' in df:
    df['PAPI_TOT_INS'] = df['PAPI_TOT_INS']-327
# print (df.head())

# # clip is only used below to avoid negative numbers, which does not make sense 
# # for a event counter.
df = df.clip(lower=0)

print (df.describe())
