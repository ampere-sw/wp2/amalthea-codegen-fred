#!/bin/bash
# based on https://github.com/evidence/test-sched-dl/blob/master/T0001-simple-run/run.sh
rm -f trace.* dmesg.txt
DIR=synthetic
if [ ! -e $DIR ]; then
	echo "ERROR: File not compiled! Type 'cd build; make'"
	exit
fi
echo "Running property 1 ..."
# run tracing
# https://stackoverflow.com/questions/39709328/why-does-perf-and-papi-give-different-values-for-l3-cache-references-and-misses
# ':u' for user space and ':k' for kernel space
# https://stackoverflow.com/questions/22712956/why-does-perf-stat-show-stalled-cycles-backend-as-not-supported?noredirect=1&lq=1
# how to inform an event from the CPU datasheet

# each of these commands have to be executed individually. it's not possible to group them into a single execution

# 1st execution
./$DIR &
APP_PID=$!
# warm up period of 1 sec before start tracing
sleep 1
perf stat -e LLC-loads:u,LLC-load-misses:u -p $APP_PID sleep 10
sleep 1
echo "Killing test $DIR..."
#killall -s SIGKILL $DIR > /dev/null
kill -9 $APP_PID
sleep 3

# 2nd execution
./$DIR &
APP_PID=$!
sleep 1
perf stat -e LLC-stores:u,LLC-prefetches:u -p $APP_PID sleep 10
sleep 1
echo "Killing test $DIR..."
#killall -s SIGKILL $DIR > /dev/null
kill -9 $APP_PID
sleep 3

# 3rd execution
./$DIR &
APP_PID=$!
sleep 1
perf stat -e instructions:u,cpu-cycles:u,context-switches,page-faults,migrations,bus-cycles:u,cache-references:u,mem-loads:u,mem-stores:u -p $APP_PID sleep 10
sleep 1
echo "Killing test $DIR..."
#killall -s SIGKILL $DIR > /dev/null
kill -9 $APP_PID
sleep 3

# other commands that might be usefull in the future
#perf stat -e instructions:u,instructions:k,cache-references:u,cache-references:k,cache-misses:u,cache-misses:k  -p $APP_PID sleep 10
#perf stat -e mem-loads:u,mem-stores:u  -p $APP_PID sleep 10
#perf stat -e LLC-loads:u,LLC-load-misses:u,LLC-stores:u,LLC-prefetches:u -p $APP_PID sleep 10
#perf stat -e instructions,cpu-cycles,context-switches,page-faults,migrations,INST_RETIRED.LOADS,INST_RETIRED.STORES -p $APP_PID sleep 10
#perf stat -e resource_stalls.any,cycle_activity.stalls_mem_any,cycles -p $APP_PID sleep 10
#perf stat -e cycles,stalled-cycles-frontend,stalled-cycles-backend,mem-stores:u,mem-loads:u -p $APP_PID sleep 10
#DRAM_BW_Use,L1D_Cache_Fill_BW,L2_Cache_Fill_BW,L3_Cache_Fill_BW,CPI,ILP

