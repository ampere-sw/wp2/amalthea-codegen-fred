
The purpose of this example app is to evaluate the the label accesses and the ticks
against the expected in the Amalthea model. This app has a single task with a single runnable.
These are its default scheduling parameters:


    #ifndef TASK_PERIOD
    #define TASK_PERIOD   50 * 1000 * 1000
    #endif
    #ifndef TASK_RUNTIME
    #define TASK_RUNTIME  10 * 1000 * 1000
    #endif
    #ifndef TASK_DEADLINE
    #define TASK_DEADLINE 11 * 1000 * 1000


The task is executed every 50 ms per 10 ms with a deadline of 11ms. Thus,
assuming this default configuration, this task is executed 20 times per second. 

The task has a single runnable that reads and writes LABEL_SIZE bytes per run. 
Thus, the expected memory bandwidth is 20 * LABEL_SIZE bytes / s for reads and 
20 * LABEL_SIZE bytes / s for writes. The default value of LABEL_SIZE is 1000.
The writing is done in the same label read at the start of the runnable. So,
it is expected to have a high L1D hit ratio for writing and low L1D hit ratio 
for reading.

    #ifndef LABEL_SIZE
    #define LABEL_SIZE 1000
    #endif

The computation time of the runnable is defined by TICK_SIZE, representing the 
number of instructions executed per run. Thus, the expected number of instructions
executed per second is above 20 * TICK_SIZE inst / s. The default value of 
TICK_SIZE is 1000000.

    #ifndef TICK_SIZE
    #define TICK_SIZE 1000 * 1000
    #endif

Demonstrated Code Properties
============================

The code is instrumented to validate the following propserties during the execution:

 - 1) The total number of memory access (L1D) per second. The expected value is 'a little' above 2 * 20 * LABEL_SIZE bytes / s;
 - 2) The total number of executed instructions per second. The expected value is 'a little' above 20 * TICK_SIZE inst / s;
 - 3) There is a negligible number of LLC_MISSES during the computation phase of the runnable;
 - 4) The number of bytes accessed (L1D) in the read and write phases of the runnable is as close as possible to LABEL_SIZE;
 - 5) The number of instructions executed during the computation phase of the runnable is as 
 close as possible to TICK_SIZE;
 - 6) The properties described above hold for different RT scheduling (SCHED_DEADLINE and SCHED_FIFO) policy and CPU stress;
 - 7) The properties described above hold for different CPU architectures: X86_64, arm64, arm7

Code Instrumentation
====================

How the code is instrumented to extract data to validate each of the properties described before.

 - 1 and 2) data extrated with __perf__;
 - 3, 4, and 5) data extrated with __PAPI__;
 - 6) run stress; run the app and plot the parameters (1-5) per about 10 s
 - 7) plot the parameters (1-5) in Xavier and Ultrascale/Pynq


Access to the hardware counters
-------------------------------

Before using perf or PAPI, execute these commands to give access to the hardware counters:

```bash
$ sudo su
$ echo 0 > /proc/sys/kernel/nmi_watchdog
$ echo -1 > /proc/sys/kernel/perf_event_paranoid
```

Add this line to __/etc/sysctl.conf__ to make this setting permanent:

```
	kernel.perf_event_paranoid = -1
```


Disabling CPU Frequency Scaling
-------------------------------

If time metrics will be extracted in the experiment, then it's better to disable CPU frequency scaling by executing the following commands.

```bash
$ sudo cpupower frequency-set --governor performance
$ ./mybench
$ sudo cpupower frequency-set --governor powersave

Using perf
----------


See __prop1.sh__ to see how __perf__ is used.

Using ftrace
------------

See __run.sh__ to see how __trace-cmd__ is used.

Using PAPI
----------

See __src/utils/perf_cnt.cpp__ and  __include/utils/perf_cnt.h__ to see how to use PAPI to capture performance data in the generated code. The captured PAPI events are defined in __include/utils/papi_events.h__.

PAPI instrumentation is already integrated in the Cmake and in the source code. Enable the following options in Cmake to test it: 

 - USE_PAPI: Enables PAPI to capture performance data;
 - LABEL_RD_PERF: Enables PAPI to capture performance data from label reads;
 - LABEL_WR_PERF: Enables PAPI to capture performance data from label writes;
 - TICK_PERF: Enables PAPI to capture performance data from ticks;
 - RUNNABLE_PERF: Enables PAPI to capture performance data from the runnable.

LABEL_RD_PERF, LABEL_WR_PERF, TICK_PERF, and RUNNABLE_PERF are mutually exclusive, so only one can be enabled. USE_PAPI must be enabled if any of the *_PERF options is enabled.

For example, the following command configures the generated code to evaluate the ticks.

```bash
$ cmake -DDEBUG=1 -DUSE_PAPI=1 -DTICK_PERF=1 ..
```

Inspecting the Generated Asm
============================

In the build dir, run the following commands to generate the assembly code.

```bash
$ objdump -drwC -Mintel CMakeFiles/synthetic.dir/src/utils/tick.cpp.o > ../tick.asm
$ objdump -drwC -Mintel CMakeFiles/synthetic.dir/src/utils/label_utils.cpp.o > ../label_utils.asm
```


Preliminary Results
===================

Counter calibration
-------------------

Before we start to evaluated the obtained results compared to PAPI, it is necessary to 
'calibrate' the perfomance counters. This means, to discount the influence of PAPI itself 
from the obtained results. The way to perform this calibrarion is to configure PAPI to 
monitor all required counters, start PAPI and immedialy later stop it and print the value 
obteined in each run. At the end, we subtract the average value for the value obtained by 
each performance counter.
Note that this procedure might generate negative numbers, which does not make sense to event counters. 
Thus, the final value is clipped to zero.


The counters PAPI_LD_INS, PAPI_SR_INS, PAPI_TOT_INS resulted in, respectively, 70, 48, 327 events,
considering the computer used in the evaluation. 


Ticks
-----

The configuration command is:

```bash
$ cmake -DCMAKE_CXX_FLAGS=-O2 -DUSE_PAPI=1 -DTICK_PERF=1 -DPAPI_EVENT=2 ..
```
The expected number of instructions is 20 Minst / s or 1Minst per runnable execution.

The obtained results per runnable execution are:

```
       PAPI_LD_INS  PAPI_SR_INS  PAPI_TOT_INS
mean     52.448889         10.0  1.015598e+06
std      45.448149          0.0  5.185067e-01
min       0.000000         10.0  1.015597e+06
25%      12.000000         10.0  1.015597e+06
50%      13.000000         10.0  1.015598e+06
75%      97.000000         10.0  1.015598e+06
max     180.000000         10.0  1.015599e+06
```

Where 1.015598e+06 represents the mean number of executed instructions (**PAPI_TOT_INS** column),
with a very low deviation. An error of about 1.56 % compared to the expected 1Minst per runnable execution. PAPI_LD_INS and PAPI_SR_INS represent the total number of loads and stores, which are reduced and independent of the executed number of ticks, i.e., they do not scale with the number of ticks.


The next PAPI configuration command is used to evalute the memory accesses during the execution phase of the runnable.

```bash
$ cmake -DCMAKE_CXX_FLAGS=-O2 -DUSE_PAPI=1 -DTICK_PERF=1 -DPAPI_EVENT=5 ..
```

To evaluate this, it would be best to have access to the LLC cache misses (PAPI_L3_DCM and PAPI_L3_ICM for data and instrcution misses). Unfortunatly, these counters are not available in the evaluated platform. Thus, the results below show the L2 cache misses (PAPI_L2_DCM and PAPI_L2_ICM) and the total L3 cache misses (PAPI_L3_TCM).
One can see that the number of misses is very reduced compared to the total number of instructions.

```
       PAPI_TOT_CYC  PAPI_TOT_INS  PAPI_L2_DCM  PAPI_L2_ICM  PAPI_L3_TCM
mean   1.003598e+06  1.015597e+06     4.719626    14.180685    13.996885
std    3.986816e+03  7.881027e-02     9.490253     8.329826     4.326155
min    1.000902e+06  1.015597e+06     0.000000     0.000000     0.000000
25%    1.001617e+06  1.015597e+06     0.000000    10.000000    13.000000
50%    1.001989e+06  1.015597e+06     0.000000    12.000000    15.000000
75%    1.003314e+06  1.015597e+06     2.000000    15.000000    16.000000
max    1.043939e+06  1.015598e+06    43.000000    47.000000    23.000000

```

Labels
------

There are 3 configurations to evaluate the labels, one for label reads and other for label writes:

```bash
$ cmake -DCMAKE_CXX_FLAGS=-O2 -DUSE_PAPI=1 -DLABEL_RD_PERF=1 -DPAPI_EVENT=2 ..
$ cmake -DCMAKE_CXX_FLAGS=-O2 -DUSE_PAPI=1 -DLABEL_WR_PERF=1 -DPAPI_EVENT=2 ..
```
The expected number of loads is 20000 bytes / s or 1000 bytes per runnable execution. The same for stores.

The obtained results for **label reads** per runnable execution are:

```
       PAPI_LD_INS  PAPI_SR_INS  PAPI_TOT_INS
mean    161.701613          1.0         392.0
std       1.306518          0.0           0.0
min     153.000000          1.0         392.0
25%     162.000000          1.0         392.0
50%     162.000000          1.0         392.0
75%     162.000000          1.0         392.0
max     165.000000          1.0         392.0
```

According to the \texttt{read_label} function, the first loop performs 8 loads of 8 bytes each per iteration. The second loop performs 1 read of 1 byte per iteration. For example, to read 1000 bytes per runnable execution, it's necessary to execute \floor(1000/8*8) = 15 iterations of the first loop, executing 120 loads to read a total of 960 bytes. The second loop accounts for the remainig 40 bytes read in 40 loads. Thus, the expected number of loads is 120+40 = 160. One can see that the mean value matches with the expected one. A detailed analysis of the Assembly code of \texttt{read_label} function, not show here to save space, also reported a match with the expected total number of instructions, which is 391.


```
       PAPI_LD_INS  PAPI_SR_INS  PAPI_TOT_INS 
mean      2.494253        160.0         354.0
std       1.234135          0.0           0.0
min       0.000000        160.0         354.0
25%       2.000000        160.0         354.0
50%       2.000000        160.0         354.0
75%       3.000000        160.0         354.0
max       8.000000        160.0         354.0
```

The results for **label writes** (\texttt{write_label} function) presents a similar trend. The number of expected stores is the same for loads, i.e., 160, while the reported number of stores is also 160. The expected total number of instructions \texttt{write_label} function also matches the number reported by PAPI.



Perf
----

To run perf, the code is configured like this:

```bash
$ cmake -DCMAKE_CXX_FLAGS=-O2 ..
$ make -j 4
```

After running the generated code for 10 seconds, these were the obtained results:

```
$ sudo ./prop1.sh 
Running property 1 ...

 Performance counter stats for process id '9781':

            12,590      LLC-loads                                                   
            11,914      LLC-load-misses           #   94.63% of all LL-cache hits   

      10.000974971 seconds time elapsed

Killing test synthetic...
./prop1.sh: line 28:  9781 Killed                  ./$DIR

 Performance counter stats for process id '9843':

               214      LLC-stores                                                  
             4,004      LLC-prefetches                                              

      10.000941594 seconds time elapsed

Killing test synthetic...
./prop1.sh: line 39:  9843 Killed                  ./$DIR

 Performance counter stats for process id '9914':

       203,319,800      instructions:u            #    1.00  insn per cycle         
       202,707,128      cpu-cycles:u                                                
               200      context-switches                                            
                 0      page-faults                                                 
                 0      migrations                                                  
         5,791,618      bus-cycles:u                                                
            21,684      cache-references:u                                          
                 0      mem-loads:u                                                 
            44,400      mem-stores:u                                                

      10.001038400 seconds time elapsed
```

During 10 seconds, the runnable is expected to run 10 * 20 = 200 times. This corresponds to the 
total number of context switches reported by perf, which means that once the runnable started to execute, it was not preempted or interrupted, which could affect the measurments. The absence of page faults and task migrations also confirm that the task was not interrupted in it's execution.

As mentioned before, only the tick part of the runnable executes 20 Minst/s. The label reads executes about 20 * 719 = 14380 inst/s while the label writes executes 20 * 681 = 13620 inst/s. So, the runnable alone executes 20M + 14380 + 13620 = 20.028.000 inst/s. So, about 200 Minst are expected to be executed by the runnable in 10 seconds. Perf reported 204Minst, 2% of error, attributed to additional instructions related to stack management in function calls and mostly loop management instructions.

During 10 seconds, the runnable is expected to read in memory 10 * 20 * 1000 = 200.000 bytes. The same number of bytes is expected for writing. **Explain these results**.

