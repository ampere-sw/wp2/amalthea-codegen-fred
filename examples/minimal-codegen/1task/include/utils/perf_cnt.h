/*
 * Description: Class that encapsulates PAPI to capture hw counters data for performance analysis.
 *  
 * Author: 
 *  Alexandre Amory (March 2021)
 * 
 * TODO:
 *   - Treat counter overflow;
 *   - Add thread support https://bitbucket.org/icl/papi/wiki/PAPI-Parallel-Programs.md;
 *   - Is software defiend events relevant ? https://bitbucket.org/icl/papi/wiki/Software_Defined_Events.md
 * 
 * Usage Example:

  const char *events [] = {
      "PAPI_TOT_CYC",// Total cycles
      "PAPI_TOT_INS",// Instructions completed
      "PAPI_STL_ICY",// Cycles with no instruction issue
      ""
  };  
  Performance_Counters pc("papi.csv",events)

  pc.start();
  // some computation to be evaluated
  pc.stop();

  // skip the computation that is not relevant

  pc.start();
  // some computation to be evaluated
  pc.stop();

  // skip the computation that is not relevant

  pc.start();
  // some other relavant computation

  // read the counters, without stoping them
  pc();

  // done capture data from the performance counters. The results are saved in a CSV file.
  pc.close();

 * Codegen Usage Example:  
 *  
 *  In the context of Amalthea codegen, it's best to declare the Performance_Counter class
 *  in the task as a global and use extern in the runnables. 
 *  See an example at examples/minimal-codegen/1task/
  */

#include <stdio.h>

class Performance_Counters
{
public:
    int num_events;
    // is it required to save the name of the events ?!??! 
    //vector<string> events;
    // use filename=stdout to print in the terminal
    Performance_Counters(const char *filename, const char *events[]);
    //~Performance_Counters();
    // start the counters
    void start();
    // reset the counters
    void reset();
    // reset the counter and then start them all
    void reset_start();
    // stop the counters, read their results and save them into a file
    void stop();
    // kill the PAPI env and close the output file
    void close();
    // reset, read the counters, and save them into a file
    void operator()();

private:
    int event_set;
    // pointer to the CSV file where the results are saved
    FILE *f;
};