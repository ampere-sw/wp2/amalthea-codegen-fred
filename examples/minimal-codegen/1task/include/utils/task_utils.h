#ifndef __TASK_UTILS
#define __TASK_UTILS

#include <time.h>
#include <stdint.h>

// source: https://wiki.linuxfoundation.org/realtime/documentation/howto/applications/cyclic
struct period_info {
        struct timespec next_period;
        long period_ns;
};

void inc_period(struct period_info *pinfo);

void periodic_task_init(struct period_info *pinfo);

void wait_rest_of_period(struct period_info *pinfo);


struct sched_attr {
    uint32_t size;
    uint32_t sched_policy;
    uint64_t sched_flags;
    int32_t sched_nice;
    uint32_t sched_priority;
    uint64_t sched_runtime;
    uint64_t sched_deadline;
    uint64_t sched_period;
};


int sched_setattr(pid_t pid, const struct sched_attr *attr, unsigned int flags);

#endif //__TASK_UTILS