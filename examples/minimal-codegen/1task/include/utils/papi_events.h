//Run 'papi_event_chooser' to test if the selected events work together
#ifndef PAPI_EVENTS__
#define PAPI_EVENTS__

// by default, the event group 1 is captured
#ifndef PAPI_EVENT
#define PAPI_EVENT 1
#endif

#if PAPI_EVENT == 1
const char *events [] = {
    "PAPI_TOT_CYC",// Total cycles
    "PAPI_TOT_INS",// Instructions completed
    "PAPI_STL_ICY",// Cycles with no instruction issue
    ""
};
#elif PAPI_EVENT == 2
const char *events [] = {
    "PAPI_LD_INS",//  Load instructions
    "PAPI_SR_INS",//  Store instructions
    "PAPI_TOT_INS",// Instructions completed
    //"PAPI_L1_TCM",//  Level 1 cache misses /// cannot capture this event
    ""
};
#elif PAPI_EVENT == 3
const char *events [] = {
    "PAPI_L2_TCR",//  Level 2 total cache reads
    "PAPI_L2_TCW",//  Level 2 total cache writes
    "PAPI_L2_TCM",//  Level 2 cache misses
    ""
};

#elif PAPI_EVENT == 4
const char *events [] = {
    "PAPI_L3_TCR",//  Level 3 total cache reads
    "PAPI_L3_TCW",//  Level 3 total cache writes
    "PAPI_L3_TCM",//  Level 3 cache misses
    ""
};


#elif PAPI_EVENT == 5
const char *events [] = {
    "PAPI_TOT_CYC",//  Level 3 total cache reads
    "PAPI_TOT_INS",//  Level 3 total cache writes
    "PAPI_L2_DCM", // Level 2 data cache misses
    "PAPI_L2_ICM", // Level 2 instruction cache misses
    // "PAPI_L3_DCM", // Level 3 data cache misses // not available
    "PAPI_L3_TCM",//  Level 3 cache misses
    ""
};
#else
const char *events [] = {};
    #error Unsupported group of PAPI events
#endif

#endif // PAPI_EVENTS__