#ifndef __LABEL_UTILS
#define __LABEL_UTILS

#include <cstring> // memset
#include <iostream>
#include <shared_mutex>
#include <thread>
#include <sstream>

#define MIN(a,b) ((a)<(b)?(a):(b))

#ifndef LABEL_SIZE
#define LABEL_SIZE 1000
#endif


/*
Alternative type for mutex-based safe shared label 
*/
template<typename T>
class Atomic_Scalar { 
private: 
    /** https://en.cppreference.com/w/cpp/thread/shared_timed_mutex
     * Shared mutexes are usually used in situations when multiple 
     * readers can access the same resource at the same time without 
     * causing data races, but only one writer can do so.
     */
    mutable std::shared_timed_mutex m; 
    T value; 
public:
    Atomic_Scalar(){
        // requires exclusive ownership to write to *this
        std::unique_lock<std::shared_timed_mutex> l(m); 
        std::exchange(value, 0); 
    }

    operator T() const{
        return this->get();
    }

    T get() const { 
        // requires shared ownership to read from other
        std::shared_lock<std::shared_timed_mutex> l(m); 
        return value; 
    } 

    T set(T newval) { 
        // requires exclusive ownership to write to *this
        std::unique_lock<std::shared_timed_mutex> l(m); 
        T oldval = this->value;
        this->value =  newval; 
        return oldval; 
    } 

    void inc(){
        // requires exclusive ownership to write to *this
        std::unique_lock<std::shared_timed_mutex> l(m); 
        this->value += 1;
    }

    friend std::ostream& operator<<( std::ostream& out, const Atomic_Scalar<T>& v){
        out << v.get();
        return out;
    }

}; 

//#########################################
/*
Alternative type for mutex-based safe shared label array.
It blocks the entire array while writing. 
It allows multiple simultaneous reads.
*/
template<unsigned array_size>
class Atomic_Array { 
private: 
    /** https://en.cppreference.com/w/cpp/thread/shared_timed_mutex
     * Shared mutexes are usually used in situations when multiple 
     * readers can access the same resource at the same time without 
     * causing data races, but only one writer can do so.
     */
    mutable std::shared_timed_mutex m; 
    //atomic<uint8_t> value[array_size]; 
    uint8_t value[array_size]; 
public:
    Atomic_Array(){
        this->set(0);
    }

    uint8_t get(const unsigned idx) const { 
        // requires shared ownership to read from other
        std::shared_lock<std::shared_timed_mutex> l(m); 
        if (idx < array_size)
            return value[idx]; 
        else
            return 0;
    } 

    void copy(uint8_t* newvet, unsigned s) { 
        // requires exclusive ownership to write to *this
        std::unique_lock<std::shared_timed_mutex> l(m); 
        memcpy(this->value, newvet, MIN(s,array_size)); 
    } 

    // initialize the entire array with the same value
    void set(uint8_t v) { 
        // requires exclusive ownership to write to *this
        std::unique_lock<std::shared_timed_mutex> l(m); 
        memset(value,v, array_size*sizeof(uint8_t));
    }  

    /*
    void set(uint8_t idx, uint8_t v) { 
        // requires exclusive ownership to write to *this
        std::unique_lock<std::shared_timed_mutex> l(m); 
        value[idx] = v;
    } 
    */ 

    // this is used to simulate a locked writing in the entire array
    void inc() { 
        // requires exclusive ownership to write to *this
        std::unique_lock<std::shared_timed_mutex> l(m); 
        for(int i=0;i<array_size;i++)
        {
            value[i] +=1;
        }
    }     

    // this is used to simulate a locked reading in the entire array
    void acc( int* val) { 
        // requires exclusive ownership to write to *this
        std::unique_lock<std::shared_timed_mutex> l(m); 
        for(int i=0;i<array_size;i++)
        {
            val += value[i];
        }
    } 

    std::string str(){
        std::ostringstream os;
        os << "["; 
        for (int i = 0; i < array_size; ++i) { 
            os << (unsigned)value[i]; 
            if (i != array_size - 1) 
                os << ", "; 
        } 
        os << "]\n"; 
        return os.str(); 
    }


}; 


// When compiled with -02, 'read_label' main loop is like this, mostly 8 * 64bits memory reads per iteration 
// 
//.L2:
//	add    rcx,rdi
//	cmp    rdi,r8
//	jae    4c <read_label(unsigned char*, int)+0x4c>
//	mov    rdx,QWORD PTR [rdi+0x8]
//	add    rdi,0x40
//	add    rdx,QWORD PTR [rdi-0x40]
//	add    rdx,QWORD PTR [rdi-0x30]
//	add    rdx,QWORD PTR [rdi-0x28]
//	add    rdx,QWORD PTR [rdi-0x20]
//	add    rdx,QWORD PTR [rdi-0x18]
//	add    rdx,QWORD PTR [rdi-0x10]
//	add    rdx,QWORD PTR [rdi-0x8]
//	add    rax,rdx
//	cmp    r8,rdi
//	ja     20 <read_label(unsigned char*, int)+0x20>	/
//....
//
//
uint64_t read_label(uint8_t *p, int size);



// 'write_label' main loop is like this, mostly 8 * 64bits memory writes per iteration 
//
//  	cmp    rdi,rcx
//  	jae    12e <write_label(unsigned char*, int)+0x4e>
//  	xchg   ax,ax
//  	mov    esi,0xaffffffe
//  	add    rdi,0x40
//  	mov    QWORD PTR [rdi-0x40],rsi
//  	mov    QWORD PTR [rdi-0x38],rsi
//  	mov    QWORD PTR [rdi-0x30],rsi
//  	mov    QWORD PTR [rdi-0x28],rsi
//  	mov    QWORD PTR [rdi-0x20],rsi
//  	mov    QWORD PTR [rdi-0x18],rsi
//  	mov    QWORD PTR [rdi-0x10],rsi
//  	mov    QWORD PTR [rdi-0x8],rsi
//  	cmp    rcx,rdi
//  	ja     100 <write_label(unsigned char*, int)+0x20>
//  	cmp    rax,rdx	
// 		...
uint64_t write_label(uint8_t *p, int size);

/*
Used to debug the content of memory regions
*/
void Dump( const void * mem, unsigned int n );

#endif // __LABEL_UTILS
