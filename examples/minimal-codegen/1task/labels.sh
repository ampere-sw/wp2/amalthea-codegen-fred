#!/bin/bash

# expecting two arguments: 1st must be RD or WR; the second is the size of the label in bytes
function label_rd {
    echo "Running $1 of $2 bytes" > label_$1_$2.txt
    rm -rf build/*
    cd build
    cmake -DCMAKE_CXX_FLAGS=-O2 -DLABEL_SIZE=$2 -DUSE_PAPI=1 -DLABEL_$1_PERF=1 -DPAPI_EVENT=2 ..
    make -j 4
    cd ..
    sudo ./synthetic &
    APP_PID=$!
    sleep 10
    echo "Killing ..."
    sudo kill -9 $APP_PID
    sleep 2
    python ./stats.py papi.csv >> label_$1_$2.txt    
}

label_access_type='RD WR'
for rd_wr in $label_access_type
do
    label_sizes='100 1000 10000 50000 100000 500000 1000000 5000000 10000000'
    for label_size in $label_sizes
    do
        label_rd $rd_wr $label_size
    done
done