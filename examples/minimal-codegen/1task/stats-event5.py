#!/usr/bin/python
import pandas as pd
import sys

## this is the stat script used only when PAPI_EVENTS=5, used to evaluate ticks

if len(sys.argv) != 2:
    print ('USAGE: stats <CSV file>')
    sys.exit(1)

df = pd.read_csv(sys.argv[1])

# delete the 1st row because this one is always bogus, w high deviation.
df = df.iloc[1:]


# When running the following code in C

# extern Performance_Counters *pc;
# ...
# pc->start();
# pc->stop();
# ...

# and then print the PAPI numbers for it, 
# it generates the following results:

#        PAPI_TOT_CYC  PAPI_TOT_INS  PAPI_L2_DCM  PAPI_L2_ICM  PAPI_L3_TCM
# count    173.000000         173.0   173.000000   173.000000   173.000000
# mean    3553.526012         295.0    16.890173    25.508671    40.404624
# std      490.672481           0.0     2.876239     3.945292     4.048885
# min     2804.000000         295.0    11.000000    14.000000    27.000000
# 25%     3197.000000         295.0    16.000000    23.000000    39.000000
# 50%     3433.000000         295.0    16.000000    26.000000    41.000000
# 75%     3771.000000         295.0    17.000000    28.000000    43.000000
# max     5518.000000         295.0    30.000000    37.000000    48.000000

# These values represent the overhead of PAPI itself, 
# and they must be discounted from the results.
# Only the mean values reported above are considered for this discount.



# print (df.head())
if 'PAPI_TOT_CYC' in df:
    df['PAPI_TOT_CYC'] = df['PAPI_TOT_CYC']-3553
if 'PAPI_L2_DCM' in df:
    df['PAPI_L2_DCM'] = df['PAPI_L2_DCM']-17
if 'PAPI_L2_ICM' in df:
    df['PAPI_L2_ICM'] = df['PAPI_L2_ICM']-25
if 'PAPI_L3_TCM' in df:
    df['PAPI_L3_TCM'] = df['PAPI_L3_TCM']-40
if 'PAPI_TOT_INS' in df:
    df['PAPI_TOT_INS'] = df['PAPI_TOT_INS']-295
# print (df.head())

# clip is only used below to avoid negative numbers, which does not make sense 
# for a event counter.
df = df.clip(lower=0)

print (df.describe())




