
This example is used to test the Amalthea FPGA flow developed for Milestone 2 in the AMPERE project. 

To run the Amalthea FPGA flow, make sure that the FPGA devicetree is configured with only one partition. You can check it in the `/boot` diretory of the Pynq board. If changes are required, do it, execute the following command in the FPGA, and reboot the board.

```
dtc -I dts -O dtb -f devicetree.dts -o devicetree.dtb
```

Before running the flow it always required to reboot the board since the fred_server cannot be restarted. Then, in the host computer, the next step is to go to the flow repository and run:

```
$ am4dart <codegen_path>/examples/minimal-codegen/fpga/minimal_w_partition.amxmi <codegen_path>/examples/minimal-codegen/fpga/minimal-out.amxmi designs/ms2-demo --partition --fpga-host pynq --fpga-user xilinx --fpga-dir amory/demo
```

The code generated in the `designs/ms2-demo/code` directory must be patched with the `ms2-demo.patch` patch file by running:

```
$ cd designs/ms2-demo/code
$ patch  -p1  < <codegen_path>/examples/minimal-codegen/fpga/ms2-demo.patch 
patching file src/runnables/in_runnable.cpp
patching file src/runnables/out_runnable.cpp
patching file src/runnables/simple_runnable.cpp
```
