
# How to compile 

The examples executed here are in xavier2, directory /home/alexandre/repos/paper/gpu/build.

The minimal GPU example can be compiled with:

'''bash 
$ ccmake -DCMAKE_CXX_FLAGS=-O2 -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ ..
'''

# The impact of optimization parameters 

Considering a piece of code like this executed 20 times:

'''C
    #pragma omp target 
    {
        for (int i=0; i < 1000000; i++);
    }
'''bash 

And the following compilation parameters:

## -O2

'''bash 
$ cmake -DCMAKE_CXX_FLAGS=-O2 -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ ..
$ nvprof --metrics inst_issued --events elapsed_cycles_sm ./synthetic 
==31852== NVPROF is profiling process 31852, command: ./synthetic
Error sched_setattr(): Invalid argument
==31852== Some kernel(s) will be replayed on device 0 in order to collect all events/metrics.
==31852== Profiling application: ./synthetic
==31852== Profiling result:
==31852== Event result:
Invocations                                Event Name         Min         Max         Avg       Total
Device "Xavier (0)"
    Kernel: __omp_offloading_b301_164311__Z15simple_runnablev_l48
         20                         elapsed_cycles_sm       68288       99240       77826     1556528

==31852== Metric result:
Invocations                               Metric Name                        Metric Description         Min         Max         Avg
Device "Xavier (0)"
         20                               inst_issued                       Instructions Issued         897         897         897
'''

## -O1

'''bash 
$ cmake -DCMAKE_CXX_FLAGS=-O1 -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ ..
$ nvprof --metrics inst_issued --events elapsed_cycles_sm ./synthetic 
==32548== Event result:
Invocations                                Event Name         Min         Max         Avg       Total
Device "Xavier (0)"
         20                         elapsed_cycles_sm       82576      151240      100594     2011880

==32548== Metric result:
Invocations                               Metric Name                        Metric Description         Min         Max         Avg
Device "Xavier (0)"
         20                               inst_issued                       Instructions Issued        1130        1130        1130
'''

## -O0

'''bash 
$ cmake -DCMAKE_C_COMPILER=clang -DCMAKE_CXX_COMPILER=clang++ ..
$ nvprof --metrics inst_issued --events elapsed_cycles_sm ./synthetic 
Device "Xavier (0)"
         20                         elapsed_cycles_sm  3424718936  3424808656  3424728164  6.8495e+10

==32232== Metric result:
Invocations                               Metric Name                        Metric Description         Min         Max         Avg
Device "Xavier (0)"
         20                               inst_issued                       Instructions Issued    28005621    28005621    28005621
'''

# Reference to NVIDA metrics and events

https://docs.nvidia.com/cuda/profiler-users-guide/index.html#event-trace-mode

# Counting number of instructions and cycles in the GPU


'''bash
$ nvprof --metrics inst_issued --events elapsed_cycles_sm ./synthetic 

==31754== Event result:
Invocations                                Event Name         Min         Max         Avg       Total
Device "Xavier (0)"
         20                         elapsed_cycles_sm     7074832     9981840     8430304   168606096

==31754== Metric result:
Invocations                               Metric Name                        Metric Description         Min         Max         Avg
Device "Xavier (0)"
         20                               inst_issued                       Instructions Issued       95916       95916       95916
'''

# Reaching close to the number of ticks in the model

The following code

'''C
#pragma omp target 
{   int n=0;
    for (int i=0; i < 300000/100; i++){
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    n ^=1;
    
    }
}
'''

Generates a similar number of instruction. 
Note that 100 was defined empiricaly and the design was compilated with -O0. 

'''bash
$ nvprof --metrics inst_issued --events elapsed_cycles_sm ./synthetic 
Invocations                                Event Name         Min         Max         Avg       Total
Device "Xavier (0)"
         20                         elapsed_cycles_sm   373966600   374009232   373999364  7479987288

==848== Metric result:
Invocations                               Metric Name                        Metric Description         Min         Max         Avg
Device "Xavier (0)"
         20                               inst_issued                       Instructions Issued     2969628     2969628     2969628
'''


# Counting number of loads and stores in the GPU

The following piece of code implies in 20000 int loads into the GPU and 10000 int stores from the GPU.

'''C
    int n = 10000;
    int a = 1;
    int *x,*y;
    x = new int[n];
    y = new int[n];
    for (int i=0;i<n;i++){
        x[i]=1;
        y[i]=1;
    }
    #pragma omp target map(to:x[0:n]) map(tofrom:y[0:n]){
        for(int i=0;i<n;++i){
            y[i] = a*x[i]+y[i];

        }
    }
'''

And running the following command, it's possible to see exatly the expected amount of loads/stores.

'''bash
$ nvprof --events global_load,global_store ./synthetic 
==31224== NVPROF is profiling process 31224, command: ./synthetic
Error sched_setattr(): Invalid argument
==31224== Profiling application: ./synthetic
==31224== Profiling result:
==31224== Event result:
Invocations                                Event Name         Min         Max         Avg       Total
Device "Xavier (0)"
    Kernel: __omp_offloading_b301_164311__Z15simple_runnablev_l47
         20                               global_load       20000       20000       20000      400000
         20                              global_store       10000       10000       10000      200000
'''

# Other profile commands

nvprof -m sysmem_read_bytes,sysmem_write_bytes ./synthetic
nvprof --events shared_ld_transactions,shared_st_transactions ./synthetic 

# List of all events supported by Xavier 

'''bash
# nvprof --events all ./synthetic
Device "Xavier (0)"
    Kernel: __omp_offloading_b301_164311__Z15simple_runnablev_l47
         20              l2_subp0_write_sector_misses           0           0           0           0
         20              l2_subp1_write_sector_misses           0           0           0           0
         20               l2_subp0_read_sector_misses           0           0           0           0
         20               l2_subp1_read_sector_misses           0           0           0           0
         20          l2_subp0_read_tex_sector_queries        1248        1253        1250       25003
         20          l2_subp1_read_tex_sector_queries        1248        1253        1250       25006
         20         l2_subp0_write_tex_sector_queries        5020        5044        5032      100655
         20         l2_subp1_write_tex_sector_queries        5024        5048        5035      100705
         20             l2_subp0_read_tex_hit_sectors           0           2           0          10
         20             l2_subp1_read_tex_hit_sectors           0           1           0           2
         20            l2_subp0_write_tex_hit_sectors        5000        5026        5013      100268
         20            l2_subp1_write_tex_hit_sectors        5004        5027        5015      100313
         20        l2_subp0_total_read_sector_queries        1262        1267        1264       25283
         20        l2_subp1_total_read_sector_queries        1258        1263        1260       25205
         20       l2_subp0_total_write_sector_queries        5034        5061        5048      100975
         20       l2_subp1_total_write_sector_queries        5028        5055        5040      100805
         20       l2_subp0_read_sysmem_sector_queries        1256        1336        1262       25245
         20       l2_subp1_read_sysmem_sector_queries        1256        1356        1263       25261
         20      l2_subp0_write_sysmem_sector_queries        5030        5055        5042      100847
         20      l2_subp1_write_sysmem_sector_queries        5026        5051        5038      100773
         20                         elapsed_cycles_sm     8589656    10007736     9791882   195837656
         20                           prof_trigger_00           0           0           0           0
         20                           prof_trigger_01           0           0           0           0
         20                           prof_trigger_02           0           0           0           0
         20                           prof_trigger_03           0           0           0           0
         20                           prof_trigger_04           0           0           0           0
         20                           prof_trigger_05           0           0           0           0
         20                           prof_trigger_06           0           0           0           0
         20                           prof_trigger_07           0           0           0           0
         20                            warps_launched           2           2           2          40
         20                              inst_issued0     2055322     2387013     2348702    46974043
         20                              inst_issued1       95916       95916       95916     1918320
         20                             inst_executed       95908       95908       95908     1918160
         20                      thread_inst_executed       96245       96245       96245     1924900
         20   not_predicated_off_thread_inst_executed       96203       96203       96203     1924060
         20                             active_cycles     1070760     1247873     1220937    24418753
         20                              active_warps     2141450     2495676     2441805    48836102
         20                               shared_atom           0           0           0           0
         20                           sm_cta_launched           1           1           1          20
         20                           shared_atom_cas           0           0           0           0
         20                               shared_load           4           4           4          80
         20                              shared_store         104         104         104        2080
         20                              generic_load           0           0           0           0
         20                             generic_store          69          69          69        1380
         20                               global_load       20000       20000       20000      400000
         20                              global_store       10000       10000       10000      200000
         20                                local_load           0           0           0           0
         20                               local_store           0           0           0           0
         20                                atom_count           8           8           8         160
         20                                gred_count           0           0           0           0
         20                    shared_ld_transactions           4           4           4          80
         20                    shared_st_transactions         104         104         104        2080
         20                   shared_ld_bank_conflict           0           0           0           0
         20                   shared_st_bank_conflict           0           0           0           0
         20                           global_atom_cas           0           0           0           0
         20                          active_cycles_pm     1087021     1236387     1221188    24423762
         20                           active_warps_pm     2173972     2472704     2442306    48846120
         20              tensor_pipe_active_cycles_s0           0           0           0           0
         20              tensor_pipe_active_cycles_s1           0           0           0           0
         20              tensor_pipe_active_cycles_s2           0           0           0           0
         20              tensor_pipe_active_cycles_s3           0           0           0           0
'''

# List of all metrics supported by Xavier

'''bash
# nvprof --metrics all ./synthetic

==31208== Metric result:
Invocations                               Metric Name                                     Metric Description         Min         Max         Avg
Device "Xavier (0)"
    Kernel: __omp_offloading_b301_164311__Z15simple_runnablev_l47
20                             inst_per_warp                                            Instructions per warp  1.7996e+05  1.7996e+05  1.7996e+05
20                         branch_efficiency                                                Branch Efficiency      99.98%      99.98%      99.98%
20                 warp_execution_efficiency                                        Warp Execution Efficiency       3.16%       3.16%       3.16%
20         warp_nonpred_execution_efficiency                         Warp Non-Predicated Execution Efficiency       3.15%       3.15%       3.15%
20                      inst_replay_overhead                                      Instruction Replay Overhead    0.000083    0.000083    0.000083
20      shared_load_transactions_per_request                      Shared Memory Load Transactions Per Request    1.000000    1.000000    1.000000
20     shared_store_transactions_per_request                     Shared Memory Store Transactions Per Request    1.000000    1.000000    1.000000
20       local_load_transactions_per_request                       Local Memory Load Transactions Per Request    0.000000    0.000000    0.000000
20      local_store_transactions_per_request                      Local Memory Store Transactions Per Request    0.000000    0.000000    0.000000
20              gld_transactions_per_request                             Global Load Transactions Per Request    1.000000    1.000000    1.000000
20              gst_transactions_per_request                            Global Store Transactions Per Request    1.000000    1.000000    1.000000
20                 shared_store_transactions                                        Shared Store Transactions         104         104         104
20                  shared_load_transactions                                         Shared Load Transactions           4           4           4
20                   local_load_transactions                                          Local Load Transactions           0           0           0
20                  local_store_transactions                                         Local Store Transactions           0           0           0
20                          gld_transactions                                         Global Load Transactions       20000       20000       20000
20                          gst_transactions                                        Global Store Transactions       10069       10069       10069
20                  sysmem_read_transactions                                  System Memory Read Transactions        2506        2682        2567
20                 sysmem_write_transactions                                 System Memory Write Transactions       10081       10081       10081
20                      l2_read_transactions                                             L2 Read Transactions       13126       13129       13126
20                     l2_write_transactions                                            L2 Write Transactions       10089       10089       10089
20                           global_hit_rate                                Global Hit Rate in unified l1/tex      91.54%      91.57%      91.55%
20                            local_hit_rate                                                   Local Hit Rate       0.00%       0.00%       0.00%
20                  gld_requested_throughput                                 Requested Global Load Throughput  86.170MB/s  86.804MB/s  86.586MB/s
20                  gst_requested_throughput                                Requested Global Store Throughput  43.652MB/s  43.973MB/s  43.863MB/s
20                            gld_throughput                                           Global Load Throughput  680.65MB/s  685.66MB/s  683.93MB/s
20                            gst_throughput                                          Global Store Throughput  342.67MB/s  345.19MB/s  344.33MB/s
20                     local_memory_overhead                                            Local Memory Overhead      79.75%      79.84%      79.80%
20                        tex_cache_hit_rate                                           Unified Cache Hit Rate      58.18%      58.19%      58.19%
20                      l2_tex_read_hit_rate                                      L2 Hit Rate (Texture Reads)       0.00%       0.08%       0.02%
20                     l2_tex_write_hit_rate                                     L2 Hit Rate (Texture Writes)      99.57%      99.66%      99.62%
20                      tex_cache_throughput                       Unified cache to Multiprocessor throughput  2.6612GB/s  2.6808GB/s  2.6740GB/s
20                    l2_tex_read_throughput                                    L2 Throughput (Texture Reads)  85.149MB/s  85.736MB/s  85.511MB/s
20                   l2_tex_write_throughput                                   L2 Throughput (Texture Writes)  342.64MB/s  345.16MB/s  344.29MB/s
20                        l2_read_throughput                                            L2 Throughput (Reads)  446.74MB/s  450.00MB/s  448.88MB/s
20                       l2_write_throughput                                           L2 Throughput (Writes)  343.35MB/s  345.88MB/s  345.01MB/s
20                    sysmem_read_throughput                                    System Memory Read Throughput  85.285MB/s  91.947MB/s  87.804MB/s
20                   sysmem_write_throughput                                   System Memory Write Throughput  343.08MB/s  345.61MB/s  344.74MB/s
20                     local_load_throughput                                     Local Memory Load Throughput  0.00000B/s  0.00000B/s  0.00000B/s
20                    local_store_throughput                                    Local Memory Store Throughput  0.00000B/s  0.00000B/s  0.00000B/s
20                    shared_load_throughput                                    Shared Memory Load Throughput  557.59KB/s  561.69KB/s  560.28KB/s
20                   shared_store_throughput                                   Shared Memory Store Throughput  14.158MB/s  14.262MB/s  14.226MB/s
20                            gld_efficiency                                    Global Memory Load Efficiency      12.66%      12.66%      12.66%
20                            gst_efficiency                                   Global Memory Store Efficiency      12.74%      12.74%      12.74%
20                    tex_cache_transactions                     Unified cache to Multiprocessor transactions       20018       20018       20018
20                             flop_count_dp                      Floating Point Operations(Double Precision)           0           0           0
20                         flop_count_dp_add                  Floating Point Operations(Double Precision Add)           0           0           0
20                         flop_count_dp_fma                  Floating Point Operations(Double Precision FMA)           0           0           0
20                         flop_count_dp_mul                  Floating Point Operations(Double Precision Mul)           0           0           0
20                             flop_count_sp                      Floating Point Operations(Single Precision)           0           0           0
20                         flop_count_sp_add                  Floating Point Operations(Single Precision Add)           0           0           0
20                         flop_count_sp_fma                  Floating Point Operations(Single Precision FMA)           0           0           0
20                         flop_count_sp_mul                   Floating Point Operation(Single Precision Mul)           0           0           0
20                     flop_count_sp_special              Floating Point Operations(Single Precision Special)           0           0           0
20                             inst_executed                                            Instructions Executed       95908      359923      241116
20                               inst_issued                                              Instructions Issued       95916       95916       95916
20                          stall_inst_fetch                         Issue Stall Reasons (Instructions Fetch)       1.07%       1.32%       1.13%
20                     stall_exec_dependency                       Issue Stall Reasons (Execution Dependency)       9.10%       9.28%       9.23%
20                   stall_memory_dependency                               Issue Stall Reasons (Data Request)      36.99%      38.00%      37.59%
20                             stall_texture                                    Issue Stall Reasons (Texture)       0.00%       0.00%       0.00%
20                                stall_sync                            Issue Stall Reasons (Synchronization)      51.57%      52.40%      51.98%
20                               stall_other                                      Issue Stall Reasons (Other)       0.00%       0.00%       0.00%
20          stall_constant_memory_dependency                         Issue Stall Reasons (Immediate constant)       0.04%       0.13%       0.07%
20                           stall_pipe_busy                                  Issue Stall Reasons (Pipe Busy)       0.00%       0.00%       0.00%
20                         shared_efficiency                                         Shared Memory Efficiency       4.43%       4.43%       4.43%
20                                inst_fp_32                                          FP Instructions(Single)           0           0           0
20                                inst_fp_64                                          FP Instructions(Double)           0           0           0
20                              inst_integer                                             Integer Instructions       60788       60788       60788
20                          inst_bit_convert                                         Bit-Convert Instructions           1           1           1
20                              inst_control                                        Control-Flow Instructions        5135        5135        5135
20                        inst_compute_ld_st                                          Load/Store Instructions       30186       30186       30186
20                                 inst_misc                                                Misc Instructions          88          88          88
20           inst_inter_thread_communication                                        Inter-Thread Instructions           4           4           4
20                               issue_slots                                                      Issue Slots       95916       95916       95916
20                                 cf_issued                                 Issued Control-Flow Instructions        5088        5088        5088
20                               cf_executed                               Executed Control-Flow Instructions        5088        5088        5088
20                               ldst_issued                                   Issued Load/Store Instructions       30195       30195       30195
20                             ldst_executed                                 Executed Load/Store Instructions       30195       30195       30195
20                       atomic_transactions                                              Atomic Transactions           8           8           8
20           atomic_transactions_per_request                                  Atomic Transactions Per Request    1.000000    1.000000    1.000000
20                      l2_atomic_throughput                                  L2 Throughput (Atomic requests)  278.79KB/s  280.84KB/s  280.14KB/s
20                    l2_atomic_transactions                                L2 Transactions (Atomic requests)          16          16          16
20                  l2_tex_read_transactions                                  L2 Transactions (Texture Reads)        2500        2503        2500
20                     stall_memory_throttle                            Issue Stall Reasons (Memory Throttle)       0.01%       0.01%       0.01%
20                        stall_not_selected                               Issue Stall Reasons (Not Selected)       0.00%       0.00%       0.00%
20                 l2_tex_write_transactions                                 L2 Transactions (Texture Writes)       10068       10068       10068
20                             flop_count_hp                        Floating Point Operations(Half Precision)           0           0           0
20                         flop_count_hp_add                    Floating Point Operations(Half Precision Add)           0           0           0
20                         flop_count_hp_mul                     Floating Point Operation(Half Precision Mul)           0           0           0
20                         flop_count_hp_fma                    Floating Point Operations(Half Precision FMA)           0           0           0
20                                inst_fp_16                                            HP Instructions(Half)           0           0           0
20                                       ipc                                                     Executed IPC    0.077860    0.099159    0.087558
20                                issued_ipc                                                       Issued IPC    0.077650    0.078816    0.078216
20                    issue_slot_utilization                                           Issue Slot Utilization       1.94%       1.97%       1.96%
20                             sm_efficiency                                          Multiprocessor Activity      12.47%      12.49%      12.47%
20                        achieved_occupancy                                               Achieved Occupancy    0.031250    0.031250    0.031250
20                  eligible_warps_per_cycle                                  Eligible Warps Per Active Cycle    0.077949    0.078702    0.078254
20                        shared_utilization                                        Shared Memory Utilization     Low (1)     Low (1)     Low (1)
20                           tex_utilization                                        Unified Cache Utilization     Low (1)     Low (1)     Low (1)
20                       ldst_fu_utilization                             Load/Store Function Unit Utilization     Low (1)     Low (1)     Low (1)
20                         cf_fu_utilization                           Control-Flow Function Unit Utilization     Low (1)     Low (1)     Low (1)
20                        tex_fu_utilization                                Texture Function Unit Utilization    Idle (0)    Idle (0)    Idle (0)
20                    special_fu_utilization                                Special Function Unit Utilization     Low (1)     Low (1)     Low (1)
20             half_precision_fu_utilization                         Half-Precision Function Unit Utilization    Idle (0)    Idle (0)    Idle (0)
20           single_precision_fu_utilization                       Single-Precision Function Unit Utilization     Low (1)     Low (1)     Low (1)
20                        flop_hp_efficiency                                       FLOP Efficiency(Peak Half)       0.00%       0.00%       0.00%
20                        flop_sp_efficiency                                     FLOP Efficiency(Peak Single)       0.00%       0.00%       0.00%
20                        flop_dp_efficiency                                     FLOP Efficiency(Peak Double)       0.00%       0.00%       0.00%
20                            stall_sleeping                                   Issue Stall Reasons (Sleeping)       0.00%       0.00%       0.00%
20                inst_executed_global_loads                         Warp level instructions for global loads       20000       20000       20000
20                 inst_executed_local_loads                          Warp level instructions for local loads           0           0           0
20                inst_executed_shared_loads                         Warp level instructions for shared loads           4           4           4
20               inst_executed_surface_loads                        Warp level instructions for surface loads           0           0           0
20               inst_executed_global_stores                        Warp level instructions for global stores       10069       10069       10069
20                inst_executed_local_stores                         Warp level instructions for local stores           0           0           0
20               inst_executed_shared_stores                        Warp level instructions for shared stores         104         104         104
20              inst_executed_surface_stores                       Warp level instructions for surface stores           0           0           0
20              inst_executed_global_atomics             Warp level instructions for global atom and atom cas           8           8           8
20           inst_executed_global_reductions                    Warp level instructions for global reductions           0           0           0
20             inst_executed_surface_atomics            Warp level instructions for surface atom and atom cas           0           0           0
20          inst_executed_surface_reductions                   Warp level instructions for surface reductions           0           0           0
20              inst_executed_shared_atomics             Warp level shared instructions for atom and atom CAS           0           0           0
20                     inst_executed_tex_ops                              Warp level instructions for texture           0           0           0
20                      global_load_requests         Total number of global load requests from Multiprocessor       20000       20000       20000
20                       local_load_requests          Total number of local load requests from Multiprocessor           0           0           0
20                     surface_load_requests        Total number of surface load requests from Multiprocessor           0           0           0
20                     global_store_requests        Total number of global store requests from Multiprocessor       10069       10069       10069
20                      local_store_requests         Total number of local store requests from Multiprocessor           0           0           0
20                    surface_store_requests       Total number of surface store requests from Multiprocessor           0           0           0
20                    global_atomic_requests       Total number of global atomic requests from Multiprocessor           8           8           8
20                 global_reduction_requests    Total number of global reduction requests from Multiprocessor           0           0           0
20                   surface_atomic_requests      Total number of surface atomic requests from Multiprocessor           0           0           0
20                surface_reduction_requests   Total number of surface reduction requests from Multiprocessor           0           0           0
20                      l2_global_load_bytes             Bytes read from L2 for misses in L1 for global loads       80000       80064       80017
20                       l2_local_load_bytes              Bytes read from L2 for misses in L1 for local loads           0           0           0
20                     l2_surface_load_bytes            Bytes read from L2 for misses in L1 for surface loads           0           0           0
20              l2_global_atomic_store_bytes                   Bytes written to L2 from L1 for global atomics         256         256         256
20               l2_local_global_store_bytes         Bytes written to L2 from L1 for local and global stores.      322176      322176      322176
20                    l2_surface_store_bytes           Bytes read from L2 for misses in L1 for surface stores           0           0           0
20                         sysmem_read_bytes                                         System Memory Read Bytes       80192       85824       82163
20                        sysmem_write_bytes                                        System Memory Write Bytes      322592      322592      322592
20                           l2_tex_hit_rate                                                L2 Cache Hit Rate      79.75%      79.85%      79.80%
20                     texture_load_requests        Total number of texture Load requests from Multiprocessor           0           0           0
20           tensor_precision_fu_utilization                       Tensor-Precision Function Unit Utilization    Idle (0)    Idle (0)    Idle (0)
20                            l2_utilization                                             L2 Cache Utilization     Low (1)     Low (1)     Low (1)
20                 tensor_int_fu_utilization                             Tensor-Int Function Unit Utilization    Idle (0)    Idle (0)    Idle (0)
20           double_precision_fu_utilization                       Double-Precision Function Unit Utilization    Idle (0)    Idle (0)    Idle (0)
'''