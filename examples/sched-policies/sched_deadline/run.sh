#!/bin/bash
# based on https://github.com/evidence/test-sched-dl/blob/master/T0001-simple-run/run.sh
TRACECMD=trace-cmd
make clean_data
DIR=sched_dead
if [ ! -e $DIR ]; then
	echo "ERROR: File not compiled! Type make"
	exit
fi
$TRACECMD reset
rm -f dmesg.txt
echo "Running test $DIR..."
dmesg -c > /dev/null
./$DIR &
APP_PID=$!
# run system wide tracing
#$TRACECMD start -a -r 90 -b 100000 -e sched -e power
# or only the app
$TRACECMD start -a -r 90 -b 100000 -e sched -e power -P $APP_PID
sleep 10
echo "Killing test $DIR..."
#killall -s SIGKILL $DIR > /dev/null
kill -9 $APP_PID
sleep 3
dmesg -c >> ./dmesg.txt
chmod 777 dmesg.txt
$TRACECMD extract -o trace.dat
$TRACECMD stop
$TRACECMD report 1> trace.txt 2> /dev/null

