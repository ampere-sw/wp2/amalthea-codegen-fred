//source: https://stackoverflow.com/questions/50082317/is-sched-deadline-officially-supported-in-ubuntu-16-04#50085457
#include <stdio.h>
#include <stdint.h>
#include <unistd.h> // close, open, write, syscall
#include <sys/syscall.h> //SYS_gettid
#include <sched.h>
#include <linux/sched.h> // SCHED_DEADLINE,

struct sched_attr {
    uint32_t size;
    uint32_t sched_policy;
    uint64_t sched_flags;
    int32_t sched_nice;
    uint32_t sched_priority;
    uint64_t sched_runtime;
    uint64_t sched_deadline;
    uint64_t sched_period;
};

int sched_setattr(pid_t pid, const struct sched_attr *attr, unsigned int flags)
{
   return syscall(__NR_sched_setattr, pid, attr, flags);
}

int main(int argc, char* argv[]) {

    struct sched_attr attr = {
        .size = sizeof(attr),
        .sched_policy   = SCHED_DEADLINE,
        // it will cause 33% of CPU load. CPU running in 10 ms out of 30 ms
        .sched_runtime  = 10 * 1000 * 1000,
        .sched_deadline = 30 * 1000 * 1000,
        .sched_period   = 30 * 1000 * 1000
    };

    pid_t tid = syscall(SYS_gettid);
    printf ("PID: %d\n", tid);

    if (sched_setattr(0, &attr, 0))
        perror("sched_setattr()");

    for (;;);
    return 0;
}
