// source: https://gist.github.com/sjames/08043e76128c22272044
#include <sched.h>
#include <stdio.h>
#include <stdlib.h>     /* atoi */
#include <sys/syscall.h> //SYS_gettid
#include <unistd.h> // syscall

int main(int argc, char **argv)
{
  if (argc != 2){
    printf("Usage: sched_fifo <priority>\n");
    return 0;
  }
  printf("Setting SCHED_FIFO and priority to %d\n",atoi(argv[1]));
  struct sched_param param;
  param.sched_priority = atoi(argv[1]);
  sched_setscheduler(0, SCHED_FIFO, &param);
  int n = 0;

  pid_t tid = syscall(SYS_gettid);
  printf ("PID: %d\n", tid);

  while(1) {
    n++;
    if (!(n % 10000000)) {
      printf("%s FIFO Prio %d running (n=%d)\n",argv[0], atoi(argv[1]),n);
    }
  }
}
