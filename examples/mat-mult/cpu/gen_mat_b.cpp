//#include <chrono>
//#include <atomic>
#include <cstdint>
//#include "label_utils.h"
//#include "tick.h"
#include <stdio.h>
#include <string.h>

#ifndef MAT_SIZE
#define MAT_SIZE 20
#endif
#define MAT_SIZE_BYTES MAT_SIZE*MAT_SIZE*sizeof(uint32_t)

/* 
  ###############################
  RUNNABLE DESCRIPTION
  Calling task: main_task
  Mapped PU: core1
  PU definition: core
  ###############################
 */
void gen_mat_b()
{
  // shared label declarations
  extern uint8_t mat_b [MAT_SIZE_BYTES];
  extern uint8_t mat_b_idx;
  // common declarations
  //uint64_t val=0;
  uint32_t i,j;
  const uint32_t n=MAT_SIZE;  
  uint32_t b[MAT_SIZE][MAT_SIZE];

  // code initialization

  // run ...
  //write_long_array(mat_b,1200);
  for ( i = 0; i < n; i++ )
  {
    for ( j = 0; j < n; j++ )
    {
        if (i==j)
            b[i][j] = mat_b_idx;
        else
            b[i][j] = 0;
    }
  }
  mat_b_idx += 1;
  memcpy(mat_b, b,MAT_SIZE_BYTES);    
}