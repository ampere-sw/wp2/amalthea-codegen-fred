//#include <chrono>
//#include <atomic>
#include <cstdint>
//#include "label_utils.h"
//#include "tick.h"
#include <stdio.h>
#include <string.h>

#ifndef MAT_SIZE
#define MAT_SIZE 20
#endif
#define MAT_SIZE_BYTES MAT_SIZE*MAT_SIZE*sizeof(uint32_t)

/* 
  ###############################
  RUNNABLE DESCRIPTION
  Calling task: main_task
  Mapped PU: core1
  PU definition: core
  ###############################
 */
void mat_mult()
{
  // shared label declarations
  extern uint8_t mat_a [MAT_SIZE_BYTES];
  extern uint8_t mat_b [MAT_SIZE_BYTES];
  extern uint8_t mat_c [MAT_SIZE_BYTES];
  // local vars
  uint32_t i,j,k;
  // local copy of the matrix with the same sizeof
  uint32_t a[MAT_SIZE][MAT_SIZE];
  uint32_t b[MAT_SIZE][MAT_SIZE];
  uint32_t c[MAT_SIZE][MAT_SIZE];
  memcpy(a,mat_a,MAT_SIZE_BYTES);
  memcpy(b,mat_b,MAT_SIZE_BYTES);

#ifdef CPU
  for ( i = 0; i < MAT_SIZE; i++ )
  {
    for ( j = 0; j < MAT_SIZE; j++ )
    {
      c[i][j] = 0.0;
      for ( k = 0; k < MAT_SIZE; k++ )
      {
        c[i][j] += + a[i][k] * b[k][j];
      }
    }
  }
#elif GPU
	double st=omp_get_wtime();
  #pragma omp target data map(from: a[MAT_SIZE][MAT_SIZE],b[MAT_SIZE][MAT_SIZE]) map (to: c[MAT_SIZE][MAT_SIZE])
  {
    for ( i = 0; i < MAT_SIZE; i++ ){
      for ( j = 0; j < MAT_SIZE; j++ ){
        uint32_t dot = 0;
        for ( k = 0; k < MAT_SIZE; k++ ){
          dot += a[i][k] * b[k][j];
        }
        c[i][j] = dot;
      }
    }
  }
	double en=omp_get_wtime();
#ifdef DEBUG  
	printf("Parallel time: %lf\n",en-st);
#endif
#else
  // FRED code 
#endif

  memcpy(mat_c, c,MAT_SIZE_BYTES);
}