#include <chrono>
#include <atomic>
#include <cstdint>
#include "label_utils.h"
#include "tick.h"
#include <omp.h> // user only for omp_get_wtime

#ifndef MAT_SIZE
#define MAT_SIZE 20
#endif
#define MAT_SIZE_BYTES MAT_SIZE*MAT_SIZE*sizeof(uint32_t)


// function to multiply two matrices
void multiplyMatrices(uint32_t first[][MAT_SIZE],
                      uint32_t second[][MAT_SIZE],
                      uint32_t result[][MAT_SIZE]) {


   // Multiplying first and second matrices and storing it in result
   for (int i = 0; i < MAT_SIZE; ++i) {
      for (int j = 0; j < MAT_SIZE; ++j) {
        result[i][j] = 0;
        for (int k = 0; k < MAT_SIZE; ++k) {
          result[i][j] += first[i][k] * second[k][j];
        }
      }
   }
}

/* 
    ###############################
    TASK DESCRIPTION
    Stimuli type: PeriodicStimuli
    Mapped PU: core1
    PU type: CPU
    PU definition: core
    ###############################
     */
void print_mat()
{
  // shared label declarations
  extern uint8_t mat_c [1600];
  extern uint32_t mat_b_idx;
  // common declarations
  uint32_t i,j;

  // local copy of the matrix with the same sizeof
  uint32_t c[MAT_SIZE][MAT_SIZE];
  memcpy(c,mat_c,MAT_SIZE_BYTES);

  // generating the expected values
  uint32_t exp_a[MAT_SIZE][MAT_SIZE];
  uint32_t exp_b[MAT_SIZE][MAT_SIZE];
  uint32_t exp_c[MAT_SIZE][MAT_SIZE];

  for ( i = 0; i < MAT_SIZE; i++ )
  {
    for ( j = 0; j < MAT_SIZE; j++ )
    {
      exp_a[i][j] = j+i*MAT_SIZE;
      if (i==j)
        exp_b[i][j] = mat_b_idx-1;
      else
        exp_b[i][j] = 0;      
    }
  } 
  double st=omp_get_wtime();
  multiplyMatrices(exp_a,exp_b,exp_c);
	double en=omp_get_wtime();
#ifdef DEBUG  
	printf("Sequential time: %lf\n",en-st);

  int mismatch=0;

  if (memcmp(c,exp_c,sizeof(exp_c))!=0){

    for ( i = 0; i < MAT_SIZE; i++ )
    {
        for ( j = 0; j < MAT_SIZE; j++ ){
          if (c[i][j] != exp_c[i][j]){
            printf ("Mismatch at pos %d %d - C: %d, exp C: %d\n", i, j, c[i][j], exp_c[i][j]);
            mismatch = 1;
            break;
          }
        }
        if (mismatch)
          break;
    }    
    printf("C:\n");
    for ( i = 0; i < MAT_SIZE; i++ ){
        for ( j = 0; j < MAT_SIZE; j++ )
            printf("%d, ",c[i][j]);
        printf("\n");
    }  
    printf("Expected C:\n");
    for ( i = 0; i < MAT_SIZE; i++ ){
        for ( j = 0; j < MAT_SIZE; j++ )
            printf("%d, ",exp_c[i][j]);
        printf("\n");
    }
    /*
    printf("DUMP C:\n");
    Dump(c,100);
    printf("DUMP Expected C:\n");
    Dump(exp_c,100);
    */
  }

#endif
}