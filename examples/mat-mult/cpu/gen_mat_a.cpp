//#include <chrono>
//#include <atomic>
#include <cstdint>
//#include "label_utils.h"
//#include "tick.h"
#include <stdio.h>
#include <string.h>

#ifndef MAT_SIZE
#define MAT_SIZE 20
#endif
#define MAT_SIZE_BYTES MAT_SIZE*MAT_SIZE*sizeof(uint32_t)

/* 
  ###############################
  RUNNABLE DESCRIPTION
  Calling task: main_task
  Mapped PU: core1
  PU definition: core
  ###############################
 */
void gen_mat_a()
{
  // shared label declarations
  extern uint8_t mat_a [MAT_SIZE_BYTES];
  // common declarations
  //uint64_t val=0;
  uint32_t i,j;
  const uint32_t n=MAT_SIZE;  
  uint32_t a[MAT_SIZE][MAT_SIZE];

  // code initialization

  // run ...
  //write_long_array(mat_a,1200);
  for ( i = 0; i < n; i++ )
  {
    for ( j = 0; j < n; j++ )
    {
      a[i][j] = j+i*n;
    }
  }
  memcpy(mat_a, a,MAT_SIZE_BYTES);  
}