#include <cstdint>
#include <iostream>
#include <stdio.h>
#include <string.h>
#include <omp.h>
#include "label_utils.h"


#ifndef MAT_SIZE
#define MAT_SIZE 20
#endif
#define MAT_SIZE_BYTES MAT_SIZE*MAT_SIZE*sizeof(int)

/* 
  ###############################
  RUNNABLE DESCRIPTION
  Calling task: main_task
  Mapped PU: core1
  PU definition: core
  ###############################
 */
void mat_mult()
{
  // shared label declarations
  extern uint8_t mat_a [MAT_SIZE_BYTES];
  extern uint8_t mat_b [MAT_SIZE_BYTES];
  extern uint8_t mat_c [MAT_SIZE_BYTES];
  // common declarations
  int i,j,k;
  const unsigned n=MAT_SIZE;

  // local copy of the matrix with the same sizeof
  int a[MAT_SIZE][MAT_SIZE];
  int b[MAT_SIZE][MAT_SIZE];
  int c[MAT_SIZE][MAT_SIZE];
  memcpy(a,mat_a,MAT_SIZE_BYTES);
  memcpy(b,mat_b,MAT_SIZE_BYTES);

#ifdef DEBUG_EXTRA
  printf("A:\n");
  Dump( a, MAT_SIZE_BYTES );
  printf("B:\n");
  Dump( b, MAT_SIZE_BYTES );
#endif

//#pragma omp target teams distribute parallel for map(to: a, b) map(tofrom: c) thread_limit(n)
#pragma omp target map(tofrom: c) map(to: a, b, n) 
{
  for ( i = 0; i < n; i++ )
  {
    for ( j = 0; j < n; j++ )
    {
      c[i][j] = 0;
      for ( k = 0; k < n; k++ )
      {
        c[i][j] += + a[i][k] * b[k][j];
      }
    }
 
  }
}

  memcpy(mat_c, c,MAT_SIZE_BYTES);

#ifdef DEBUG_EXTRA
  printf("C in mult:\n");
  Dump( c, MAT_SIZE_BYTES );  
#endif
}