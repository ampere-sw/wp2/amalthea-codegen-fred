//#include <chrono>
//#include <atomic>
#include <cstdint>
//#include "label_utils.h"
//#include "tick.h"
#include <stdio.h>
#include <string.h>
#include "fred_lib.h"

/* tick exec in microseconds */
//#define TICK_EXECUTION_TIME 0

#ifndef MAT_SIZE
#define MAT_SIZE 20
#endif
#define MAT_SIZE_BYTES MAT_SIZE*MAT_SIZE*sizeof(int)

#define HW_TASK_ID 0

//--------------------------------------------------------------------------
// FRED prototypes
struct fred_data;
struct fred_hw_task;
int fred_init(struct fred_data **self);
int fred_bind(struct fred_data *self, struct fred_hw_task **hw_task, uint32_t hw_task_id);
int fred_accel(struct fred_data *self, const struct fred_hw_task *hw_task);
void fred_free(struct fred_data *self);
void *fred_map_buff(const struct fred_data *self, struct fred_hw_task * hw_task, int buff_idx);

/* 
  ###############################
  RUNNABLE DESCRIPTION
  Calling task: main_task
  Mapped PU: core1
  PU definition: core
  ###############################
 */
void mat_mult()
{
  // shared label declarations
  extern uint8_t mat_a [MAT_SIZE_BYTES];
  extern uint8_t mat_b [MAT_SIZE_BYTES];
  extern uint8_t mat_c [MAT_SIZE_BYTES];

  // FRED declarations
  struct fred_data *fred;
  struct fred_hw_task *hw_task;
  uint32_t hw_task_id = HW_TASK_ID;

  // mapping the FRED buffers to the global vars
  void *a = (void *)&mat_a;
  void *b = (void *)&mat_b;
  void *c = (void *)&mat_c;

  /* Initialize communication and bind a HW-task */
  fred_init(&fred);
  fred_bind(fred, &hw_task, hw_task_id);

  /* Map the buffers */
  a = fred_map_buff(fred, hw_task, 0);
  b = fred_map_buff(fred, hw_task, 1);
  c = fred_map_buff(fred, hw_task, 2);

  /* Call the HW-task */
  fred_accel(fred, hw_task);  

}