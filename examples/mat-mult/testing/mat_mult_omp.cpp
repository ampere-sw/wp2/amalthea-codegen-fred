/*
 adapted from: https://people.sc.fsu.edu/~jburkardt/c_src/mxm_openmp/mxm_openmp.c
 Alexandre Amory

 GPU Offloading example tested in the Xavier board

 compilation:
 $ clang++ -O2 mat_mult_omp.cpp -o mat -fopenmp=libomp -fopenmp-targets=nvptx64-nvidia-cuda
*/
# include <stdlib.h>
# include <stdio.h>
# include <math.h>
# include <time.h>
# include <omp.h>

# define MAT_SIZE 10

int main ( void );
void timestamp ( void );

/******************************************************************************/

int main ( void )

/******************************************************************************/
/*
  Purpose:

    MAIN is the main program for MXM_OPENMP.

  Licensing:

    This code is distributed under the GNU LGPL license. 

  Modified:

    13 October 2011

  Author:

    John Burkardt
*/
{
  int a[MAT_SIZE][MAT_SIZE];
  double angle;
  int b[MAT_SIZE][MAT_SIZE];
  int c[MAT_SIZE][MAT_SIZE];
  int i;
  int j;
  int k;
  int n = MAT_SIZE;
  int thread_num;
  double wtime;

  timestamp ( );

  printf ( "\n" );
  printf ( "MXM_OPENMP:\n" );
  printf ( "  C/OpenMP version\n" );
  printf ( "  Compute matrix product C = A * B.\n" );

  thread_num = omp_get_max_threads ( );

  printf ( "\n" );
  printf ( "  The number of processors available = %d\n", omp_get_num_procs ( ) );
  printf ( "  The number of threads available    = %d\n", thread_num );

  printf ( "  The matrix order N                 = %d\n", n );
/*
  Loop 1: Evaluate A.
*/

  wtime = omp_get_wtime ( );


  //# pragma omp for
  for ( i = 0; i < n; i++ )
  {
    for ( j = 0; j < n; j++ )
    {
      a[i][j] = j+i*n;
    }
  }
/*
  Loop 2: fill B with the diagonal == 2.
  an expensive mat * scalar
*/
  //# pragma omp for
  for ( i = 0; i < n; i++ )
  {
    for ( j = 0; j < n; j++ )
    {
        if (i==j)
            b[i][j] = 2;
        else
            b[i][j] = 0;
    }
  }
/*
  Loop 3: Compute C = A * B.
*/
#pragma omp target teams distribute parallel for map(to: a, b) map(tofrom: c) thread_limit(128)
//#pragma omp target data map(tofrom: c[0:MAT_SIZE]) map(to: a[0:MAT_SIZE], b[0:MAT_SIZE])
//# pragma omp parallel shared ( a, b, c, n ) private ( i, j, k)
{
  //# pragma omp for
  for ( i = 0; i < n; i++ )
  {
    for ( j = 0; j < n; j++ )
    {
      c[i][j] = 0.0;
      for ( k = 0; k < n; k++ )
      {
        c[i][j] += + a[i][k] * b[k][j];
      }
    }
  }

}

  // compare with the expected result
  int wrong = 0;
  for ( i = 0; i < n; i++ )
  {
    for ( j = 0; j < n; j++ )
    {
        if (c[i][j] != 2*(j+i*n)){
            wrong = 1;
            printf("\n\nERROR: b[%d][%d] == %d, but expected is %d\n",i,j,c[i][j],2*(j+i*n));
            break;
        }

    }
    if (wrong == 1){
        break;
    }
  }

  // debug
  if (wrong == 1){
    printf ("MISMATCH!\n\n");
    printf("A:\n");
    for ( i = 0; i < n; i++ )
    {
        for ( j = 0; j < n; j++ )
            printf("%d, ",a[i][j]);
        printf("\n");
    }    
    printf("B:\n");
    for ( i = 0; i < n; i++ )
    {
        for ( j = 0; j < n; j++ )
            printf("%d, ",b[i][j]);
        printf("\n");
    }    
    printf("C:\n");
    for ( i = 0; i < n; i++ )
    {
        for ( j = 0; j < n; j++ )
            printf("%d, ",c[i][j]);
        printf("\n");
    }    
  }


  wtime = omp_get_wtime ( ) - wtime;
  printf ( "  Elapsed seconds = %g\n", wtime );
  printf ( "  C(100,100)  = %d\n", c[99][99] );
/*
  Terminate.
*/
  printf ( "\n" );
  printf ( "MXM_OPENMP:\n" );
  printf ( "  Normal end of execution.\n" );

  printf ( "\n" );
  timestamp ( );

  return 0;
}
/******************************************************************************/

void timestamp ( void )

/******************************************************************************/
/*
  Purpose:

    TIMESTAMP prints the current YMDHMS date as a time stamp.

  Example:

    31 May 2001 09:45:54 AM

  Licensing:

    This code is distributed under the GNU LGPL license. 

  Modified:

    24 September 2003

  Author:

    John Burkardt

  Parameters:

    None
*/
{
# define TIME_SIZE 40

  static char time_buffer[TIME_SIZE];
  const struct tm *tm;
  time_t now;

  now = time ( NULL );
  tm = localtime ( &now );

  strftime ( time_buffer, TIME_SIZE, "%d %B %Y %I:%M:%S %p", tm );

  printf ( "%s\n", time_buffer );

  return;
# undef TIME_SIZE
}
