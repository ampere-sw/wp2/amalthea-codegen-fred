This directory has the actual runnable code of the mat-mult model.
Select the versions of mat-mult (cpu, gpu, fred) and copy its files to the generated code directory.

Current status:

 - cpu: tested in the host and in the Xavier board. Not tested in the Xilinx UltraScale+ ZCU102 board;
 - gpu: tested with offloading in the Xavier board only;
 - fred: not tested.
 