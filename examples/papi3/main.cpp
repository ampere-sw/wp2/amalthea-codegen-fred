// https://github.com/insumity/slate/blob/master/src/start_counter_minimal_example.c
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
//#include <pthread.h>
//#include <sched.h>
#include <papi.h>
#include "papi_events.h"
#include "perf_cnt.h"


/*

// matrix size
#define N 30
#define M 30

int matmul( ) {
	int i, j, k;
	double *A, *B, *C;

	long long counters[3];
	int PAPI_events[] = {
		PAPI_TOT_CYC,
		PAPI_L2_DCM,
		PAPI_L2_DCA };

	PAPI_library_init(PAPI_VER_CURRENT);

	A = (double *)malloc(N*M*sizeof(double));
	B = (double *)malloc(N*M*sizeof(double));
	C = (double *)malloc(N*N*sizeof(double));

	if ( A == NULL || B == NULL || C == NULL ) {
		fprintf(stderr,"Error allocating memory!\n");
		exit(1);
	}

	// initialize A & B 
	for ( i = 0; i < N; i++ ) {
		for ( j = 0; j < M; j++ ) { 
			A[M*i+j] = 3.0;
			B[N*j+i] = 2.0;
		}
	}

	for ( i = 0; i < N*N; i++ ) {
		C[i] = 0.0;
	}

	i = PAPI_start_counters( PAPI_events, 3 );

	for ( i = 0; i < N; i++ ) {
		for ( j = 0; j < N; j++ ) {
			for ( k = 0; k < M; k++ ) {
				C[N*i+j] += A[M*i+k]*B[N*k+j];
			}
		}
	}

	PAPI_read_counters( counters, 3 );

	printf("%lld L2 cache misses (%.3lf%% misses) in %lld cycles\n", 
		counters[1],
		(double)counters[1] / (double)counters[2],
		counters[0] );

	free(A);
	free(B);
	free(C);
}
*/

// TODO: treat counter overflow
// TODO: add thread support https://bitbucket.org/icl/papi/wiki/PAPI-Parallel-Programs.md
// TODO: is software defiend events relevant ? https://bitbucket.org/icl/papi/wiki/Software_Defined_Events.md

int main(int argc, char* argv[]) {
 
  // const char *events [] = {
  //     "PAPI_TOT_CYC",// Total cycles
  //     "PAPI_TOT_INS",// Instructions completed
  //     "PAPI_STL_ICY",// Cycles with no instruction issue
  //     ""
  // };  
  extern const char *events[];

  int iEvents[] = {PAPI_TOT_CYC, PAPI_TOT_INS, PAPI_STL_ICY};
  Performance_Counters pc("papi2.csv",events);

  int iter = 5;
  while (iter > 0) {
    int one_ms_in_us = 1000;
    int wait_time_in_ms = 500 * one_ms_in_us;
    usleep(wait_time_in_ms);
    pc();
    iter --;
  }
  pc.close();

/*
  // THIS PART DOES NOT WORK. IT SEEMS THAT IT'S NOT POSSIBLE TO CHANGE THE EVENTS IN RUNTIME
  const char *events2 [] = {
      "PAPI_LD_INS",//  Load instructions
      "PAPI_SR_INS",//  Store instructions
      "PAPI_TOT_CYC",//  Level 1 cache misses
      ""
  };
  Performance_Counters pc2("papi3.csv",events2);
  for(int i=0;i<10000;i++);
  pc2();
  pc2.close();
*/
  return 0;
}

