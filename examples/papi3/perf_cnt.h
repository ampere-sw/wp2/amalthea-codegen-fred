#include <stdio.h>

/*

Example of usage:

  const char *events [] = {
      "PAPI_TOT_CYC",// Total cycles
      "PAPI_TOT_INS",// Instructions completed
      "PAPI_STL_ICY",// Cycles with no instruction issue
      ""
  };  
  Performance_Counters pc("papi.csv",events)

  // put some computation here

  // read the counters
  pc();

  // put some computation here

  // read the counters
  pc();

  // done capture data from the performance counters. The results are saved in a CSV file.
  pc.close();

*/

class Performance_Counters
{
public:
    int num_events;
    // is it required to save the name of the events ?!??! 
    //vector<string> events;
    // use filename=stdout to print in the terminal
    Performance_Counters(const char *filename, const char *events[]);
    //~Performance_Counters();
    // read the counters and save them into a file
    void operator()();
    // kill the PAPI env and close the output file
    void close();

private:
    int event_set;
    // pointer to the CSV file where the results are saved
    FILE *f;
};