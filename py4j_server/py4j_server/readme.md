You might need to edit build.xml to point to the correct path of the jar files.
It's recommended to place the jar files like this: 

```
    <pathelement location="../../Amalthea.jar"/>
    <pathelement location="../../junit-4.11.jar"/>
    <pathelement location="../../py4j-0.10.9.1.jar"/>
```
