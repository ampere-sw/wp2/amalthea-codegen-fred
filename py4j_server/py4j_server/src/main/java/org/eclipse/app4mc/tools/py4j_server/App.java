/**
 ********************************************************************************
 * Copyright (c) 2018 Robert Bosch GmbH.
 * 
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 * 
 * SPDX-License-Identifier: EPL-2.0
 * 
 * Contributors:
 *     Robert Bosch GmbH - initial API and implementation
 *     Alexandre Amory - 10/2020 - ReTiS Lab, Scuola Sant'Anna, Pisa, Italy
 ********************************************************************************
 */

package org.eclipse.app4mc.tools.py4j_server;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.app4mc.amalthea.model.Amalthea;
import org.eclipse.app4mc.amalthea.model.AmaltheaPackage;
import org.eclipse.app4mc.amalthea.model.impl.AmaltheaPackageImpl;
import org.eclipse.app4mc.amalthea.model.io.AmaltheaLoader;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClassifier;

import py4j.GatewayServer;
import py4j.Py4JNetworkException;

public class App 
{
    public static void main( String[] args )
    {
		
		if (args.length != 2){
			System.out.println("ERROR: missing arguments.\n Expecting <model file name> <py4j port number>.");
			return;
		}
		
		final File inputFile = new File(args[0]);

		int py4j_port = 25334;
		try {		
			py4j_port = Integer.parseInt(args[1]);
			if (py4j_port <= 1024){
				throw new NumberFormatException();
			}

		} catch (NumberFormatException nfe) {
        	// Command is illegal. Display error message.
        	System.out.println("ERROR: the second argument must be an Integer > 1024");
			System.exit(1);
    	} 

		// ***** Load *****
		Amalthea model = AmaltheaLoader.loadFromFile(inputFile);
		if (model == null) {
			System.out.println("ERROR: model " + args[0] + " was not loaded!");
			return;
		}

		System.out.println("Amalthea model is available. Model file loaded from location : "+model.eResource().getURI());
		Map<Object, Object> dataMap = new HashMap<Object, Object>();

		AmaltheaPackage amltPackage = AmaltheaPackageImpl.init();
		EList<EClassifier> eClassifiers = amltPackage.getEClassifiers();
		List<String> classNames = new ArrayList<>();

		for (EClassifier eClassifier : eClassifiers) {
			classNames.add(eClassifier.getName());
			Class<?> instanceClass = eClassifier.getInstanceClass();
			dataMap.put(eClassifier.getName()+".class", instanceClass);
		}
		
		dataMap.put("amaltheaModel", model);

		// sort the name of the classes to print it out		
		Collections.sort(classNames);
		// System.out.println("==============================");
		// for(String str: classNames) System.out.println(str);
		// System.out.println("==============================");


		GatewayServer gatewayServer = new GatewayServer(dataMap, py4j_port);
		try {
			gatewayServer.start();
		} catch (Py4JNetworkException e) {
			System.out.println("ERROR: could not start the server. Perhaps port " + py4j_port + " is being used");
			System.exit(1);
		}

		System.out.println("Gateway Amalthea Server Started");
	}

}
